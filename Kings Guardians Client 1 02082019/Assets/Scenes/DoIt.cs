﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoIt : MonoBehaviour {

    [SerializeField]
    bool execute = false;

    [SerializeField]
    List<Transform> units = new List<Transform>();
    [SerializeField]
    GameObject parent;

    private void OnDrawGizmos()
    {
        if(execute)
             task();
        
    }

    void task()
    {
        //Detach
        parent.transform.DetachChildren();

        //reverse List
        units.Reverse();
        int i = 1;
        foreach (Transform unit in units)
        {
            unit.gameObject.name = "Sphere (" + i + ")"; 
            //Make Child of parent
            unit.parent = parent.transform;
            i++;
        }
        
        //Add again in reverse order

        execute = false;
    }


    void detachChildren()
    {

    }

    void reverse()
    {


    }
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
