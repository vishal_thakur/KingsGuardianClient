﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelLoader : MonoBehaviour
{
    #region Singleton
    public static LevelLoader Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject prefab = Resources.Load<GameObject>("LevelLoader");
                GameObject go = (GameObject) MonoBehaviour.Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;

                _instance = go.GetComponent<LevelLoader>();
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }
    static LevelLoader _instance;
    #endregion

    public GameObject Window;
    public StatusBar ProgressBar;
    public Text Message;
    public Text ErrorLog;

    AsyncOperation _levelLoading = null;
    float _progress = 0f;
    bool _calculateProgress = false;
    float _progressPerSeconds = 0f;
    float _progressTime = 0f;

    int _nextSubscene = 0;

    void Awake()
    {
        ErrorLog.text = "";
        Window.SetActive(false);
    }

    public void LoadScene(string levelName)
    {       
        Show();
        StartCoroutine(LoadSceneAsync(levelName));
        _nextSubscene = 0;
        transform.SetAsLastSibling();
    }

    void LoadSubScene(string levelName)
    {
        StartCoroutine(LoadSceneAsync(levelName, true));
    }

    public void Show() { Window.SetActive(true); }
    public void Hide() { Window.SetActive(false); }



    IEnumerator LoadSceneAsync(string levelName, bool additive = false)
    {
        Message.text = "Loading "+((additive) ? "sub":"")+"level: " + levelName;

        var mode = (additive) ? UnityEngine.SceneManagement.LoadSceneMode.Additive : UnityEngine.SceneManagement.LoadSceneMode.Single;
       
        _levelLoading = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync (levelName, mode);
        yield return StartCoroutine(SceneLoadingProgress());
        _levelLoading = null;

        transform.SetAsLastSibling();

        // Load subscenes
		//try{
	        var script = GameObjectExtended.FindObjectOfInterface<ISceneScript>();
	        if (script.SubScenes.Length > 0 && _nextSubscene < script.SubScenes.Length)
	        {
	            LoadSubScene(script.SubScenes[_nextSubscene]);            
	            _nextSubscene++;
	        }
	        else
	        {
//				Debug.LogError();
	            Message.text = "Building up the level...";
				script.OnLoadingDone();
//				Debug.LogError("Fook!!");
//				Messenger.Broadcast(Events.OnScoutModeSceneLoaded);
				GameObject.FindObjectOfType<DummyScoutManager>().RefreshScene();
	        }
//		}
//		catch(System.Exception e){
////			Notification.Instance.Show ("Connection Lost!" , NotificationTypes.Error);
////			DestroyImmediate (gameObject);
//			ErrorLog.text = "";
//			Window.SetActive(false);
//            Debug.LogError("no scene script found!" + e.ToString());
//        }
    }


    IEnumerator SceneLoadingProgress ()
    {
        while (!_levelLoading.isDone)
            yield return _levelLoading;
    }

    void Update()
    {
        if (_levelLoading != null)
            _progress = _levelLoading.progress;

        if( _calculateProgress )
        {
            float deltaTime = Time.deltaTime;
            _progress += deltaTime * _progressPerSeconds;
            _progressTime -= deltaTime;

            if(_progressTime <= 0f)
                _calculateProgress = false;
        }

        if (_progress > 1f)
            _progress = 1f;

        ProgressBar.Value = Mathf.RoundToInt(_progress*100);
    }

    public void Log(string msg) { ErrorLog.text = msg; ErrorLog.color = Color.white; }
    public void LogWarning(string msg) { ErrorLog.text = "<b>WARNING:</b>"+ msg;  ErrorLog.color = Color.yellow; }
    public void LogError(string msg) { ErrorLog.text = "<b>ERROR:</b>"+ msg;  ErrorLog.color = Color.red; }

	/**string _message = "";
		

	


	#region Custom Loading with realtime update
	public void LoadCustom(string msg, float currentProgress)
	{
		if(LoadingScreen.activeSelf == false)
			LoadingScreen.SetActive(true);

		_message = msg;
		_progress = currentProgress;
	}

	public void LoadCustom(string msg, float targetProgress, float seconds)
	{
		if(LoadingScreen.activeSelf == false)
			LoadingScreen.SetActive(true);

		_message = msg;
		_progressPerSeconds = targetProgress/seconds;
		_progressTime = seconds;
		_calculateProgress = true;

	}
	#endregion

	#region Scene Loading Async
	/// <summary>
	/// Loads a given scene asyncronously.
	/// </summary>
	/// <param name="levelName">Level name.</param>


   
	

	

	#endregion

	

   
*/
}
