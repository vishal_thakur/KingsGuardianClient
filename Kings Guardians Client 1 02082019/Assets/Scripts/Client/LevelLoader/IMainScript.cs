﻿using UnityEngine;


public interface ISceneScript 
{
	MonoBehaviour monobehaviour { get; }
    bool Ready { get; set; }

    string[] SubScenes { get; }
    void OnLoadingDone();
}
