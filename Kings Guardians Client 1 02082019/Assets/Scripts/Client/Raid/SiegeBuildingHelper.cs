﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SiegeBuildingHelper : MonoBehaviour 
{
    public bool AttackAnimation;
    public Transform SpawnPoint; 
   
    [System.Serializable]
    public class Particle
    {
        public bool Instantiate;
        public GameObject Prefab;   
        public AudioClip Sound;
        public float DestroyTime = -1;
    }
    public Particle particle =new Particle();


    [System.Serializable]
    public class Projectile
    {
        public bool BallisticMode;
        public float Angle = 30f;
        public GameObject Prefab;
        public float Speed;
    }
    public Projectile projectile = new Projectile();

    public GameObject Explosion;

    Animator _animator;
    AudioSource _audioSource;

    GameObject _particle;
    GameObject _projectile;
    Vector3 _destination;

    List<Vector3> _ballisticPath = new List<Vector3>();
    int _ballisticPathStep = -1;

    void Awake()
    {
        _animator = GetComponentInChildren<Animator>(); 
        _audioSource = GetComponent<AudioSource>();

    }

    void Update()
    {
        if (_projectile != null)
        {
            if (projectile.BallisticMode)
                BallisticProjectileMovement();
            else
                SimpleProjectileMovement();         
        }
    }




    public bool Attack()
    {
		Debug.LogError ("asdasd");
        if (!AttackAnimation)          
			return false;
		
        _animator.SetTrigger("Attack");
        return true;
    }

    public bool CreateParticle()
    {
        if (particle.Prefab == null)
            return false;

        if (particle.Instantiate)
        {
            _particle = Instantiate(particle.Prefab, SpawnPoint.position);
            if (_particle == null)
                return false;              
            _particle.transform.LookAt(_particle.transform.position + transform.forward * 100);
           
            if(particle.DestroyTime >= 0)
                Destroy(_particle, particle.DestroyTime);
        }
        else
        {
            var p = particle.Prefab.GetComponent<ParticleSystem>();
            p.Emit(1);
        }

        if (particle.Sound != null)
            _audioSource.PlayOneShot(particle.Sound);

        return true;
    }


    public bool LaunchProjectile(Vector3 position)
    {
        _projectile = Instantiate(projectile.Prefab, SpawnPoint.position);
        if (_projectile == null)
            return false;               
     
        _destination = position;
        _projectile.transform.LookAt(_destination);
        return true;
    }



    GameObject Instantiate(GameObject prefab, Vector3 position)
    {
        if (prefab == null)
            return null;

        var go = (GameObject)Instantiate(prefab, position, Quaternion.identity) as GameObject;
        return go;
    }


    void SimpleProjectileMovement()
    {
        _projectile.transform.position = Vector3.MoveTowards(_projectile.transform.position, _destination, projectile.Speed * Time.deltaTime);

        if (Vector3.Distance(_destination, _projectile.transform.position) <= 0.5f)
            OnDestinationReached();         
              
    }

    void BallisticProjectileMovement()
    {
        if (_ballisticPathStep < 0)
        {
            Debug.Log("Ballistic Path created");
            _ballisticPath = CalculateBalisticPath(
                transform.position + SpawnPoint.position,
                _destination,
                projectile.Angle,
                projectile.Speed * Time.deltaTime,
                20
            );
            _ballisticPathStep = 0;
        }

        else
        {
            _projectile.transform.position = Vector3.MoveTowards(_projectile.transform.position, _ballisticPath[_ballisticPathStep], projectile.Speed * Time.deltaTime);
            if (Vector3.Distance(_destination, _ballisticPath[_ballisticPathStep]) <= 0.2f)
            {
                _ballisticPathStep++;
                if (_ballisticPathStep >= _ballisticPath.Count)
                    OnDestinationReached();
            }
        }
    }



    void OnDestinationReached()
    {
        var go = Instantiate(Explosion, _projectile.transform.position);
        if (go != null) Destroy(go, 2f);

        Destroy(_projectile,0.2f);
        _projectile = null;

        if(onProjectileCallback != null)
            onProjectileCallback();  
    }









   



    public delegate void Delegate();
    public Delegate onAnimatorCallback;
    public Delegate onProjectileCallback;

    void OnAnimationCallback()
    {
        if (onAnimatorCallback != null)
            onAnimatorCallback();
    }




    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        GUIStyle style = GUI.skin.label;
        style.active.textColor = Color.cyan;
        Gizmos.color = Color.cyan;

        if (SpawnPoint != null)
        {
            UnityEditor.Handles.Label(SpawnPoint.position, "Spawn Point", style);
            Gizmos.DrawSphere(SpawnPoint.position, 0.2f);
        }     
    }
    #endif







    List<Vector3> CalculateBalisticPath( Vector3 start, Vector3 end, float angle, float speed, int resulution)
    {
        float height = speed * Mathf.Pow(Mathf.Sin(angle), 2f) / 2 * 9.84f;
        List<Vector3> path = new List<Vector3>();
        for ( float i = 0; i < resulution + 1; i++ )
        {
            path.Add(SampleParabola( start, end, height, i / resulution ));          
        }
        return path;
    }

    Vector3 SampleParabola ( Vector3 start, Vector3 end, float height, float t ) 
    {
        float parabolicT = t * 2 - 1;
        if ( Mathf.Abs( start.y - end.y ) < 0.1f )
        {
            //start and end are roughly level, pretend they are - simpler solution with less steps
            Vector3 travelDirection = end - start;
            Vector3 result = start + t * travelDirection;
            result.y += ( -parabolicT * parabolicT + 1 ) * height;
            return result;
        } 
        else
        {
            //start and end are not level, gets more complicated
            Vector3 travelDirection = end - start;
            Vector3 levelDirecteion = end - new Vector3( start.x, end.y, start.z );

            Vector3 right = Vector3.Cross( travelDirection, levelDirecteion );
            Vector3 up = Vector3.Cross( right, levelDirecteion );
            if ( end.y > start.y ) up = -up;

            Vector3 result = start + t * travelDirection;
            result += ( ( -parabolicT * parabolicT + 1 ) * height ) * up.normalized;
            return result;
        }
    }



}
