﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GridSystem;

/// <summary>
/// This class handles the building's behaviour. 
/// </summary>
public class RaidBuildingBehaviour : BuildingBehaviour, IDamageable
{ 
	#region IDamageable implementation
	public bool Exists { get { return Reference != null;}}
	public int MaxHP { get { return Reference.MaxHP; }}
	public int CurrHP { get { return Reference.CurrHP; }}
	public int Shield { get { return Reference.Shield; }}
	public Vector3 Position { get { return transform.position; }}
	#endregion


	Vector3 _lookDirection = Vector3.zero;
	List<IDamageable> _targets;

	RaidUnitStat _raidUnitStat;

	bool _siegeBuilding;

	public void Attack(Vector3 targetPosition)
	{
		if (siegeController == null) return;

		_lookDirection = targetPosition - transform.position;
		_lookDirection.y = transform.position.y;
		if (_lookDirection.magnitude > 1)
			_lookDirection.Normalize();

		siegeController.Attack(targetPosition);
	}




	public void OnAttacked(AttackResult result, int damage, Element damageType, int currHP, int currShield)
	{
//		Debug.LogError ("Result\t"+  result + "\t\t" + "Damage\t" +  damage + "\t\t" + "damageType\t" + damageType + "\t\t" + "currHP\t" + currHP + "\t\t" + "currShield\t" + currShield);
//		StartCoroutine (OnAttackedInSync(result , damage , damageType , currHP , currShield));
		switch (result)
		{
			case AttackResult.Hit:
				Reference.UpdateHP(currHP);
				Reference.UpdateShield(currShield);
				break;

			case AttackResult.Miss:
				break;
		}
	}

//	IEnumerable OnAttackedSynced(){
//		float waitTime = 1;
//
//		while()
//		}
//

//	IEnumerator OnAttackedInSync(AttackResult result, int damage, Element damageType, int currHP, int currShield){
//
//		yield return new WaitForSeconds (1f);
//		switch (result)
//		{
//		case AttackResult.Hit:
//			Reference.UpdateHP(currHP);
//			Reference.UpdateShield(currShield);
//			break;
//
//		case AttackResult.Miss:
//			break;
//		}
//		StopCoroutine ("OnAttackedInSync");
//	}


	protected override void OnUpdate()
	{
		if(controller.FootprintVisible)
			controller.ToggleFootprint(false);


		// Rotate to the target look
		if (_siegeBuilding &&  _lookDirection != Vector3.zero && transform.forward != _lookDirection)
			controller.SetRotation(_lookDirection, Time.deltaTime);

		_raidUnitStat.Update();
	}



	protected override void OnInitialize()
	{
		if (Reference.ContentID == "Landmine") controller.HiddenBuilding(true);
		_raidUnitStat = new RaidUnitStat();
		_siegeBuilding = GetComponent<SiegeBuildingController>() != null;
	}

	protected override void InitBuildingIndicator()
	{
		_raidUnitStat = new RaidUnitStat();
		var hud = GetComponentInChildren<RaidStatusHUD>();
		_raidUnitStat.Init(this, hud);
		_raidUnitStat.Toggle(true);   

		var h = GetComponentInChildren<BuildingHUD>();
		h.Toggle(false);
	}


	protected override void OnAttackCallback()
	{
		// update characters
		/*foreach (var pair in _targets)
		{
		} */

		Debug.Log(Reference.InstanceID +  " - Attack on effect callback");
	}  
}
