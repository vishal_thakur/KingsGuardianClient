﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GridSystem;
using System;

public class RaidLogic : SingletonMono<RaidLogic>, ISceneScript  
{
    public MonoBehaviour monobehaviour { get { return this; } }
    public bool Ready 
    {
        get { return _ready; }
        set { 
            _ready = value;
            if(_ready) LevelLoader.Instance.Hide();            
        }
    }

    public string[] SubScenes { get { return new string[]{ "VillageBase" }; } }

	public int AbilityPoints = 0;

    public TownLoader<RaidBuildingBehaviour> TownLoader = new TownLoader<RaidBuildingBehaviour>();      

    public BaseWindowGUI RewardWindow;

    Dictionary<string, RaidCharacterBehaviour> _spawnedCharacters = new Dictionary<string, RaidCharacterBehaviour>();
    string _selectedCharacter = "";
    bool _requestSent = false;
    bool _ready = false;



    public void OnLoadingDone()
    {
        TownLoader.logic = this;
        var cc = Camera.main.GetComponentInParent<CameraControl>();
        cc.Init();            
        cc.Enable = true;

        // Get enemy player
        TownLoader.Load (GameManager.Instance.TargetPlayer);
        RaidHUD.Instance.Init();
		Ready = true;
    }


    void Update()
    {
        if (_selectedCharacter == "" || _requestSent)
            return;

		var raycastResult = MultiplatformInput.Raycast();
        if (raycastResult.Touch && raycastResult.ValidHit)
        {
            if (!_spawnedCharacters.ContainsKey(_selectedCharacter))
            {
                _requestSent = true;
                Point p = new Point(raycastResult.Hit.point);

                var msg = new SimpleMessage();
                msg.Add(ParameterCode.CharacterInstanceID, _selectedCharacter);
                msg.Add(ParameterCode.Position, p.ToString());
				NetworkManager.SendRequest(RequestCode.SpawnCharacter, msg);
            }
            else
            {
                var building = raycastResult.Hit.transform.GetComponent<RaidBuildingBehaviour>();
                if (building != null)
                {                   
					if (_spawnedCharacters[_selectedCharacter].CurrentTarget != building.Reference.InstanceID)
					{
						var msg = new SimpleMessage();
						msg.Add(ParameterCode.CharacterInstanceID, _selectedCharacter);
						msg.Add(ParameterCode.InstanceID, building.Reference.InstanceID);
						NetworkManager.SendRequest(RequestCode.TargetBuilding, msg);
						_requestSent = true;

//						Debug.LogError ("sent" + Time.time);
					}
                }
            }           
        }
    }





    public void SelectCharacter(string contentID)
    {
//		RaidCharacterBehaviour value = null;

		foreach (KeyValuePair<string , RaidCharacterBehaviour> obj in _spawnedCharacters) {
			if (obj.Key == contentID) {
				//Enable Selection marker
				obj.Value.ToggleSelectionMarker(true);
			}
			else//Disable selection Marker
				obj.Value.ToggleSelectionMarker(false);
		}

		if (_selectedCharacter == contentID) {
			// use special attack.
//			_spawnedCharacters [contentID].ToggleSelectionMarker (false);
			//Show selection Marker
		}
		else {
			_selectedCharacter = contentID;
//			_spawnedCharacters.TryGetValue()ToggleSelectionMarker (true);
		}
    }

    public void UseActiveAbility(string instanceID, int abilityNumber)
    {
		var c = _spawnedCharacters[instanceID];
		if (c.AuraActive)
			Notification.Error("You can't use this ability why it's still active!");

		else if (abilityNumber >= 0 && c.Statistics.ContentData.ActiveAbilities.Count > abilityNumber)
		{
			var a = c.Statistics.ContentData.ActiveAbilities[abilityNumber];
			if (a != null && AbilityPoints >= a.Cost)
			{
				var msg = new SimpleMessage();
				msg.Add(ParameterCode.CharacterInstanceID, instanceID);
				msg.Add(ParameterCode.AbilityNumber, abilityNumber.ToString());
				NetworkManager.SendRequest(RequestCode.ActivateRaidAbility, msg);
			}
			else
				Notification.Error("You don't have enough Ability Points to use this ability ("+a.Name+")!");
		}
		Debug.LogError("Ability ("+abilityNumber+") doesn't exists on this character ("+instanceID+")");
    }

	public void RaidAbilityResponse(SimpleMessage msg)
	{
		string character = msg.GetValue(ParameterCode.CharacterInstanceID);
		int ability = int.Parse(msg.GetValue(ParameterCode.AbilityNumber));
		bool activate = msg.GetValue(ParameterCode.Action) == "activate";

		if (_spawnedCharacters.ContainsKey(character))
			_spawnedCharacters[character].AbilityEvent(ability, activate);
	}

	public void CustomCharacterEvent(SimpleMessage msg)
	{
		var instanceID = msg.GetValue(ParameterCode.InstanceID);
		var eventName = msg.GetValue(ParameterCode.EventName);

		if (_spawnedCharacters.ContainsKey(instanceID))
			_spawnedCharacters[instanceID].CustomEvent(eventName);
	}



    public void BackToVillage()
    {
        LevelLoader.Instance.LoadScene("Village");
    }

	public void CustomCharacterSpawn(CustomCharacterSpawn msg)
	{
		var c = new CharacterReference();
		c.Read(msg.Character);

		var cb = CharacterSpawner.SpawnCharacter<RaidCharacterBehaviour>(msg.Position.vector3, Quaternion.identity, c);      
		_spawnedCharacters.Add(msg.Character.InstanceID, cb);
	}

    public void SpawnCharacterResponse(SimpleMessage msg)
    {
        var characterInstanceID = msg.GetValue(ParameterCode.CharacterInstanceID);
        Point pos = Point.Parse(msg.GetValue(ParameterCode.Position));

		CharacterReference character = GameManager.Instance.LocalPlayer.Characters.Find(x => x.InstanceID == characterInstanceID);
		var cb = CharacterSpawner.SpawnCharacter<RaidCharacterBehaviour>(pos.vector3, Quaternion.identity, character);      
		_spawnedCharacters.Add(characterInstanceID, cb);

        _requestSent = false;



		foreach (KeyValuePair<string , RaidCharacterBehaviour> obj in _spawnedCharacters) {
			if (obj.Key == characterInstanceID) {
				//Enable Selection marker
				obj.Value.ToggleSelectionMarker(true);
			}
			else//Disable selection Marker
				obj.Value.ToggleSelectionMarker(false);
		}
    }

    public void TargetSelectedEvent(SimpleMessage msg)
	{			
		string c = msg.GetValue(ParameterCode.CharacterInstanceID);
		string t = msg.GetValue(ParameterCode.InstanceID);
		Vector3 pos = Vector3Extended.Parse(msg.GetValue(ParameterCode.Position));
		Vector3 lookPos = Vector3Extended.Parse(msg.GetValue(ParameterCode.LookAtPosition));

		if (_spawnedCharacters.ContainsKey(c))
		{
//			Debug.LogError (c + "Selected");
			_spawnedCharacters[c].SetTarget(t, pos, lookPos);
			_requestSent = false;
		}
		else
		{
			var building = TownLoader.GetBuilding(c);
			if (building != null)
				building.Attack(lookPos);   
		}       
    }

	public void OnDestinationReached(SimpleMessage msg)
	{
		string c = msg.GetValue(ParameterCode.CharacterInstanceID);
		_spawnedCharacters[c].Attack();
	}


	public void UnitDamageTaken(RaidDamageTakenMessage msg)
	{
		if (_spawnedCharacters.ContainsKey(msg.InstanceID))
		{
			var character = _spawnedCharacters[msg.InstanceID];
			character.OnAttacked(msg.AttackResult, msg.Damage, msg.DamageType, msg.CurrentHP);  
		}

		// if building
		else
		{
			var building = TownLoader.GetBuilding(msg.InstanceID);
			if (building != null)
				building.OnAttacked(msg.AttackResult, msg.Damage, msg.DamageType, msg.CurrentHP, msg.CurrentShield);    
		}
	}

	public void AbilityPointsUpdate(SimpleMessage msg)
	{
		AbilityPoints = int.Parse(msg.GetValue(ParameterCode.AbilityPoints));
		Debug.Log("Ability udpate: " +AbilityPoints);
	}

    public void RaidFinished(SimpleMessage msg)
    {
        int cnt = int.Parse(msg.GetValue(ParameterCode.BuildingCount));

        if (onRaidFinished != null)
            onRaidFinished(cnt);
    }

    public void GetNextReward()
    {
        Debug.Log("Get Next Reward [Raid Logic]");
        NetworkManager.SendRequest(RequestCode.GetNextReward, new UnityEngine.Networking.NetworkSystem.EmptyMessage());
    }


	//Parse the recievced Rewards Message
    public void NextReward(SimpleMessage msg)
    {
		string reward = msg.GetValue(ParameterCode.StringValue);
		if (string.IsNullOrEmpty(reward))
        {
            if (onRewardGet != null)
				onRewardGet(null, 0 , false);
        }
        else
        {
			//Split Reward String
            string[] AllRewards = reward.Split('|');
			string[] individualReward = null;
			foreach(string rewardValue in AllRewards){
				//Debug.LogError (">>>>>>>>>>>" + rewardValue + "<<<<<<<<<<<");
				//Get reward in form of (Currency : rewardAmmount)
				individualReward = rewardValue.Split(':');

				//Parse and Grab the material Currency type
				var material = (Currency)Enum.Parse(typeof(Currency), individualReward[0]);

				//Parse and Grab the material reward ammount
				int amount = int.Parse(individualReward[1]);

				//Grab respective Icon
				var icon = AssetPackManager.GetCurrencyIcon(material);

				if (onRewardGet != null) {
					if(individualReward.Length == 2)
						onRewardGet (icon, amount , false);
					else if(individualReward.Length == 3)
						onRewardGet (icon, amount , true);
				}
			}
				

//            if (data.Length > 1)
//            {                
//                var material = (Currency)Enum.Parse(typeof(Currency), data[0]);
//                int amount = int.Parse(data[1]);
//                var icon = AssetPackManager.GetCurrencyIcon(material);
//
//                if (onRewardGet != null)
//                    onRewardGet(icon, amount);
//            }
//            else
//            {
//                var item = GameManager.Instance.GameContent.GetItem(reward);
//                var icon = AssetPackManager.GetCardIcon(item.ContentID, true);
//               
//                if (onRewardGet != null)
//                    onRewardGet(icon, 1);
//            }

        }
    }


	public delegate void OnRewardGet(Sprite icon, int amount , bool isSpecialReward);
    public OnRewardGet onRewardGet;

    public delegate void OnRaidFinished(int cnt);
    public OnRaidFinished onRaidFinished;
}
