﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GridSystem;

[RequireComponent(typeof(CapsuleCollider))]
public class RaidCharacterBehaviour : CharacterBehaviour, IDamageable
{
	public string CurrentTarget { get; private set; }

	#region IDamageable implementation
	public bool Exists { get { return Statistics != null;}}
	public int MaxHP { get { return Statistics.MaxHP; }}
	public int CurrHP { get { return Statistics.CurrHP; }}
	public int Shield { get { return Statistics.Shield; }}
	public Vector3 Position { get { return transform.position; }}
	#endregion

	RaidUnitStat _raidUnitStat;

	bool _wallJumper;

	//SHows The active Character
	[SerializeField] GameObject SelectionMarker;

	protected override void OnInit()
	{
//		Debug.LogError("Loda" + this.controller.gameObject.name);
		base.OnInit();



		//Grab The Selection Marker
		try{
			SelectionMarker = transform.GetChild(0).Find("aura").gameObject;
			//Set Selection Marker as disabled Initially
//			SelectionMarker.GetComponent<ParticleSystem>().Stop();
//			Debug.LogError("aura is off now for " + gameObject.name);
		}
		catch(System.Exception e){
//			Debug.LogError ("Aura not found for" + gameObject.name);
		}

		var jumptrait = Statistics.ContentData.Traits.Find(x => x.GetType() == typeof(WallJumpTrait));
		_wallJumper = jumptrait != null;

		_raidUnitStat = new RaidUnitStat();
		var hud = GetComponentInChildren<RaidStatusHUD>();
		_raidUnitStat.Init(this, hud);
		_raidUnitStat.Toggle(true);  

		var h = GetComponentInChildren<BattleUnitStatUI>();
		h.gameObject.SetActive(false);





	}


	/// <summary>
	/// Toggles the selection marker.
	/// </summary>
	/// <param name="flag">If set to <c>true</c> flag.</param>
	public void ToggleSelectionMarker(bool flag){
		try{

			SelectionMarker.transform.GetChild(0).gameObject.SetActive(flag);
//			if(flag)
//				SelectionMarker.transform.GetChild(0).gameObject.SetActive(true);
//			else
//				SelectionMarker.transform.GetChild(0).gameObject.SetActive(false);
		}
		catch(System.Exception e){
		
		}
	}


	public void SetTarget(string targetID, Vector3 destination, Vector3 targetPos)
	{
		CurrentTarget = targetID;
		MovePathfinding(destination);
	}

	public void Attack()
	{	
//		Debug.LogError ("Attacked" + Time.time);
		controller.BasicAttack();
	}

	public void OnAttacked(AttackResult result, int damage, Element damageType, int currHP)
	{
		switch (result)
		{
			case AttackResult.Hit:
				if (damage > 0 && Statistics.CurrHP > currHP)
					controller.OnHit();

				Statistics.CurrHP = currHP;
				controller.SetDead(Statistics.CurrHP == 0);
				break;

			case AttackResult.Miss:	break;
		}
	}



	void OnFixedUpdate()
	{
		if (_wallJumper && controller.CurrentAction == ActionID.MoveForward)
		{
			// wall = 8x8
			RaycastHit hit;
			if (Physics.Raycast(transform.position + Vector3.up, Vector3.forward, out hit, 0.2f))
			{
				var building = hit.transform.GetComponent<RaidBuildingBehaviour>();
				if (building != null && building.Reference.ContentID == "Wall")
				{
					controller.Jump();
					//TODO : Jump 3 tile forward.
				}
			}
		}
	}

	protected override void OnDestinationReached (ActionID action)
	{     
		// Send request to the server
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.CharacterInstanceID, Statistics.InstanceID);
		NetworkManager.SendRequest(RequestCode.DestinationReached, msg);
	}


	public bool AuraActive { get { return _auraFX != null; }}
	GameObject _auraFX;

	protected override void OnActionStarted(ActionID action)
	{
		Debug.Log("OnActionStarted " + action);
	}

	protected override void OnAction(ActionID action)
	{
		Debug.Log("OnAction " + action);
	}

	protected override void OnActionFinished(ActionID action)
	{
		Debug.Log("OnActionFinished " + action);
	}
        
	public override void ParticleCallback()
	{
		Debug.Log("ParticleCallback");
	}
   	   


	public void AbilityEvent(int ability, bool activate)
	{
		if (activate)
		{
			if (ability >= 0 && Statistics.ContentData.ActiveAbilities.Count > ability)
			{
				var a = Statistics.ContentData.ActiveAbilities[ability];
				var fx = AssetPackManager.GetRaidFX(a.Name);

				if (fx != null && fx.AuraFX != null)
				{
					_auraFX = (GameObject)Instantiate(fx.AuraFX, transform.position + Vector3.up * .25f, Quaternion.identity) as GameObject;
					_auraFX.transform.SetParent(transform);
				}
			}
			else
				Debug.Log(this.Statistics.InstanceID + " activates Ability: " + ability);
		}
		else
		{
			if (_auraFX != null)
			{
				Destroy(_auraFX);
				_auraFX = null;
			}
			else
				Debug.Log(this.Statistics.InstanceID + " deactivates Ability: " + ability);
		}
	}
   

	public void CustomEvent(string eventName)
	{
		switch (eventName)
		{
			case "Kamikaze":
				Notification.Info(Statistics.InstanceID + " destroyed itself with 'Kamikaze' ability");
				break;

			case "WallJumpAdded": 
				_wallJumper = true;
				break; 

			case "WallJumpRemoved":
				var jumptrait = Statistics.ContentData.Traits.Find(x => x.GetType() == typeof(WallJumpTrait));
				_wallJumper = jumptrait != null;
				break;
		}
	}


	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		UnityEditor.Handles.color = Color.white;
		UnityEditor.Handles.Label( transform.position + Vector3.up, Position + " | "+ new GridSystem.Point(Position)); 
	}
	#endif
}


