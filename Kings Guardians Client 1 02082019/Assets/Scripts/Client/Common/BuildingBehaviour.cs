﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GridSystem;

/// <summary>
/// This class handles the building's behaviour. 
/// </summary>
public class BuildingBehaviour : MonoBehaviour
{ 
    // Properties
    public BuildingReference Reference { get; private set; }   
    public BuildingState CurrentState { get { return (Reference != null) ? Reference.CurrentState : BuildingState.Inactive; } }    

    public bool Initialized { get { return initialized; }}

    public bool Selected { get; set; }
	public WallController WallController 
	{
		get { 
			if (_wallController == null)
			{
				_wallController = controller.ActiveModel.GetComponent<WallController>();
				_wallController.InstanceID = Reference.InstanceID;
			}
			return _wallController;
		}
	}

	WallController _wallController;

	//Building Attack Range
	GameObject _range;
    // Protected variables
    protected bool initialized { get; private set; }

	protected BuildingController controller { get; private set; }  
	protected SiegeBuildingController siegeController { get; private set; }
   

    // Private variables
    BoxCollider _collider;
    GameObject _model;

    // Cache
	BuildingState _lastState = BuildingState.Active;
//	Point _lastPosition;
	Quaternion _lastRotation;
	Vector3 _lastPosition;
    float _referenceLastChecked = -1f;
	   
	protected Vector3 centerOffset;


	//----------Show Range Logic--------------------
	public bool RangeVisible 
	{
		get{ return (_range != null) ? _range.activeSelf : false; }
		set{ 
			if(_range != null)
				_range.SetActive(value);
		}
	}  

	public void ShowRange(){
			// Create the range indicator
		BuildingBehaviour building = this;

		if (building.Reference.ContentData.Category == BuildingCategory.Defense){
			var gridSize = GridHandlerPlayer.Instance.TileSize;
			var upgrade = building.Reference.GetUpgrade();
			var range = Resources.Load<GameObject>("AttackRange");

			_range = (GameObject)MonoBehaviour.Instantiate(range, building.transform.position+ Vector3.up * 0.05f, building.transform.rotation) as GameObject;
//			_range.transform.localScale = new Vector3(gridSize.x,0.01f, gridSize.y) * upgrade.Range*2;
			//Vishal no need of *2
			_range.transform.localScale = new Vector3(gridSize.x,0.01f, gridSize.y) * upgrade.Range;

			_range.transform.localPosition = (controller.Footprint.transform.localPosition) * -1;
			_range.transform.SetParent(building.transform);	
		}  
	}

	//-----------------------------------------------------

    #region Public Methods
    public void Initialize(BuildingReference reference)
    {
        //Debug.Log("Init");
//		Debug.LogError ("Called bitch!" + reference.ContentData.Name);
        Reference = reference;

		transform.name = transform.name.Replace("(Clone)", "");

		controller = GetComponent<BuildingController>();
	
		siegeController = GetComponent<SiegeBuildingController>();
		if (siegeController != null)
			siegeController.onHitCallback = OnAttackCallback;

		// Init collider
        _collider = GetComponent<BoxCollider> ();
        if (_collider == null)
            _collider = gameObject.AddComponent<BoxCollider>();
		//Vishal Reposition Collider to match the Footprint
		_collider.center = (controller.Footprint.transform.localPosition) * -1;
		_collider.size = new Vector3(controller.ModelBaseSize.x, 5,  controller.ModelBaseSize.y);

		centerOffset = Vector3.zero;


		//************** GridCodeNew ***********************
		//For Player Buildings
		if (Reference.Size.x % 2 == 0)
			centerOffset.x = GridHandlerPlayer.Instance.TileSize.x / 2;
		if (Reference.Size.y % 2 == 0)
			centerOffset.z = -GridHandlerPlayer.Instance.TileSize.y / 2;


		//For Enemy Buildings
//		if (TownLogic.Instance.ScoutMode) {
//			if (Reference.Size.x % 2 == 0)
//				centerOffset.x = GridHandlerPlayer.Instance.TileSize.x / 2;
//			if (Reference.Size.y % 2 == 0)
//				centerOffset.z = -GridHandlerPlayer.Instance.TileSize.y / 2;
//		}
		//****************************



		//
//		centerOffset = Vector3.zero;
//		centerOffset = new Vector3();
//		if (Reference.Size.x % 2 == 0)
//			centerOffset.x = GridHandler.Instance.TileSize.x + 20;
//		if (Reference.Size.y % 2 == 0)
//			centerOffset.z = -GridHandler.Instance.TileSize.y;

		OnInitialize();
        initialized = true;

		OnMoved();
		ChangeReference();


		//StartCoroutine (SpawnAtLegitLocation ());
//		LayoutManager.Instance.AddUnitToLayoutManager (this);
    } 


	IEnumerator SpawnAtLegitLocation(){
//		Debug.LogError (gameObject.name + "<<<<");
		//Grab Ref
		TownBuildingBehaviour TownBuildingBehaviourRef = GetComponent<TownBuildingBehaviour> ();

		if (TownBuildingBehaviourRef != null) {

			//Bounds Check Flags
			bool HorizontalPositiveAvailable = true;
			bool HorizontalNegativeAvailable = true;
			bool VerticalPositiveAvailable = true;
			bool VerticalNegativeAvailable = true;

			Point newPosition = null;
			yield return new WaitForEndOfFrame ();

			Point OrignalPoint = TownBuildingBehaviourRef.Reference.Position;
//		if(TownBuildingBehaviourRef.contro)

			//If Building is not at a valid Position
			//Recall the function To check Building is now at a valid location
			while (!TownBuildingBehaviourRef.ValidPosition) {
//			yield return null;

				//Grab Current Position
				Point currentPosition = TownBuildingBehaviourRef.Reference.Position;
//			Point currentPosition = new Point((int)transform.localPosition.x , (int)transform.localPosition.y );
//			Debug.LogError ("Old pos x: " + currentPosition.x + "Old pos y: " + currentPosition.y);



				//-------------First Check-------------
				//************** GridCodeNew ***********************
				//Horizontal Positive Max value Check
				if (currentPosition.x <= GridHandlerPlayer.Instance.HorizontalTiles && HorizontalPositiveAvailable) {
//				Debug.LogError ("First Check");
					//Increment X axis Relocation
					newPosition = new Point (currentPosition.x + 1, OrignalPoint.y);
				} else
					HorizontalPositiveAvailable = false;




				//-------------Second Check-------------
				//Horizontal Positive Max value Check
				if (currentPosition.x >= 0 && HorizontalNegativeAvailable && !HorizontalPositiveAvailable) {
//				Debug.LogError ("Second Check");
					//Decrement X axis Relocation
					newPosition = new Point (currentPosition.x - 1, OrignalPoint.y);

					if (currentPosition.x <= 0)
						HorizontalNegativeAvailable = false;
				}



				//-------------Third Check-------------
				//Vertical Positive Max value Check
				if (currentPosition.y <= GridHandlerPlayer.Instance.VerticalTiles && VerticalPositiveAvailable && !HorizontalPositiveAvailable && !HorizontalNegativeAvailable) {
//				Debug.LogError ("Third Check");
					//Increment Y axis Relocation
					newPosition = new Point (OrignalPoint.x, currentPosition.y + 1);


					if (currentPosition.y >= GridHandlerPlayer.Instance.VerticalTiles)
						VerticalPositiveAvailable = false;
				}


//			//-------------Fourth Check-------------
//			//Vertical Positive Max value Check
				if (currentPosition.y >= 0 && !VerticalPositiveAvailable && !HorizontalPositiveAvailable && !HorizontalNegativeAvailable && VerticalNegativeAvailable) {
//				Debug.LogError ("Fourth Check");

					//Decrement Y axis Relocation
					newPosition = new Point (OrignalPoint.x, currentPosition.y - 1);
				}


//			Debug.LogError ("Final New pos x: " + newPosition.x + "Old pos y: " + newPosition.y);
				Reference.Position = newPosition;

				OnMoved ();
				ChangeReference ();
				//Reposition To a Valid Position
//			TownBuildingBehaviourRef.Move(newPosition);
			}
		}
//		Debug.LogError ("Now At a legit Position");
	}


    public void ToggleCollider(bool b)
    { 
        if(_collider.enabled != b)
            _collider.enabled = b; 
    }

	public void Rotate(Rotation rot)
	{
		Reference.Rotation = rot;	
	}
    #endregion



    #region Private Methods
    void Update()
    { 
        if (!initialized) return;

        if (Reference.LastUpdated > _referenceLastChecked)
            ChangeReference();

		if (Reference.Dead && !controller.RuinsVisible)
			controller.ToggleRuin(true);

		if (!Reference.Dead && controller.RuinsVisible)
			controller.ToggleRuin(false);


        OnUpdate();
    }


    void ChangeReference()
    {
		// If the reference is missing exit from this method.
		if (Reference != null)
		{
			controller.ChangeModel(Reference.Upgrade);
			if (CurrentState == BuildingState.Inactive)
				controller.ShowStand(Reference.Upgrade);
			else
				controller.HideStand();
		}
		else
			controller.ChangeModel(1);

        // Building indicator setup
        InitBuildingIndicator();    

        // caching...
        _lastState = CurrentState;
        _referenceLastChecked = Time.time;

		if (Reference.InstanceID == "Wall")
		{
			_wallController = controller.ActiveModel.GetComponent<WallController>();
			if (_wallController != null)
				_wallController.InstanceID = Reference.InstanceID;
		}

        OnReferenceChanged();   
    }
    #endregion
    protected virtual void OnReferenceChanged(){}
    protected virtual void OnInitialize(){}
    protected virtual void OnUpdate() {}
    protected virtual void InitBuildingIndicator() { }
	protected virtual void OnAttackCallback(){}


    #if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        GUIStyle style = GUI.skin.label;
        style.normal.textColor = Color.white;
        UnityEditor.Handles.color = Color.white;
		Gizmos.color = Color.gray;

		var corners = (Corner[])Enum.GetValues(typeof(Corner));

		foreach (var c in corners)
		{
			var tile = Footprint.Tile(Reference.Position, Reference.Size, c);
//			Debug.LogError ("Name:" + Reference.ContentData.Name + "\tsize" + Reference.Size+ "\tposition" + Reference.Position);
			var pos = Footprint.WorldPosition(Reference.Position, Reference.Size, c);

			UnityEditor.Handles.Label( pos + Vector3.up, c.ToString() + ": "+tile.ToString() +"\n (" + pos.ToString()+")" , style); 
			Gizmos.DrawSphere(pos, 0.2f);
		}
    }
    #endif


	protected virtual void OnMoved()
	{
		//************** GridCodeNew ***********************
//		return;
//		Debug.LogError (Reference.Position);
		Vector3 pos = Footprint.WorldPosition(Reference.Position, Reference.Size, Corner.Center);
		pos.z = pos.z - GridHandlerPlayer.Instance.TileSize.y * 0.25f;       
		transform.position = pos + centerOffset;
		transform.rotation = Quaternion.Euler(new Vector3(0, (float)((int)Reference.Rotation * 90), 0));
		GridHandlerPlayer.Instance.AddObstacle(Reference.ContentID, new Footprint(){ Position = Reference.Position, Size = Reference.Size }); 
	}



	//Reset Unit's Position and Rotation as per the Current layout
	public void ResetPositionAndRotationCurrentLayout(){
		transform.position = _lastPosition;
		transform.rotation = _lastRotation;

		//************** GridCodeNew ***********************
		GridHandlerPlayer.Instance.AddObstacle(Reference.ContentID, new Footprint(){ Position = Reference.Position, Size = Reference.Size }); 
	}


	//Saves Pos and rot for reverting later
	public void SavePosAndRot(){
		_lastPosition = transform.position;
		_lastRotation = transform.rotation;
	}



	//Save Position Of Building
	public void SaveTransformToServer()
	{
		RequestCode code = RequestCode.Replace;
		NetworkManager.SendRequest(code, new ConstructRequest() { 
			ContentID = Reference.ContentID,
			InstanceID = Reference.InstanceID,
			Position = Reference.Position,
			Rotation = Reference.Rotation 
		});
	}


}
