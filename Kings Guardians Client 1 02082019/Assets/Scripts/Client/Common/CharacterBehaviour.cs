﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

public class CharacterBehaviour : MonoBehaviour, IParticleMessageReceiver
{
	public CharacterReference Statistics;
    public Sprite Icon { get; private set; }
	
	protected new CapsuleCollider collider;

	protected CharacterController controller;

	Pathfinder _pathfinder;
	List<Point> _path;
	int _currentDestination = -1;
	Vector3 _finalDestination;
	bool _moveWithPathfinding = false;

	public void Init()
	{
		collider = GetComponent<CapsuleCollider> ();
		if (collider == null)
			collider = gameObject.AddComponent<CapsuleCollider>();

		collider.height = 2f;
		collider.center = new Vector3(0f, 1f, 0f);

		Icon = AssetPackManager.GetCharacterAssets(Statistics.ContentID).Icon;

		controller = GetComponent<CharacterController>();
		controller.OnDestinationReached = DestinationReached;
		controller.OnActionStarted = OnActionStarted;
		controller.OnAction = OnAction;
		controller.OnActionFinished = OnActionFinished;

		controller.Init();

		_pathfinder = new Pathfinder();
		_pathfinder.DefaultObstacleCost = 1000;
		_pathfinder.SetObstacleCost("Wall", 100);

		OnInit();
	}

	protected void MovePathfinding(Vector3 destination)
	{
		_finalDestination = destination;   

		Point start = new Point(transform.position);
		Point dest = new Point(_finalDestination);

		_pathfinder.Calculate(start, dest);
		if (_pathfinder.HasPath)
		{
			_path = _pathfinder.GetPath();
			_currentDestination = -1;
			_moveWithPathfinding = true;
			MoveToNextPoint();
		}
		else
			Debug.LogError("Didn't find a path to destination: " + _finalDestination + "("+dest+")");

		_pathfinder.Reset(); 
	}

	void DestinationReached(ActionID action)
	{
		if (_moveWithPathfinding)
		{
			if (_currentDestination + 1 < _path.Count)
				MoveToNextPoint();
			else
				OnDestinationReached(action);
		}
		else
			OnDestinationReached(action);
	}

	void MoveToNextPoint()
	{
		_currentDestination++;
		controller.Move(_path[_currentDestination].vector3);
	}


	#region Virtual Methods
	public virtual void ParticleCallback() { }

	protected virtual void OnInit() { }
	protected virtual void OnDestinationReached (ActionID action) { }

	protected virtual void OnActionStarted (ActionID action){}
	protected virtual void OnAction (ActionID action){}
	protected virtual void OnActionFinished (ActionID action){}
	#endregion





}
