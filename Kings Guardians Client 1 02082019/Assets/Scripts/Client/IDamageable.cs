﻿using UnityEngine;
using System.Collections;

public interface IDamageable
{
    bool Exists { get; }

    int MaxHP { get; }
    int CurrHP { get; }
    int Shield { get; } 

	Vector3 Position { get; }
}
