﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.EventSystems;
using GridSystem;
using UnityEngine.Networking.NetworkSystem;

/// <summary>
/// This is the logic in the town scene.
/// This handles the building selection and construction.
/// </summary>
public class TownLogic : SingletonMono<TownLogic>, ISceneScript 
{
    public enum TScoutMode
    {
        Native,
        Battle
    }

    public static TScoutMode typeScoutMode = TScoutMode.Native;
	public bool ScoutMode { get; private set; }
	public bool LoadEnemyBase = false;

    public MonoBehaviour monobehaviour { get { return this; } }
    public bool Ready 
    {
        get { return _ready; }
        set { 
            _ready = value;
            if(_ready) LevelLoader.Instance.Hide();            
        }
    }

    public string[] SubScenes { get { return new string[]{ "VillageBase" }; } }

    public enum Mode { Basic, Building, WallBuilding, Grid }
    public Mode CurrentMode { get; private set; } 

    public TownBuildingBehaviour SelectedBuilding { get { return (_ready) ? _basic.Selected : null; }}

    public TownLoader<TownBuildingBehaviour> TownLoader = new TownLoader<TownBuildingBehaviour>();

	/// <summary>
	/// Basic player functionality.
	/// Selecting buildings, and moving the camera.
	/// </summary>
    BasicManager _basic;

	/// <summary>
	/// Player input for constructing, placing and moving buildings. 
	/// </summary>
    BuildingManager _building;

	/// <summary>
	/// Player input for constructing, placing and moving walls. 
	/// </summary>
	WallBuildingManager _wallBuilding;

	/// <summary>
	/// Input for grid manager. Not used.
	/// Planned for manipulate the grid, adding new sections.
	/// </summary>
    GridManager _grid;


    TownManagerBase _activeController;

    bool _ready;

    public void OnLoadingDone()
    {
//		Debug.LogWarning("Scene Loaded" + Time.time);

        TownLoader.logic = this;
        _basic = new BasicManager(this);
        _building = new BuildingManager(this);
		_wallBuilding = new WallBuildingManager(this);
        _grid = new GridManager(this);
        _activeController = _basic;

        var cc = Camera.main.GetComponentInParent<CameraControl>();
        cc.Init();            

		// Handle Scouting
		if (GameManager.Instance.TargetPlayer != null) {
            //Debug.LogError("Loading Both");
			//Load Player's Village and Enemie's village
			ScoutMode = true;
//			LoadEnemyBase = true;
			TownLoader.Load(GameManager.Instance.TargetPlayer);
				
		}
		//Normal Player's Village Load
		else
        {
            //Debug.LogError("Loading Only mine");
            ScoutMode = false;
//			LoadEnemyBase = false;
			TownLoader.Load ();

			GameManager.Instance.LocalPlayer.RefreshSquad();

			//Force Construct Castle if not loaded
			if (GameManager.Instance.LocalPlayer.Buildings.Items.Find( x => x.ContentID == "Castle") == null)
				ConstructBuilding("Castle");
			Debug.LogWarning("Town loaded" + Time.time);
		}     

		Ready = true;
    }

    void Update()
    {
		if (!_ready) return;            
		if (ScoutMode) return;
        _activeController.Update(MultiplatformInput.Raycast());
    }

    public void Rotate(float angle)
    {
		if (ScoutMode) return;
		if (_activeController != null) {
			
			_activeController.Rotate (angle);
		}
    }

    public void Apply()
    {
		//Donot Save position if in scout mode
		if (ScoutMode) return;

        if (_activeController != null && _activeController.Apply())
            _basic.Activate();
    }

    public void Cancel()
    {
		if (ScoutMode) return;
        if (_activeController != null)
            _activeController.Cancel();             
          
        _basic.Activate();
    }

    public void ChangeController(TownManagerBase controller)
    {
		if (ScoutMode) return;
        if( _activeController != null )
            _activeController.Deactivate();        
        _activeController = controller;

        if (_activeController.GetType() == typeof(BasicManager))
            CurrentMode = Mode.Basic;
        else if (_activeController.GetType() == typeof(BuildingManager))
            CurrentMode = Mode.Building;
		else if (_activeController.GetType() == typeof(WallBuildingManager))
			CurrentMode = Mode.WallBuilding;
        else if (_activeController.GetType() == typeof(GridManager))
            CurrentMode = Mode.Grid;
    }

    public void ConstructBuilding(string contentID)
    {
		if (ScoutMode) return;
        var building = TownLoader.InstantiateBuilding(contentID);
		if (building.Reference.ContentID == "Wall")
		{
			_wallBuilding.Create = true;
			_wallBuilding.Activate(building);
		}
		else
		{
			_building.Create = true;
			_building.Activate(building);
		}        
    }

    public void MoveBuilding()
    {
		if (ScoutMode) return;
		if (SelectedBuilding.Reference.ContentID == "Wall")
		{
			_wallBuilding.Create = false;
			_wallBuilding.Activate(SelectedBuilding);
		}
		else
		{
			_building.Create = false;
			_building.Activate(SelectedBuilding);
		}        
    }

    public void UpgradeBuilding(BuildingReference building)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.InstanceID, building.InstanceID);
        NetworkManager.SendRequest(RequestCode.Upgrade, msg);
    }

    public void RepairBuilding(BuildingReference building)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.InstanceID, building.InstanceID);
        NetworkManager.SendRequest(RequestCode.Repair, msg);
    }

	public void Collect(BuildingReference building)
	{
		if (ScoutMode) return;
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.InstanceID, building.InstanceID);
		msg.Add(ParameterCode.BoolValue, "true");
		NetworkManager.SendRequest(RequestCode.Collect, msg);
	}

	public void Collect(Remains remains)
	{
		if (ScoutMode) return;
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.InstanceID, remains.ID);
		msg.Add(ParameterCode.BoolValue, "false");
		NetworkManager.SendRequest(RequestCode.Collect, msg);
	}

	public void AttachWarlock(BuildingReference building)
	{
		if (ScoutMode) return;
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.InstanceID, building.InstanceID);
		NetworkManager.SendRequest(RequestCode.AttachWarlock, msg);
	}

	public void DetachWarlock(BuildingReference building)
	{
		if (ScoutMode) return;
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.InstanceID, building.InstanceID);
		NetworkManager.SendRequest(RequestCode.DetachWarlock, msg);
	}

    public void StartBuildingProduction(string buildingID, string itemContentID = "")
    {
		if (ScoutMode) return;
        RequestCode code = (itemContentID == "") ? RequestCode.Mine : RequestCode.Craft;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.InstanceID, buildingID);
        msg.Add(ParameterCode.ContentID, itemContentID);
        NetworkManager.SendRequest(code, msg);
    }

    public void CancelBuildingProduction(BuildingReference building)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.InstanceID, building.InstanceID);
        NetworkManager.SendRequest(RequestCode.CancelBuilding, msg);
    }

    public void CancelBuildingWork()
    {
		if (ScoutMode) return;
        if (SelectedBuilding == null)
            return;

        var worker = SelectedBuilding.Reference.AssignedWorker;

        var msg = new SimpleMessage();
        msg.Add(ParameterCode.InstanceID, worker.InstanceID);
        NetworkManager.SendRequest(RequestCode.CancelWorker, msg);
    }

    public void CreateWorker(bool warlock)
    {   
		if (ScoutMode) return;
        var code = (warlock) ? RequestCode.CreateWarlock : RequestCode.CreateWorker;        
		Debug.Log("Send message " + code);
        NetworkManager.SendRequest(code, new EmptyMessage());  
    }


	public void Equip(CharacterReference character, ItemReference item, int slot)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.CharacterInstanceID, character.InstanceID);
        msg.Add(ParameterCode.ContentID, item.ContentID);
        msg.Add(ParameterCode.ItemSlot, slot.ToString());
        NetworkManager.SendRequest(RequestCode.Equip, msg);
    }

	public void Unequip(CharacterReference character, ItemReference item, int slot)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.CharacterInstanceID, character.InstanceID);
        msg.Add(ParameterCode.ContentID, item.ContentID);
        msg.Add(ParameterCode.ItemSlot, slot.ToString());
        NetworkManager.SendRequest(RequestCode.Unequip, msg);
    }

	public void UpdateSquad(CharacterReference character, int slot)
    {
		if (ScoutMode) return;
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.CharacterInstanceID, character.InstanceID);
        msg.Add(ParameterCode.SquadSlot, slot.ToString());
        NetworkManager.SendRequest(RequestCode.ChangeSquad, msg);
    }


	public void ScoutEnemy(string username, TScoutMode mode)
	{
		if (ScoutMode) return;
		var opponent = GameManager.Instance.Opponents.Find(x => x.Username == username);
		if (opponent == null)
		{
			Notification.Error("The opponent is invalid");
			return;
		}

        typeScoutMode = mode;
        print("Switch");
        GameManager.Instance.SetTargetPlayer(opponent, mode, OnScoutResponse);
	}

	void OnScoutResponse()
	{
		if (ScoutMode) return;
		// If return load scout window
		LevelLoader.Instance.LoadScene("Village");
	}
		
	public void ExitScoutMode()
	{
		if (!ScoutMode) return;
        typeScoutMode = TScoutMode.Native;
        GameManager.Instance.SetTargetPlayer(null);
		LevelLoader.Instance.LoadScene("Village");
	}

    public void StartBattle(string username)
    {
		if (ScoutMode) return;
        var opponent = GameManager.Instance.Opponents.Find(x => x.Username == username);
        if (opponent == null)
        {
            Notification.Error("The opponent is invalid");
            return;
        }

        GameManager.Instance.SetTargetPlayer(opponent);
        var msg = new SimpleMessage();
        msg.Add(ParameterCode.Username, opponent.Username);
        NetworkManager.SendRequest(RequestCode.StartBattle, msg);
    }
        
}
