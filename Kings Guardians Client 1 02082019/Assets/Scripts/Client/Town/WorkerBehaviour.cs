﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

public class WorkerBehaviour : MonoBehaviour 
{
	public string InstanceID;
	public WorkerReference Reference { get { return GameManager.Instance.LocalPlayer.GetWorker(InstanceID); } }

	CharacterController _controller;
	TownBuildingBehaviour _targetBuilding;

	Pathfinder _pathfinder = new Pathfinder();
	List<Point> _path;
	int _currentDestination = 0;
	Vector3 _finalDestination;


	public void Init()
	{
		_controller = GetComponent<CharacterController>();
		_controller.OnDestinationReached = OnDestinationReached;
		_controller.Init();

		_pathfinder.DefaultObstacleCost = 1000;
		_pathfinder.SetObstacleCost("Wall", 100);

	}

	void OnDestinationReached (ActionID action)
	{     
		SetNextRandomDestination();
	}  


    void LateUpdate()
    {      
		if (!Reference.Free)
			Workwork();
    }

    void SetNextRandomDestination()
    {
		if (_controller.CurrentAction != ActionID.None) return;

        float angle = Random.Range(0f, 360f);
        Quaternion quat = Quaternion.AngleAxis(angle, Vector3.up);
        Vector3 newForward = quat * Vector3.forward;
        newForward.y = 0;
        newForward.Normalize();

		_finalDestination = transform.position + newForward * 20f;   

		Point start = new Point(transform.position);
		Point dest = new Point(_finalDestination);

		_pathfinder.Calculate(start, dest);
		if (_pathfinder.HasPath)
		{
			_path = _pathfinder.GetPath();
			_currentDestination = 0;
			_controller.Move(_path[_currentDestination].vector3);
		}
		else
			Debug.LogError("Didn't find a path to destination: " + _finalDestination + "("+dest+")");

		_pathfinder.Reset();  
    }

    void Workwork()
    {
        if( _targetBuilding == null)
            _targetBuilding = TownLogic.Instance.TownLoader.GetBuilding(Reference.WorkingOn.ItemContentID);

		if (_targetBuilding != null)
		{
			// Calculate Destination
			Point currPos = new Point(transform.position);
			Point destPos = Footprint.ClosestCorner(currPos, _targetBuilding.Reference.Position, _targetBuilding.Reference.Size);

			if (GridHandlerPlayer.ManhattanDistance(currPos, destPos) > 1)
			{
				var pos = destPos.vector3;
				_controller.Move(pos);
			}
			else
				_controller.BasicAttack();
		}
		else
			_controller.Stop();
    }


  

}
