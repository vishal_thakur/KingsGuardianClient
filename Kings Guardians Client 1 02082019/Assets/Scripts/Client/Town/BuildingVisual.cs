﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridSystem
{    
    [AddComponentMenu("Kings Guardians/Buildings/BuildingVisual")]
	public class BuildingVisual : MonoBehaviour
	{
        public bool Initialized { get; private set; }

        public bool Allowed
        {
            get{ return _allowed; }
            private set
            { 
                if(GridOverlay.Instance != null)
                    _footprintMesh.material = (_allowed) ? GridOverlay.Instance.GreenMaterial : GridOverlay.Instance.RedMaterial ;
                _allowed = value;               
            }
        }

        #region Private Variables
        BuildingBehaviour _building;

        // Precreated building parts
        Transform _root;
        Dictionary<int, GameObject> _models = new Dictionary<int, GameObject>();
        Dictionary<int, GameObject> _upgradings = new Dictionary<int, GameObject>();
        GameObject _ruins;
        GameObject _base;     

        // Generated building parts
        GameObject _range;
        MeshRenderer _footprintMesh;

        bool _allowed;
        bool _upgradeMode;
        int _currentTier = -1;

        int _maxUpgrades = -1;

        bool _footprintVisible;

        #endregion

        public bool FootprintVisible 
        {
            get{ return _footprintMesh.enabled; }
            set{ 


                bool v = value;
				if (!v && GridOverlay.Instance != null && GridOverlay.Instance.ShowOverlay)
                {
                    _footprintMesh.material = GridOverlay.Instance.DarkGreenMaterial;
                    v = true;
                }

                _base.SetActive(!v);
                _footprintMesh.enabled = v; 
                _footprintVisible = v;
            }
        }       

        public bool RangeVisible 
        {
            get{ return (_range != null) ? _range.activeSelf : false; }
            set{ 
                if(_range != null)
                    _range.SetActive(value);
            }
        }  

        public void Init(BuildingBehaviour building)
        {    
            _root = building.transform;
            _building = building;

            // Collect parts automatically.
            foreach (Transform t in transform)
            {        
                if (t.name == "Footprint")
                    _base = t.gameObject;
                else
                {
                    string[] name = t.name.Split('_');
                    if (name[1] == "R")
                        _ruins = t.gameObject;
                    else
                    {
                        int u = int.Parse(name[1]);
                        if (name[0] == transform.name)
                            _models.Add(u, t.gameObject);
                        else if (name[0] == "Upgrade")
                            _upgradings.Add(u, t.gameObject);
                    }
				//	t.localScale = Vector3.one * AssetPackManager.Instance.Scale;
                } 
                t.gameObject.SetActive(false);

            }


            // Create the footprint mesh
            GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("Footprint"), transform.position + Vector3.up * 0.1f, transform.rotation) as GameObject;
            go.transform.Rotate(new Vector3(90, 0, 0));
            go.transform.localScale = new Vector3((float)building.Reference.ContentData.ModelSize.x, (float)building.Reference.ContentData.ModelSize.y, 1);

			go.transform.SetParent(transform);
            _footprintMesh = go.GetComponent<MeshRenderer>();          


            // Create the range indicator
            if (building.Reference.ContentData.Category == BuildingCategory.Defense)
			{
				//************** GridCodeNew ***********************
                var gridSize = GridHandlerPlayer.Instance.TileSize;
                var upgrade = building.Reference.GetUpgrade();
                var range = Resources.Load<GameObject>("AttackRange");

                _range = (GameObject)MonoBehaviour.Instantiate(range, building.transform.position+ Vector3.up * 0.05f, building.transform.rotation) as GameObject;
                _range.transform.localScale = new Vector3(gridSize.x,0.01f, gridSize.y) * upgrade.Range*2;
                _range.transform.SetParent(building.transform);
            }  

            _maxUpgrades = _building.Reference.ContentData.Upgrades.Length;          
            Initialized = true;
        }
   

        public void UpdateTier()
        {    
            int modelTier = GetValidTier(_models);
            if (_building.Reference.Dead && !_ruins.activeSelf)
            {
                _models[modelTier].SetActive(false);
                _ruins.SetActive(true); 
                return;
            }

            if (!_building.Reference.Dead && _ruins.activeSelf)
            {
                _models[modelTier].SetActive(true);
                _ruins.SetActive(false);
            }

            int steps = (_maxUpgrades > _models.Count) ? Mathf.RoundToInt((float)_maxUpgrades / (float)_models.Count) : 1;
            int tier = Mathf.FloorToInt (_building.Reference.Upgrade / steps);

            if (tier != _currentTier)
            {
                if(_models.ContainsKey(modelTier))
                    _models[modelTier].SetActive(false);              
                               
                ToggleWorkVisual(false);
                _currentTier = tier;
                modelTier = GetValidTier(_models);
                _models[modelTier].SetActive(true);
            }
        }

        public void ToggleWorkVisual(bool b)
        {
            if (_upgradings.Count == 0) return;
            int tier = GetValidTier(_upgradings);
            _upgradings[tier].SetActive(b);
        }


        public void OnMoved()
        {
            Vector3 pos = _building.Reference.Position.vector3;

			//************** GridCodeNew ***********************
            pos.z = pos.z - GridHandlerPlayer.Instance.TileSize.y * 0.25f;
          //  if (_building.Footprint.Size.x % 2 == 0) pos.x -= GridHandler.Instance.TileSize.x / 2;
          //  if (_building.Footprint.Size.y % 2 == 0) pos.z += GridHandler.Instance.TileSize.y / 4;           
            _root.position = pos + Vector3.up * 0.2f;
			_root.rotation = Quaternion.Euler(new Vector3(0, (float)((int)_building.Reference.Rotation * 90), 0));

			bool allowed = Footprint.ValidFootprint(_building.Reference.Position, _building.Reference.Size); 

            if (TownLogic.Instance != null)
            {               
                if (allowed)
                {
                    foreach (var b in TownLogic.Instance.TownLoader.GetBuildings())
                    {                       
                        if (b.Reference.InstanceID != _building.Reference.InstanceID &&
							GridHandlerPlayer.OverlappingFootprints(new Footprint(){Position = b.Reference.Position, Size = b.Reference.Size}, new Footprint(){Position = _building.Reference.Position, Size = _building.Reference.Size}))
                        {
                            allowed = false;
                            break;
                        }
                    }
                }
                Allowed = allowed;
            }
            else
                Allowed = false;
        }  


        int GetValidTier(Dictionary<int, GameObject> collection)
        {
            int validTier = _currentTier;
            if (!collection.ContainsKey(validTier))
            {
                for (int i = validTier; i > 0; i--)
                {
                    if (collection.ContainsKey(i))
                    {
                        validTier = i;
                        break;
                    }
                }
            }
            return (validTier < 1) ? 1 : validTier;
        }



        void OnDrawGizmosSelected()
        {
            Gizmos.DrawLine(transform.position + Vector3.up, transform.position + transform.forward * 10 + Vector3.up);
        }

	} 
}
