﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

/// <summary>
/// Player input for constructing, placing and moving buildings. 
/// </summary>
public class BuildingManager : TownManagerBase
{
    public bool Create = true;

    bool _dragging = false;
    CameraControl _camera;

	Point _originalPos;

	Point _originalPosinCurrentLayout;

    Rotation _originalRot;

    public BuildingManager(TownLogic logic):base(logic)
    {
        _camera = GameObject.FindObjectOfType<CameraControl>();
    }


    protected override void OnActivate()
    {
        building.Controlled = true;
        building.ToggleCollider(true);

		if(LayoutManager.Instance.IsLayoutModeActive)
			Messenger.Broadcast (Events.OnBuildingSelected);

        if (Create)
        {
			Point p = GridHandlerPlayer.Instance.Center;
            var ray = _camera.Cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))             
				p = GridHandlerPlayer.Vector3ToPoint(hit.point);                
			building.Move(p);
        }

        GridOverlay.Instance.ShowOverlay = true;
        building.Selected = true;

        if (!Create)
        {
			_originalPos = building.Reference.Position;
			_originalRot = building.Reference.Rotation;
        }
    }

    protected override void OnDeactivate()
    {
        if (building != null)
            building.Controlled = false;

        _dragging = false;
        GridOverlay.Instance.ShowOverlay = false;
        _camera.Enable = true;
    }

    public override void Rotate(float angle)
    {
        if (building != null)
            building.Reference.Rotate(); 
    }

    public override void Update(RaycastResult raycastResult)
    {
        if (building == null) return;

        _camera.Enable = !_dragging;            

        if (raycastResult.Touch) 
        {
            if (raycastResult.ValidHit) 
            {
                // !dragging VS Input.GetTouch (0).phase == TouchPhase.Began
                if (!_dragging && raycastResult.Hit.transform == building.transform)
                {
                    building.ToggleCollider(false);
                    _dragging = true;
                }
                //  && Input.GetTouch (0).phase == TouchPhase.Moved
                else if (_dragging && raycastResult.Hit.transform.name == "_Grid")
                {
                    Point p = new Point (raycastResult.Hit.point);
//					Debug.Log("Move building to :" + p + " ("+raycastResult.Hit.point+")");
					building.Move(p);   
                }
            }
        } 

        else if ( _dragging )
        {
            building.ToggleCollider(true);
            _dragging = false;
        }
    }


	//Save Position Of Building
    public override bool Apply()
    {
//		Debug.LogError (building.name +  "\tApply");
        // Check if the position of the building is valid.
        if (building == null || !building.ValidPosition) return false;      

        Deactivate();

        GridOverlay.Instance.ShowOverlay = false;


		if (LayoutManager.Instance.IsLayoutModeActive)
			return true;

		if (Create)
		{
			building.Reference.CreateTemporaryID();
			GameManager.Instance.LocalPlayer.Buildings.Add(building.Reference);
		}

		
		RequestCode code = (Create) ? RequestCode.Construct : RequestCode.Replace;
		NetworkManager.SendRequest(code, new ConstructRequest() { 
			ContentID = building.Reference.ContentID,
			InstanceID = building.Reference.InstanceID,
			Position = building.Reference.Position,
			Rotation = building.Reference.Rotation 
		});
        return true;
    }

    public override void Cancel()
    {
		if (Create)
			MonoBehaviour.Destroy(building.gameObject);
		else
		{
			building.Move(_originalPos);
			building.Rotate(_originalRot);
		}

        Deactivate();
        building = null;
    }


}
