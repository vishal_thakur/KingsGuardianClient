﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Basic player functionality.
/// Selecting buildings, and moving the camera.
/// </summary>
public class BasicManager : TownManagerBase
{
    public TownBuildingBehaviour Selected { get; private set; }

    CameraControl _camera;
    public BasicManager(TownLogic logic) : base (logic) 
    {
        _camera = GameObject.FindObjectOfType<CameraControl>();
        _camera.Enable = true;
    }


    protected override void OnActivate()
    {
        base.OnActivate();

        if (building != null)
            building.Controlled = false;
        _camera.Enable = true;  
    }

    public override void Update(RaycastResult raycastResult)
    {
        var phase = (Application.platform == RuntimePlatform.WindowsEditor) ? PlayerInput.Phase : Input.GetTouch(0).phase;

        if (raycastResult.Touch && phase != TouchPhase.Moved && raycastResult.ValidHit && !raycastResult.Dragging)
        {
			Remains r = raycastResult.Hit.transform.GetComponent<Remains>();
			bool hasStorage = GameManager.Instance.LocalPlayer.GetMaterial(Currency.Gold) < GameManager.Instance.LocalPlayer.GetStorage(Currency.Gold);
			if (r != null && hasStorage)
				TownLogic.Instance.Collect(r);
			else
			{
				TownBuildingBehaviour b = raycastResult.Hit.transform.GetComponent<TownBuildingBehaviour>();
				if (b != null)
				{
					if (Selected != null)
						Selected.Selected = false;

					if (b.CanCollect)
						TownLogic.Instance.Collect(b.Reference);
					else
					{
						
//						Debug.LogError ("Building Selected" + b);

							

						Selected = b;
						b.Selected = true;


						//If base Layout editor is active then Show Move Building Behaviour Directly
						if (LayoutManager.Instance.IsLayoutModeActive) {
							TownLogic.Instance.MoveBuilding ();
						}
						
					}
				}
				else if (Selected != null)
				{
//					TownLogic.Instance.Cancel();
					Selected.Selected = false;
					Selected = null;
				}
			}
        }
    
    }
}
