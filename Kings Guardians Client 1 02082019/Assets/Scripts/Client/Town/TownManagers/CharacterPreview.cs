﻿using UnityEngine;
using System.Collections;

public class CharacterPreview : MonoBehaviour
{
    public Transform SpawnPoint;
    GameObject _current;

    public void ShowCharacter(GameObject prefab)
    {
        if (_current != null) Destroy (_current);

        if (SpawnPoint != null && prefab != null)
        {
            _current = (GameObject)Instantiate (prefab, SpawnPoint.position, SpawnPoint.rotation) as GameObject;
			var c = _current.GetComponent<CharacterController>();
			c.Init();
        }
    }
}
