﻿using UnityEngine;
using System.Collections;

public class TownManagerBase
{
    TownLogic _logic;
    protected TownBuildingBehaviour building;

    public TownManagerBase(TownLogic logic)
    {
        _logic = logic;
    }

    public void Activate(TownBuildingBehaviour building = null)
    {
        this.building = building;
        _logic.ChangeController(this);
        OnActivate();
    }

    public void Deactivate()
    {
        OnDeactivate();
    }


    public virtual void Rotate(float angle){ }
    public virtual void Update(RaycastResult raycastResult){}
    public virtual bool Apply(){ return true; }
    public virtual void Cancel(){}

    protected virtual void OnActivate(){}
    protected virtual void OnDeactivate(){}
}
