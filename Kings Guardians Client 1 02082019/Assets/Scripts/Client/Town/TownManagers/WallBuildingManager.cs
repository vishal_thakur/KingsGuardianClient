﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

/// <summary>
/// Player input for constructing, placing and moving walls. 
/// </summary>
public class WallBuildingManager : TownManagerBase
{
    public bool Create = true;

    bool _dragging = false;
    CameraControl _camera;
	WallHUD _hud;
	WallLogic _wallLogic;

	class WallData
	{
		public TownBuildingBehaviour Building;
		public Point OriginalPos;
	}
	List<WallData> _walls = new List<WallData>();
	WallData _currentWall;

	public WallBuildingManager(TownLogic logic):base(logic)
    {
        _camera = GameObject.FindObjectOfType<CameraControl>();
		_wallLogic = logic.TownLoader.WallLogic;
    }


	void AddNewWall(TownBuildingBehaviour building, Point position)
	{
		_currentWall = new WallData() { Building = building };
		_currentWall.Building.Controlled = true;
		_currentWall.Building.ToggleCollider(true);

		if (!Create)
			_currentWall.OriginalPos = building.Reference.Position;

		_currentWall.Building.Move(position);
		_currentWall.Building.Selected = true;

		_hud.transform.position = _currentWall.Building.Reference.Position.vector3;
		_hud.transform.SetParent(_currentWall.Building.transform);

		_walls.Add(_currentWall);	
	}

	void WallUIFeedback(WallParts direction)
	{
		var building = TownLogic.Instance.TownLoader.InstantiateBuilding("Wall");
		AddNewWall(building, _wallLogic.Add(direction, _currentWall.Building.Reference.Position));
	}

    protected override void OnActivate()
    {
		Point position = building.Reference.Position;

		var go = MonoBehaviour.Instantiate(Resources.Load<GameObject>("WallHUD"));
		_hud = go.GetComponent<WallHUD>();
		_hud.wallUIFeedback = WallUIFeedback;
	
		if (Create)
		{
			Point p = GridHandlerPlayer.Instance.Center;
			var ray = _camera.Cam.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))             
				p = GridHandlerPlayer.Vector3ToPoint(hit.point);                
			position = p;
		}

		AddNewWall(building, position);
    
        GridOverlay.Instance.ShowOverlay = true; 				
    }

    protected override void OnDeactivate()
    {
		foreach (var wall in _walls)
			wall.Building.Controlled = false;

        _dragging = false;
        GridOverlay.Instance.ShowOverlay = false;
        _camera.Enable = true;

		MonoBehaviour.Destroy(_hud.gameObject);
    }

    public override void Update(RaycastResult raycastResult)
    {
        if (_walls == null) return;

        _camera.Enable = !_dragging;            

        if (raycastResult.Touch) 
        {
            if (raycastResult.ValidHit) 
            {
                // !dragging VS Input.GetTouch (0).phase == TouchPhase.Began
                if (!_dragging && raycastResult.Hit.transform == building.transform)
                {
                    building.ToggleCollider(false);
                    _dragging = true;
					_hud.gameObject.SetActive(false);
                }
                //  && Input.GetTouch (0).phase == TouchPhase.Moved
                else if (_dragging && raycastResult.Hit.transform.name == "_Grid")
                {
                    Point p = new Point (raycastResult.Hit.point);
					building.Move(p);   
                }
            }
        } 

        else if ( _dragging )
        {
            building.ToggleCollider(true);
            _dragging = false;
			_hud.gameObject.SetActive(true);
        }
    }




	//Save The Wall Positions to Server
    public override bool Apply()
    {
        // Check if the position of the building is valid.
        if (building == null || !building.ValidPosition) return false;      

        Deactivate();

		bool create = Create;

        GridOverlay.Instance.ShowOverlay = false;
		var buildings = new List<WallConstructRequest.Building>();

		foreach (var wall in _walls)
		{
			var b = wall.Building.GetComponent<BuildingBehaviour>();

			if (Create || !_wallLogic.WallExists(wall.Building.Reference.InstanceID))
			{
				b.Reference.CreateTemporaryID();
				GameManager.Instance.LocalPlayer.Buildings.Add(b.Reference);
				if (!create) create = true;
			}

			// Create the message data
			buildings.Add(new WallConstructRequest.Building() {
				ContentID = b.Reference.ContentID,
				InstanceID = b.Reference.InstanceID,
				Position = b.Reference.Position,
				Rotation = b.Reference.Rotation
			});       
		}

		_walls.Clear();
		RequestCode code = (create) ? RequestCode.ConstructWall : RequestCode.ReplaceWall;
		NetworkManager.SendRequest(code, new WallConstructRequest() { Buildings = buildings });
        return true;
    }

    public override void Cancel()
    {
		foreach (var wall in _walls)
		{
			if (Create)
				MonoBehaviour.Destroy(wall.Building.gameObject);
			else
				wall.Building.Move(wall.OriginalPos);
		}

        Deactivate();

		_walls.Clear();
		_currentWall = null;
    }


}
