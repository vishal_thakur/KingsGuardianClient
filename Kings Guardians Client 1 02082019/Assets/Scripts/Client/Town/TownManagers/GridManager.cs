﻿using UnityEngine;
using System.Collections;
using GridSystem;

/// <summary>
/// Input for grid manager. Not used.
/// Planned for manipulate the grid, adding new sections.
/// </summary>
public class GridManager : TownManagerBase
{
    public GridManager(TownLogic logic) : base (logic)
	{
		GridHandlerPlayer.Instance.Init ();
		GridHandlerEnemy.Instance.Init ();
    }

    protected override void OnActivate()
    {
        GridOverlay.Instance.ShowOverlay = true;
//        GridHandler.Instance.temporarySection = new GridHandler.TemporarySection (){ Position = new Point (0, 0) };
    }

    protected override void OnDeactivate()
    {
        GridOverlay.Instance.ShowOverlay = false;
    }

    public override void Update(RaycastResult raycastResult)
    {

    }

    public override void Cancel()
    {
        GridOverlay.Instance.ShowOverlay = false;
    }

    public override bool Apply()
    {
        GridOverlay.Instance.ShowOverlay = false;
        return true;       
    }

}
