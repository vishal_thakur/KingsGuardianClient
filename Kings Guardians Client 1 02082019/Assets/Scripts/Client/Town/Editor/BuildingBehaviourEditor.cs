﻿using UnityEditor;

[CustomEditor(typeof(BuildingBehaviour))]
public class BuildingBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BuildingBehaviour mytarget = (BuildingBehaviour)target;
        string info = "";

        info += "State: "+mytarget.CurrentState+" \n";

        info += "REFERENCE\n";
        if (mytarget.Reference != null)
        {
            info += " Instance ID: " + mytarget.Reference.InstanceID + "\n";
            info += " Worker: " + ((mytarget.Reference.AssignedWorker != null) ? mytarget.Reference.AssignedWorker.InstanceID : "none") + "\n";
            info += " HP: " + mytarget.Reference.CurrHP + "/" + mytarget.Reference.MaxHP + "\n";
            info += " Upgrade: " + mytarget.Reference.Upgrade + "\n";
          
            info += " Size: " + mytarget.Reference.Size + "\n";
			info += " Position: " + mytarget.Reference.Position + "\n";
			info += " Rotation: " + mytarget.Reference.Rotation + "\n";

            if (mytarget.Reference.Queue != null)
            {
                info += " Progress: (" + mytarget.Reference.Queue.Count + ")  \n";
                foreach (var q in mytarget.Reference.Queue)
                {
                    float p = Utils.TimeLeft(q.StartTime, q.Duration);
                    info += " - " + ((q.ItemContentID != "") ? q.ItemContentID : "Mine") + "(" + q.Amount + ") time left: " + Utils.SecondsToString(p) + " mins";
                }
            }
        }
        else
            info += " missing\n";

        EditorGUILayout.HelpBox(info, MessageType.None);
        DrawDefaultInspector();
    }
}
