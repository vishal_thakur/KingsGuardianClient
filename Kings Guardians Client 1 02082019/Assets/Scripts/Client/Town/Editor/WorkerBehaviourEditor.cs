﻿using UnityEditor;

[CustomEditor(typeof(WorkerBehaviour))]
public class WorkerBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        WorkerBehaviour mytarget = (WorkerBehaviour)target;
        string info = "";

        info += "Instance ID: " + mytarget.InstanceID +"\n";

        if (mytarget.Reference != null)
        {
            if (mytarget.Reference.Free)
                info += " FREE\n";
            else
            {
                info += "Building: " + ((mytarget.Reference.Building!= null) ? mytarget.Reference.Building.InstanceID : "none") + "\n";
                info += "StartDateTime: " + mytarget.Reference.WorkingOn.StartTime + "\n";
                info += "Duration: " + mytarget.Reference.WorkingOn.Duration + "\n";
            }
        }
        else
            info += " missing\n";

        EditorGUILayout.HelpBox(info, MessageType.None);
        DrawDefaultInspector();
    }
}
