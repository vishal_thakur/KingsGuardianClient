﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using GridSystem;

/// <summary>
/// This class handles the building's behaviour. 
/// </summary>
public class TownBuildingBehaviour : BuildingBehaviour
{   
    // Public Variables
	public bool ValidPosition { get { return _allowed; }}
    public bool Controlled{ get; set; }

	public bool ValidPosDebug = false;
	public bool ControlledDebug = false;

	public bool CanCollect 
	{
		get 
		{ 
			var mat = Reference.ContentData.Material;
			return HasStoredMaterials && GameManager.Instance.LocalPlayer.GetMaterial(mat) < GameManager.Instance.LocalPlayer.GetStorage(mat);
		}
	}


	public bool HasStoredMaterials{ get { return Reference.ContentData.Category == BuildingCategory.Resource_generator && Reference.StoredMaterial > 0; } }

    // Private Variables
    bool _gridOverlayExists;

	BuildingIndicator _indicator;

	bool _overlayVisible;
	bool _thisController;

	bool _allowed = true;

    public void Move(Point pos)
	{
//		return;
		Reference.Position = pos;
		OnMoved();
	}

    protected override void OnInitialize()
    {

		if (Reference.ContentID == "Landmine" ){
			//If Landmine , then Shows Its range
			this.GetComponent<BuildingBehaviour>().ShowRange();

			//If in Scout Mode then hide The Building(Landmine) visibility
			if(TownLogic.Instance.ScoutMode){
				controller.HiddenBuilding (true);
		}
		}
        _gridOverlayExists = GridOverlay.Instance != null;
		_indicator = new BuildingIndicator();
    }


	//void OnGUI(){
	//	GUILayout.TextField (GridOverlay.Instance.ShowOverlay.ToString());
	//}

    protected override void OnUpdate()
    {
		ValidPosDebug = ValidPosition;
		ControlledDebug = Controlled;
		//Debug.LogError(Time.time.ToString());
		bool showOverlay = GridOverlay.Instance.ShowOverlay;

		if (_overlayVisible != showOverlay || _thisController != Controlled){
			controller.ToggleFootprint(showOverlay);

			controller.ChangeColor((Controlled) ? FootprintColor.Green : FootprintColor.LightGreen);

			_overlayVisible = showOverlay;
			_thisController = Controlled;
		}

        if (!_gridOverlayExists) return;

        // If the grid overlay is on, but this is not the controlled building, toggle its collider off.
		if (showOverlay && !Controlled)
            ToggleCollider(false);
//		else if (!showOverlay)
		else if (!showOverlay)
            ToggleCollider(true); 

		if (LayoutManager.Instance.IsLayoutModeActive && !LayoutManager.Instance.InitialBuildingMoved)
			ToggleCollider (true);

		// TODO: toggle range (siege)
		this.GetComponent<BuildingBehaviour>().RangeVisible = Selected;

		_indicator.Update();
		_indicator.ToggleCollectIcon(HasStoredMaterials);

    }



    protected override void InitBuildingIndicator()
    {      
//		Debug.LogError ("Selected");
		var hud = GetComponentInChildren<BuildingHUD>();
		_indicator.Init(hud, this);
		_indicator.Toggle(false);     


		var rhud = GetComponentInChildren<RaidStatusHUD>();
		if(rhud != null)
			rhud.Toggle(false);
    }       

   
	protected override void OnMoved()
	{
		base.OnMoved();
//		Debug.LogError ("Name\t" + Reference.ContentData.Name + "\tPosition : " + Reference.Position + "\tSize " + Reference.Size  + "\tvector 3 Position : " + Reference.Position.vector3);
		bool allowed = Footprint.ValidFootprint(Reference.Position, Reference.Size);

		if (allowed)
		{
			foreach (var b in TownLogic.Instance.TownLoader.GetBuildings())
			{
				if (b.Reference.InstanceID != Reference.InstanceID)
				{
					//************** GridCodeNew ***********************
					bool overlapping = GridHandlerPlayer.OverlappingFootprints(new Footprint(){ Position = b.Reference.Position, Size = b.Reference.Size }, 
						new Footprint(){ Position = Reference.Position, Size = Reference.Size });

					if (overlapping)
					{
						allowed = false;
						break;
					}
				}
			}
		}


		if (_allowed != allowed)
		{
			_allowed = allowed;
			controller.ChangeColor((_allowed) ? FootprintColor.Green : FootprintColor.Red);
		}
	}
   
}
