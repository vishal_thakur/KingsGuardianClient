﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitScoutButton : MonoBehaviour
{
	public Button Button;

	bool _scoutMode = true;

	void Update () 
	{
		if (_scoutMode != TownLogic.Instance.ScoutMode)
		{
			Button.gameObject.SetActive(TownLogic.Instance.ScoutMode);
			_scoutMode = TownLogic.Instance.ScoutMode;
		}
	}
}
