﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridSystem;

public class WallLogic
{
	readonly Point UP = new Point(0, 1);
	readonly Point RIGHT = new Point(1, 0);

	Dictionary<string, BuildingBehaviour> _walls =  new Dictionary<string, BuildingBehaviour>();
	Dictionary<Point, string> _positionWall = new Dictionary<Point, string>();
	Dictionary<string, Point> _wallPositions = new Dictionary<string, Point>();

	public void AddWall(BuildingBehaviour building)
	{
		string id = building.Reference.InstanceID;
		Point pos = building.Reference.Position;

		if (!_walls.ContainsKey(building.Reference.InstanceID))
		{
			_walls.Add(id, building);
			_positionWall.Add(pos, id);
			_wallPositions.Add(id, pos);
		}
		else
		{
			if (_wallPositions[id] != pos)
			{
				_positionWall.Remove(_wallPositions[id]);
				_positionWall.Add(pos, id);
				_wallPositions[id] = pos;
			}
		}
	}

	public void Refresh()
	{
		foreach (var pair in _walls)
			Refresh(_wallPositions[pair.Key], pair.Value);
	}

	public void Refresh(BuildingBehaviour building)
	{
		var pos = building.Reference.Position;
		Refresh(pos, building);
	}

	public void Refresh(Point position, BuildingBehaviour building)
	{
		building.WallController.Toggle(WallParts.TOP, _positionWall.ContainsKey(Add(WallParts.TOP, position)));
		building.WallController.Toggle(WallParts.BOTTOM, _positionWall.ContainsKey(Add(WallParts.BOTTOM, position)));
		building.WallController.Toggle(WallParts.LEFT, _positionWall.ContainsKey(Add(WallParts.LEFT, position)));
		building.WallController.Toggle(WallParts.RIGHT, _positionWall.ContainsKey(Add(WallParts.RIGHT, position)));		
	}  


	public Point Add(WallParts direction, Point position)
	{
		Point pos = position;
		switch (direction)
		{
			case WallParts.TOP: pos += UP; break;
			case WallParts.BOTTOM: pos -= UP; break;
			case WallParts.LEFT: pos -= RIGHT; break;
			case WallParts.RIGHT: pos += RIGHT; break;
		}
		return pos;
	}

	public bool WallExists(string instanceID)
	{
		return _walls.ContainsKey(instanceID);
	}

}
