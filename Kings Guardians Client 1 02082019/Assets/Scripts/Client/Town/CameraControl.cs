﻿using UnityEngine;
using System.Collections;

/// <summary>
/// In town view this scripts controls the camera.
/// </summary>
public class CameraControl : MonoBehaviour 
{
    public Camera Cam;
    public LayerMask GridLayers;

	//Zoom Params
	[SerializeField]
	float maxZoom = 75;

	[Space(10) ,Header("Zoom Params") , SerializeField]
	float minZoom = 10;

	[SerializeField]
	float zoomCoeff = 1f;

	[SerializeField]
	float zoomLerpCoeff = 1;

	[SerializeField]
	bool isZooming;


	[Space(10) ,Header("Rotate Params")]
	[SerializeField]
	float maxRotation = 90;

	[SerializeField]
	float minRotation = -90;

	float velocity = 0.0f;

	[SerializeField]
	float targetFOV = 23.2f;

	[SerializeField]
	float targetRotation = 0f;

	Globals.MovementDirection MovementDirection;

	Vector3 CameraRotateAroundPoint;

//	[SerializeField]
//	bool isRotating;


	float currentFov;


    /// <summary>
    /// The speed of the camera movement.
	/// </summary>
	public float CameraRotationSpeed = 1.4f;
	public float CameraMovementSpeed = 0.001f;

    public bool Enable //{ get; set; }
    {
        get { return _enable; }
        set { _enable = value; }
    }

    bool _enable;

    /// <summary>
    /// The offset between the camera transform and the viewport center hitpoint on the grid.
    /// </summary>
    Vector3 _centerOffset;

    /// <summary>
    /// The viewport size in the world coordinates.
    /// </summary>
    Vector2 _viewportSize;


    Vector2 _gridMin;
	Vector2 _gridMax;


	//Gesture Recognizers;
//	TKPinchRecognizer PinchRecognizer;
//	TKRotationRecognizer RotationRecognizer;

    void OnEnable()
	{

		//Subscribe To Pinch/Streth Gesturs For Zooming in/out
		var Pinchrecognizer = new TKPinchRecognizer();
		Pinchrecognizer.gestureRecognizedEvent += ( r ) =>
		{
			Cam.fieldOfView -= Pinchrecognizer.deltaScale * zoomCoeff;
			Cam.fieldOfView = Mathf.Clamp(Cam.fieldOfView , minZoom , maxZoom);

			CalculateCameraMovementSpeedFromFOV();
		};
		TouchKit.addGestureRecognizer(Pinchrecognizer);


		//Subscribe To Rotate Gesturs For Zooming in/out
		var Rotationrecognizer = new TKRotationRecognizer();
		Rotationrecognizer .gestureRecognizedEvent += ( r ) =>
		{
//			float rotation = targetRotation + (twist.deltaDistance * CameraRotationSpeed);
//			targetRotation = Mathf.Min(maxRotation, rotation);
//			targetRotation = rotation;


			transform.RotateAround (CameraRotateAroundPoint , Vector3.up , -Rotationrecognizer.deltaRotation * CameraRotationSpeed);
//			cube.Rotate( Vector3.back, recognizer.deltaRotation );
//			Debug.Log( "rotation recognizer fired: " + r );
		};
		TouchKit.addGestureRecognizer(Rotationrecognizer);



    }

    

    void OnDisable(){
		TouchKit.removeAllGestureRecognizers();
	}

//	Vector2 direction;
//
//    private void OnGUI(){
//		GUILayout.TextField ("Pan Direction" + Input.GetTouch(0).deltaPosition.ToString());
//		GUILayout.TextField ("Current Dir:\t" + MovementDirection.ToString());
//    }
//

    public void Init()
	{
        Cam = GetComponentInChildren<Camera>();

		//************** GridCodeNew ***********************
		var grid = GridSystem.GridHandlerPlayer.Instance;
		var gridCollider = grid.GetComponent<BoxCollider> ();

        // Get the grid boundaries
        Vector3 center = grid.transform.position + gridCollider.center;
        _gridMin = new Vector2(center.x - gridCollider.size.x * grid.transform.localScale.x / 2, center.y - gridCollider.size.z * grid.transform.localScale.z / 2 );
        _gridMax = new Vector2(center.x + gridCollider.size.x * grid.transform.localScale.x / 2, center.y + gridCollider.size.z * grid.transform.localScale.z / 2 );

        Vector3 centerPos = Vector3.zero;
        Vector3 bottomLeftPost = Vector3.zero;

        // Get the camera and view center offset
        RaycastHit hit;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        if (Physics.Raycast(ray, out hit, 1000, GridLayers))
            centerPos = hit.point;

        ray = Camera.main.ViewportPointToRay(new Vector3(0F, 0F, 0));
        if (Physics.Raycast(ray, out hit, 1000, GridLayers))
            bottomLeftPost = hit.point;

        _centerOffset = transform.position - centerPos;

        Vector3 diff = centerPos - bottomLeftPost;

        _viewportSize = new Vector2(Mathf.Abs(diff.x) * 2 , Mathf.Abs(diff.z) * 2);
	}

//	public void OnSwipe(GestureInfoSwipe swipe){
//
////		if (PlayerInput.Phase == TouchPhase.Moved) {
////			PlayerInput.
////		}
//	}

//	public void CallbackWhilePanning(GestureInfoPan pan){
//		//Horizontal
//		if (pan.deltaDirection.x < 0)
//			MovementDirection = Globals.MovementDirection.Left;
//		else if (pan.deltaDirection.x > 0)
//			MovementDirection = Globals.MovementDirection.Right;
//
//		//Vertical
//		if (pan.deltaDirection.y < 0)
//			MovementDirection = Globals.MovementDirection.Top;
//		else if (pan.deltaDirection.y > 0)
//			MovementDirection = Globals.MovementDirection.Bottom;
//		
//
//	}

//    //On Pinching Gesture Callback
//    public void CallbackWhileStretching(GestureInfoZoom zoom)
//    {
//		float currentFov = Cam.fieldOfView;
//        float fov = Cam.fieldOfView - (zoom.deltaDistance *  zoomCoeff);
//
//		targetFOV = Mathf.Max(minZoom , fov);
//
//        //Cam.fieldOfView = Mathf.Lerp(Cam.fieldOfView, fov, Time.deltaTime * zoomLerpCoeff);
//	}
//
//	//On Streching Gesture Callback
//	public void CallbackWhilePinching(GestureInfoZoom zoom)
//	{
//		float fov = Cam.fieldOfView + (zoom.deltaDistance * zoomCoeff);
//
//		targetFOV = Mathf.Min(maxZoom , fov);
//
//		//Cam.fieldOfView = Mathf.Lerp(Cam.fieldOfView, fov, Time.deltaTime * zoomLerpCoeff);
//	}
//
//	//On Streching Gesture Callback
//	public void CallbackWhileTwisting(GestureInfoTwist twist)
//	{
//		if (!twist.clockwise)
//		{
//			float rotation = targetRotation + (twist.deltaDistance * CameraRotationSpeed);
////			targetRotation = Mathf.Min(maxRotation, rotation);
//			targetRotation = rotation;
//		}
//		else
//		{
//			float rotation = targetRotation - (twist.deltaDistance * CameraRotationSpeed);
////			targetRotation = Mathf.Max(minRotation, rotation);
//			targetRotation = rotation;
//		}
//	}

//	public void CallbackWhileStopTwisting(GestureInfoTwist twist){
//
////		isRotating = false;
//	}
//



//    //On Pinching Gesture Callback
//    public void CallbackStopPinching(GestureInfoZoom zoom)
//    {
//        Globals.message.Add(Time.time + "Stopped Pinching");
////        isZooming = false;
//    }
//
//    //On Pinching Gesture Callback
//    public void CallbackStopStreching(GestureInfoZoom zoom)
//    {
//        Globals.message.Add(Time.time + "Stopped Streching");
////        isZooming = false;
//    }

//	void OnGUI(){
//	}
//
	void FixedUpdate(){
        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle && ManagerBattleDeck.instance.currentUnit && ManagerBattleDeck.instance.currentUnit) { return; }
        //		if (Mathf.Abs (transform.rotation.y) >= 0.3f) {
        //			targetRotation = 0;
        //		}

        //		Cam.fieldOfView = Mathf.Lerp (Cam.fieldOfView, targetFOV, Time.deltaTime * zoomLerpCoeff);
        //Do Zooming if Pinching in Action
        if (Input.touchCount > 1) {
			//Zoom
//			Cam.fieldOfView = Mathf.Lerp (Cam.fieldOfView, targetFOV, Time.deltaTime * zoomLerpCoeff);
//			Cam.fieldOfView = targetFOV;

			if (PlayerInput.Phase == TouchPhase.Moved) {


				//Grap Direction of Swipe
//				direction = PlayerInput.DeltaPoisiton(0);

				//Rotation
//				float rot = Mathf.SmoothDampAngle (Cam.transform.localRotation.eulerAngles.y, this.targetRotation, ref velocity, 0.03f);
//				Quaternion rotation = Quaternion.Euler (new Vector3 (Cam.transform.rotation.x, rot, Cam.transform.rotation.z));

				RaycastHit hit;

				if (Physics.Raycast (transform.position, Camera.main.transform.forward, out hit)) {
					CameraRotateAroundPoint = hit.point;
//					transform.RotateAround (hit.point, Vector3.up, targetRotation * Time.deltaTime);
//					Debug.DrawLine (transform.position, hit.point);
				}
			}
			return;
		}
		else
			targetRotation = 0;


//		Debug.LogError ("Nothing FOund");
	}
		

//	void OnGUI(){
//		GUILayout.TextField ("Cam Speed\t" + CameraMovementSpeed);
//		GUILayout.TextField ("Cam FOV\t" + Cam.fieldOfView);
//	}

    void Update()
    {
        if (Input.GetKey(KeyCode.X))
        {

            Cam.fieldOfView -= 6 * Time.deltaTime;
            Cam.fieldOfView = Mathf.Clamp(Cam.fieldOfView, minZoom, maxZoom);

            CalculateCameraMovementSpeedFromFOV();
        }
        if (Input.GetKey(KeyCode.C))
        {

            Cam.fieldOfView += 6 * Time.deltaTime;
            Cam.fieldOfView = Mathf.Clamp(Cam.fieldOfView, minZoom, maxZoom);

            CalculateCameraMovementSpeedFromFOV();
        }


        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle && ManagerBattleDeck.instance.currentUnit && ManagerBattleDeck.instance.currentUnit) { return; }
		if (!Enable || Globals.IsAnyUIPanelActive) return;


//
//		if (Input.GetTouch(0).deltaPosition.x < 0)
//			MovementDirection = Globals.MovementDirection.Right;
//		else if (Input.GetTouch(0).deltaPosition.x > 0)
//			MovementDirection = Globals.MovementDirection.Left;
//
//		if (Input.GetTouch(0).deltaPosition.y < 0)
//			MovementDirection = Globals.MovementDirection.Top;
//		else if (Input.GetTouch(0).deltaPosition.y > 0)
//			MovementDirection = Globals.MovementDirection.Bottom;

		// MOUSE CLICK
		if( Application.platform == RuntimePlatform.WindowsEditor)
		{
			if (PlayerInput.Active && PlayerInput.Phase == TouchPhase.Moved) 
			{
				Vector2 touchDeltaPosition = PlayerInput.DeltaPoisiton(1);
				if( touchDeltaPosition.magnitude > 0.1f && !isZooming)
					TranslateCamera (touchDeltaPosition.x * CameraMovementSpeed/500, 0, touchDeltaPosition.y * CameraMovementSpeed*2/500);
			}
		}

		// TOUCH
		else
		{
			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Moved) 
			{
				Vector2 touchDeltaPosition = Input.GetTouch (0).deltaPosition;

				if( touchDeltaPosition.magnitude > 0.1f)
					TranslateCamera (-touchDeltaPosition.x * CameraMovementSpeed, 0, -touchDeltaPosition.y * CameraMovementSpeed*2);
			}
		}
	}


	//Using this When Zoomed in The pan speed will be lower and When zoomed out the Pan speed will be higher 
	void CalculateCameraMovementSpeedFromFOV(){
		CameraMovementSpeed = 0.1f * (Cam.fieldOfView / 32);
	}

	/// <summary>
	/// Translates the camera.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="y">The y coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	void TranslateCamera(float x, float y, float z)
	{
		Vector3 pos = transform.localPosition - _centerOffset;
//        if (pos.x + _viewportSize.x/2 + x > _gridMax.x || pos.x - _viewportSize.x/2 + x < _gridMin.x)
//			x = 0f;		
//        if (pos.z + _viewportSize.y/2 + z > _gridMax.y || pos.z - _viewportSize.y/2 + z < _gridMin.y)
//			z = 0f;

//		transform.forward = Vector3.zero;
//		if(Globals.CanCameraMoveTowardsTop)
//        	transform.Translate(x, y, z);
//		else
			transform.Translate(x, y, z);
    }


    void OnDrawGizmosSelected()
    {
       // Gizmos.DrawSphere(
    }


//	/// <summary>
//	/// Calculates the Camera's movement direction.
//	/// </summary>
//	void CalculateMovementDirection(){
//		//Find Horizontal Movement
//		Debug.LogError(transform.position.x. normalized.ToString());
//		//Find Vertical Movement
//	}
}