﻿using UnityEngine;
using System.Collections;
using GridSystem;
using System.Collections.Generic;

/// <summary>
/// This class can load a town of the player defined in the SceneManager as Player variable.
/// </summary>
public class TownLoader<T> where T : BuildingBehaviour
{   
    public ISceneScript logic;

    public Dictionary<string, WorkerBehaviour> Workers { get; private set; }

	public WallLogic WallLogic = new WallLogic();

    bool bLoadingAllyBase = false;

    [SerializeField]
	List<T> _buildingsPlayer = new List<T>();

    [SerializeField]
    List<T> _buildingsEnemy = new List<T>();

    public T Castle { get { return _buildingsPlayer.Find(x => x.Reference.ContentID == "Castle"); }}
   
    public int BuildingCount { get { return _buildingsPlayer.FindAll(x => x.Reference.ContentID != "Wall").Count; }}
    public int DestroyedBuildingCount { get { return _buildingsPlayer.FindAll(x => x.Reference.ContentID != "Wall" && x.Reference.ContentID != "Landmine" && x.Reference.Dead).Count; }}  

    public List<T> GetBuildings(string contentID = "")
    {
        if (_buildingsPlayer != null)
        {
            if (contentID == "")
                return _buildingsPlayer;
            else
				return _buildingsPlayer.FindAll(x => x.Reference.ContentID == contentID);
        }
        return new List<T>();
    }

    public T GetBuilding(string instanceID)
    {
		if (_buildingsPlayer!= null)
			return _buildingsPlayer.Find(x => x.Reference.InstanceID == instanceID);
        return null;
    }




    /// <summary>
    /// Load the town of the given player. If no player is set, the method will load the local players town.
    /// </summary>
    /// <param name="player">Player.</param>
    public void Load(Player player = null)
    {
        // If the player is null, get the local player
        bool localPlayer = false;
        if (player == null)
        {
            player = GameManager.Instance.LocalPlayer;
            localPlayer = true;
		}
        

        // Spawn the buildings of the Player's Village
        bLoadingAllyBase = true;
        _buildingsPlayer = new List<T> ();
		foreach( var building in GameManager.Instance.LocalPlayer.Buildings.GetAll())
		{        
			if (GridHandlerPlayer.ValidGridPoint( building.Position))
			{
				var behaviour = InstantiateBuilding(building.ContentID, false);
				behaviour.Initialize(building);     
                
				if (building.ContentID == "Wall")
					WallLogic.AddWall(behaviour);

                //Add AI Unit Behaviour To this Building unit
                //Messenger.Broadcast(Events.OnBattleInitialize ,);
            }
        }

        // Spawn the buildings of the Enemy's Village

        if (TownLogic.Instance.ScoutMode)
        {
			TownLogic.Instance.LoadEnemyBase = true;
            bLoadingAllyBase = false;
            _buildingsEnemy = new List<T> ();
            

			foreach (var building in player.Buildings.GetAll())
            {    
				if (GridHandlerEnemy.ValidGridPoint (building.Position))
                {
                    var behaviour = InstantiateBuilding (building.ContentID, false);

                   // Debug.LogError("Enemy building added to scene" + building.ContentID);
                    behaviour.Initialize (building);     

					if (building.ContentID == "Wall")
						WallLogic.AddWall (behaviour);

                    //Add AI Unit Behaviour To this Building unit
                    
                }
            }   	
		}

        bLoadingAllyBase = false;

        // If its a local player spawn the workers and place attackers remains
        Workers = new Dictionary<string, WorkerBehaviour>();
        if (localPlayer)
        {
            foreach (var workers in player.Workers)
                InstantiateWorker(workers);


			foreach (var remain in player.Remains)
			{
//				GameObject go = (GameObject)MonoBehaviour.Instantiate(Resources.Load<GameObject>("Remains"), remain.Position, Quaternion.identity) as GameObject;
				GameObject go = (GameObject)MonoBehaviour.Instantiate(Resources.Load<GameObject>("Remains"), new Vector3(remain.x , remain.y , remain.z), Quaternion.identity) as GameObject;
				var r = go.GetComponent<Remains>();
				r.ID = remain.InstanceID;
			}
        } 

		WallLogic.Refresh();
    }


    #region Buildings
    public T InstantiateBuilding(string itemID, bool createReference = true)
    {
		var b = AssetPackManager.GetBuildingAssets(itemID);
		GameObject go = (GameObject)MonoBehaviour.Instantiate(b.Model) as GameObject;
		go.transform.position = Vector3.zero;
		go.transform.position += Vector3.up * 0.1f;
		go.transform.localScale = Vector3.one;

		var building = go.AddComponent<T>();

		if (createReference)
		{
			var reference = new BuildingReference(itemID);
			building.Initialize(reference);           
		}

        //If Battle mode then Add building to AI manager as AI UNIT
        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {
            Globals.battleActive = true;


            AIUnit battleBuildingUnit = go.GetComponentInChildren<AIUnit>();
            

            if (battleBuildingUnit)
            {

                battleBuildingUnit.gameObject.tag = "Unit";
                battleBuildingUnit.gameObject.layer = 16;

                //IF loading Ally base
                if (bLoadingAllyBase)
                {
                    battleBuildingUnit.typeUnit = AIUnit.TUnit.Ally;
                    AIManager.Instance.listAllyTower.Add(battleBuildingUnit);
                    //AIManager.Instance.bufferlistAllyTower.Add(go);
                    //Messenger.Broadcast<GameObject, AIUnit.TUnit>(Events.OnBattleInitialize, go, AIUnit.TUnit.Ally);
                }
                else
                {
                   // Debug.LogError("Eenemy building added " + battleBuildingUnit.name);
                    battleBuildingUnit.typeUnit = AIUnit.TUnit.Enemy;
                    AIManager.Instance.listEnemyTower.Add(battleBuildingUnit);
                    //AIManager.Instance.bufferlistEnemyTower.Add(go);
                    //Messenger.Broadcast<GameObject, AIUnit.TUnit>(Events.OnBattleInitialize, go, AIUnit.TUnit.Enemy);
                }

                //Initialie the battlBuildingUnit
                battleBuildingUnit.Initialization(0);
            }
            else
            {
               // Debug.LogError(go.name + " Not added to Tower List");
            }

        }

		_buildingsPlayer.Add(building);
//		_buildingsEnemy.Add(building);
		return building;
    }
    #endregion


    #region Workers
    public void InstantiateMissingWorkers()
    {              
        foreach (var w in GameManager.Instance.LocalPlayer.Workers)
        {
            if (!Workers.ContainsKey(w.InstanceID))
                InstantiateWorker(w);
        }
    }

	WorkerBehaviour InstantiateWorker(WorkerReference worker)
    {
		string prefabName = (worker.Type == WorkerType.Warlock) ? "Warlock" : "Worker";
		var character = AssetPackManager.GetCharacterAssets(prefabName);
		if (character == null)
			return null;
		
		GameObject go = (GameObject)MonoBehaviour.Instantiate(character.Model, GridHandlerPlayer.Instance.Center.vector3, Quaternion.identity) as GameObject;

        go.name = worker.InstanceID;
        var w = go.AddComponent<WorkerBehaviour>();
        w.InstanceID = worker.InstanceID;
		w.Init();

        Workers.Add(w.InstanceID, w);
        return w;
    }

    #endregion

    public void Destroy(string instanceID)
    {
        var b = GetBuilding(instanceID);

        if (b != null)   
            MonoBehaviour.Destroy(b.gameObject);
        else
            Debug.LogWarning("Can't destroy building with instance id of " + instanceID + " becouse it doesnt exists");
    }   

}
