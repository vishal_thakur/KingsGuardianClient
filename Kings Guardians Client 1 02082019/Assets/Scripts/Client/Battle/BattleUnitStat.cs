﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUnitStat 
{
	BattleUnitStatUI _hud;
	BattleCharacterBehaviour _unit;

	public void Init(BattleCharacterBehaviour unit, BattleUnitStatUI hud)
	{
		_hud = hud;
		if (_hud == null)
			return;
		_hud.Init();

		_unit = unit;
		if(_unit != null)
		{     
			_hud.Init();
			_hud.Set(_unit.Statistics.Level, _unit.Statistics.MaxHP, _unit.Statistics.CurrHP, _unit.Statistics.Shield);
		}

		BattleLogic.Instance.OnBuffRefreshed += OnBuffChanged;
	}

	public void AddText(string text)
	{
		_hud.FloatCustomText(text);
	}

	public void Update()
	{
		if (_unit == null || _hud == null) return;

		_hud.UpdateBars(_unit.Statistics.CurrHP, _unit.Statistics.Shield, _unit.Statistics.Readiness);
	}

	public void Toggle(bool t)
	{
		if(_hud != null)
			_hud.gameObject.SetActive(t);
	}

	void OnBuffChanged()
	{
		_hud.HideBuffs();

		int cnt = 0;
		foreach (var p in _unit.Buffs)
		{
			int duration = p.Value;
			Sprite icon = AssetPackManager.GetBuffFX(p.Key).Icon;
			_hud.UpdateBuff(cnt, duration, icon);         
			cnt++;
		}
	}
}
