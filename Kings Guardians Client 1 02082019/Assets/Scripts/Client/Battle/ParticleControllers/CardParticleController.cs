﻿using UnityEngine;
using System.Collections;

public class CardParticleController : MonoBehaviour 
{
	public enum Spawn { Caster, Front_of_the_caster, Target }
	public Spawn SpawnOn = Spawn.Caster;

	public enum SpellControl { Normal, Look_at_target, Move_to_target }
	public SpellControl ControlMode = SpellControl.Normal;
	public float Speed = 1f;

	public enum FinishEvent { Normal, OnAwake, Particle_Collision, Normal_Collision, AfterTime, ParticleFinishes }
	public FinishEvent finishEvent = FinishEvent.Normal;
	public float Seconds = 0f;

	public enum Impact { Normal, Only_Sound, Only_Particle, Particle_and_Sound}
	public Impact ImpactEffect;
	public AudioClip ImpactSound;
	public GameObject ImpactParticle;

	public bool ShakeCameraOnImpact = false;
	public float ShakeTime = 0.5f;
	public float TimeToDestroyAfterFinish = 0.25f;

	IParticleMessageReceiver _caster;
	BattleCharacterBehaviour _target;

	AudioSource _audioSource;
	ParticleSystem _particleSystem;

	float _started = 0f;
	bool _applied = false;
	bool _finished = false;

	void Awake()
	{
		_audioSource = GetComponent<AudioSource> ();
		if(_audioSource == null)
			_audioSource = GetComponentInChildren<AudioSource> ();
		
		_particleSystem = GetComponent<ParticleSystem> ();
		if(_particleSystem == null)
			_particleSystem = GetComponentInChildren<ParticleSystem> ();

		if( finishEvent == FinishEvent.Normal_Collision && GetComponent<Collider>() == null)
			Debug.LogError( name + " effect doesn't have collider attached to it, but it's finish event set to Normal Collision!");
	}

	/// <summary>
	/// Set the specified target.
	/// </summary>
	/// <param name="target">Target.</param>
	public void Set(BattleCharacterBehaviour target)
	{
		Set (null, target);
	}
	

	/// <summary>
	/// Set the specified caster and target.
	/// </summary>
	/// <param name="caster">Caster.</param>
	/// <param name="target">Target.</param>
	public void Set(IParticleMessageReceiver caster, BattleCharacterBehaviour target)
	{
		_caster = caster;
		_target = target;
		
		switch (SpawnOn)
		{
		case Spawn.Target:
			transform.position = target.transform.position;
			break;
		case Spawn.Caster:  transform.position = caster.transform.position; break;
		case Spawn.Front_of_the_caster:
			transform.position = caster.transform.position + caster.transform.forward + transform.up;
			break;
		}

		if( ControlMode == SpellControl.Look_at_target )
			transform.LookAt (_target.transform.position + Vector3.up); 

		if (finishEvent == FinishEvent.OnAwake)
			ApplyEffect ();
		
		_started = Time.time;
	}


	/// <summary>
	/// Applys the effect of the spell.
	/// </summary>
	void ApplyEffect()
	{
        Debug.Log("Apply Effect");

		if (_applied || _caster == null) 
			return;
		
		_caster.ParticleCallback ();
		_applied = true;
	}

	void Update()
	{
		if( (finishEvent == FinishEvent.ParticleFinishes || finishEvent == FinishEvent.OnAwake) && !_particleSystem.IsAlive() && !_finished)
			FinishEffect ();	
		
		if ( finishEvent == FinishEvent.AfterTime && _started > 0f && _started + Seconds <= Time.time && !_finished)
			FinishEffect ();
		
		if( ControlMode == SpellControl.Move_to_target && !_finished) 
		{
			if(_target == null) return;

			float step = Speed * Time.deltaTime;
			Vector3 dest = _target.transform.position + Vector3.up;
			transform.position = Vector3.MoveTowards(transform.position, dest, step);

			if( Vector3.Distance( transform.position, dest) < 0.2f)
				FinishEffect();
		}
	}

	void FinishEffect()
	{
        if (_finished) 
			return;
		
        Debug.Log("Finish Effect");

	/*	if(ShakeCameraOnImpact)
			CameraShake.Instance.StartShake(ShakeTime) ;
*/
		if( ImpactEffect == Impact.Only_Sound || ImpactEffect == Impact.Particle_and_Sound)
		{
			_audioSource.Stop();
			_audioSource.PlayOneShot (ImpactSound);
		}

		if( ImpactEffect == Impact.Only_Particle || ImpactEffect == Impact.Particle_and_Sound)
		{
			GameObject go = (GameObject)Instantiate (ImpactParticle, _target.transform.position + Vector3.up, Quaternion.identity) as GameObject; 	
			Destroy( go, 1f );
		}

		if(finishEvent != FinishEvent.OnAwake)
			ApplyEffect ();
		
		Destroy(gameObject, TimeToDestroyAfterFinish);
		_finished = true;
	}

	void OnParticleCollision(GameObject other)
	{
		if (finishEvent != FinishEvent.Particle_Collision)
			return;

		FinishEffect ();		
	}


	void OnCollisionEnter(Collision collision) 
	{
		if (finishEvent != FinishEvent.Normal_Collision)
			return;
		
		FinishEffect ();		
	}

}
