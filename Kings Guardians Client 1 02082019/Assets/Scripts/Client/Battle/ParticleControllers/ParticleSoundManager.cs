﻿using UnityEngine;
using System.Collections;

public class ParticleSoundManager : MonoBehaviour
{
	public AudioClip OnBirthSound;
	public AudioClip OnDeathSound;
	public float OnePerSeconds = 1f;

	AudioSource _audio;
	ParticleSystem _particle;
		
	int _numberOfParticles = 0;
	float _lastSound = 0f;

	void Awake()
	{
		_audio = GetComponent<AudioSource> ();
		_particle = GetComponent<ParticleSystem> ();		 
	}


	void Update()
	{ 
		if (_lastSound + OnePerSeconds >= Time.time)
			return;

		var count = _particle.particleCount;

		//particle has died
		if (OnDeathSound != null && count < _numberOfParticles)
			_audio.PlayOneShot(OnDeathSound); 

		//particle has been born
		else if ( OnBirthSound != null && count > _numberOfParticles)
			_audio.PlayOneShot(OnBirthSound); 

		_numberOfParticles = count; 
		_lastSound = Time.time;
	}
}
