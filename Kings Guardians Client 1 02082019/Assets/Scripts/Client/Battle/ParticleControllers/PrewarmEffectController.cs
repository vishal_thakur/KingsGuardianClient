﻿using UnityEngine;
using System.Collections;

public class PrewarmEffectController : MonoBehaviour 
{
	public ParticleSystem MainParticle;

	public void Kill()
	{
		MainParticle.Stop ();
		MainParticle.Clear (false);

		Destroy (gameObject, 1);
	} 
}
