﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CardParticleController))]
public class CardParticleControllerEditor : Editor 
{

	public override void OnInspectorGUI ()
	{
		CardParticleController myTarget = (CardParticleController)target;

		EditorGUI.BeginChangeCheck ();

		myTarget.SpawnOn = (CardParticleController.Spawn)EditorGUILayout.EnumPopup ("Spawn On", myTarget.SpawnOn);

		myTarget.ControlMode = (CardParticleController.SpellControl)EditorGUILayout.EnumPopup ("Control Mode", myTarget.ControlMode);
		if (myTarget.ControlMode == CardParticleController.SpellControl.Move_to_target)
			myTarget.Speed = EditorGUILayout.FloatField ("Move Speed", myTarget.Speed);

		myTarget.finishEvent = (CardParticleController.FinishEvent)EditorGUILayout.EnumPopup ("Finish event", myTarget.finishEvent);
		if (myTarget.finishEvent == CardParticleController.FinishEvent.AfterTime)
			myTarget.Seconds = EditorGUILayout.FloatField ("Seconds until finish", myTarget.Seconds);

		myTarget.ImpactEffect = (CardParticleController.Impact)EditorGUILayout.EnumPopup ("Impact effect", myTarget.ImpactEffect);

		if (myTarget.ImpactEffect == CardParticleController.Impact.Only_Sound || myTarget.ImpactEffect == CardParticleController.Impact.Particle_and_Sound )
			myTarget.ImpactSound = (AudioClip)EditorGUILayout.ObjectField ("Impact Sound", myTarget.ImpactSound, typeof(AudioClip), false);

		if (myTarget.ImpactEffect == CardParticleController.Impact.Only_Particle || myTarget.ImpactEffect == CardParticleController.Impact.Particle_and_Sound )
			myTarget.ImpactParticle = (GameObject)EditorGUILayout.ObjectField ("Impact Particle", myTarget.ImpactParticle, typeof(GameObject), false);

		myTarget.ShakeCameraOnImpact = EditorGUILayout.Toggle ("Shake camera on impact", myTarget.ShakeCameraOnImpact);
		if(myTarget.ShakeCameraOnImpact)
			myTarget.ShakeTime = EditorGUILayout.FloatField ("Shake Time", myTarget.ShakeTime);

		myTarget.TimeToDestroyAfterFinish = EditorGUILayout.FloatField ("Time to destroy after finish", myTarget.TimeToDestroyAfterFinish);

		if (EditorGUI.EndChangeCheck ())
			EditorUtility.SetDirty (myTarget);
	}
}
