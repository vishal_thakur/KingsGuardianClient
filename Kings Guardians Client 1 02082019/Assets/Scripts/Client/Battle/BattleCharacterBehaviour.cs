﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(CapsuleCollider))]
public class BattleCharacterBehaviour : CharacterBehaviour
{
	public bool AI { get; set; }
	public bool Selectable;   

    public Dictionary<string, int> Buffs = new Dictionary<string, int>();
    public Dictionary<string, GameObject> BuffFX = new Dictionary<string, GameObject>();

    public delegate void DelegatedMethod(BattleCharacterBehaviour caster);
    public DelegatedMethod OnFinished;
    public DelegatedMethod OnCommandApply;
       
    Vector3 _spellLaunchPosition;
   
    BattleCharacterBehaviour _target;
    CombatCardContent _card;
    CardFX _cardFx;

    PrewarmEffectController _preCastEffect;
    GameObject _cardEffect;

	Vector3 _originalPosition;
	Vector3 _originalForward;

	BattleUnitStat _battleUnitStats;


	protected override void OnInit()
	{
		base.OnInit();

		// Set the spell launch position
		_spellLaunchPosition = transform.position + transform.forward + transform.up;
		Selectable = false;

		_originalPosition = transform.position;
		_originalForward = transform.forward;

		_battleUnitStats = new BattleUnitStat();
		var hud = GetComponentInChildren<BattleUnitStatUI>();
		_battleUnitStats.Init(this, hud);

		var h = GetComponentInChildren<RaidStatusHUD>();
		if(h != null)
			h.gameObject.SetActive(false);
	}

    public void AdjustHP(int currHP)
    {
        if (Statistics.CurrHP > currHP)
            controller.OnHit();
        
        Statistics.CurrHP = currHP;
        controller.SetDead(Statistics.CurrHP == 0);
    }

    public void RefreshStats(List<SCharacterStat> stats)
    {
        foreach (var s in stats)
            Statistics.Stats[s.Stat] = s.Value;

		_battleUnitStats.Update();
    }



    public void UpdateBuff(string id, int rounds)
    {       
        if (Buffs.ContainsKey(id))
        {
            if (rounds == -1)
            {
                Buffs.Remove(id);
                if (BuffFX.ContainsKey(id))
                {
                    Destroy(BuffFX[id]);
                    BuffFX.Remove(id);
                }                    
            }
            else
                Buffs[id] = rounds;
        }
        else
        {
            Buffs.Add(id, rounds);
            var fx = AssetPackManager.GetBuffFX(id).Prefab;
            if( fx != null)
            {
                var go = (GameObject)Instantiate(fx, transform.position, transform.rotation) as GameObject;
                go.transform.SetParent(transform);
                BuffFX.Add(id, go);
            }
        }
    }

    public void Play( BattleCharacterBehaviour target, bool rage)
    {
        _target = target;
		if (controller.BasicAttackSequence.MoveCloserFirst)
			controller.Move(target.transform.position);
		else
			controller.BasicAttack();
    }      

    public void Play(BattleCharacterBehaviour target, CombatCardContent card)
    {
        _target = target;
        _card = card;
        _cardFx = AssetPackManager.GetCardFX(_card.ContentID);

		if (card.Action == ActionID.BasicAttack && controller.BasicAttackSequence.MoveCloserFirst)
			controller.Move(target.transform.position, true);
		
		else if (card.Action == ActionID.Offensive)
			controller.CastOffensive();
		
		else if (card.Action == ActionID.Buff)
			controller.CastBuff();
    }


	protected override void OnActionStarted(ActionID action)
	{    
		if (_cardFx != null)
		{
			if (_cardFx.PrewarmFX == null)
			{
				if(_card != null)
					Debug.Log(_card.ContentID + " has no prewarm FX");
				return;
			}
			GameObject go = (GameObject)Instantiate (_cardFx.PrewarmFX, transform.position + Vector3.up *0.1f, Quaternion.identity) as GameObject;    
			_preCastEffect = go.GetComponent<PrewarmEffectController>();			
		} 
    }

	protected override void OnAction(ActionID action)
	{
		// Apply particle effect
		if (_cardFx != null && _cardFx.FX != null )
		{
			_cardEffect = (GameObject)Instantiate (_cardFx.FX, _spellLaunchPosition, Quaternion.identity) as GameObject;  
			var c = _cardEffect.GetComponent<CardParticleController> ();
			c.Set (this, _target);
		}
		else
		{
			if(_card != null)
				Debug.LogWarning( "There is no particle effect set for " + _card.ContentID );
			ParticleCallback();
		}

		// If there is a precast effect, kill it.
		if (_preCastEffect != null) _preCastEffect.Kill ();
    }

	protected override void OnActionFinished(ActionID action)
	{
		switch (action)
		{
			case ActionID.BasicAttack:
				if (controller.BasicAttackSequence.MoveCloserFirst)
					controller.Move(_originalPosition, true);
				else
				{
					controller.Stop();
					transform.forward = _originalForward;
					TurnFinished();
				}
				break;
			case ActionID.Buff:
			case ActionID.Offensive:
				controller.Stop();
				break;
		}

		_target = null;
		_card = null;
		_cardFx = null;
	}

    protected override void OnDestinationReached (ActionID action)
    {
        switch (action) 
        {
			case ActionID.MoveForward:
				controller.BasicAttack();
                break;

			case ActionID.MoveBackward:
				controller.Stop();
				transform.forward = _originalForward;
                TurnFinished();
                break;
        }
    }	 

    void TurnFinished()
    {
        if (OnFinished != null)
            OnFinished(this); 
    }

    public override void ParticleCallback()
    {
        if(OnCommandApply != null)
            OnCommandApply(this);

		if(controller.CurrentAction != ActionID.BasicAttack || !controller.BasicAttackSequence.MoveCloserFirst)
            TurnFinished();
    } 

	public void AddTextToFloat(string text)
	{
		_battleUnitStats.AddText(text);
	}
}


