﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script handles the character spawning in the Battle scene.
/// </summary>
public class CharacterSpawner
{
	/// <summary>
	/// Start to spawn the characters on the given spawnpoints.
	/// As parameter only need the array of spawn points and an array of enemies.
	/// </summary>
	/// <param name="spawnPoints">Spawn points.</param>
	/// <param name="enemies">Enemies.</param>
    public static void Start( SpawnPoint[] spawnPoints, List<SquadInfo> enemies )
	{
        // Load UnitStat panel to instantiate.
        foreach (SpawnPoint point in spawnPoints)
        {			
			CharacterReference c = new CharacterReference();
            if (point.Player)
            {
                if (point.Number < GameManager.Instance.LocalPlayer.Squad.Length && point.Number >= 0 && GameManager.Instance.LocalPlayer.Squad[point.Number] != null)
                    c = GameManager.Instance.LocalPlayer.Squad[point.Number];
            }
            else
            {
                if (point.Number < enemies.Count && point.Number >= 0 && enemies[point.Number] != null)
                    c.ReadSquad(enemies[point.Number]); 
            }

            if (c == null || c.ContentID == null || c.ContentID == "") continue;

            var cb = SpawnCharacter<BattleCharacterBehaviour>(point.transform.position, point.transform.rotation, c);
            cb.AI = !point.Player;
			cb.Init();
            MonoBehaviour.Destroy( point.gameObject );
            BattleLogic.Instance.Characters.Add(c.InstanceID, cb); 
        } 
	}

	public static T SpawnCharacter<T>(Vector3 pos, Quaternion rot, CharacterReference c) where T : CharacterBehaviour
    {    
		// TODO : spawn summon, pit, and characters
		var asset = AssetPackManager.GetCharacterAssets(c.ContentID);
		GameObject go = (GameObject)MonoBehaviour.Instantiate(asset.Model, pos, rot) as GameObject;
		go.layer = 8; // Character Layer.        

        T cb = go.AddComponent<T>();
        cb.Statistics = c;
        cb.name = cb.Statistics.ContentData.Name;
        cb.Init();   

        return cb;
    }
}
