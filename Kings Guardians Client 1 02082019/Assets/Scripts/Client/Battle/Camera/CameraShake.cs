﻿using UnityEngine;
using System.Collections;

public class CameraShake : SingletonMono<CameraShake> 
{
	public AnimationCurve curveX;
	public AnimationCurve curveY;
	public AnimationCurve curveZ;

	public float Damper = 10f;
	public bool SetupOnAwake = false;

	bool _shaking = false;
	float _time = 2f;
	Vector3 _originalPosition;

	float _currTime = 0f;

	void Awake()
	{
		if (SetupOnAwake)
			Setup ();
	}

	public	void Setup()
	{
		_originalPosition = transform.position;
	}

	public void StartShake(float time)
	{
		_time = time;
		_shaking = true;
	}

	void Update () 
	{
		if (_shaking) {
			transform.position = _originalPosition + Shake ();

			if (_currTime >= _time) {
				_currTime = 0f;
				_shaking = false;
			}
		} else if (Vector3.Distance (transform.position, _originalPosition) > 0.1f)
			transform.position = Vector3.Lerp(transform.position, _originalPosition, Time.deltaTime);

	}
	
	Vector3 Shake ()
	{
		_currTime += Time.deltaTime;
		Vector3 pos = Vector3.zero;

		pos.x = Mathf.PerlinNoise (_currTime / Damper, 0f) * curveX.Evaluate (_currTime);
		pos.y = Mathf.PerlinNoise (_currTime / Damper, 0f) * curveY.Evaluate (_currTime);
		pos.z = Mathf.PerlinNoise (_currTime / Damper, 0f) * curveZ.Evaluate (_currTime);

		return pos;
	}

}

