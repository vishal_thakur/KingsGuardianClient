﻿using UnityEngine;
using System.Collections;

public class BattleCamera : MonoBehaviour 
{
	public Transform CameraPlayPosition;

	Animator _animator;
	CameraShake _shake;

	void Start()
	{
		_animator = GetComponent<Animator> ();
		_shake = GetComponent<CameraShake> ();
	}

	public void SetCameraPosition()
	{
		_animator.enabled = false;
		transform.position = CameraPlayPosition.position;
		transform.rotation = CameraPlayPosition.rotation;
		_shake.Setup ();
	}
}
