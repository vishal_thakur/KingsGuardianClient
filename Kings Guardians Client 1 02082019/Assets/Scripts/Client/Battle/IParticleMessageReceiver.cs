﻿
using UnityEngine;

public interface IParticleMessageReceiver 
{
	Transform transform { get; }
	void ParticleCallback();
}
