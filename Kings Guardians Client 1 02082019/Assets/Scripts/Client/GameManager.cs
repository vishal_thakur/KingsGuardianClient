﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using GridSystem;

public class GameManager : SingletonMono<GameManager>
{
    public ContentManager GameContent { get; private set; }
    public Player LocalPlayer { get; set; }
//	[SerializeField] GameObject PlayerBannedMsgPanel;
    public Player TargetPlayer { get; private set; }
    public PlayerInfo SelectedOpponent { get; private set; }
    public List<PlayerInfo> Opponents { get; set; }
//	public bool IsPlayerBanned = false;

    public float OpponentsUpdated = -1f;
    public BattleMessage BattleStartMsg;

	public Vector2 TileSize;

	void Start(){
		//No Screen Sleeping when game is Active
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //		Caching.CleanCache ();
        //		RefreshOpponents();
       // PlayerPrefs.DeleteAll();
	}

    public void GetGameContent(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<GameContentMessage>();   

        if (GameContent == null)
            GameContent = new ContentManager();

        if (msg.Characters != null)
            GameContent.Load(msg.Characters);
        if (msg.Buildings != null)
            GameContent.Load(msg.Buildings);
        if (msg.Items != null)
            GameContent.Load(msg.Items);
        if (msg.Weapons != null)
            GameContent.Load(msg.Weapons);

		TileSize = msg.TileSize;

		//Refresh Opponents
		RefreshOpponents();

    }
  
    public void SetPlayer(PlayerMessage msg)
    {		
        LocalPlayer = new Player();
        LocalPlayer.Read(msg);

		//Show Ban Message
//		if (LocalPlayer.IsBanned) {
//			PlayerBannedMsgPanel.SetActive (true);
//		}

        //		else {
			Debug.LogWarning ("Load Scene" + Time.time);
			LevelLoader.Instance.LoadScene ("Village");
//		}
    }

    public void SetTargetPlayer(PlayerInfo player, TownLogic.TScoutMode mode = TownLogic.TScoutMode.Native, DelegatedMethod delegated = null)
    {
		_onTargetPlayerSet = delegated;

		if (player == null)
		{
			SelectedOpponent = null;
			TargetPlayer = null;
		}
		else
		{
			SelectedOpponent = player;
			TargetPlayer = new Player();

			var msg = new SimpleMessage();
			msg.Add(ParameterCode.Username, player.Username);
			NetworkManager.SendRequest(RequestCode.GetOtherPlayer, msg);
		}
    }   

	public void OnTargetPlayerSet(PlayerMessage msg)
	{
		TargetPlayer.Read(msg);   
		if (_onTargetPlayerSet != null)
		{
			_onTargetPlayerSet();
			_onTargetPlayerSet = null;
		}
	}

    public void RefreshOpponents()
    {
        NetworkManager.SendRequest(RequestCode.GetOpponents, new UnityEngine.Networking.NetworkSystem.EmptyMessage());
    }  


	public delegate void DelegatedMethod();
	DelegatedMethod _onTargetPlayerSet;



}
