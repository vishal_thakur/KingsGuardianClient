﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ContentManager 
{
    public Dictionary<string, CharacterContent> Characters = new Dictionary<string, CharacterContent>();
    public Dictionary<string, BuildingContent> Buildings = new Dictionary<string, BuildingContent>();
    public Dictionary<string, ItemContent> Items = new Dictionary<string, ItemContent>();

    public void Load(List<CharacterContent> characters)
    {
        foreach (var c in characters)
        {
            if(!Characters.ContainsKey(c.ContentID))
                Characters.Add(c.ContentID, c);
        }
    }

    public void Load(List<BuildingContent> buildings)
    {
        foreach (var b in buildings)
        {
            if(!Buildings.ContainsKey(b.ContentID))
                Buildings.Add(b.ContentID, b);
        }
    }

    public void Load( List<ItemContent> items)
    {
        foreach (var i in items)
        {
            if (!Items.ContainsKey(i.ContentID))
                Items.Add(i.ContentID, i);
        }
    }
		 
    public CharacterContent GetCharacter(string contentID)
    {
        if(Characters != null && Characters.ContainsKey(contentID))
            return Characters[contentID];
        return null;
    }

    public BuildingContent GetBuilding(string contentID)
    {
        if(Buildings != null && Buildings.ContainsKey(contentID))
            return Buildings[contentID];
        return null;
    }

    public ItemContent GetItem(string contentID)
    {
        if(Items != null && Items.ContainsKey(contentID))
            return Items[contentID];
        return null;
    }

    public T Get<T>(string contentID) where T : GameItemContent
    {
        if (CheckInheritance(typeof(T), typeof(BuildingContent)))
            return GetBuilding(contentID) as T;
        else if (CheckInheritance(typeof(T), typeof(CharacterContent)))
            return GetCharacter(contentID) as T;
        else if (CheckInheritance(typeof(T), typeof(ItemContent)))
            return GetItem(contentID) as T;
        else
        {
            if(Characters != null && Characters.ContainsKey(contentID))
                return GetCharacter(contentID) as T;
            if(Buildings != null && Buildings.ContainsKey(contentID))
                return GetBuilding(contentID) as T;
            if(Items != null && Items.ContainsKey(contentID))
                return GetItem(contentID) as T;
            return null;
        }       
    }

    bool CheckInheritance(System.Type type, System.Type from)
    {
        return type.IsSubclassOf(from) || type == from;
    }


  

}
