﻿using UnityEngine;
using System.Collections;

public class TargetTest : MonoBehaviour {

    public bool Move;
    public float Speed = 10;

    float _lastChange = -1;
    Vector3 _destination;

	// Update is called once per frame
	void Update () 
    {
	    if (Move)
        {
            if (_lastChange + 5f <= Time.time)
            {
                int x = Random.Range(-1, 2);
                int z = Random.Range(-1, 2);
                _destination = transform.position + new Vector3(x, 0, z) * 100;
                _destination.y = transform.position.y;
                _lastChange = Time.time;
            }         

            transform.position = Vector3.MoveTowards(transform.position, _destination, Speed * Time.deltaTime);
                    
        }
	}
}
