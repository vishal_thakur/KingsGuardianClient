﻿using UnityEngine;
using System.Collections;
using GridSystem;

public class SiegeWeaponTest : MonoBehaviour 
{
    public bool AttackNow;

    Transform Target;
    BuildingVisual buildingVisual;
    SiegeBuildingHelper _siegeHelper;

    Animator _animator;

    void Awake()
    {
        Target = GameObject.Find("TARGET").transform;
        buildingVisual = GetComponent<BuildingVisual>();
        buildingVisual.ToggleWorkVisual(false);

        _siegeHelper = buildingVisual.GetComponentInChildren<SiegeBuildingHelper>();
        _siegeHelper.onAnimatorCallback = AnimationCallback;
        _siegeHelper.onProjectileCallback = OnEffectCallback;

       
    }

    void Update()
    {
        if (AttackNow)
        {
            Attack();           
        }
    }







    void Attack()
    {
        AttackNow = false;

        var lookat = Target.position;
        lookat.y = transform.position.y;
        transform.LookAt(lookat);

        if ( !_siegeHelper.Attack() )
            AnimationCallback();
    }

    void AnimationCallback()
    {
        Debug.Log("on animator callback");
        bool particle = _siegeHelper.CreateParticle();
        bool projectile = _siegeHelper.LaunchProjectile(Target.position);

        if (!projectile) 
            OnEffectCallback();
    }

  

    void OnEffectCallback()
    {
        // update characters
        Debug.Log("Damage happened");  
    }
}
