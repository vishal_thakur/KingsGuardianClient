﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestGUI : MonoBehaviour {

    public bool PlayerInfo = false;


    public Text text;
    bool _loaded = false;


    void Update()
    {
        if (_loaded)
            return;

        if (PlayerInfo)
        {
            var content = GameManager.Instance.LocalPlayer;
            if (content != null)
            {
                text.text += "<b>Characters</b>\n";
                foreach (var c in content.Characters)
                {
                    text.text += "-" + c.ContentID + "\n";
                    foreach (var ci in c.Inventory)
                        text.text += " --- " +ci.ContentID + "("+ ci.Stacks + "), ";
                    text.text += "\n";
                }

                text.text += "<b>Buildings</b>\n";
                foreach (var b in content.Buildings.Items)
                      text.text += "-" + b.ContentID + " " + b.Position + "\n";
                

                text.text += "<b>Items</b>\n";
                foreach (var i in content.Inventory.Items)
                    text.text += "-"+i.ContentID+" "+ ((ItemContent)i.ContentData).Category +"\n";

                _loaded = true;
            }
        }
        else
        {
            var content = GameManager.Instance.GameContent;
            if (content != null)
            {
                text.text += "<b>Characters</b>\n";
                foreach (var c in content.Characters)
                    text.text += "-"+c.Value.Name+"\n";

               text.text += "<b>Buildings</b>\n";
                foreach (var b in content.Buildings)
                    text.text += "-"+b.Value.Name+" ["+b.Value.Category+"] ["+b.Value.ItemCategory+"]\n";
                
                text.text += "<b>Items</b>\n";
                foreach (var i in content.Items)
                    text.text += "-"+i.Value.Name+ " "+ i.Value.Category +"\n";

                _loaded = true;
            }
        }
    }
	
}
