﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public StressTest StressTest;
    public Image BG;
    public Text Label;
    public bool Enabled;

    public void Set(string s, bool b)
    {
        Label.text = s;
        Enabled = b;
        BG.color = (b) ? Color.white : Color.red;
    }

    public void ToggleEnable()
    {
        SetEnable(!Enabled);
    }

    public void SetEnable(bool b)
    {
        Enabled = b;
        BG.color = (b) ? Color.white : Color.red;
        StressTest.Toggle(Label.text);
    }
	
}
