﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GridSystem;
using System;
using System.Collections.Generic;
using System.Linq;

public class StressTest : MonoBehaviour
{
    public enum Item : int { Buildings = 1, Characters = 2 }

    public Item Spawning = Item.Characters;
    public Text Report;
    public InputField Numbers;
    public List<Slot> Slots = new List<Slot>();
    public Text PageLabel;

    int _pages {
        get { 
            int count = 0;
            if (Spawning == Item.Buildings)
                count = _buildings.Count;
            else if (Spawning == Item.Characters)
                count = _characters.Count;            
            return Mathf.CeilToInt( count / Slots.Count) + 1;
        } }
    
    int _page;

    int _cnt = 0;
    int _spawnedBuildings = 0;
    int _spawnedCharacters = 0;

    Dictionary<string, bool> _buildings = new Dictionary<string, bool>();
    Dictionary<string, bool> _characters = new Dictionary<string, bool>(); 

    List<GameObject> _models = new List<GameObject>();

    List<string> _list = new List<string>();

    void Awake()
    {
        foreach (var b in AssetPackManager.Instance.GetBuildingIDs())
            _buildings.Add(b, true);

        foreach (var c in AssetPackManager.Instance.GetCharacterIDs())
            _characters.Add(c, true);

        ShowCharacters();
    }

    public void ShowBuildings()
    {
        Spawning = Item.Buildings;
        SetPage(1);
    }

    public void ShowCharacters()
    {
        Spawning = Item.Characters;
        SetPage(1);
    }

    public void Toggle(string name)
    {
        if (Spawning == Item.Buildings)
            _buildings[name] = !_buildings[name];
        if (Spawning == Item.Characters)
            _characters[name] = !_characters[name];
    }


    public void Spawn()
    {        
        _list.Clear();
        var dict = (Spawning == Item.Buildings) ? _buildings : _characters;
        foreach (var p in dict)
        {
            if (p.Value)
                _list.Add(p.Key);
        }

        _cnt = int.Parse(Numbers.text);
    }
 
    public void Clear()
    {
        foreach (var go in _models)
            Destroy(go);
        _models.Clear();

        _cnt = 0;
        _spawnedBuildings = 0;
        _spawnedCharacters = 0;
    }

    public void SetPage(int p)
    {
        p = Mathf.Clamp(p, 1, _pages);
        _page = p;

        int minIndex = (p-1) * Slots.Count;

        for (int i = 0; i < Slots.Count; i++)
        {
            int index = minIndex + i;

            if (_characters.Count > index)
            {
                var pair = (Spawning == Item.Characters) ? _characters.ElementAtOrDefault(index) : _buildings.ElementAtOrDefault(index);
                Slots[i].Set(pair.Key, pair.Value);   
            }
            else
                Slots[i].Set("", true);  
        }

        if (PageLabel != null)
            PageLabel.text = _page + "/" + _pages;
    }

    public void NextPage() { SetPage(_page + 1); }
    public void PrevPage() { SetPage(_page - 1); }

    void Update()
    {         
        if( Spawning == Item.Buildings && _spawnedBuildings < _cnt)
            SpawnBuilding(new Point(UnityEngine.Random.Range(-50, 50), UnityEngine.Random.Range(-50, 50)));
        else if ( Spawning == Item.Characters && _spawnedCharacters < _cnt)
            SpawnCharacter(new Point(UnityEngine.Random.Range(-50, 50), UnityEngine.Random.Range(-50, 50)));

        CalcFPS();

        string report = "<b>Kings Guardians - Stress Test</b>\n" +
                         " - Spawned Buildings: " + _spawnedBuildings + "\n" +
                         " - Spawned Characters: " + _spawnedCharacters + "\n" +
                         "\n" + _fps;
        
        Report.text = report;
    }

    void SpawnBuilding(Point p)
    {   
       /* var list = AssetPackManager.GetBuildingAssets(_list[UnityEngine.Random.Range(0, _list.Count)]).Model;
        _models.Add((GameObject)Instantiate( list[UnityEngine.Random.Range(0,list.Count-1)], p.vector3, Quaternion.identity) as GameObject);            
        _spawnedBuildings++;*/       
    }

    void SpawnCharacter(Point p)
    {   
        var m = AssetPackManager.GetCharacterAssets(_list[UnityEngine.Random.Range(0, _list.Count)]).Model;
        _models.Add((GameObject)Instantiate( m, p.vector3, Quaternion.identity) as GameObject);            
        _spawnedCharacters++;       
    }


    float _deltaTime = 0.0f;
    public  float updateInterval = 0.5F;
    private float accum   = 0; // FPS accumulated over the interval
    private int   frames  = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval

    string _fps;

    void CalcFPS()
    {
        _deltaTime += (Time.deltaTime - _deltaTime) * 0.1f;

        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if (timeleft <= 0.0)
        {
            // display two fractional digits (f2 format)
            float fps = accum / frames;
            float msec = _deltaTime * 1000.0f;

            string color = "#ffffff";
            if (fps < 30)
                color = "#ffff00";
            else if (fps < 10)
                color = "#ff0000";
            else
                color = "#00ff00";

            _fps = string.Format("FPS: <color=" + color + ">{0:0.}</color> ({1:0.0} ms)", fps, msec);  

            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
        }
    }
}
