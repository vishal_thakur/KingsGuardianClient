﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPoint : MonoBehaviour {

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, .5f);
        GUIStyle style = new GUIStyle();

        style.fontSize = 25;
        UnityEditor.Handles.Label(transform.position, ".", style);
    }
#endif

}
