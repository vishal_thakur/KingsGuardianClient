﻿public enum Currency 
{
    Wood,
    Stone,
    Iron, 
    BloodCrystal,
    Gold,
    Diamond,
    Ether,   
}

public enum WorkerType { Worker, Warlock }