﻿using UnityEngine;
using System.Collections;

namespace GridSystem
{
	//Showing the green Grid Lines in building Move mode
    [AddComponentMenu("Kings Guardians/GridSystem/CameraOverlay")]
    public class GridOverlay : SingletonMono<GridOverlay>
    {
        public bool ShowOverlay = false;

        public Material DarkGreenMaterial;

        /// <summary>
        /// The grid overlay line material.
        /// </summary>
        public Material GreenMaterial;

        /// <summary>
        /// The invalid footprint material.
        /// </summary>
        public Material RedMaterial;

        /// <summary>
        /// Raises the post render event.
        /// </summary>
        public void OnPostRender()
        {      
            if (!ShowOverlay) return;

			//************** GridCodeNew ***********************
			Vector3 pos = GridHandlerPlayer.Instance.Origo + GridHandlerPlayer.Instance.Offset;

			float horizontalLength = GridHandlerPlayer.Instance.HorizontalTiles * GridHandlerPlayer.Instance.TileSize.x;
			float verticalLength = GridHandlerPlayer.Instance.VerticalTiles * GridHandlerPlayer.Instance.TileSize.y;

            Material lineMaterial = GreenMaterial;
            lineMaterial.SetPass (0);

            GL.Begin( GL.LINES );   

			// Vertical lines
			for (int x = 0; x < GridHandlerPlayer.Instance.HorizontalTiles; x++)
            {        
				float xPos = pos.x + x * GridHandlerPlayer.Instance.TileSize.x;
                GL.Vertex3 (xPos, 0.2f, pos.z);
                GL.Vertex3 (xPos, 0.2f, pos.z + verticalLength);
            }

			// Horizontal lines
			for (int y = 0; y < GridHandlerPlayer.Instance.VerticalTiles; y++) 
            {       
				float zPos = pos.z + y * GridHandlerPlayer.Instance.TileSize.y;
                GL.Vertex3 (pos.x, 0.2f, zPos);
                GL.Vertex3 (pos.x + horizontalLength, 0.2f, zPos);
            }
            GL.End ();
        }
    }

}