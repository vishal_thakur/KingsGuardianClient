﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GridSystem
{  
	 /// <summary>
    /// This class defines the size of the buildings and other obstacles on the grid.
    /// The footprint is a rectangle, measured in grid points.
    /// Every object on the grid should have this component.
    /// </summary>
    public class Footprint
    {
		public Point TopLeft { get { return Tile(Position, Size, Corner.TopLeft); }}
		public Point BottomLeft { get { return Tile(Position, Size, Corner.BottomLeft); }}
		public Point TopRight { get { return Tile(Position, Size, Corner.TopRight); }}
		public Point BottomRight { get { return Tile(Position, Size, Corner.BottomRight); }}


		public Point Size { get; set; }
		public Point Position { get; set; }


		#region Static Methods

		public static Point Tile(Point position, Point size, Corner corner)
		{
			if (corner == Corner.Center)
				return position;

			int dxLeft = (size.x % 2 == 0) ? (size.x / 2) - 1 : Mathf.FloorToInt((float)(size.x - 1) / 2f);
			int dyTop = (size.y % 2 == 0) ? (size.y / 2) - 1 : Mathf.FloorToInt((float)(size.y - 1) / 2f);


			// Calculate top-left corner
			Point tile = new Point(position.x - dxLeft, position.y + dyTop);

			switch (corner)
			{
				case Corner.TopRight: tile.x += size.x - 1; break;
				case Corner.BottomLeft: tile.y -= size.y - 1; break;
				case Corner.BottomRight: 
					tile.x += size.x - 1;
					tile.y -= size.y - 1;
					break;	
			}
			return tile;
		}


		public static Vector3 WorldPosition (Point position, Point size, Corner corner)
		{
			bool loadEnemyBase = TownLogic.Instance.LoadEnemyBase;
			Vector3 centerOffset = Vector3.zero;
			if (size.x % 2 == 0) {
				if(!loadEnemyBase)
					centerOffset.x = GridHandlerPlayer.Instance.TileSize.x / 2;
				else
					centerOffset.x = GridHandlerEnemy.Instance.TileSize.x / 2;
			}
			if (size.y % 2 == 0) {
				if(!loadEnemyBase)
					centerOffset.z = GridHandlerPlayer.Instance.TileSize.y / 2;
				else
					centerOffset.z = GridHandlerEnemy.Instance.TileSize.y / 2;
			}
			
			var tile = Tile(position, size, corner);

			if (!loadEnemyBase)
				return GridHandlerPlayer.PointToVector3 (tile, corner);
			else
				return GridHandlerEnemy.PointToVector3(tile, corner) ;
		}


		public static bool ValidFootprint(Point position, Point size)
		{
			if (!TownLogic.Instance.LoadEnemyBase) {
				return GridHandlerPlayer.ValidGridPoint (Tile (position, size, Corner.TopLeft)) &&
				GridHandlerPlayer.ValidGridPoint (Tile (position, size, Corner.BottomRight)) &&
				GridHandlerPlayer.ValidGridPoint (Tile (position, size, Corner.TopRight)) &&
				GridHandlerPlayer.ValidGridPoint (Tile (position, size, Corner.BottomLeft));
			} 
			else {
				return GridHandlerEnemy.ValidGridPoint (Tile (position, size, Corner.TopLeft)) &&
					GridHandlerEnemy.ValidGridPoint (Tile (position, size, Corner.BottomRight)) &&
					GridHandlerEnemy.ValidGridPoint (Tile (position, size, Corner.TopRight)) &&
					GridHandlerEnemy.ValidGridPoint (Tile (position, size, Corner.BottomLeft));
			}
		}

		public static Point ClosestCorner (Point from, Point position, Point size)
		{
			var topLeft = Tile(position, size, Corner.TopLeft);
			var bottomLeft = Tile(position, size, Corner.BottomLeft);
			var topRight = Tile(position, size, Corner.TopRight);
			var bottomRight = Tile(position, size, Corner.BottomRight);
			int dTL = 0; 
			int dBR = 0;
			int dTR = 0;
			int dBL = 0;


			if (!TownLogic.Instance.LoadEnemyBase) { 
				dTL = GridHandlerPlayer.ManhattanDistance (topLeft, from);
				dBR = GridHandlerPlayer.ManhattanDistance (bottomRight, from);
				dTR = GridHandlerPlayer.ManhattanDistance (topRight, from);
				dBL = GridHandlerPlayer.ManhattanDistance (bottomLeft, from);
			}
			else {
				dTL = GridHandlerEnemy.ManhattanDistance (topLeft, from);
				dBR = GridHandlerEnemy.ManhattanDistance (bottomRight, from);
				dTR = GridHandlerEnemy.ManhattanDistance (topRight, from);
				dBL = GridHandlerEnemy.ManhattanDistance (bottomLeft, from);
			}
			if (dTL < dBR && dTL < dTR && dTL < dBL)
				return topLeft;			
			if (dBR < dTR && dBR < dBL)
				return bottomRight;			
			if (dTR < dBL)
				return topRight;			
			return bottomLeft;
		}

		public static List<Point> GetPoints(Point position, Point size)
		{
			Point topLeft = Tile(position, size, Corner.TopLeft);

			List<Point> points = new List<Point>();
			for (int x = topLeft.x; x <= topLeft.x + size.x - 1; x++)
				for (int y = topLeft.y; y <= topLeft.y - size.y + 1; y--)
					points.Add(new Point(x, y));
			return points;
		}


		#endregion     
               
      
        

    }
}