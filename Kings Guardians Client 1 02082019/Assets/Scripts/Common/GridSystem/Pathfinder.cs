﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace GridSystem
{
    /// <summary>
    /// A* Pathfinding
    /// </summary>
    public class Pathfinder
    {
        public bool HasPath { get { return _goal != null && _cameFrom.ContainsKey(_goal); } }
        public int Length { get { return (HasPath) ? _cameFrom.Count : 0; }}

        public int DefaultObstacleCost = 1;
        Dictionary<string, int> _obstacleCosts = new Dictionary<string, int>();



        Point _start;
        Point _goal;

        PriorityQueue _frontier = new PriorityQueue();
        Dictionary<Point, Point> _cameFrom = new Dictionary<Point, Point>();    // Current, From
        Dictionary<Point, int> _costSoFar = new Dictionary<Point, int>();       // Point, total cost

        public List<Point> GetPath()
        {
            List<Point> path = new List<Point>();
            Point current = _goal;
            path.Add(_goal);
            while (current != _start)
            {
                var next = _cameFrom[current];
                path.Insert(0, next);
                current = next;
            }
            return path;
        }

        public void Calculate(Point start, Point goal)
        {
            _start = start;
            _goal = goal;

            _frontier.Add(start, 0);
            _cameFrom.Add(start, null);
            _costSoFar.Add(start, 0);

            while (_frontier.Count > 0)
            {
                Point current = _frontier.Get();

                // Early exit.
                if (current == goal) break;

				//************** GridCodeNew ***********************
                foreach (var next in GridHandlerPlayer.Neighbors(current))
                {
                    int newCost = _costSoFar[current] + CalculateCost(current, next);
                    if (!_costSoFar.ContainsKey(next) || newCost < _costSoFar[next])
                    {
                        _costSoFar[next] = newCost;
                        int priority = newCost + GridHandlerPlayer.ManhattanDistance(goal, next);
                        _frontier.Add(next, priority);
                        _cameFrom[next] = current;
                    }
                }         
            }
        }

        public int CalculateCost(Point a, Point b)
        {
			int cost = 1;
			//************** GridCodeNew ***********************
            string name = GridHandlerPlayer.Instance.GetObstacle(b);
            if (name != "")
            {                
                if (_obstacleCosts.ContainsKey(name))
                    cost = _obstacleCosts[name];
                else
                    cost = DefaultObstacleCost;                    
			}      
			//************** GridCodeNew ***********************
            return GridHandlerPlayer.ManhattanDistance(a, b) * cost; 
        }

        public void Reset()
        {
            _frontier.Clear();
            _cameFrom.Clear();
            _costSoFar.Clear();
        }

        public void SetObstacleCost(string name, int cost)
        {
            if (_obstacleCosts.ContainsKey(name))
                _obstacleCosts[name] = cost;
            else
                _obstacleCosts.Add(name, cost);                
        }
    }

    public class PriorityQueue
    {
        class Element{ public int Priority; public Point Point; }

        List<Element> _queue = new List<Element>();

        public int Count { get { return _queue.Count; } }

        public Point Get()
        {
            Point p = _queue[0].Point;
            _queue.RemoveAt(0);
            Sort();
            return p;
        }

        public void Add(Point p, int priority)
        {
            _queue.Add(new Element(){ Point = p, Priority = priority } );
            Sort();
        }

        public void Clear()
        {
            _queue.Clear();
        }

        void Sort()
        {
            _queue.OrderByDescending(x => x.Priority);
        }


    }   
}