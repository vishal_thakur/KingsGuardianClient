﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using GridSystem;

public class CustomCharacterSpawn : MessageBase, IBigMessage
{
	public CharacterInfo Character;
	public Point Position;

	public int Size(ref string log)
	{
		var b = SerializationBinary.CalculateSize(Character);

		string s = "";
		int size = Utils.Add(out s, b);

		log += s;
		return size; 
	}

	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);   
		writer.Write(Position.ToString());
		writer.WriteBytesFull(SerializationBinary.Serialize(Character));
	}

	public override void Deserialize(NetworkReader reader)
	{		
		base.Deserialize(reader);
		Position = Point.Parse(reader.ReadString());
		Character = SerializationBinary.Deserialize<CharacterInfo>(reader.ReadBytesAndSize());
	}
}