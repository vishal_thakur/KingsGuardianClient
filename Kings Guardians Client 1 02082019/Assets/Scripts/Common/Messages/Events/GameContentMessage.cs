﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using GridSystem;

public class GameContentMessage : MessageBase, IBigMessage
{
    public List<CharacterContent> Characters;
    public List<BuildingContent> Buildings; 
    public List<ItemContent> Items;
    public List<ItemContent> Weapons;
    public List<SBuildingLimit> BuildingLimits;
	public Vector2 TileSize;

    byte[] _c;
    byte[] _b;
    byte[] _i;
    byte[] _w;
    byte[] _l;

    public int Size(ref string log)
    {
        _c = SerializationBinary.Serialize(Characters);
        _b = SerializationBinary.Serialize(Buildings);
        _i = SerializationBinary.Serialize(Items);
        _w = SerializationBinary.Serialize(Weapons);
        _l = SerializationBinary.Serialize(BuildingLimits);

        string s = "";
        int size = Utils.Add(out s, _c.Length, _b.Length, _i.Length, _w.Length, _l.Length);

        log += s;
        return size; 
    }


    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.WriteBytesFull(_c);
        writer.WriteBytesFull(_b);
        writer.WriteBytesFull(_i);
        writer.WriteBytesFull(_w);
        writer.WriteBytesFull(_l);
		writer.Write(TileSize);
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        Characters = SerializationBinary.Deserialize<List<CharacterContent>>(reader.ReadBytesAndSize());
        Buildings = SerializationBinary.Deserialize<List<BuildingContent>>(reader.ReadBytesAndSize());
        Items = SerializationBinary.Deserialize<List<ItemContent>>(reader.ReadBytesAndSize());
        Weapons = SerializationBinary.Deserialize<List<ItemContent>>(reader.ReadBytesAndSize());
        BuildingLimits = SerializationBinary.Deserialize<List<SBuildingLimit>>(reader.ReadBytesAndSize());
		TileSize = reader.ReadVector2();
    }
}

[System.Serializable]
public class SBuildingLimit
{
    public int Upgrade;
    public string ContentID;
    public int Limit;
}