﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class RaidUpdateMessage : MessageBase, IBigMessage
{
	public int AbilityPoints = 0;
	public RaidUnitClientInfo RaidParticipant;

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
		writer.Write(AbilityPoints);
        writer.WriteBytesFull(SerializationBinary.Serialize(RaidParticipant));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);   
		AbilityPoints = reader.ReadInt32();
		RaidParticipant = SerializationBinary.Deserialize<RaidUnitClientInfo>(reader.ReadBytesAndSize());       
    }

    public int Size(ref string log)
    {
        int size = SerializationBinary.CalculateSize(RaidParticipant);
        log += " " + size;
        return size; 
    }

    public bool Check(int maxBytes)
    {
        int m = SerializationBinary.CalculateSize(RaidParticipant);
        Debug.Log("Check message size: " + m + " (max: " + maxBytes + ")");
        if (m > maxBytes)
            return true;
        return false;
    }
}


[System.Serializable]
public class RaidUnitClientInfo
{
	public string InstanceID;
	public RaidAction Action = RaidAction.Idle;

    public int Health;
    public int Shield;
    public string position;
    public string lookatPos;

    public Vector3 Position 
    {
        get { return Vector3Extended.Parse(position);}
        set { position = value.ToString(); }
    }

    public Vector3 LookatPos 
    {
        get { return Vector3Extended.Parse(lookatPos);}
        set { lookatPos = value.ToString(); }
    }

}