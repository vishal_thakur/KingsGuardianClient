﻿using UnityEngine.Networking;
using System.Collections.Generic;
using System;
using UnityEngine;

public class RaidAttackMessage : MessageBase
{
	public string InstanceID;
	public List<string> TargetsInstanceID= new List<string>();

	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(InstanceID);
		writer.WriteBytesFull(SerializationBinary.Serialize(TargetsInstanceID));
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);   

		InstanceID = reader.ReadString();
		TargetsInstanceID = SerializationBinary.Deserialize<List<string> >(reader.ReadBytesAndSize());   
	}
}


//public class RaidDamageTakenMessage : MessageBase
//{
//	public string InstanceID;
//	public AttackResult AttackResult = AttackResult.Hit;
//	public int Damage = 0;
//	public Element DamageType = Element.Untyped;
//	public int CurrentHP;
//	public int CurrentShield = 0;
//
//	public override void Serialize(NetworkWriter writer)
//	{
//		base.Serialize(writer);
//		writer.Write(InstanceID);
//		writer.Write(AttackResult.ToString());
//		writer.Write(Damage);
//		writer.Write(DamageType.ToString());
//		writer.Write(CurrentHP);
//		writer.Write(CurrentShield);
//	}
//
//	public override void Deserialize(NetworkReader reader)
//	{
//		base.Deserialize(reader);   
//
//		InstanceID = reader.ReadString();
//		AttackResult = (AttackResult)Enum.Parse(typeof(AttackResult), reader.ReadString());
//		Damage = reader.ReadInt32();
//		DamageType = (Element)Enum.Parse(typeof(Element), reader.ReadString());
//		CurrentHP = reader.ReadInt32();
//		CurrentShield = reader.ReadInt32();
//	}
//}


public class RaidMovementMessage : MessageBase
{
	public string InstanceID;
	public Vector3 Position;
	public Vector3 LookAt;
	public RaidAction Action = RaidAction.Idle;

	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);
		writer.Write(InstanceID);
		writer.Write(Position);
		writer.Write(LookAt);
		writer.Write(Action.ToString());
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);   

		InstanceID = reader.ReadString();
		Position = reader.ReadVector3();
		LookAt = reader.ReadVector3();
		Action = (RaidAction)Enum.Parse(typeof(RaidAction), reader.ReadString());
	}
}
