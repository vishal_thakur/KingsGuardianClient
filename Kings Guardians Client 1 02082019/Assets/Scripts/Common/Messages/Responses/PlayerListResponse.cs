﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerListResponse : MessageBase, IBigMessage
{
    public List<PlayerInfo> Players = new List<PlayerInfo>();

    public int Size(ref string log)
    {
        var b = SerializationBinary.CalculateSize(Players);

        string s = "";
        int size = Utils.Add(out s, b);

        log += s;
        return size; 
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);       
        writer.WriteBytesFull(SerializationBinary.Serialize(Players));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        Players = SerializationBinary.Deserialize<List<PlayerInfo>>(reader.ReadBytesAndSize());
    }
}
