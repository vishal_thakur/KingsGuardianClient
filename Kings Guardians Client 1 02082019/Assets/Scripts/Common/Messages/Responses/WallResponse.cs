﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class WallResponse : MessageBase, IBigMessage
{
	[System.Serializable]
	public class Wall
	{
		public string TemporaryInstanceID;
		public BuildingInfo Building;
	}
	public List<Wall> Walls = new List<Wall>();
	public List<string> RemoveThese = new List<string>();
    public List<SMaterial> Materials = new List<SMaterial>();


    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.WriteBytesFull(SerializationBinary.Serialize(Walls));
		writer.WriteBytesFull(SerializationBinary.Serialize(RemoveThese));
        writer.WriteBytesFull(SerializationBinary.Serialize(Materials));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);   
		Walls = SerializationBinary.Deserialize<List<Wall>>(reader.ReadBytesAndSize());
		RemoveThese = SerializationBinary.Deserialize<List<string>>(reader.ReadBytesAndSize());
        Materials = SerializationBinary.Deserialize<List<SMaterial>>(reader.ReadBytesAndSize());       
    }


    public int Size(ref string log)
    {
		int b = SerializationBinary.CalculateSize(Walls);
		int m = SerializationBinary.CalculateSize(RemoveThese);
		int w = SerializationBinary.CalculateSize(Materials);

        string s = "";
        int size = Utils.Add(out s, b, m, w);

        log += s;
        return size; 
    }
}
