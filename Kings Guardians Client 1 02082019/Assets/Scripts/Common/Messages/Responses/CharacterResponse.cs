﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CharacterResponse : MessageBase, IBigMessage
{
    public CharacterInfo Character;

    public int Size(ref string log)
    {
        var b = SerializationBinary.CalculateSize(Character);

        string s = "";
        int size = Utils.Add(out s, b);

        log += s;
        return size; 
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);       
        writer.WriteBytesFull(SerializationBinary.Serialize(Character));
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        Character = SerializationBinary.Deserialize<CharacterInfo>(reader.ReadBytesAndSize());
    }
}
