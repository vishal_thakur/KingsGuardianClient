﻿using UnityEngine.Networking;
using System;

public class ErrorMessage : MessageBase 
{
    public ErrorCode ErrorCode = ErrorCode.UnknownError;
    public string Message = "";

    public RequestCode requestCode;
    public string InstanceID;
    public string ContentID;

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.Write(ErrorCode.ToString());
        writer.Write(Message);
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);       
        ErrorCode = (ErrorCode)Enum.Parse(typeof(ErrorCode), reader.ReadString());
        Message = reader.ReadString();
    }
	
}
