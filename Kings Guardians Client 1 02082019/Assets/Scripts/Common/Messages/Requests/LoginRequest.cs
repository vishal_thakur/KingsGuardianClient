﻿using UnityEngine.Networking;

public class LoginRequest : MessageBase
{
    public LoginMethod method;

	public string ID;
	public string Password; // Only used with Username method.
	public string GCMFirebaseToken; // Only used with Username method.


    public override void Deserialize(NetworkReader reader)
    {
		ID = reader.ReadString();
		Password = reader.ReadString();
		GCMFirebaseToken = reader.ReadString();
    }

    public override void Serialize(NetworkWriter writer)
    {
		writer.Write(ID);
		writer.Write(Password);
		writer.Write(GCMFirebaseToken);
    }
}