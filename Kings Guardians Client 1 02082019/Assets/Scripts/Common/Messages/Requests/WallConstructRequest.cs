﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using GridSystem;
using System.Collections.Generic;

public class WallConstructRequest : MessageBase
{
	[System.Serializable]
	public class Building
	{
		public string InstanceID;
    	public string ContentID;
    	public Point Position;
    	public Rotation Rotation;
	}
	public List<Building> Buildings = new List<Building>();


    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
		Buildings = SerializationBinary.Deserialize<List<Building>>(reader.ReadBytesAndSize());
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
		writer.WriteBytesFull(SerializationBinary.Serialize(Buildings));
    }
}
