﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerMessage : MessageBase, IBigMessage
{
	public PlayerInfo Account = null;
	public List<BuildingInfo> Buildings = new List<BuildingInfo>();
	public List<ItemInfo> Inventory = new List<ItemInfo>();
	public List<SMaterial> Materials = new List<SMaterial>();
	public List<CharacterInfo> Characters = new List<CharacterInfo>();        
	public List<string> Squad = new List<string>();
	public List<WorkerInfo> Workers = new List<WorkerInfo>();
	public List<Warlock> Warlocks = new List<Warlock>();
	public List<RemainsInfo> Remains = new List<RemainsInfo>();

	public int Size(ref string log)
	{
		var acc = SerializationBinary.CalculateSize(Account);
		var buildings = SerializationBinary.CalculateSize(Buildings);
		var inventory = SerializationBinary.CalculateSize(Inventory);
		var materials = SerializationBinary.CalculateSize(Materials);
		var characters = SerializationBinary.CalculateSize(Characters);
		var squad = SerializationBinary.CalculateSize(Squad);
		var workers = SerializationBinary.CalculateSize(Workers);
		var warlocks = SerializationBinary.CalculateSize(Warlocks);
		var remains = SerializationBinary.CalculateSize(Remains);

		string s = "";
		int size = Utils.Add(out s, acc, buildings, inventory, materials, characters, squad, workers, warlocks, remains);

		log += s;
		return size;
	}      

	public override void Serialize(NetworkWriter writer)
	{
		base.Serialize(writer);    
		writer.WriteBytesFull(SerializationBinary.Serialize(Account));        
		writer.WriteBytesFull(SerializationBinary.Serialize(Buildings));
		writer.WriteBytesFull(SerializationBinary.Serialize(Inventory));
		writer.WriteBytesFull(SerializationBinary.Serialize(Materials));
		writer.WriteBytesFull(SerializationBinary.Serialize(Characters));
		writer.WriteBytesFull(SerializationBinary.Serialize(Squad));
		writer.WriteBytesFull(SerializationBinary.Serialize(Workers));
		writer.WriteBytesFull(SerializationBinary.Serialize(Warlocks));
		writer.WriteBytesFull(SerializationBinary.Serialize(Remains));
	}

	public override void Deserialize(NetworkReader reader)
	{
		base.Deserialize(reader);
		Account = SerializationBinary.Deserialize<PlayerInfo>(reader.ReadBytesAndSize());
		Buildings = SerializationBinary.Deserialize<List<BuildingInfo>>(reader.ReadBytesAndSize());
		Inventory = SerializationBinary.Deserialize<List<ItemInfo>>(reader.ReadBytesAndSize());
		Materials = SerializationBinary.Deserialize<List<SMaterial>>(reader.ReadBytesAndSize());
		Characters = SerializationBinary.Deserialize<List<CharacterInfo>>(reader.ReadBytesAndSize());
		Squad = SerializationBinary.Deserialize<List<string>>(reader.ReadBytesAndSize());
		Workers = SerializationBinary.Deserialize<List<WorkerInfo>>(reader.ReadBytesAndSize());
		Warlocks = SerializationBinary.Deserialize<List<Warlock>>(reader.ReadBytesAndSize());
		Remains = SerializationBinary.Deserialize<List<RemainsInfo>>(reader.ReadBytesAndSize());
	}

}
