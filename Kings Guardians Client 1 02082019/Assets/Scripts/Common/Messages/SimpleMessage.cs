﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class SimpleMessage : MessageBase
{
    [System.Serializable]
    public class Parameter
    {
        public ParameterCode Key;
        public string Value;
    }
   
    public List<Parameter> Parameters = new List<Parameter>();


    public string GetValue(ParameterCode code)
    {
        var p = Parameters.Find(x => x.Key == code);
        if (p != null)
            return p.Value;
        return "";
    }

    public void Add(ParameterCode code, string value)
    {
//		Debug.LogError ("Add request >>\t code:"+code + "\tvalue:" + value);
		if (Parameters.Find (x => x.Key == code) == null) {

//			Debug.LogError ("param Added >>\t code:"+code + "\tvalue:" + value);
			Parameters.Add (new Parameter (){ Key = code, Value = value });
		}
    }

    public override void Deserialize(NetworkReader reader)
    {
        base.Deserialize(reader);
        Parameters = SerializationBinary.Deserialize<List<Parameter>>(reader.ReadBytesAndSize());
    }

    public override void Serialize(NetworkWriter writer)
    {
        base.Serialize(writer);
        writer.WriteBytesFull(SerializationBinary.Serialize(Parameters));
    }
	

    public override string ToString()
    {
        string s = "Simple Message\n";
        foreach (var p in Parameters)
            s += "-" +p.Key + ": " + p.Value + "\n";
        return s;
    }
}
