﻿
/// <summary>
/// Request codes. The client can send request messages with these codes.
/// The server will response with the same code.
///     - Building Requests: 1100 - 1199
/// </summary>
public enum RequestCode : short
{
    Register = 1000,
    Login = 1001,



    // Town
    Construct = 1100,
    Upgrade = 1101,
    Repair = 1102,
    Replace = 1103,

    Mine = 1104,
    Craft = 1105,
    CancelWorker = 1106,
    CancelBuilding = 1107,

    CreateWorker = 1108,
    CreateWarlock = 1109,

	AttachWarlock = 1110,
	DetachWarlock = 1111,

	ConstructWall = 1112,
	ReplaceWall = 1113,


    // Characters
    Equip = 1200,
    Unequip = 1201,   
    UpgradeCharacter = 1202,

    // Player
    GetOpponents = 1300,
    ChangeSquad = 1301,
    GetOtherPlayer = 1302,
	Collect = 1312,

    // BATTLE
    StartBattle = 1400,
    TurnRequest = 1401,
    BattleEnd = 1402,

    ForceWin = 1499,    // TEST


    // RAID 1500-1600
    StartRaid = 1500,
    SpawnCharacter = 1501,
    TargetBuilding = 1502,
    GetNextReward = 1503,
    ActivateRaidAbility = 1504,

	DestinationReached = 1550,
	Attack = 1551,


	//Admin Controls 
	BanPlayer = 1600,
	UnbanPlayer = 1601,
	//RewardPlayer
	RewardPlayer = 1602,

	//Toggle PushNotif Enabled
	TogglePushNotifEnabled = 1700,

		

}