﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class UnityExtensions 
{
    public static I GetInterfaceComponent<I>(this GameObject go) where I : class
    {
        return go.GetComponent(typeof(I)) as I;
    }

    public static I GetInterfaceComponentInChildren<I>(this GameObject go) where I : class
    {
        return go.GetComponentInChildren(typeof(I)) as I;
    }

    public static I GetInterfaceComponentInParent<I>(this GameObject go) where I : class
    {
        return go.GetComponentInParent(typeof(I)) as I;
    }

}

public class Vector3Extended
{
    public static Vector3 Parse(string positionString)
    {        
        string[] s = positionString.Replace("(","").Replace(")","").Split(',');
        return new Vector3(float.Parse(s[0]), float.Parse(s[1]), float.Parse(s[2]));
    } 
}


public class GameObjectExtended
{
    public static I FindObjectOfInterface<I>() where I : class
    {
        MonoBehaviour[] monoBehaviours = GameObject.FindObjectsOfType<MonoBehaviour>();

        foreach(MonoBehaviour behaviour in monoBehaviours)
        {
            I component = behaviour.GetComponent(typeof(I)) as I;

            if(component != null)
                return component;            
        }
        return null;
    }

    public static List<I> FindObjectsOfInterface<I>() where I : class
    {
        MonoBehaviour[] monoBehaviours = GameObject.FindObjectsOfType<MonoBehaviour>();
        List<I> list = new List<I>();

        foreach(MonoBehaviour behaviour in monoBehaviours)
        {
            I component = behaviour.GetComponent(typeof(I)) as I;

            if(component != null)
            {
                list.Add(component);
            }
        }
        return list;
    }
}
