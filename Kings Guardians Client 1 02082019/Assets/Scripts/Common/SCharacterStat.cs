﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SCharacterStat 
{
    public CharacterStats Stat;
    public int Value;	
}

[System.Serializable]
public class SCharacterStatMod
{
    public CharacterStats Stat;
    public float Value;
}

[System.Serializable]
public class SCharacterDefence
{
    public Element Element;
    public int Amount;
}