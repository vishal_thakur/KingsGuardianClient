﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SerializationBinary 
{
    public static byte[] Serialize(object o)
    {
        if( o == null )
            return new byte[0];
        
        MemoryStream ms = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(ms, o);

        return ms.ToArray();
    }

    public static T Deserialize<T>(byte[] bin) where T : class
    {
        if (bin == null) return default(T);

        MemoryStream ms = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();

        ms.Write(bin, 0, bin.Length);
        ms.Position = 0;

        return bf.Deserialize(ms) as T;
    }

    public static int CalculateSize(object o)
    {
        if (o == null)
            return 0;

        byte[] b = Serialize(o);
        return b.Length;
    }
}
