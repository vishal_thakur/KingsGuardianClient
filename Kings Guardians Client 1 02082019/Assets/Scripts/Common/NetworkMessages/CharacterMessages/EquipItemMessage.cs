﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EquipItemMessage : MessageBase {

    /// <summary>
    /// The building ID.
    /// </summary>
    public string CharacterID;

    /// <summary>
    /// The item instance ID to equip or unequip.
    /// </summary>
    public string ItemID;

    public override void Deserialize(NetworkReader reader)
    {
        CharacterID = reader.ReadString(); 
        ItemID = reader.ReadString();
    }

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(CharacterID);
        writer.Write(ItemID);
    }
}
