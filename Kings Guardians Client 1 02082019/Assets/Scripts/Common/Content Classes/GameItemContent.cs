﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameItemContent
{
    public string ContentID;
    public string Name;
    public string Description;
    public List<SMaterial> Price;
    public float CraftTime;	

    string _description = "";

    public virtual List<SMaterial> GetPrice()
    {
        return Price;
    }

    public string GetDescription(int param = 1, bool reset = false)
    {
        if (_description == "" || reset )
        {
            foreach (var pair in DescriptionParameters(param))
                _description.Replace(pair.Key, pair.Value);
        }
        return _description;
    }


    protected virtual Dictionary<string, string> DescriptionParameters(int param)
    {
        return new Dictionary<string, string>();
    }
}
