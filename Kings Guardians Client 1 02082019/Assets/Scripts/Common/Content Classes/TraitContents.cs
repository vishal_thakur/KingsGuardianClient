﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TraitBase{}

[System.Serializable]
/// <summary>
/// This trait incease the owner's Damage 
/// </summary>
public class DamageIncreaseTrait : TraitBase
{
	public float Multiplier = 1;

	/// <summary>
	/// Against the specified unit
	/// </summary>
	public UnitType VSUnitType;

	/// <summary>
	/// Against a actors with the specified contentID
	/// </summary>
	public string VSContentID = "";
}

[System.Serializable]
/// <summary>
/// The trait absorbs % amount of incoming untyped damage.
/// </summary>
public class DamageResistanceTrait : TraitBase
{
	public float Percentage = 1;
}

[System.Serializable]
/// <summary>
/// Character with this trait has % chance to deflect any damage.
/// </summary>
public class MissTrait : TraitBase
{
	public float Chance = 1;

	/// <summary>
	/// Against the specified unit
	/// </summary>
	public UnitType VSUnitType;

	/// <summary>
	/// Against a actors with the specified contentID
	/// </summary>
	public string VSContentID = "";
}

[System.Serializable]
/// <summary>
/// The character with this trait will explode when attacks someone. But can deal increased damage.
/// </summary>
public class ExplosionTrait : TraitBase
{
	public float Multiplier = 1;
}

[System.Serializable]
/// <summary>
/// The trait changes the character's damage type
/// </summary>
public class AlternateDamageTrait : TraitBase
{
	public Element DamageType;
}

[System.Serializable]
/// <summary>
/// The character can jump above the wall.
/// </summary>
public class WallJumpTrait : TraitBase {}

[System.Serializable]
/// <summary>
/// This trait increase a certain stat by an amount.
/// </summary>
public class StatIncreaseTrait : TraitBase
{
	public CharacterStats Stat;
	public float Amount;
}

[System.Serializable]
/// <summary>
/// This trait periodically summons a creature (up to the specified value)
/// </summary>
public class NecromancyTrait : TraitBase
{
	public int SummonsCount = 5;
}

[System.Serializable]
public class DoTTrait : TraitBase
{
	public int Damage = 1;
	public Element DamageType = Element.Untyped;
}

[System.Serializable]
public class StunTrait : TraitBase
{
	
}


[System.Serializable]
public class TraitData
{
	public TraitType Type;
	public float FloatParameter = 1;

	public UnitType Unit;
	public string ContentID = "";
	public Element DamageType = Element.Untyped;
	public CharacterStats Stat;

	public int IntParameter = 5;
}

