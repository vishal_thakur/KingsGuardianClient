﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DefenseContent : ItemContent
{
    public Element Element;
    public int Amount;
}
