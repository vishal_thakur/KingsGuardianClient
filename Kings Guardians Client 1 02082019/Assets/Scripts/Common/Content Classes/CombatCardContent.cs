﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CombatCardContent : ItemContent
{
    public ActionID Action;
    public TargetType Target;

	/// <summary>
	/// The duration. Measured in turns.
	/// </summary>
    public int Duration;

	/// <summary>
	/// Whether the card can be used on dead targets.
	/// </summary>
    public bool UseOnDead;

    [System.NonSerialized] public List<EffectBase> Effects = new List<EffectBase>(); 
}
