﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CharacterContent : GameItemContent
{
	/// <summary>
	/// Every character gain a Card when created. 
	/// This is the ID of the card which he/she gains.
	/// </summary>
    public string NativeCardContentID;
    public List<SCharacterStat> BaseStats;
    public int BuildingLevelReq = 0;

	#region Raid Parameters
	public bool Flying;
    public float MovementSpeed = 1;
    public int AttackRange = 1;

	/// <summary>
	/// Determines whether or not the character can target a land unit in raid mode.
	/// </summary>
	public bool LandTarget;

	/// <summary>
	/// Determines whether or not the character can target an air unit in raid mode.
	/// </summary>
	public bool AirTarget;

	public List<TraitBase> Traits = new List<TraitBase>();
	public List<ActiveAbility> ActiveAbilities = new List<ActiveAbility>();
	#endregion
}


[System.Serializable]
public class ActiveAbility
{
	public string Name;

	/// <summary>
	/// Ability Point Cost to activate.
	/// </summary>
	public int Cost;	

	/// <summary>
	/// The radius measured in Tiles
	/// </summary>
	public int Radius;

	/// <summary>
	/// Duration, measured in seconds.
	/// </summary>
	public int Duration;

	/// <summary>
	/// Type of the target the ability can affect.
	/// </summary>
	public TargetType Target;

	/// <summary>
	/// Type of the aura.
	/// </summary>
	public AuraType AuraType;

	public TraitBase Trait;
	public float Amount;

	public TraitData TraitData = new TraitData();
}