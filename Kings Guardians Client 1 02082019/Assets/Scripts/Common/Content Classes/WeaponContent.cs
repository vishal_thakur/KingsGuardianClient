﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeaponContent : ItemContent
{
    public List<SCharacterStatMod> StatBonuses = new List<SCharacterStatMod>();
    public List<SCharacterDefence> DefenceBonuses = new List<SCharacterDefence>();
}
