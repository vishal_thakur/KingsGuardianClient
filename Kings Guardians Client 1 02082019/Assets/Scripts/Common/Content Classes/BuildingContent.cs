﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GridSystem;

[System.Serializable]
public class BuildingContent : GameItemContent
{
    /// <summary>
    /// This is the size of the building footprint in unity unit.
    /// </summary>
    public Point ModelSize;

    public BuildingCategory Category;
    public BuildingUpgrade[] Upgrades;
	public int[] MaxAmount = new int[9];

    public Currency Material;
    public List<SMaterial> MiningPrices = new List<SMaterial>();
    public ItemCategory ItemCategory;

    public float ProductionTime 
    {
        get 
        { 
            float time = 0;
            switch (Category)
            {
                case BuildingCategory.Resource_mine: time = 5 * 60; break;			  // Mine creation time is measured in minutes
                case BuildingCategory.Resource_generator: time = 1 * 60 * 60; break;  // Generation time is measured in hours
            }
            return time;
        }
    }

    public BuildingUpgrade GetUpgrade(int upgrade)
    {
        if (upgrade < 1)
            upgrade = 1;

        if (Upgrades != null && Upgrades.Length >= upgrade)
            return Upgrades[upgrade-1];
        return null;
    }

    public List<SMaterial> RepairCost(int upgrade)
    {
        var u = GetUpgrade(upgrade);
        if (u == null)
            return new List<SMaterial>();

        List<SMaterial> mats = new List<SMaterial>();
        foreach (var c in u.Cost)
            mats.Add(new SMaterial(){ Material = c.Material, Amount = Mathf.CeilToInt(c.Amount * 0.01f) });
        return mats;
    }

    public override List<SMaterial> GetPrice()
    {
        return Utils.CraftCost(this);
    }

    protected override Dictionary<string, string> DescriptionParameters(int param)
    {
        return new Dictionary<string, string>()
        {
            {"{amount}", GetUpgrade(param).ProductionAmount.ToString() },
            {"{storage}", GetUpgrade(param).Storage.ToString()},
            {"{category}", ItemCategory.ToString()},

            {"{attackspeed}", GetUpgrade(param).AttackSpeed.ToString()},
            {"{damage}", GetUpgrade(param).Damage.ToString()},
            {"{damageradius}", GetUpgrade(param).DamageRadius.ToString()},
            {"{range}", GetUpgrade(param).Range.ToString()},

        };
    }
}


[System.Serializable]
public class BuildingUpgrade
{    
    public float UpgradeTime = 0;
    public List<SMaterial> Cost = new List<SMaterial>();

    public int MaxHealth = 0;
   
    public int Storage = 100;
    public int ProductionAmount;


    // DEFENSE BUILDINGS
    public int Range = 1;         // Measured in Tiles
    public int DamageRadius = 0;  // Measured in tiles, 0 = single target.
    public float AttackSpeed = 1; // measured in seconds, 0 = activates only once.
    public int Damage; 

	// Targeting
	public bool LandUnit;
	public bool FlyingUnit;


    public int WorkerPrice = -1;  // -1 is not available.
    public int WarlockPrice = -1; // -1 is not available.
}