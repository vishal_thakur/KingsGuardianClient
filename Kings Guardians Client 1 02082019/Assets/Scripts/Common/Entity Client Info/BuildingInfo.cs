﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BuildingInfo : BaseInfo
{
    public bool Active;
    public int CurrentHP;

    public string Position = "0,0";
    public string Rotation = "";
    public string FootprintSize = "1,1";
    public int Upgrade;

    public List<QueueInfo> Queue;

    // Mine Building
    public int StoredMaterials;   
}
