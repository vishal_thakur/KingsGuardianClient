﻿using System.Collections.Generic;

[System.Serializable]
public class QueueInfo
{
    public string StartTime = "1990/01/01 00:00:01";
    public float Duration = 0;
    public string ItemContentID = "";
    public List<SMaterial> Costs = new List<SMaterial>();
    public int EnumData;

    public int Amount = 1;
}