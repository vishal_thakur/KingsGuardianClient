﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class WorkerInfo : BaseInfo
{
    public WorkerType Type;
    public QueueInfo ActiveWork;
    public string BuildingInstanceID;
}
