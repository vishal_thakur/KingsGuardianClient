﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RemainsInfo : BaseInfo
{
	//	public Vector3 Position;
	public float x;
	public float y;
	public float z;
}
