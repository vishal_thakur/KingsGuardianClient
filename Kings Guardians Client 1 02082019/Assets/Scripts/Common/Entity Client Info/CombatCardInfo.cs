﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CombatCardInfo : ItemInfo
{
    public ActionID Action;
    public TargetType Target;
    public int Duration;
}
