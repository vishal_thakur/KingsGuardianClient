﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AdminPlayerUnit : MonoBehaviour {

	[SerializeField] Text BanStatusTextRef;

	[SerializeField] Text PlayerNameTextRef;

	PlayerInfo myPlayer;

//	void Start(){
//		if (Random.Range (0, 5) > 2)
//			Initialize ("player" + Time.time , true);
//		else
//			Initialize ("player" + Time.time , false);
//	}

	/// <summary>
	/// Initialize this instance.
	/// </summary>
	public void Initialize(PlayerInfo player){
		//Grab the Player Ref
		myPlayer = player;
		//Set Player name 
		PlayerNameTextRef.text = myPlayer .Username;


		//Player Ban Status
		if(myPlayer.IsBanned){
			BanStatusTextRef.text = "Banned";
			BanStatusTextRef.color = Color.red;
		}
		else{
			BanStatusTextRef.text = "Ok";
			BanStatusTextRef.color = Color.green;
		}
	}


	public void OnPlayerSelected(){
		AdminManager.Instance.OnPlayerSelected (myPlayer);
	}
}