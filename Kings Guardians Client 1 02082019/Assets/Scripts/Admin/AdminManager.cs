﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminManager : SingletonMono<AdminManager>  {


	[SerializeField] List<AdminPlayerUnit> adminPlayerUnitList = new List<AdminPlayerUnit>();

	[SerializeField] public  PlayerInfo SelectedPlayerRef;

	[SerializeField] Transform GridParentRef;

	[SerializeField] AdminPlayerControlsManager AdminPlayerControlsManagerRef;

//	[SerializeField] GameObject PlayerInfoPanelRef;


//	[SerializeField] List<PlayerInfo> Players = new List<PlayerInfo> ();

	void OnEnable(){

		foreach(PlayerInfo player in GameManager.Instance.Opponents){
			AddPlayer (player);
		}
//		Refresh();
	}


	public void Refresh(){
//		AdminPlayerUnit[] allChildren = GridParentRef.transform.GetComponentsInChildren<AdminPlayerUnit> ();
//
//		Debug.LogError ("Child Count\t" + allChildren.Length);
//		foreach (AdminPlayerUnit unit in allChildren)
//			DestroyImmediate (unit.gameObject);
		for(int i = 0; i < adminPlayerUnitList.Count; i++) {
			adminPlayerUnitList[i].Initialize (GameManager.Instance.Opponents [i]);
		}

//		adminPlayerUnitList.RemoveAll ();

	}

	public void QuitApp(){
		Application.Quit ();
	}



	void OnDisable(){
		adminPlayerUnitList.Clear ();
	//Remove All Players
	foreach (AdminPlayerUnit obj in  GridParentRef.GetComponentsInChildren<AdminPlayerUnit>()) {
			GameObject.Destroy (obj.gameObject);
		}
	}



	/// <summary>
	/// Adds the player to the All Players Grid
	/// </summary>
	void AddPlayer(PlayerInfo player){

//		if (adminPlayerUnitList.Count <= GameManager.Instance.Opponents.Count) {
			AdminPlayerUnit newPlayerObj = GameObject.Instantiate (Resources.Load ("Admin/Player", typeof(AdminPlayerUnit)))as AdminPlayerUnit;

			newPlayerObj.transform.SetParent (GridParentRef.transform, false);

			newPlayerObj.Initialize (player);

			adminPlayerUnitList.Add (newPlayerObj);
//		}
	}


	public void OnPlayerRewarded(Currency rewardCurrency , int rewardAmmount){
		var msg = new SimpleMessage();
		msg.Add(ParameterCode.StringValue , SelectedPlayerRef.Username + "." + rewardCurrency.ToString() + "." + rewardAmmount);
		NetworkManager.SendRequest (RequestCode.RewardPlayer, msg);
	}


	/// <summary>
	/// Raises the player selected event.
	/// </summary>
	/// <param name="player">Player.</param>
	public void OnPlayerSelected(PlayerInfo player){
		//Grab and Save new Player as the Current player
		SelectedPlayerRef = player;

		//Disable Player Info Panel
//		PlayerInfoPanelRef.SetActive (false);
		//Enable GO
		AdminPlayerControlsManagerRef.gameObject.SetActive (true);

		//Show Admin Player Control menu with details and possible actions of the selected player
		AdminPlayerControlsManagerRef.Initialize (player);
	}


//	public void OnPlayerBanRequested(){
//		NetworkManager.SendRequest (RequestCode.BanPlayer);
//	}
//
//	public void OnPlayerUnBanRequested(){
//		NetworkManager.SendRequest (RequestCode.UnbanPlayer);
//	}
//



	public void OnPlayerBanToggled(bool isBanned){
		SelectedPlayerRef.IsBanned = isBanned;
		//Send Message to Server to Ban/Unban the specified Player
		if (isBanned) {
			var msg = new SimpleMessage ();
			msg.Add (ParameterCode.Username, SelectedPlayerRef.Username);
			NetworkManager.SendRequest (RequestCode.BanPlayer, msg);
		}
		else {
			var msg = new SimpleMessage();
			msg.Add(ParameterCode.Username , SelectedPlayerRef.Username);
			NetworkManager.SendRequest (RequestCode.UnbanPlayer , msg);
		}
	}


	public void OnPlayerBanToggleCallback(bool isBanned){
		//On Successful Call back from Player BanToggle Event
		AdminPlayerControlsManagerRef.OnPlayerBanToggleCallback(isBanned);

		Notification.Instance.Show (SelectedPlayerRef.Username + " banned : " + isBanned ,isBanned == true ? NotificationTypes.Success : NotificationTypes.Error);
	}


	public void OnPlayerRewardedCallback(){
		Notification.Instance.Show (SelectedPlayerRef.Username + " Rewarded Successfully!!", NotificationTypes.Success);
	}
}