﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardUnit : MonoBehaviour {

	public Currency currency;

	[SerializeField] InputField CurrencyAmmountInputFieldRef; 

	public void OnRewardPlayerSelected(){
		int rewardAmmount = 0;

		if (string.IsNullOrEmpty (CurrencyAmmountInputFieldRef.text))
			return;
		
		rewardAmmount = int.Parse(CurrencyAmmountInputFieldRef.text);

		if(rewardAmmount > 0)
			AdminManager.Instance.OnPlayerRewarded (currency , rewardAmmount);
	}
}
