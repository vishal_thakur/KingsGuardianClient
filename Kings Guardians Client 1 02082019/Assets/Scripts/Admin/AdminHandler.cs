﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdminHandler : BaseHandler {


	protected override void OnEventRecieved(Message message)
	{
		Debug.LogError ("Here>>>>>>\t" + message.EventCode.ToString());
		switch (message.EventCode)
		{

		case EventCode.PlayerBanned:        
			//Player Ban Successful , Reflect the same in The Admin Console
			//			Debug.LogError ("PLayer Banned Success !");
			//True = Player Banned
			AdminManager.Instance.OnPlayerBanToggleCallback(true);
			break; 

		case EventCode.PlayerUnbanned:            
			//Player Unban Successful , Reflect the same in The Admin Console
			//			Debug.LogError ("PLayer UnBanned Success !");
			//False = Player Unbanned
			AdminManager.Instance.OnPlayerBanToggleCallback (false);
			break;

		case EventCode.PlayerRewarded:            
			//Player Unban Successful , Reflect the same in The Admin Console
			//			Debug.LogError ("PLayer UnBanned Success !");
			//False = Player Unbanned
			AdminManager.Instance.OnPlayerRewardedCallback ();
			break;     
		}
	}
}
