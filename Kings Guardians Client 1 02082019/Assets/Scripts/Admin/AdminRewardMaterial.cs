﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminRewardMaterial : MonoBehaviour {

	[SerializeField] Text RewardNameTextRef;



	public void Initialize(string rewardName){
		RewardNameTextRef.text = rewardName;
	}

}
