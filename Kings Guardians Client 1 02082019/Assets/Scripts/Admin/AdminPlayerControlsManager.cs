﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdminPlayerControlsManager : MonoBehaviour {

//	[SerializeField]
//	AdminManager AdminManagerRef; 


	[SerializeField]
	Text PlayerNameTextRef;

	[SerializeField]
	Text PlayerBanButtonTextRef;

	[SerializeField]
	Toggle PlayerBanToggleStatusRef;

//	[SerializeField] Toggle BanPlayerToggleRef;


	void Start(){
//		AdminManagerRef = transform.parent.GetComponent<AdminManager> ();
//		BanPlayerToggleRef.onValueChanged.AddListener (OnPlayerBanToggle);
	}

	void OnEnable(){
		Globals.IsAnyUIPanelActive = true;
	}

	void OnDisable(){
		Globals.IsAnyUIPanelActive = false;
	}

	/// <summary>
	/// Initialize the specified player.
	/// </summary>
	/// <param name="player">Player.</param>
	public void Initialize(PlayerInfo player){
		PlayerNameTextRef.text = player.Username;
		//Show admin Controls WRT to the specified player
		OnPlayerBanToggleCallback(player.IsBanned);

	
		if (player.IsBanned) {
			PlayerBanButtonTextRef.text = "Player Banned : Yes";
			PlayerBanToggleStatusRef.isOn = true;
		}
		else {
			PlayerBanButtonTextRef.text = "Player Banned : No";
			PlayerBanToggleStatusRef.isOn = false;
		}
	}



	/// <summary>
	/// Raises the player banned event.
	/// </summary>
	public void OnPlayerBanToggle(Toggle toggle){
//		if(!isOn)
//			PlayerBanButtonTextRef.color = Color.green;
//		else
//			PlayerBanButtonTextRef.color = Color.red;

		AdminManager.Instance.OnPlayerBanToggled (toggle.isOn);
	}


	public void OnPlayerBanToggleCallback(bool isBanned){

		if (isBanned) {
			PlayerBanButtonTextRef.text = "Player Banned : Yes";
			PlayerBanToggleStatusRef.isOn = true;
		}
		else {
			PlayerBanButtonTextRef.text = "Player Banned : No";
			PlayerBanToggleStatusRef.isOn = false;
		}
		
	}

}
