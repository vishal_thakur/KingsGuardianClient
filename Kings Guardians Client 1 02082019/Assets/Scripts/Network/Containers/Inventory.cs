﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory <T> where T : GameItemReference
{
    public int MaxSlot { get; set; }                // 0 : unlimited
    public List<T> Items { get; private set; }
    public bool FreeSlot { get { return MaxSlot == 0 || MaxSlot > Items.Count; } }

    public Inventory()
    {
        MaxSlot = 0;
        Items = new List<T>(); 
    }

    public void Add( T item)
    {
        Items.Add(item);
    }

    public T Get(string id)
    {
        T item = Items.Find(x => x.InstanceID == id);
        return item;
    }

    public int Count( string contentID )
    {
        return Items.FindAll(x => x.ContentID == contentID).Count;
    }

    public List<T> GetAll()
    {
        return Items;
    }

    public void Refresh<D>(List<D> list) where D : GameItemReference
    {
        Items = list as List<T>;
    }


}