﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Runemark.AssetEditor;

public enum ClientStatus
{ 
	Disconnected,
	Connecting,
	CheckUpdates,
	Updating,
	LoadingAssets,
	Connected, 

	Login, 
	LoggedIn,
	Logout,
	LoggedOut,

	Disconnecting 
}

public class NetworkManager : SingletonMono<NetworkManager>
{
	string[] ASSETBUNDLE_NAMES = new string[]{ "building", "character" };
	int _assetBundleCnt = -1;
	string _lastUpdate;


	public bool ManualConnect;

    const float CONNECT_ATTEMT_TIME = 10; //seconds
    const float WAIT_FOR_CONNECT = 5;

    public bool LocalhostInEditor = true;

    public int ServerPort = 4444;
    public string Host = "mail.idro.org.uk";

    public float NextConnectionAttempt{ get { return _lastConnectAttempt + WAIT_FOR_CONNECT + CONNECT_ATTEMT_TIME; } }
    public ClientStatus Status { get; private set; }

    NetworkClient _myClient;
    float _lastConnectAttempt = 0f;

    PlayerHandler _playerHandler;
    BuildingHandler _buildingHandler;
    CharacterHandler _characterHandler;
    BattleHandler _battleHandler;
    RaidHandler _raidHandler;
	AdminHandler _adminHandler;

	bool AllAssetsLoaded = false;

	[SerializeField] GameObject PlayerBannedMsgPanel;

    [SerializeField] bool deleteAllPlayerPrefs = false;
    void Awake()
    {
        if (deleteAllPlayerPrefs)
        {
            PlayerPrefs.DeleteAll();
        }
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        Status = ClientStatus.Disconnected;      

        _playerHandler = new PlayerHandler();
        _buildingHandler = new BuildingHandler();
        _characterHandler = new CharacterHandler();
		_battleHandler = new BattleHandler();
		_raidHandler = new RaidHandler();
		_adminHandler = new AdminHandler();

        // Setup Client
        _myClient = new NetworkClient();
        ConnectionConfig config = new ConnectionConfig();
        config.AddChannel(QosType.Reliable);
        config.AddChannel(QosType.Unreliable);
        config.AddChannel(QosType.ReliableFragmented);
        HostTopology topology = new HostTopology(config, 10000);
        _myClient.Configure(topology);

		RegisterHandlers();

		if(!ManualConnect) Connect();
    }


	#region Public Methods
	public void Connect()
	{
		Status = ClientStatus.Connecting;

		#if UNITY_EDITOR
		if(LocalhostInEditor) Host = "localhost";
		#endif

		_myClient.Connect(Host, ServerPort);
		_lastConnectAttempt = Time.time;
	}

	public void Register(string username, string password)
	{
		var m = new LoginRequest(){ ID = username, Password = password, GCMFirebaseToken = FirebaseNotificationManager.Instance.GCMFirebaseToken , method = LoginMethod.Username };
		SendRequest(RequestCode.Register, m);
	}

	public void Login(string username, string password)
	{
		Status = ClientStatus.Login;
		var m = new LoginRequest(){ ID = username + "." + FirebaseNotificationManager.Instance.GCMFirebaseToken , Password = password, method = LoginMethod.Username };
		SendRequest(RequestCode.Login, m);
		Debug.LogWarning("Login request" + Time.time);
	}

	public void Login(string id, LoginMethod method)
	{
		Status = ClientStatus.Login;
		var m = new LoginRequest() { ID = id, method = method };
		SendRequest(RequestCode.Login, m);
		Debug.LogWarning("Login request" + Time.time);
	}
	#endregion


	#region Handlers  

	void OnConnected(NetworkMessage netMsg)
	{
		Debug.Log("On Connected");
		if (Status == ClientStatus.Connecting)
		{

			if (AllAssetsLoaded) {
				Status = ClientStatus.Connected;
				return;
			}

			Status = ClientStatus.CheckUpdates;
			if (netMsg != null && netMsg.reader != null)
			{
				_assetBundleCnt = -1;
				var msg = netMsg.ReadMessage<SimpleMessage>();
				_lastUpdate = msg.GetValue(ParameterCode.LastAssetUpdate);

                //Remove Me after Done
                //Status = ClientStatus.LoadingAssets;
                //StartCoroutine(AssetPackManager.Instance.LoadAssets());
                ///
                
                NextAssetPackCheck();
			}
			else
				Status = ClientStatus.Connecting;
		}
	}

	void OnGUI(){
//		GUILayout.TextField ("Last update\t" + _lastUpdate.ToString());
	}

	void NextAssetPackCheck()
	{
		if (_lastUpdate != "")
		{
			_assetBundleCnt++;
//			Debug.LogError (_assetBundleCnt);
			if (_assetBundleCnt < ASSETBUNDLE_NAMES.Length)
			{			
				System.DateTime time =	System.Convert.ToDateTime(_lastUpdate);
				if (AssetBundleLoader.CheckForUpdates(ASSETBUNDLE_NAMES[_assetBundleCnt], time))
				{
					Status = ClientStatus.Updating;
					StartCoroutine(AssetBundleLoader.DownloadUpdates(ASSETBUNDLE_NAMES[_assetBundleCnt]));
				}
				else
					NextAssetPackCheck();
			}
			else
			{
				Status = ClientStatus.LoadingAssets;
				StartCoroutine(AssetPackManager.Instance.LoadAssets());
			}
		}
	}


	void OnDisconnected(NetworkMessage netMsg)
	{ 
		Status = ClientStatus.Disconnected;
		LevelLoader.Instance.LoadScene("MainMenu");
	}

	void OnLogin(NetworkMessage netMsg) 
	{  
		Debug.LogWarning("Login response" + Time.time);
		Status = ClientStatus.LoggedIn;
		var msg = netMsg.ReadMessage<PlayerMessage>();
		Debug.LogWarning("Login response, message readed " + Time.time);
		GameManager.Instance.SetPlayer(msg);
	}

	void OnRegister(NetworkMessage netMsg) 
	{ 
		Status = ClientStatus.Connected;
		Debug.Log("Registration is done.");
	}
    #endregion



    //void OnGUI() {
    //    foreach(string msg in Globals.message)
    //        GUILayout.TextField(msg);
    //}

    void Update()
    {
        if (_myClient == null) return;
    
        switch (Status)
        {
            case ClientStatus.Connecting: 
                if (_lastConnectAttempt + WAIT_FOR_CONNECT < Time.time)
                    Status = ClientStatus.Disconnected;
                break;
            case ClientStatus.Disconnected:
                if (!ManualConnect && NextConnectionAttempt < Time.time)
                    Connect();
                break;		

			case ClientStatus.Updating:
				if (AssetBundleLoader.Progress == 1f)
					NextAssetPackCheck();
				break;

		case ClientStatus.LoadingAssets:
//			if (AllAssetsLoaded) {
//				if (_myClient.isConnected) {
//					Status = ClientStatus.Connected;
//					break;
//				}
//			}

			if (AssetPackManager.Instance.Progress == 1f) {
				if (_myClient.isConnected) {
					Status = ClientStatus.Connected;
					AllAssetsLoaded = true;
				} 
				else
					Status = ClientStatus.Disconnected;
//				Status = (_myClient.isConnected) ? ClientStatus.Connected : ClientStatus.Disconnected;

			}	
			break;
        }
    }

    
   
	public void LoadScene(string sceneName){
//		UnityEngine.SceneManagement.SceneManager.LoadScene (sceneName);
		Application.Quit();
	}

   
   

    public static void SendRequest(RequestCode code, MessageBase message = null)
    {
//		Debug.LogError (code + "\tsent");
        if (message == null) message = new EmptyMessage();
        instance._myClient.Send((short)code, message);
    }

    void OnError(NetworkMessage netMsg)
    {
        var msg = netMsg.ReadMessage<ErrorMessage>();

        string error = "Server Error (" + (short)msg.ErrorCode + "): " + msg.ErrorCode.ToString();
        Notification.Error(error);
        Debug.LogError(error);

        short c = (short)msg.requestCode;
        var handler = GetHandler(c);


		//Player has been banned Show Ban Panel !
		if (error == "Server Error (3004): PlayerBanned") {
			PlayerBannedMsgPanel.SetActive(true);
			return;
		}

        if (handler != null)
            handler.ErrorRecived(netMsg);
    }


    BaseHandler GetHandler( short code)
    {
        if( code >= 1100 && code < 1200) 
            return _buildingHandler;
        if (code >= 1200 && code < 1300)
            return _characterHandler;
        if (code >= 1300 && code < 1400)
            return _playerHandler;
        if (code >= 1400 && code < 1500)
            return _battleHandler;
        if (code >= 1500 && code < 1600)
            return _raidHandler;
        

		// Building Events
		if( code >= 2100 && code < 2200) 
			return _buildingHandler;
		// Raid Events
		if (code >= 2500 && code < 3000)
			return _raidHandler;
		// Admin Controls Events
		if (code >= 3001 && code < 3500)
			return _adminHandler;
		
        return null;
    }


	public void SetHost(string host)
	{
		
	}






	void RegisterHandlers()
	{
		// Register Handlers
		_myClient.RegisterHandler((short)EventCode.Error, OnError);
		_myClient.RegisterHandler(MsgType.Connect, OnConnected); 
		_myClient.RegisterHandler(MsgType.Disconnect, OnDisconnected); 

		// Authentication
		_myClient.RegisterHandler((short)RequestCode.Login, OnLogin);
		_myClient.RegisterHandler((short)RequestCode.Register, OnRegister);

		// Events
		_myClient.RegisterHandler((short)EventCode.GameContent, GameManager.Instance.GetGameContent);

		// Player Events
		_myClient.RegisterHandler((short)EventCode.RefreshMaterial, _playerHandler.EventRecieved);

		// Building & Raid Events
		var events = (EventCode[])System.Enum.GetValues(typeof(EventCode));
		foreach (var code in events)
		{
			short c = (short)code;
			var handler = GetHandler(c);
			if( handler != null )
				_myClient.RegisterHandler(c, handler.EventRecieved); 
		}

		// Request Handlers
		var codes = (RequestCode[])System.Enum.GetValues(typeof(RequestCode));
		foreach (var code in codes)
		{
			short c = (short)code;
			var handler = GetHandler(c);
			if( handler != null )
				_myClient.RegisterHandler(c, handler.RequestRecieved); 
		}
	}
}
