﻿using UnityEngine;
using System.Collections;
using System;
using GridSystem;
using System.Collections.Generic;

public enum BuildingState { Active, Inactive, Destroyed }
public class BuildingReference : GameItemReference
{   
    public new BuildingContent ContentData{ get { return GetContentData<BuildingContent>(); } }
    public bool Dead { get { return CurrHP == 0; }}       

    public BuildingState CurrentState {
        get
        {
            BuildingState state = (!_inactive) ? BuildingState.Active : BuildingState.Inactive;           
            if (Dead) state = BuildingState.Destroyed;                    
            return state;
        }
    }   

    public int MaxHP  { get { return (ContentData != null) ? GetUpgrade().MaxHealth : 1; } }    
    public int CurrHP { get; private set; }
    public int Shield { get; private set; }
    public int Upgrade  { get; private set; }

	public Point Position { get; set; }
	public Point Size { get; private set; }
	public Rotation Rotation { get; set; }

    public List<QueueInfo> Queue { get; private set; }
	public WorkerReference AssignedWorker;

	public int StoredMaterial { get; private set; }

    bool _inactive; // If the server marks this building as inactive, this stored here.

    public BuildingReference(string contentID = "")
    {
        if (contentID != "")
        {
            ContentID = contentID;
            UpdateHP(MaxHP);

			Size = GridHandlerPlayer.FootprintSize(ContentData.ModelSize, GameManager.Instance.TileSize);

//			if(TownLogic.Instance.ScoutMode)
//				Size = GridHandlerPlayer.FootprintSize(ContentData.ModelSize, GameManager.Instance.TileSize);
				
			Position = Point.zero;
			Rotation = Rotation.D180;
        }    

		Upgrade = 1;
		CurrHP = 1;
		Shield = 0;
    }

	public void Rotate ()
	{
		int r = (int)Rotation + 1;
		if (r > 3) r = 0;

		if ((int)Rotation % 2 != r % 2)
			Size = new Point (Size.y, Size.x);              

		Rotation = (Rotation)r;
	}

    public override void Read(BaseInfo info)
    {
        if (info.GetType() != typeof(BuildingInfo)) return;
        base.Read(info);

        BuildingInfo bData = (BuildingInfo)info;
        _inactive = !bData.Active;

		Position = Point.Parse(bData.Position);

		//************** GridCodeNew ***********************
		Size = GridHandlerPlayer.FootprintSize(ContentData.ModelSize, GameManager.Instance.TileSize);

		//Load size according to Enemy Grid Base
//		if(TownLogic.Instance.ScoutMode)
//			Size = GridHandlerPlayer.FootprintSize(ContentData.ModelSize, GameManager.Instance.TileSize);



		Rotation = (Rotation)int.Parse(bData.Rotation);

        Upgrade = bData.Upgrade;
        CurrHP = bData.CurrentHP;

        if(Queue != null) Queue.Clear();
        Queue = bData.Queue;

		if (ContentData.Category == BuildingCategory.Resource_generator)
			StoredMaterial = bData.StoredMaterials;

		LastUpdated = Time.time;
    }

    public BuildingUpgrade GetUpgrade()
    {
        return ContentData.GetUpgrade(Upgrade);
    }
       
    public void UpdateHP(int hp)
    {
//		StartCoroutine (UpdateHpInSync(hp));	
        CurrHP = Mathf.Clamp(hp, 0, MaxHP);
        LastUpdated = Time.time;
    }    

//
//	IEnumerator UpdateHpInSync(int hp){
//		yield return new WaitForSeconds ();
//
//		CurrHP = Mathf.Clamp(hp, 0, MaxHP);
//		LastUpdated = Time.time;
//		StopCoroutine ("UpdateHpInSync");
//	}

	public void UpdateShield(int shield)
	{
		Shield = Mathf.Clamp(shield, 0, MaxHP);
		LastUpdated = Time.time;
	}      
}
