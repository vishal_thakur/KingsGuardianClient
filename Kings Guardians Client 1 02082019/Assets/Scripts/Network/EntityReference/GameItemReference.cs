﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameItemReference 
{
    public string InstanceID { get; protected set; }
    public string ContentID { get; protected set; }

    public GameItemContent ContentData{ get{ return GetContentData<GameItemContent>(); } }
    public virtual string Description { get { return (ContentData != null) ? ContentData.GetDescription() : ""; } }

    public float LastUpdated = 0f;

    public void CreateTemporaryID()
    {
        InstanceID = ContentID + GUID.Create();
    }

    public virtual void Read(BaseInfo data)
    {
        InstanceID = data.InstanceID;
        ContentID = data.ContentID;
        LastUpdated = Time.time;
    }

    public T GetContentData<T>() where T : GameItemContent
    {
        return GameManager.Instance.GameContent.Get<T>(ContentID);
    }

}

