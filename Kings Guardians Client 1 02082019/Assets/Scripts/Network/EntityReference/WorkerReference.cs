﻿using UnityEngine;
using System.Collections;

public class WorkerReference : GameItemReference
{    
    public WorkerType Type { get; private set; }
    public QueueInfo WorkingOn { get; private set; }
    public BuildingReference Building { get; private set; }

    public bool Free 
    { 
        get 
        { 
            return WorkingOn == null;
        } 
    }

    public override void Read(BaseInfo info)
    {
        if (info.GetType() != typeof(WorkerInfo))
            return;

        base.Read(info);
        WorkerInfo wData = (WorkerInfo)info;

        Type = wData.Type;

        if (Building != null) Building.AssignedWorker = null;

        if (wData.BuildingInstanceID == "")
            WorkingOn = null;
        else
        {
            WorkingOn = wData.ActiveWork;  
            Building = GameManager.Instance.LocalPlayer.Buildings.Get(wData.BuildingInstanceID);

            if( Building != null ) Building.AssignedWorker = this;        
        }
    }
}
