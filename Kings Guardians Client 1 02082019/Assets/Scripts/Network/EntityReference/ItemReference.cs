﻿using UnityEngine;
using System.Collections;

public class ItemReference : GameItemReference
{
    public new ItemContent ContentData{ get { return GetContentData<ItemContent>(); } }

    public int Stacks { get; private set; }
   
	public override void Read(BaseInfo data)
    {
        if (data == null || (data.GetType() != typeof(ItemInfo) && !data.GetType().IsSubclassOf(typeof(ItemInfo))))
            return;
        
        ItemInfo IData = (ItemInfo)data;
        base.Read(data);
        Stacks = IData.Stacks;
    }
}
