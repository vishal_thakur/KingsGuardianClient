﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterReference : GameItemReference
{
    public new CharacterContent ContentData{ get { return GetContentData<CharacterContent>(); } }

    public float LevelProgress 
    {
        get
        {
            int xpNeed = LevelEnd - LevelStart;
            int xp = Exp - LevelStart;

            return (float)xp / (float)xpNeed;
        }
    }
    public int Exp{ get; set; }
    public int LevelStart{ get; private set; }
    public int LevelEnd{ get; private set; }
    public int Level { get; private set; }

    public int MaxHP  { get; private set; }
    public int CurrHP { get; set; }
    public int Shield { get; set; }

    public float Rage { get; set; }

    public int TotalStatUpgrades { get; private set; }
    public Dictionary<CharacterStats, int> Stats { get; private set; }

	public ItemReference Weapon { get; private set; }
	public List<ItemReference> Defences = new List<ItemReference>();
	public List<ItemReference> Inventory = new List<ItemReference>();

    public float Readiness;

	public void AddItem(ItemReference item)
    {
        switch (item.ContentData.Category)
        {
            case ItemCategory.Defence:
                Defences.Add(item);
                break;
            case ItemCategory.Weapon:
                Weapon = item;
                break;
            default:
                Inventory.Add(item);
                break;
        }
    }

	public ItemReference RemoveItem(string itemID)
    {
		ItemReference item = null;
        if (Weapon != null && Weapon.InstanceID == itemID)
        {
            item = Weapon;
            Weapon = null;
            return item;
        }

        foreach (var d in Defences)
        {
            if (d.InstanceID == itemID)
            {
                item = d;
                Defences.Remove(d);
                return item;
            }
        }

        foreach (var c in Inventory)
        {
            if (c.InstanceID == itemID)
            {
                item = c;
                Inventory.Remove(c);
                return item;
            }
        }
        return null;
    }

    public override void Read(BaseInfo info)
    {
        base.Read(info);
        CharacterInfo cData = (CharacterInfo)info;

        MaxHP = cData.MaxHP;
        CurrHP = cData.CurrentHP;
        Rage = cData.Rage;
        Shield = cData.Shield;

        Exp = cData.Exp;
        LevelStart = cData.ExpStart;
        LevelEnd = cData.ExpEnd;
        Level = cData.Level;

        if(Stats == null )
            Stats = new Dictionary<CharacterStats, int>();
        Stats.Clear();
        foreach (var stat in cData.Stats)
            Stats.Add(stat.Stat, stat.Value);

        TotalStatUpgrades = Stats.Count;

        if (cData.Weapon != null)
        {
			var weapon = new ItemReference();
            weapon.Read(cData.Weapon);
            Weapon = weapon;
        }
        else
            Weapon = null;

        Defences.Clear();
        foreach (var defInfo in cData.Defences)
        {
			var def = new ItemReference();
            def.Read(defInfo);
            Defences.Add(def);
        }

        Inventory.Clear();
        foreach (var iInfo in cData.Inventory)
        {   
            if (iInfo != null)
            {
				var item = new ItemReference();    
                item.Read(iInfo);
                Inventory.Add(item);
            }
            else
                Inventory.Add(null);           
        }
    }

    public void ReadSquad(SquadInfo s)
    {        
        ContentID = s.ContentID;
        InstanceID = s.InstanceID;
        MaxHP = s.MaxHP;
        CurrHP = s.HP;
        Level = s.Level;

        Readiness = s.Readiness;
    }
}
