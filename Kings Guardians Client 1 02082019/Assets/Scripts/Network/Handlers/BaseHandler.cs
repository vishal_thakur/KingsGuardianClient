﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

public class BaseHandler 
{
    public void RequestRecieved(NetworkMessage netMsg)
    {
        OnRequestRecieved(new Message(netMsg));
    }

    public void ErrorRecived(NetworkMessage netMsg)
    {
        OnRequestRecieved(new Message(netMsg));
    }

    public void EventRecieved(NetworkMessage netMsg)
    {
        OnEventRecieved(new Message(netMsg));
    }

    protected virtual void OnRequestRecieved(Message message)
    {

    }

    protected virtual void OnEventRecieved(Message message)
    {

    }

    protected virtual void OnErrorRecieved(Message message)
    {

    }


}

public class Message
{
    public readonly NetworkMessage NetMsg;

    public NetworkConnection Client { get; private set; }
    public RequestCode ResponseCode { get; private set; }
    public EventCode EventCode { get; private set; }

    public Message(NetworkMessage netMsg)
    {
        NetMsg = netMsg;
        Client = netMsg.conn;

        ResponseCode = (RequestCode)netMsg.msgType;
        EventCode = (EventCode)netMsg.msgType;
    }

    public T GetMessage<T>() where T : MessageBase, new() { return NetMsg.ReadMessage<T>(); } 
}
