﻿using UnityEngine;
using System.Collections;

public class CharacterHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        switch (message.ResponseCode)
        {
            case RequestCode.Equip: 
            case RequestCode.Unequip:
                var m = message.GetMessage<PlayerMessage>();
                GameManager.Instance.LocalPlayer.RefreshInventory(m.Inventory);
                GameManager.Instance.LocalPlayer.RefreshExsistedCharacters(m.Characters);
                break;     

        }
    }
}
