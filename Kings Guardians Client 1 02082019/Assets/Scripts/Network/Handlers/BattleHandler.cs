﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class BattleHandler : BaseHandler
{
    
    protected override void OnRequestRecieved(Message message)
    {
        switch (message.ResponseCode)
        {
		case RequestCode.StartBattle:
//			Debug.LogError ("Yudh Arambh!!!");
                GameManager.Instance.BattleStartMsg = message.GetMessage<BattleMessage>();
                LevelLoader.Instance.LoadScene("Battle");  
                break; 

            case RequestCode.TurnRequest :
                BattleLogic.Instance.ProcessCommandResult(message.GetMessage<BattleMessage>());
                break;        

            case RequestCode.BattleEnd:
                var msg = message.GetMessage<BattleMessage>();
                BattleLogic.Instance.Finished = true;
                BattleLogic.Instance.Won = msg.BattleEnd.Win;
                BattleLogic.Instance.ProcessCommandResult(msg);
                GameManager.Instance.LocalPlayer.RefreshMaterials(msg.BattleEnd.Materials);
                GameManager.Instance.LocalPlayer.RefreshCharacters(msg.BattleEnd.Characters);
                GameManager.Instance.LocalPlayer.RefreshMaterials(msg.BattleEnd.Materials);
                break;
        }
    }


    protected override void OnEventRecieved(Message message)
    {
        //switch (message.EventCode)
        //{
        //   /* case EventCode.RefreshMaterial:       
        //        var m = message.GetMessage<MaterialUpdateMessage>();
        //        GameManager.Instance.LocalPlayer.RefreshMaterials(m.Materials);                           
        //        break;  */       
        //}   
    }
}
