﻿using UnityEngine;
using System.Collections;

public class RaidHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        switch (message.ResponseCode)
        {
            case RequestCode.SpawnCharacter:
                RaidLogic.Instance.SpawnCharacterResponse(message.GetMessage<SimpleMessage>());
                break; 

            case RequestCode.GetNextReward:
                RaidLogic.Instance.NextReward(message.GetMessage<SimpleMessage>());
                break; 

			case RequestCode.ActivateRaidAbility:
				RaidLogic.Instance.RaidAbilityResponse(message.GetMessage<SimpleMessage>());
				break;

			case RequestCode.DestinationReached:
				RaidLogic.Instance.OnDestinationReached(message.GetMessage<SimpleMessage>());
				break;
        }
    }




    protected override void OnEventRecieved(Message message)
    {
		if (RaidLogic.Instance == null || message == null)
			return;

        switch (message.EventCode)
        {
		case EventCode.DamageTaken:
//			Debug.LogError (Time.time + "Attack");
				RaidLogic.Instance.UnitDamageTaken(message.GetMessage<RaidDamageTakenMessage>());          
				break;		

			case EventCode.AbilityPointUpdate:
				RaidLogic.Instance.AbilityPointsUpdate(message.GetMessage<SimpleMessage>());          
				break;	

			case EventCode.TargetUnit:
				RaidLogic.Instance.TargetSelectedEvent(message.GetMessage<SimpleMessage>());
				break; 

            case EventCode.RaidFinished:
                RaidLogic.Instance.RaidFinished(message.GetMessage<SimpleMessage>());          
                break;

			case EventCode.RaidAbilityEvent:
				RaidLogic.Instance.RaidAbilityResponse(message.GetMessage<SimpleMessage>());
				break;

			case EventCode.CustomCharacterSpawn:
				RaidLogic.Instance.CustomCharacterSpawn(message.GetMessage<CustomCharacterSpawn>());
				break;

			case EventCode.CustomRaidEvent:
				RaidLogic.Instance.CustomCharacterEvent(message.GetMessage<SimpleMessage>());
				break;
        }   
    }
}

