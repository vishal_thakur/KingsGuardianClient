﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        switch (message.ResponseCode)
        {
			case RequestCode.Collect:
				var mc = message.GetMessage<MaterialUpdateMessage>();
				if (mc != null)
					GameManager.Instance.LocalPlayer.RefreshMaterials(mc.Materials);
				break;

            case RequestCode.GetOpponents:            
                var m = message.GetMessage<PlayerListResponse>();
                GameManager.Instance.Opponents = m.Players;
                GameManager.Instance.OpponentsUpdated = Time.time;
                break;     

            case RequestCode.ChangeSquad:            
                GameManager.Instance.LocalPlayer.Read(message.GetMessage<PlayerMessage>());               
                break;     

            case RequestCode.GetOtherPlayer:
                GameManager.Instance.OnTargetPlayerSet(message.GetMessage<PlayerMessage>());   
                break;

        }
    }


    protected override void OnEventRecieved(Message message)
    {
        if (GameManager.Instance.LocalPlayer == null)
            return;

        switch (message.EventCode)
        {
            case EventCode.RefreshMaterial:       
                var m = message.GetMessage<MaterialUpdateMessage>();
                GameManager.Instance.LocalPlayer.RefreshMaterials(m.Materials);                           
                break;         
        }   
    }
}
