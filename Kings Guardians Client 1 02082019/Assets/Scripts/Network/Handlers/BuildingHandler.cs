﻿using UnityEngine;
using GridSystem;

public class BuildingHandler : BaseHandler
{
    protected override void OnRequestRecieved(Message message)
    {
        switch (message.ResponseCode)
        {
			case RequestCode.ConstructWall: 
			case RequestCode.ReplaceWall: 
				var w = message.GetMessage<WallResponse>();

				foreach (var wall in w.Walls)
				{
					var wallBuilding = TownLogic.Instance.TownLoader.GetBuilding(wall.TemporaryInstanceID);
					wallBuilding.Reference.Read(wall.Building);
					TownLogic.Instance.TownLoader.WallLogic.AddWall(wallBuilding);
				}

				TownLogic.Instance.TownLoader.WallLogic.Refresh();

				foreach (var r in w.RemoveThese)
					TownLogic.Instance.TownLoader.Destroy(r);  
               
				if(w.Materials != null)
					GameManager.Instance.LocalPlayer.RefreshMaterials(w.Materials);
				break;

            case RequestCode.Construct: 
            case RequestCode.Replace: 
            case RequestCode.Upgrade:
            case RequestCode.Repair:
            case RequestCode.CancelWorker:
            case RequestCode.Mine:
            case RequestCode.Craft:
            case RequestCode.CancelBuilding:                
                var m = message.GetMessage<BuildingResponse>();

                var building = TownLogic.Instance.TownLoader.GetBuilding((message.ResponseCode == RequestCode.Construct) ? m.TemporaryInstanceID : m.Building.InstanceID);
                building.Reference.Read(m.Building);

                if(m.Worker != null)
                    GameManager.Instance.LocalPlayer.RefreshWorker(m.Worker);                
                if(m.Materials != null)
                    GameManager.Instance.LocalPlayer.RefreshMaterials(m.Materials);
                break;

			case RequestCode.CreateWorker:
			case RequestCode.CreateWarlock:
				var msg = message.GetMessage<PlayerMessage>();
				GameManager.Instance.LocalPlayer.RefreshMaterials(msg.Materials);
				GameManager.Instance.LocalPlayer.RefreshWorkers(msg.Workers);   
				GameManager.Instance.LocalPlayer.RefreshWarlocks(msg.Warlocks);
                TownLogic.Instance.TownLoader.InstantiateMissingWorkers();
                break;

			case RequestCode.AttachWarlock:
			case RequestCode.DetachWarlock:
				var wMsg = message.GetMessage<PlayerMessage>();
				GameManager.Instance.LocalPlayer.RefreshWarlocks(wMsg.Warlocks);  
				break;
                
           
        }
    }

    protected override void OnErrorRecieved(Message message)
    {        
        var m = message.GetMessage<ErrorMessage>();
        switch (message.ResponseCode)
        {
            case RequestCode.Construct: 
                TownLogic.Instance.TownLoader.Destroy(m.InstanceID);                   
                break;

        }
    }

    protected override void OnEventRecieved(Message message)
    { 
        switch (message.EventCode)
        {
            case EventCode.WorkerJobDone: 
            case EventCode.MineProgressDone:              
                var m = message.GetMessage<BuildingResponse>();

                GameManager.Instance.LocalPlayer.RefreshBuilding(m.Building);
                GameManager.Instance.LocalPlayer.RefreshStorage();

                if(m.Worker != null)
                    GameManager.Instance.LocalPlayer.RefreshWorker(m.Worker);                
                if(m.Materials != null)
                    GameManager.Instance.LocalPlayer.RefreshMaterials(m.Materials);                           
                break;         

            case EventCode.CraftProgressDone: 
                GameManager.Instance.LocalPlayer.Read(message.GetMessage<PlayerMessage>());
                break;

        }   
    }

}
