﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player 
{
	public string Username{ get; private set; }
	public bool IsBanned{ get; private set; }

    public string DeviceID{ get; private set; }
    public string FacebookToken{ get; private set; }
    public string GoogleToken{ get; private set; }
    public Sprite Avatar { get; private set; }

    public float LevelProgress 
    {
        get
        {
            int xpNeed = LevelEnd - LevelStart;
            int xp = Exp - LevelStart;

            return (float)xp / (float)xpNeed;
        }
    }
    public int Exp{ get; private set; }
    public int LevelStart{ get; private set; }
    public int LevelEnd{ get; private set; }
    public int Level{ get; private set; }

    public int CastleUpgrade { get; set; }
      
    public Dictionary<Currency, int> Materials = new Dictionary<Currency, int>();
    public Dictionary<Currency, int> Storages = new Dictionary<Currency, int>();
	public Inventory<ItemReference> Inventory = new Inventory<ItemReference>();
    public Inventory<BuildingReference> Buildings = new Inventory<BuildingReference>();

	public List<CharacterReference> Characters = new List<CharacterReference>();
	public CharacterReference[] Squad = new CharacterReference[3];
    public string[] SquadCharacterIDs = new string[3];


	public List<WorkerReference> Workers = new List<WorkerReference>();
	public List<Warlock> Warlocks = new List<Warlock>();

    public int FreeWorkers { get { return (Workers.Count > 0) ? Workers.FindAll( x => x.Free).Count : 0; }}
	public int FreeWarlocks { get { return (Warlocks.Count > 0) ? Warlocks.FindAll( x => x.Building == "" || x.Building == null).Count : 0; }}

	public int MaxWorkers { get; private set; }
	public int MaxWarlocks { get; private set; }

	public int NextWorkerPrice { get; private set; }
	public int NextWarlockPrice { get; private set; }

    public float LastUpdate { get; private set; }

	public List<RemainsInfo> Remains { get; private set; }

	#region Get information
	public WorkerReference GetWorker(string instanceID)
    {
        return Workers.Find(x => x.InstanceID == instanceID);
    }
    public bool HasWorker()
    {
        bool has = FreeWorkers > 0;
        if( !has) Notification.Error("Builders are busy.");
        return has;
    }
    public bool HasWarlock()
    {
        bool has = FreeWarlocks > 0;
        if( !has) Notification.Error("Warlocks are busy.");
        return has;
    }

    public void AddMaterial(Currency material, int value)
    {
        if (Materials.ContainsKey(material))
        {
            Materials[material] += Mathf.Abs(value);
        }
    }

    public void RemoveMaterial(Currency material, int value)
    {
        if (Materials.ContainsKey(material))
            Materials[material] -= Mathf.Abs(value);
    }

    public int GetStorage(Currency material)
    {
        if (Storages.ContainsKey(material))
            return Storages[material];
        return 0;
    }

    public int GetMaterial(Currency material)
    {
        if (Materials.ContainsKey(material))
            return Materials[material];
        return 0;
    }

    public bool HasMaterial (Currency material, int amount)
    {
        if (GetMaterial(material) < amount)
        {
            Notification.Error("Not enough " + material.ToString());
            return false;
        }
        return true;
    }

    public bool HasMaterials(List<SMaterial> cost)
    {
        foreach (var p in cost)
        {
            if (!HasMaterial(p.Material, p.Amount))
                return false;
        }
        return true;
    }


	public class BuildingLimitInfo
	{
		public int Built;
		public int Limit;
		public bool Full { get { return Built == Limit; }}
	}

	public BuildingLimitInfo GetBuildingLimit(BuildingContent building)
	{
		BuildingLimitInfo info = new BuildingLimitInfo();

		int castleUpgrade = GameManager.Instance.LocalPlayer.CastleUpgrade;
		if (castleUpgrade >= building.MaxAmount.Length)
			castleUpgrade = building.MaxAmount.Length - 1;
		info.Limit = building.MaxAmount[castleUpgrade-1];

		info.Built = GameManager.Instance.LocalPlayer.Buildings.Count(building.ContentID);

		return info;
	}

	public int GetCastleUpgradeLimitIncreases(BuildingContent building, int limit = -1)
	{
		if (limit < 0)
			limit = GetBuildingLimit(building).Limit;

		for (int i = CastleUpgrade-1; i < building.MaxAmount.Length; i++)
		{
			if (building.MaxAmount[i] > limit)
			{
				return i + 1;
			}
		}

		return 0;
	}

	public Dictionary<string, int> GetNextCastleUpgradeInfo()
	{
		Dictionary<string, int> info = new Dictionary<string, int>();
		foreach (var b in GameManager.Instance.GameContent.Buildings)
		{
			if (b.Value.ContentID == "Castle")
				continue;
				

			int u = CastleUpgrade;
			if (u >= b.Value.MaxAmount.Length)
				u = b.Value.MaxAmount.Length - 1;

			int limit = b.Value.MaxAmount[u];

			u++;
			if (u >= b.Value.MaxAmount.Length)				
				u = b.Value.MaxAmount.Length - 1;
			int nextLimit = b.Value.MaxAmount[u];

			if (limit != nextLimit)
				info.Add(b.Value.Name, nextLimit - limit);
		}
		return info;
	}

    #endregion


    #region READ
    public void Read(PlayerMessage message)
    {
        if (message == null)
            return;

        if (message.Account != null)
            RefreshPlayerData(message.Account);

        if( message.Materials != null && message.Materials.Count > 0 )
            RefreshMaterials(message.Materials);

        if (message.Buildings != null && message.Buildings.Count > 0)
        {
            RefreshBuildings(message.Buildings);
            var castle = Buildings.Items.Find(x => x.ContentID == "Castle");
            CastleUpgrade = (castle != null) ? castle.Upgrade : 0;
        }

        if( message.Inventory != null && message.Inventory.Count > 0 )
            RefreshInventory(message.Inventory);
        
        if( message.Workers != null && message.Workers.Count > 0 )
            RefreshWorkers(message.Workers);

		if (message.Warlocks != null && message.Warlocks.Count > 0)
			RefreshWarlocks(message.Warlocks);
        
        if( message.Characters != null && message.Characters.Count > 0 )
            RefreshCharacters(message.Characters);
        
        if( message.Squad != null && message.Squad.Count > 0 )
            RefreshSquad(message.Squad);   

		Remains = (message.Remains != null) ? message.Remains : new List<RemainsInfo>();
		       
        LastUpdate = Time.time;
    }


 
    public void UpdateAvatar(string avatarID)
    {
        Avatar = AssetPackManager.GetAvatar(avatarID);
    }

    public void RefreshPlayerData(PlayerInfo account)
    {
        Username = account.Username;
		IsBanned = account.IsBanned;
        DeviceID = account.DeviceID;
        FacebookToken = account.FacebookToken;
        GoogleToken = account.GoogleToken;
        UpdateAvatar(account.AvatarID);

        Exp = account.Exp;
        LevelStart = account.ExpStart;
        LevelEnd = account.ExpEnd;
        Level = account.Level;
    }

    public void RefreshMaterials(List<SMaterial> mats)
    {
        if (Materials == null) Materials = new Dictionary<Currency, int>();
       
        foreach (var mat in mats)
        {
            if (Materials.ContainsKey(mat.Material))
                Materials[mat.Material] = mat.Amount;
            else 
                Materials.Add(mat.Material, mat.Amount);   
        }   
        LastUpdate = Time.time;
    }

    public void RefreshWorker(WorkerInfo worker)
    {
        var w = GetWorker(worker.InstanceID);
        if (w != null)
            w.Read(worker);
        LastUpdate = Time.time;
		CalculateMaxWorkers();
    }


    public void RefreshWorkers(List<WorkerInfo> workers)
    {
        Workers.Clear();
        foreach (var w in workers)
        {
			WorkerReference worker = new WorkerReference();
            worker.Read(w);
            Workers.Add(worker);
        }
		CalculateMaxWorkers();
    }

	public void RefreshWarlocks(List<Warlock> warlocks)
	{
		Warlocks = warlocks;
		foreach (var b in Buildings.GetAll())
			b.LastUpdated = Time.time;

		CalculateMaxWarlocks();
	}

    void RefreshBuildings(List<BuildingInfo> buildings)
    {
        foreach (var b in buildings)
            RefreshBuilding(b);
        RefreshStorage();
    }

    public void RefreshBuilding(BuildingInfo b)
    {
        BuildingReference building = Buildings.Get(b.InstanceID);
        if (building == null)
        {
            building = new BuildingReference();
            Buildings.Add(building);
        }               

        building.Read(b);

		if (building.ContentID == "Castle")
		{
			CastleUpgrade = building.Upgrade;
			CalculateMaxWarlocks();
			CalculateMaxWorkers();
		}
	}


    public void RefreshInventory(List<ItemInfo> items)
    {
        Inventory.Items.Clear();

        foreach (var i in items)
        {
			var item = new ItemReference();
            item.Read(i);
            Inventory.Add(item);           
        }
    }

    public void RefreshExsistedCharacters(List<CharacterInfo> list)
    {
        foreach (var c in list)
        {
            var character = Characters.Find(x => x.InstanceID == c.InstanceID);
            if (character != null)
                character.Read(c);
        }
    }

    public void RefreshCharacters(List<CharacterInfo> list)
    {
        if (Characters == null)
			Characters = new List<CharacterReference>();
        Characters.Clear();

        foreach (var c in list)
        {
			var character = new CharacterReference();
            character.Read(c);
            Characters.Add(character);         
        }          
    }

    public void RefreshSquad(List<string> squad)
    {
        for (int i = 0; i < squad.Count; i++)
        {
            if (squad[i] == "")
                Squad[i] = null;
            else
            {
                var c = Characters.Find(x => x.InstanceID == squad[i]);
                Squad[i] = c;
                SquadCharacterIDs[i] = (c != null) ? c.InstanceID : "";
            }
        } 
        LastUpdate = Time.time;
    }

    public void RefreshSquad()
    {
        for (int cnt = 0; cnt < Squad.Length; cnt++)
        {
            if (Squad[cnt] != null && Squad[cnt].InstanceID != "")
                Squad[cnt] = Characters.Find(x => x.InstanceID == Squad[cnt].InstanceID);
            else
                Squad[cnt] = null;
        }
    }

    public void RefreshStorage()
    {   
        Storages.Clear();    
        foreach (var b in Buildings.Items)
        {            
            if (b.ContentData.Category == BuildingCategory.Resource_storageOnly || b.ContentData.Category == BuildingCategory.Resource_mine)
            {
                if (Storages.ContainsKey(b.ContentData.Material))
                    Storages[b.ContentData.Material] += b.ContentData.GetUpgrade(b.Upgrade).Storage;
                else
                    Storages.Add(b.ContentData.Material, b.ContentData.GetUpgrade(b.Upgrade).Storage);
            } 
        }       
    }
    #endregion

	void CalculateMaxWorkers()
	{
		var castle = Buildings.Get("Castle");
		if (castle == null)
			return;
		
		MaxWorkers = 0;
		NextWorkerPrice = 0;

		for (int u = 1; u <= castle.Upgrade; u++)
		{
			if (castle.ContentData.GetUpgrade(u).WorkerPrice >= 0)
			{
				if (Workers.Count == MaxWorkers)
					NextWorkerPrice = castle.ContentData.GetUpgrade(u).WorkerPrice;
				MaxWorkers++;
			}
		}
	}

	void CalculateMaxWarlocks()
	{
		var castle = Buildings.Get("Castle");
		if (castle == null)
			return;

		MaxWarlocks = 0;
		NextWarlockPrice = 0;

		for (int u = 1; u <= castle.Upgrade; u++)
		{
			if (castle.ContentData.GetUpgrade(u).WarlockPrice >= 0)
			{
				if (Warlocks.Count == MaxWarlocks)
					NextWarlockPrice = castle.ContentData.GetUpgrade(u).WarlockPrice;
				MaxWarlocks++;
			}
		}
	}

}
