﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayoutManager : SingletonMono<LayoutManager>{

	public bool IsLayoutModeActive = false;

	public bool InitialBuildingMoved = false;


	[SerializeField] public List<BuildingBehaviour> AllUnits = new List<BuildingBehaviour>();

	// Use this for initialization
	public void AddUnitToLayoutManager(BuildingBehaviour unit) {
		AllUnits.Add (unit);
	}

	public void LoadAvailableBuildings(){
		TownBuildingBehaviour[] tempAllUnits = GameObject.FindObjectsOfType<TownBuildingBehaviour> ();

		foreach(TownBuildingBehaviour unit in tempAllUnits)
			AllUnits.Add(unit);
	}


	//Signal Building to save their original pos and rot because we are entering editing mode
	public void Initialize(){
		LoadAvailableBuildings ();
		StartCoroutine (SaveLayoutBeforeEditing());
	}



	//-----------------------Core Logic of Base Layout Editing-----------------------

	//Remove a single Unit from Base and place it into UI List
	public void RemoveUnit(LayoutManagerWindow layoutManagerWindow){
		//Hide Building from Base
		TownLogic.instance.SelectedBuilding.gameObject.SetActive (false);

		//Show Unit in The UI List
//		layoutManagerWindow.
	}



	//Remove All Units from Base and place them into UI List
	public void RemoveAll(){
	
	}



	//Saves the Editied Layout and Sets it as current Layout
	//Discards Old Layout
	//Works Only if All Units are placed in the base and the UI list is empty
	public void SaveLayout(){
		IsLayoutModeActive = false;
		StartCoroutine (SaveCurrentLayout());
		
	}



	//Exits The Base Layout Editing Mode and goes back to Village 
	public void Cancel(){
		IsLayoutModeActive = false;
		StartCoroutine (RepositionBuildingsToCurrentLayout());
//		TownLogic.instance.OnLoadingDone ();
	}


	IEnumerator RepositionBuildingsToCurrentLayout(){
		foreach (BuildingBehaviour building in AllUnits) {
//			Debug.LogError ("Pos and Rot reset for\t" + building.name);
			yield return null;
			building.ResetPositionAndRotationCurrentLayout ();
		}
	}


	IEnumerator SaveCurrentLayout(){
		foreach (BuildingBehaviour building in AllUnits) {
//			Debug.LogError ("Saving \t" + building.name);
			yield return null;
			building.SaveTransformToServer ();
		}
	}


	IEnumerator SaveLayoutBeforeEditing(){
		foreach (BuildingBehaviour building in AllUnits) {
//			Debug.LogError ("Entered editing mode\tPos and Rot saved for\t" + building.name);
			yield return null;
			building.SavePosAndRot ();
		}
	}

	//------------------------------------------------------------------------------------
}
