﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GridSystem;

public class LayoutManagerWindow : MonoBehaviour {
	[SerializeField] GameObject BuildingListUnit;

	[SerializeField] RectTransform BuildingListParent;

	[SerializeField] GameObject InteractionUI;

//
//	struct LayoutUIListUnit{
//		public string name;
//	}

	void Update(){
//		if (GridOverlay.Instance.ShowOverlay)
//			InteractionUI.SetActive (true);
//		else
//			InteractionUI.SetActive (false);
	}


	void OnDisable(){
		GridOverlay.Instance.ShowOverlay = false;
		LayoutManager.Instance.InitialBuildingMoved = false;
		Messenger.RemoveListener (Events.OnBuildingSelected , OnBuildingSelected);
	}


	public void Init(){
		Messenger.AddListener(Events.OnBuildingSelected , OnBuildingSelected);
		LayoutManager.Instance.Initialize ();
		LayoutManager.Instance.IsLayoutModeActive = true;
		GridOverlay.Instance.ShowOverlay = true;
	}



	void OnBuildingSelected(){
//		Debug.LogError ("Building Selected");
		InteractionUI.SetActive (true);
		LayoutManager.Instance.InitialBuildingMoved = true;
	}

	public void OnSave(){
		LayoutManager.Instance.SaveLayout ();
	}



	//Remove Selected Building from the base and PLace it in The UI List
	public void OnRemove(){
//		LayoutManager.Instance.RemoveUnit ();
	}


	//Remove Selected Building from the base and PLace it in The UI List
	public void OnRemoveAll(){
		LayoutManager.Instance.RemoveAll ();
	}



	public void OnCancel(){
		//Hide LayoutEditing UI
		//Show Village UI
		LayoutManager.Instance.Cancel();
	}


//	/// <summary>
//	/// Adds the unit to list.
//	/// </summary>
//	void AddUnitToList(){
//		//Load Unit Dummy Layout
//		Instantiate(BuildingListUnit , BuildingListParent);
//
//	}
}
