﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarLane : MonoBehaviour {

    public enum LaneType
    {
        left,
        center,
        right
    }


    [SerializeField]
    LaneType myLaneType;

    

    public LaneType GetLaneType()
    {
        return myLaneType;
    }

}
