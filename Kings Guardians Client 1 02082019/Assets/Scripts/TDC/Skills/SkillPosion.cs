﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPosion : SkillPlay
{
    public int damage;
    public List<AIUnit> listUnits = new List<AIUnit>();

    public GameObject poisonPrefabEffect;

    int counter = 5;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void StartSkill()
    {
        base.StartSkill();

        StartCoroutine(DelayStartSkill());
    }

    private IEnumerator DelayStartSkill()
    {
        print("Start skill area");

        foreach (AIUnit unit in listUnits)
        {
            print("Start skill area find");

            if (unit.typeUnit == AIUnit.TUnit.Enemy)
            {
                if (unit.currentHealth > 0)
                {
                    Instantiate(poisonPrefabEffect, unit.transform);
                    print("Start skill area find damage");
                    unit.Damage(damage);
                }

                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();

                //unit.GetComponent<Rigidbody>().AddForce((unit.transform.position - transform.position).normalized * 5, ForceMode.Impulse);
            }
        }

        yield return new WaitForSeconds(2f);

        if(counter > 0)
        {
            counter--;
            StartCoroutine(DelayStartSkill());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (startSkill) { return; }

        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (!listUnits.Contains(unit))
            {
                listUnits.Add(unit);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (startSkill) { return; }

        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (listUnits.Contains(unit))
            {
                listUnits.Remove(unit);
            }
        }
    }
}
