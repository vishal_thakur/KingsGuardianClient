﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPlay : MonoBehaviour
{
    [Header("Data")]
    public float interactionDelay = 0;
    public float destroyTimer = 5;
    private float curTimer = 0;

    [Header("Effect")]
    public GameObject prefab;

    public bool startSkill = false;

    #region Unity

    protected virtual void Start()
    {
        //if (prefab) { prefab.SetActive(false); }
    }

    protected virtual void Update()
    {
        if (!startSkill) { return; }

        if(curTimer >= destroyTimer)
        {
            EndSkill();
            Destroy(gameObject);
        }
        else
        {
            curTimer += Time.deltaTime;
        }
    }

    public virtual void StartSkill()
    {
        if (prefab) { prefab.SetActive(true); }
        startSkill = true;
    }

    public virtual void EndSkill()
    {
        startSkill = false;
    }

    #endregion
}