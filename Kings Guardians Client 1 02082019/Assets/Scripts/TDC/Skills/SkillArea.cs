﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillArea : SkillPlay
{
    public int damage;
    public List<AIUnit> listUnits = new List<AIUnit>();

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void StartSkill()
    {
        base.StartSkill();

        StartCoroutine(DelayStartSkill());
    }

    private IEnumerator DelayStartSkill()
    {
        print("Start skill area");

        foreach (AIUnit unit in listUnits)
        {
            print("Start skill area find");

            if (unit.typeUnit == AIUnit.TUnit.Enemy)
            {
                print("Start skill area find damage");
                unit.Damage(damage);

                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();

                //unit.GetComponent<Rigidbody>().AddForce((unit.transform.position - transform.position).normalized * 5, ForceMode.Impulse);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (!listUnits.Contains(unit))
            {
                listUnits.Add(unit);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Unit")
        {
            AIUnit unit = other.GetComponent<AIUnit>();

            if (listUnits.Contains(unit))
            {
                listUnits.Remove(unit);
            }
        }
    }
}
