﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISupport : MonoBehaviour
{
    [Header("Data")]
    public int indexParent;
    public float speed = 1f;
    public bool invers = false;

    public AIUnit aiUnit;
    public Animator animControl;
    public Rigidbody rig;

    public AIDataParent aiDataParent;
    public int currentStep = 0;
    private Transform localTranscform;
    private Vector3 direction;

    #region Unity

    private void Update()
    {
        CoreUpdate();
    }

    #endregion

    #region Core

    public void Initialization(AIDataParent dataParent)
    {
        aiDataParent = dataParent;
        localTranscform = transform;

        try
        {
            if (invers)
            {
                localTranscform.position = dataParent.listPoints[dataParent.indexEnemySpawn].point.position;
                currentStep = dataParent.indexEnemySpawn;
            }
            else
            {
                //localTranscform.position = dataParent.listPoints[dataParent.indexAllySpawn].point.position;
                //currentStep = dataParent.indexAllySpawn;
            }
        }
        catch(System.Exception e)
        {
            Debug.LogError("araa");
        }
    }

    public void CoreUpdate()
    {
        
    }

    string distanceString = "null";


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        GUIStyle style = new GUIStyle();

        style.fontSize = 25;
        //UnityEditor.Handles.Label(transform.position, "distance = " + distanceString + "  |  Current Step = " + currentStep, style);
        UnityEditor.Handles.Label(transform.position, "forward = " + transform.forward.z, style);
    }
#endif

    public void Locomotion(Vector3 position, bool folowPoint = false)
    {
        if (aiUnit.freeze) { return; }
        if (aiUnit.death) { return; }

        //Standing Idle State
        if (aiUnit.typeState == AIUnit.TState.Idle)
        {
            animControl.SetBool("Movement", false);
            Rotate((position - transform.position).normalized);
            return;
        }
        //Attacking State
        else if (aiUnit.typeState == AIUnit.TState.Attack)
        {
            animControl.SetBool("Movement", false);
            Rotate((position - transform.position).normalized);
            return;
        }
        //Movement State
        else if (!aiUnit.targetTower && aiUnit.typeState == AIUnit.TState.Attack)
        {
            if (animControl && currentStep >= aiDataParent.listPoints.Count || currentStep < 0 || aiUnit.typeState == AIUnit.TState.Attack)
            {
                animControl.SetBool("Movement", false);
                return;
            }
        }

        float distacne = Vector3.Distance(localTranscform.position, position);
        distanceString = ((int)distacne).ToString();
        if (distacne > 7f)
        {
            var Heg = position - localTranscform.position;
            var Dist = Heg.magnitude;

            direction = Vector3.Lerp(direction, Heg / Dist, 0.05f);

            rig.velocity = Vector3.Lerp(rig.velocity, (direction) * (speed), 0.1f);

            if (animControl)
            {
                if (aiUnit.typeState == AIUnit.TState.Attack)
                {
                    animControl.SetBool("Movement", false);
                    Rotate((aiUnit.listEnemyUnits[aiUnit.indexEnemy].transform.position - transform.position).normalized);
                }
                else
                {
                    animControl.SetBool("Movement", true);
                    Rotate(direction);
                }
            }
        }
        else if(folowPoint)
        {
            bool canDebug = false;//GetComponent<AIUnit>().typeUnit == AIUnit.TUnit.Enemy;

            if(canDebug)
                Debug.LogError("Gonna change Current Step = " + currentStep);
            if(invers)
            {

                currentStep--;
                if (canDebug)
                    Debug.LogError("Decrement Current Step = " + currentStep);
            }
            else
            {
                currentStep++;
                if (canDebug)
                    Debug.LogError("Increment Current Step = " + currentStep);
            }
        }
    }

    public void Rotate(Vector3 dir)
    {
        Quaternion lerpRotate = Quaternion.LookRotation(dir, Vector3.up);

        localTranscform.rotation = Quaternion.Lerp(localTranscform.rotation, lerpRotate, 0.05f);
    }

    #endregion
}
