﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIUnit : MonoBehaviour
{
    public enum TObject
    {
        Unit,
        Tower
    }

    public enum TUnit
    {
        Enemy,
        Ally
    }

    public enum TState
    {
        Idle,
        Movement,
        Attack
    }

    public enum TAttack
    {
        Mele,
        Range
    }

    [Header("Data")]
    public TObject typeObject = TObject.Unit;
    public TState typeState = TState.Idle;
    public TUnit typeUnit;
    public TAttack typeAttack = TAttack.Mele;
    public List<AIUnit> listEnemyUnits = new List<AIUnit>();
    public bool death = false;
    public AIUnit targetTower;
    public AIUnit targetUnit;
    public NavMeshAgent navAgent;

    [Space]
    [Header("Health")]
    public int maxHealth = 100;
    public int currentHealth = 100;

    [Space]
    [Header("Damage")]
    public int minDamage = 8;
    public int maxDamage = 16;
    public float attackDistance = 1f;
    public float delayAttack = 1f;

    public AISupport aiSupport;
    public LayerMask layerView;
    private float timerAttack = 0;
    private List<Collider> listRagdollCollider = new List<Collider>();
    private CapsuleCollider colliderSoldier;

    public bool stateInitialization = false;
    public int indexEnemy = 0;

    public bool freeze = false;

    //[Space(10), SerializeField]
   // SkinnedMeshRenderer[] troopMeshRef;

    [SerializeField]
    Material redMat;
    [SerializeField]
    Material greenMat;
    #region Unity

    private void Awake()
    {
        aiSupport = GetComponent<AISupport>();
        colliderSoldier = GetComponent<CapsuleCollider>();

        navAgent = GetComponent<NavMeshAgent>();

    }


    //IEnumerator PaintTroopAsPerBaseSide()
    //{
    //    //yield return new WaitForSeconds(2);
    //    if (typeUnit == TUnit.Ally)
    //    {
    //        //Green
    //        foreach (SkinnedMeshRenderer mesh in troopMeshRef)
    //        {
    //            yield return null;
                
    //            mesh.material = greenMat;
    //        }
    //    }
    //    else if(typeUnit == TUnit.Enemy)
    //    {
    //        //Red
    //        foreach (SkinnedMeshRenderer mesh in troopMeshRef)
    //        {
    //            yield return null;
    //            mesh.material = redMat;
    //        }
    //    }
    //}

    private void Update()
    {
        CoreUpdate();
    }

    #endregion

    #region Core

    public void Initialization(int indexpart)
    {
        if (stateInitialization) { return; }


        //StartCoroutine(PaintTroopAsPerBaseSide());

        if (typeObject != TObject.Tower)
        {
            aiSupport.indexParent = indexpart;
            aiSupport.Initialization(AIManager.Instance.listData[indexpart]);

            foreach (Collider box in aiSupport.animControl.GetComponentsInChildren<Collider>())
            {
                box.enabled = false;
                box.GetComponent<Rigidbody>().isKinematic = true;
                listRagdollCollider.Add(box);
            }
        }

        InvokeRepeating("FindEnemy", 1f, 1f);

        stateInitialization = true;
    }


    private void OnDisable()
    {
        CancelInvoke();
    }

    private void FindEnemy()
    {
        if (listEnemyUnits.Count == 0) { return; }

        indexEnemy = 0;

        if (listEnemyUnits.Count == 1) { return; }

        float curDistance = Vector3.Distance(transform.position, listEnemyUnits[0].transform.position);

        for (int i = 1; i < listEnemyUnits.Count; i++)
        {
            float findDistance = Vector3.Distance(transform.position, listEnemyUnits[i].transform.position);

            if (findDistance < curDistance)
            {
                curDistance = findDistance;
                indexEnemy = i;
            }
        }
    }

    private void CoreUpdate()
    {
        if (!stateInitialization) { return; }

        if(freeze)
        {
            aiSupport.rig.isKinematic = true;
            return;
        }

        if (aiSupport.animControl)
        {
            if (aiSupport.animControl.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
            {
                aiSupport.rig.isKinematic = true;
            }
            else if (aiSupport.animControl.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
            {
                aiSupport.rig.isKinematic = true;
            }
            else if (aiSupport.animControl.GetCurrentAnimatorStateInfo(0).IsTag("Movement"))
            {
                aiSupport.rig.isKinematic = false;
            }

            ViewControl();
        }
    }

    private void ViewControl()
    {
        if (!stateInitialization) { return; }
        if (death) { return; }
        if (typeObject == TObject.Tower) { return; }
        if (freeze) { return; }

        if (timerAttack < delayAttack)
        {
            timerAttack += Time.deltaTime;
        }

        if (listEnemyUnits.Count > 0 && indexEnemy < listEnemyUnits.Count && listEnemyUnits[indexEnemy])
        {
            if (Vector3.Distance(transform.position, listEnemyUnits[indexEnemy].transform.position) <= attackDistance)
            {
                targetUnit = GetEnemy();

                if (targetUnit)
                {

                    typeState = TState.Attack;

                    navAgent.radius = Mathf.Lerp(navAgent.radius, 0.3f, 0.03f);

                    aiSupport.Locomotion(targetUnit.transform.position);

                    if (timerAttack >= delayAttack)
                    {
                        aiSupport.animControl.SetTrigger("Attack");
                        //StartCoroutine(InvokeDamage(getEnemy));
                        timerAttack = 0;
                    }
                }
            }
            else
            {
                if (GetRay(listEnemyUnits[indexEnemy].transform.position))
                {
                    navAgent.radius = Mathf.Lerp(navAgent.radius, 0.3f, 0.03f);
                    navAgent.SetDestination(listEnemyUnits[indexEnemy].transform.position);
                    aiSupport.Locomotion(navAgent.steeringTarget);
                    //aiSupport.Locomotion(listEnemyUnits[indexEnemy].transform.position);
                    aiSupport.animControl.ResetTrigger("Attack");
                    typeState = TState.Movement;

                    navAgent.enabled = true;
                }
                else
                {
                    typeState = TState.Idle;
                }
            }

            //colliderSoldier.enabled = true;

        }
        else
        {
            navAgent.radius = Mathf.Lerp(navAgent.radius, 0.05f, 0.03f);
            navAgent.enabled = true;
            aiSupport.animControl.ResetTrigger("Attack");
            //typeState = TState.Movement;
            timerAttack = 0;

            int bufferStep = aiSupport.currentStep;
            
            if (aiSupport.currentStep >= 0 && aiSupport.currentStep < aiSupport.aiDataParent.listPoints.Count)
            {
                //if(typeUnit == TUnit.Enemy)
                //    Debug.LogError("Step was " + aiSupport.currentStep);
                if (typeState == TState.Attack)
                {
                    float distance = Vector3.Distance(transform.position, aiSupport.aiDataParent.listPoints[aiSupport.currentStep].point.position);
                    int indexStep = aiSupport.currentStep;
                    float angle = 0;
                    for (int i = 0; i < aiSupport.aiDataParent.listPoints.Count; i++)
                    {
                        //Distance Between this and next Listpoint
                        float findDistance = Vector3.Distance(transform.position, aiSupport.aiDataParent.listPoints[i].point.position);

                        //Debug.LogError("findDistance = " + findDistance);

                        angle = Vector3.Angle((aiSupport.aiDataParent.listPoints[i].point.position - transform.position).normalized, (aiSupport.aiDataParent.listPoints[aiSupport.currentStep].point.position - transform.position).normalized);


                        //Debug.LogError("angle = " + angle);
                        if (angle < 30 && findDistance < distance)
                        {
                            distance = findDistance;
                            indexStep = aiSupport.aiDataParent.listPoints[i].index;
                        }
                    }
                    aiSupport.currentStep = indexStep;
                    //if (typeUnit == TUnit.Enemy)
                    //    Debug.LogError("Changed to " + aiSupport.currentStep);
                }

                typeState = TState.Movement;
                aiSupport.Locomotion(aiSupport.aiDataParent.listPoints[aiSupport.currentStep].point.position, true);
            }
            else if(!targetTower)
            {
                List<AIUnit> buffer = new List<AIUnit>();

                if (typeUnit == TUnit.Ally)
                {
                    buffer = new List<AIUnit>(AIManager.Instance.listEnemyTower);
                }
                else if(typeUnit == TUnit.Enemy)
                {
                    buffer = new List<AIUnit>(AIManager.Instance.listAllyTower);
                }

                float distance = 999;
                AIUnit findTower = null;

                for (int i = 0; i < buffer.Count; i++)
                {
                    if (buffer[i] != null)
                    {
                        if (findTower == null) { findTower = buffer[i]; }

                        float findDistance = Vector3.Distance(transform.position, buffer[i].transform.position);

                        if (findDistance < distance)
                        {
                            distance = findDistance;
                            findTower = buffer[i];
                        }
                    }
                }

                if (findTower)
                {
                    targetTower = findTower;
                }
                else
                {
                    aiSupport.Locomotion(Vector3.zero);
                    typeState = TState.Idle;
                }
            }
            else if(targetTower)
            {
                aiSupport.Locomotion(targetTower.transform.position);
                typeState = TState.Movement;
            }
        }
    }

    private bool GetRay(Vector3 pos)
    {
        Vector3 fixPos = transform.position;
        fixPos.y += 0.5f;

        Ray ray = new Ray(fixPos, (pos - transform.position));
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, attackDistance / 3, layerView))
        {
            print(hit.transform.name);
            return false;
        }
        else
        {
            return true;
        }
    }

    private AIUnit GetEnemy()
    {
        float findDistance = 0;


        //If it has only one enemy
        if(listEnemyUnits.Count == 1)
        {
            //Donot Return null 
            if (listEnemyUnits[0])
                return listEnemyUnits[0];
            else//Clear the list
            {
                listEnemyUnits.Clear();
                return null;
            }
        }
        //If has more than multiple enemies
        else
        {
            try
            {
                AIUnit findUnit = null;

                //Find distance of the First Enemy in the list
                if (listEnemyUnits[0])
                {
                    findDistance = Vector3.Distance(transform.position, listEnemyUnits[0].transform.position);
                    findUnit = listEnemyUnits[0];
                }
                else
                    listEnemyUnits.RemoveAt(0);

                for (int i = 1; i < listEnemyUnits.Count; i++)
                {
                    float newDinstace = Vector3.Distance(transform.position, listEnemyUnits[i].transform.position);

                    if (findDistance > newDinstace)
                    {
                        findDistance = newDinstace;
                        findUnit = listEnemyUnits[i];
                    }
                }

                return findUnit;
            }
            catch(System.Exception e)
            {
                //Debug.LogError("Something wrong here" + e.ToString());
                findDistance = 0;
                //for(int i=0;i<listEnemyUnits.Count;i++)
                //{
                //    if (listEnemyUnits[i] == null)
                //        listEnemyUnits.RemoveAt(i);
                //}
                return null;
            }
        }
    }

    public void DamageTarget(AIUnit unitTarget)
    {
        if (!stateInitialization) { return; }
        if(unitTarget)
            unitTarget.Damage(Random.Range(minDamage, maxDamage));
    }


    //Take Damage , Unit shall die if health less than 0
    public void Damage(int damage)
    {
        if (!stateInitialization) { return; }

        //No point of Damaging a dead Unit
        if (death) { return; }
        
        //Reduce Health 
        currentHealth -= damage;

        //All Health Depleted, Unit Will die
        if (currentHealth <= 0)
        {
            Death();
        }
        //Unit takes damage, Signal Animator to play the corresponding Animation
        else
        {
            if (typeObject == TObject.Unit) { aiSupport.animControl.SetTrigger("Hit"); }
        }
    }

    private void Death()
    {
        if (!stateInitialization) { return; }

        //What happens on Death for Tower | Unit
        switch(typeObject)
        {
            case TObject.Tower:
                //Mark Death boolean as true which states that this Unit is dead
                death = true;

                //Look for Result  of the battle
                GameplayBattle.instance.CellResultBattle(GameplayBattle.ResultBattle.processing);
                Destroy(gameObject);
                return;
            case TObject.Unit:
                if (aiSupport.animControl)
                {
                    if (typeUnit == TUnit.Ally)
                    {
                        AIManager.Instance.listAllyUnits.Remove(this);
                    }
                    else if (typeUnit == TUnit.Enemy)
                    {
                        AIManager.Instance.listEnemyUnits.Remove(this);
                    }

                    aiSupport.animControl.enabled = false;
                    death = true;
                    GetComponent<CapsuleCollider>().enabled = false;

                    for (int i = 0; i < listRagdollCollider.Count; i++)
                    {
                        listRagdollCollider[i].enabled = true;
                        if (listRagdollCollider[i].GetComponent<Rigidbody>()) { listRagdollCollider[i].GetComponent<Rigidbody>().isKinematic = false; }
                    }

                    Destroy(gameObject);
                }
                return;
        }
    }

    #endregion
}
