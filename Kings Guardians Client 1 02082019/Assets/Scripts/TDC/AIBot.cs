﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBot : MonoBehaviour
{
    public static AIBot instance;

    public List<DataCard> listCard = new List<DataCard>();

    [SerializeField]
    bool canSpawnTroops;


    [Header("Reaction")]
    [Range(0f,200f)] public float timeReaction;
    [Range(0f, 10f)] public float offsetReaction;
    public float pointReaction;
    [Range(0f, 10f)] public float curReaction;

    #region Unity

    private void Awake()
    {
        instance = this;
    }


    private void OnLevelWasLoaded(int level)
    {
        //Debug.LogError("initializing");
        Initialization();
    }

    //private void Start()
    //{
    //    Initialization();
    //}

    private void Update()
    {
        CoreUpdate();

        if (Input.GetKeyUp(KeyCode.Space))
            canSpawnTroops = !canSpawnTroops;
    }

    #endregion

    #region Core

    public void Initialization()
    {
        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {
            GameplayBattle.instance.GetStateMana(DataMana.TType.Bot);

            //Clear list so that it has the updated cards lists as  per the player
            listCard.Clear();

            //Only add Units , Skip units for now as they donot have Aiunit script over them(potential future cards)
            foreach(DataCard card in DatabaseCard.instance.listCard)
            {
                if(card.typeCard == DataCard.TypeCard.Unit)
                    listCard.Add(card);
            }

            //listCard = new List<DataCard>(DatabaseCard.instance.listCard);
        }
    }

    public void CoreUpdate()
    {
        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {
            ReactionControl();
        }
    }

    #region Reaction

    private void ReactionControl()
    {
        curReaction += Time.deltaTime;

        if(curReaction >= pointReaction && canSpawnTroops)
        {
            curReaction = 0;
            pointReaction = Random.Range(timeReaction - offsetReaction, timeReaction + offsetReaction) + GetAgressionPart();
            Interaction();
        }
    }

    private void Interaction()
    {
        List<DataCard> switchCard = new List<DataCard>();

        for (int i = 0; i < listCard.Count; i++)
        {
            if(GameplayBattle.instance.GetStateMana(DataMana.TType.Bot).curMana >= listCard[i].manaCost)
            {
                switchCard.Add(listCard[i]);
            }
        }

        if(switchCard.Count > 0)
        {
            try
            {
                DataCard dataCard = switchCard[Random.Range(0, switchCard.Count)];
                var newEnemy = Instantiate(dataCard.unit);
                var dataEnemy = newEnemy.GetComponent<AIUnit>();
                dataEnemy.typeUnit = AIUnit.TUnit.Enemy;
                dataEnemy.aiSupport.invers = true;
                dataEnemy.Initialization(GetPart());
                AIManager.Instance.listEnemyUnits.Add(dataEnemy);

                GameplayBattle.instance.GetStateMana(DataMana.TType.Bot).curMana -= dataCard.manaCost;
            }
            catch(UnityException e)
            {
                Debug.LogError(e.ToString());
            }
        }

    }

    private float GetAgressionPart()
    {
        List<int> counterBot = new List<int>();

        for (int i = 0; i < AIManager.Instance.listAllyUnits.Count; i++)
        {
            counterBot.Add(AIManager.Instance.listAllyUnits[i].aiSupport.indexParent);
        }

        int maxIndexBot = 0;
        int enemyUnitsBot = 0;

        for (int i = 0; i < counterBot.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counterBot.Count; w++)
            {
                if (counterBot[i] == counterBot[w])
                {
                    buffer++;
                }
            }

            if (buffer > maxIndexBot)
            {
                maxIndexBot = counterBot[i];
                enemyUnitsBot = buffer;
            }
        }

        //----------------------

        List<int> counter = new List<int>();

        for (int i = 0; i < AIManager.Instance.listEnemyUnits.Count; i++)
        {
            counter.Add(AIManager.Instance.listEnemyUnits[i].aiSupport.indexParent);
        }

        int maxIndex = 0;
        int playerUnits = 0;

        for (int i = 0; i < counter.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counter.Count; w++)
            {
                if (counter[i] == counter[w])
                {
                    buffer++;
                }
            }

            if (buffer > maxIndex)
            {
                maxIndex = counter[i];
                playerUnits = buffer;
            }
        }

        print("Line agr " + enemyUnitsBot);
        print("power agr " + (((playerUnits - enemyUnitsBot) * 0.8f)));
        return ((playerUnits - enemyUnitsBot) * 0.85f);
    }

    private int GetPart()
    {
        List<int> counter = new List<int>();

        for (int i = 0; i < AIManager.Instance.listEnemyUnits.Count; i++)
        {
            counter.Add(AIManager.Instance.listEnemyUnits[i].aiSupport.indexParent);
        }

        int maxIndex = 0;

        for (int i = 0; i < counter.Count; i++)
        {
            int buffer = 0;

            for (int w = 0; w < counter.Count; w++)
            {
                if(counter[i] == counter[w])
                {
                    buffer++;
                }
            }

            if(buffer > maxIndex)
            {
                maxIndex = counter[i];
            }
        }

        if (counter.Count > 0 && Random.Range(0, 49) <= 49)
        {
            return maxIndex;
        }
        else
        {
            return Random.Range(0, AIManager.Instance.listData.Count);
        }
    }

    #endregion

    #endregion
}
