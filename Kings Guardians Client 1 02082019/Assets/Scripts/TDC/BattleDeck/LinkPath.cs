﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkPath : MonoBehaviour
{
    public int indexParent;

    public int MyIndex = 0;


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        GUIStyle style = new GUIStyle();

        style.fontSize = 25;
        UnityEditor.Handles.Label(transform.position, MyIndex.ToString(), style);
    }
#endif

}