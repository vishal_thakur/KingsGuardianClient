﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour
{
    public enum TCard
    {
        Battle,
        Shop
    }

    [Header("Data")]
    public TCard typeCard = TCard.Battle;
    public DataCard card;
    public bool freeze = false;
    public int indexFeez = 0;

    [Header("UI")]
    public Image imgCard;
    public Text manaCost;
    public Text txtName;
    public Text txtDescription;
    public Button myButton;
    public Text txtBuyCost;

    public GameObject contentBuy;
    //public GameObject contentDontBuy;

    private void Awake()
    {
        myButton = GetComponent<Button>();
    }

    public void Initialization(DataCard dataCard)
    {
        //contentBuy.SetActive(dataCard.buy);
        contentBuy.SetActive(true);
        //contentDontBuy.SetActive(!dataCard.buy);

        card = dataCard;
        imgCard.sprite = card.visionSprite;
        manaCost.text = card.manaCost.ToString();
        txtName.text = card.unitName;
        txtDescription.text = card.description;
        //txtBuyCost.text = dataCard.buyCost.ToString();
    }

    public void Enter()
    {
        //if (!card.buy) { return; }
        if (!myButton.interactable) { return; }

        switch (typeCard)
        {
            case TCard.Battle: ManagerBattleDeck.instance.SelectedUnit(this); break;
            case TCard.Shop: ShopBattleDeck.instance.SelectedUnit(this); break;
        }
    }

    public void Drag()
    {
        //if (!card.buy) { return; }
        if (!myButton.interactable) { return; }

        //ManagerBattleDeck.instance.SelectedUpdate();
    }

    public void Exit()
    {
        //if (!card.buy) { return; }
        if (!myButton.interactable) { return; }

        switch (typeCard)
        {
            case TCard.Battle: ManagerBattleDeck.instance.SelectedEnd(); break;
            case TCard.Shop: ShopBattleDeck.instance.SelectedEnd(); break;
        }
    }

    public void Buy()
    {
        
       // if (card.buy) { return; }
        //if (GameManager.Instance.LocalPlayer.GetMaterial(Currency.BloodCrystal) < card.buyCost) { return; }

        //GameManager.Instance.LocalPlayer.RemoveMaterial(Currency.BloodCrystal, card.buyCost);
        //card.buy = true;
        //Initialization(card);
        //DatabaseCard.instance.SaveListCard();
    }
}
