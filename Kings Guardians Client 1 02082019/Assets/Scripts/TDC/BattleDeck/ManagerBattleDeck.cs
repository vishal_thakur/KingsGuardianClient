﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class FreezeCard
{
    public int index;
    public Transform center;
}

public class ManagerBattleDeck : MonoBehaviour
{
    public static ManagerBattleDeck instance;

    public Canvas parentCanvas;

    [Header("Card")]
    public RectTransform exampleCard;
    public List<CardUI> listCard = new List<CardUI>();
    public Transform parentContentCard;
    public Transform parentMoveCard;
    private Vector3 awakePositionCard;

    public int layerIndexCard;
    public HorizontalLayoutGroup layerGroupCard;

    [Header("Card")]
    public Image nextImgCard;
    public Text nextCostCard;
    public Text nextNameCard;

    [Header("Result Battle")]
    public GameObject parentResult;
    public GameObject parentWin;
    public GameObject parentLoss;
    public Text txtResultValue;
    public Color colorWin;
    public Color colorLoss;

    [Header("Mana")]
    public List<Image> listManaProperty = new List<Image>();
    public Color manaDisable;
    public Color manaEnable;

    [Header("Freeze")]
    public List<FreezeCard> listFreeze = new List<FreezeCard>();
    public Sprite freeSlot;
    public bool enterFreez;
    public int indexFeez;

    [Header("Deleted")]
    public bool enterDeleted = false;

    [Header("Grid")]
    public bool selected = false;
    public LayerMask ground;
    [HideInInspector] public LinkPath linkPath;
    public GameObject currentUnit;
    public AIView currentUnitAIView;
    public CardUI currentCard;
    [HideInInspector] public DataCard nextCard;
    public GameObject parentBattle;

    [Header("Timer")]
    public Text txtBattleTimer;



    bool cardsLoadedSuccessfully = false;
    #region Unity

    private void Awake()
    {
        instance = this;
        exampleCard.gameObject.SetActive(false);
        //Debug.LogError("subscribed to Start battle event managerBattleDeck");
        ////Add Listner New Building Create
        //Messenger.AddListener<GameObject, AIUnit.TUnit>(Events.OnBattleInitialize, OnBuildingUnitCreated);

    }

    //private void OnBuildingUnitCreated(GameObject go, AIUnit.TUnit unitType)
    //{
    //    Debug.LogError("Battle start event received in ManagerBattleDeck");
    //    Globals.battleActive = true;
    //}

    //private void OnGUI()
    //{
    //    foreach(CardUI card in listCard)
    //    {
    //        GUILayout.TextField("New card" + card.card.unitName);
    //        //Debug.LogError("New card" + card.card.unitName);
    //    }
    //}

    void Start()
    {
        //yield return new WaitForSeconds(2);
        if (AIManager.Instance)
        {
            AIManager.Instance.listAllyUnits.Clear();
            AIManager.Instance.listEnemyUnits.Clear();

            AIManager.Instance.listAllyTower.Clear();
            AIManager.Instance.listEnemyTower.Clear();

            AIManager.Instance.parentTriggerZone.SetActive(false);
            //AIManager.Instance.listEnemyTower.Clear();
            //AIManager.Instance.listAllyTower.Clear();
            cardsLoadedSuccessfully = false;
        }
    }

    //private void OnEnable()
    //{
    //    //if(AIManager.Instance == null)
    //    //{
    //    //    AIManager.Instance = GameObject.FindObjectOfType<AIManager>();
    //    //}
    //}

    //private void OnDestroy()
    //{
    //    //Debug.LogError("Unsubscribed from Start battle event");

    //    //Messenger.RemoveListener<GameObject, AIUnit.TUnit>(Events.OnBattleInitialize, OnBuildingUnitCreated);
    //}
    private void Update()
    {
        if (!AIManager.Instance)
            return;

        //if (!Globals.battleActive)
        //    return;

        //if(!AIManager.Instance)
        //{
        //    Debug.LogError("AIMANAGER nullaaa");
        //    return;
        //}
        //If Battle mode
        if(TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {
            if (Globals.battleActive)
            {
                AIManager.Instance.parentTowers.SetActive(true);

                //Debug.LogError(SceneManager.GetActiveScene().name);
                if (!parentBattle.activeSelf)
                {
                    parentBattle.SetActive(true);
                    GameplayBattle.instance.resultBattle = GameplayBattle.ResultBattle.processing;
                }
                //if (!AIManager.Instance.parentTowers.activeSelf)
                //{
                // Debug.LogError("bar bar!"); 
                //AIManager.Instance.parentTowers.SetActive(true);

                if (!cardsLoadedSuccessfully)
                {
                    //Do it once only
                    for (int i = 0; i < 6; i++)
                    {
                        RandomAddCard();
                    }
                    cardsLoadedSuccessfully = true;
                }
                //}

                ManaControl();
            }
        }
        //Normal mode
        else if(TownLogic.typeScoutMode != TownLogic.TScoutMode.Battle)
        {
            if (parentBattle.activeSelf) { parentBattle.SetActive(false); }
            //if (AIManager.Instance)
            //{
                if (AIManager.Instance.parentTowers.activeSelf) { AIManager.Instance.parentTowers.SetActive(false); }
            //}
            //else
            //{
            //    Debug.LogError("AIManager.Instance null");
            //}
            
        }

        if(selected)
        {
            SelectedUpdate();
        }
    }

    #endregion

    #region Core

    public void SelectedUnit(CardUI cardUI)
    {
        if (selected) { return; }

        layerGroupCard.enabled = false;
        layerIndexCard = cardUI.transform.GetSiblingIndex();

        selected = true;
        currentCard = cardUI;
        awakePositionCard = currentCard.transform.position;
        currentCard.transform.SetParent(parentMoveCard);
        currentUnit = Instantiate(cardUI.card.unit);
        currentUnit.SetActive(false);

        if (cardUI.card.typeCard == DataCard.TypeCard.Unit)
        {
            currentUnit.GetComponent<Collider>().enabled = false;
            currentUnit.GetComponent<NavMeshAgent>().enabled = false;

            //Grab Unit's AI View and Disable it as we donot want enemies to bother this unit, because it has not been deployed yet
            currentUnitAIView = currentUnit.GetComponentInChildren<AIView>();
            currentUnitAIView.gameObject.SetActive(false);
        }

        if (cardUI.card.typeCard == DataCard.TypeCard.Unit)
        {
            AIManager.Instance.parentTriggerZone.SetActive(true);
        }

        SetActiveCard(currentCard.gameObject, false);
    }

    public void SelectedUpdate()
    {
        Vector2 screenPoint;
        Vector3 inputSource = Input.mousePosition;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

#if UNITY_ANDROID
        foreach(Touch t in Input.touches)
        {
            if(t.phase == TouchPhase.Began || t.phase == TouchPhase.Moved)
            {
                inputSource = t.position;
                ray = Camera.main.ScreenPointToRay(t.position);
                break;
            }
        }
#endif

        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            inputSource, parentCanvas.worldCamera,
            out screenPoint);

        currentCard.transform.position = parentCanvas.transform.TransformPoint(screenPoint);

        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 100f , ground) && GetAllowedCard(hit, currentCard.card))
        {
            if (currentCard.card.typeCard == DataCard.TypeCard.Unit)
            {
                if (!currentUnit.activeSelf) { currentUnit.SetActive(true); }

                linkPath = hit.transform.GetComponent<LinkPath>();

                currentUnit.transform.position = hit.transform.position;
                print("Cast");
            }
            else
            {
                if (!currentUnit.activeSelf) { currentUnit.SetActive(true); }
                Vector3 fixPosition = hit.point;
                fixPosition.y = 0;
                currentUnit.transform.position = fixPosition;
            }
        }
        else
        {
            linkPath = null;
            if (currentUnit.activeSelf) { currentUnit.SetActive(false); }
        }
    }

    private bool GetAllowedCard(RaycastHit hit, DataCard card)
    {
        switch(card.typeCard)
        {
            case DataCard.TypeCard.Unit:
                if (hit.transform.tag == "TriggerSpawn") {

                    //Debug.LogError("Hit with" + hit.transform.tag + " GetAllowedCard is true for Unit");
                    return true;
                }
                break;

            case DataCard.TypeCard.SkillArea:
                //Debug.LogError("Hit with" + hit.transform.tag + " GetAllowedCard is true for SkillArea");
                return true; 
                break;
        }

        //Debug.LogError("Hit with" + hit.transform.tag + "GetAllowedCard is False ");
        return false;
    }

    public void SelectedEnd()
    {
        if(DeletedControll())
        {
            RandomAddCard();
        }
        else if (FreezeControl())
        {
            currentCard.transform.SetParent(parentMoveCard);
        }
        else
        {
            if (!currentCard.freeze)
            {
                currentCard.transform.SetParent(parentContentCard);
                currentCard.transform.SetSiblingIndex(layerIndexCard);
            }
        }

        if (currentCard.card.typeCard == DataCard.TypeCard.Unit)
        {
            if (linkPath)
            {
                currentUnit.GetComponent<AIUnit>().Initialization(linkPath.indexParent);

                currentUnit.GetComponent<AIUnit>().aiSupport.currentStep = linkPath.transform.GetSiblingIndex();
                AIManager.Instance.listAllyUnits.Add(currentUnit.GetComponent<AIUnit>());
                RemoveCard(currentCard);
                if (!currentCard.freeze) { RandomAddCard(); }
                GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana -= currentCard.card.manaCost;
                print("Spawn");
               // Debug.LogError("Spawn Unit");
                currentUnit.GetComponent<Collider>().enabled = true;
                currentUnit.GetComponent<NavMeshAgent>().enabled = true;
                currentUnitAIView.gameObject.SetActive(true);
            }
            else
            {
                Destroy(currentUnit);
                print("Delete");
               // Debug.LogError("Delete Unit");
            }
        }
        else if (currentCard.card.typeCard == DataCard.TypeCard.SkillArea)
        {
            currentUnit.SetActive(true);

            if (currentUnit.GetComponent<SkillArea>()) { currentUnit.GetComponent<SkillArea>().StartSkill(); }
            if (currentUnit.GetComponent<SkillPosion>()) { currentUnit.GetComponent<SkillPosion>().StartSkill(); }
            if (currentUnit.GetComponent<SkillFreeze>()) { currentUnit.GetComponent<SkillFreeze>().StartSkill(); }


            RemoveCard(currentCard);
            if (!currentCard.freeze) { RandomAddCard(); }
            GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana -= currentCard.card.manaCost;
        }

        if (currentCard) { SetActiveCard(currentCard.gameObject, true); }

        currentCard = null;
        linkPath = null;
        currentUnit = null;
        selected = false;
        AIManager.Instance.parentTriggerZone.SetActive(false);
        awakePositionCard = Vector3.zero;
        layerGroupCard.enabled = true;
    }

    #region Card

    private void SetActiveCard(GameObject target, bool value)
    {
        foreach (Image img in target.GetComponentsInChildren<Image>())
        {
            img.raycastTarget = value;
        }

        foreach (Text txt in target.GetComponentsInChildren<Text>())
        {
            txt.raycastTarget = value;
        }
    }

    private void RandomAddCard()
    {
        if(!nextCard)
        {
            nextCard = DatabaseCard.instance.GetCard().listHandCard[Random.Range(0, DatabaseCard.instance.GetCard().listHandCard.Count)];
        }

        AddCard(nextCard);

        nextCard = DatabaseCard.instance.GetCard().listHandCard[Random.Range(0, DatabaseCard.instance.GetCard().listHandCard.Count)];
        UpdateNextCard();
    }

    private void AddCard(DataCard card)
    {
        try
        {
            RectTransform newcard = RectTransform.Instantiate(exampleCard, parentContentCard.transform, false);
            newcard.gameObject.SetActive(true);

            //CardUI dataCard = (RectTransform.Instantiate(exampleCard,parentContentCard , false)as CardUI);
            CardUI dataCard = newcard.GetComponent<CardUI>();

            dataCard.imgCard.sprite = card.visionSprite;
            dataCard.manaCost.text = card.manaCost.ToString();
            dataCard.txtName.text = card.unitName;
            dataCard.txtDescription.text = card.description;

            dataCard.card = card;
            listCard.Add(dataCard);
            //dataCard.Initialization(card);

            UpdateNextCard();
           // Debug.LogError("new card adde to list " + dataCard.gameObject.name);
        }
        catch(System.Exception e)
        {
            Debug.LogError("Kuch aur hi scene hai" + e);
        }
    }

    private void RemoveCard(CardUI cardUI)
    {
      //  Debug.LogError("Card removed from list" + cardUI.gameObject.name);
        listCard.Remove(cardUI);
        Destroy(cardUI.gameObject);
        UpdateNextCard();
    }

    private void UpdateNextCard()
    {
        if (!nextCard) { return; }

        nextImgCard.sprite = nextCard.visionSprite;
        nextCostCard.text = nextCard.manaCost.ToString();
        nextNameCard.text = nextCard.unitName;
    }

    #endregion

    #region Freeze

    public void EnterFreeze(int index)
    {
        enterFreez = true;
        indexFeez = index;
        print("Enter");
    }

    public void ExitFreeze(int index)
    {
        enterFreez = false;
        print("Exit");
    }

    private bool FreezeControl()
    {
        if (!enterFreez)
        {
            currentCard.transform.position = awakePositionCard;
            return false;
        }

        if (!currentCard.freeze) { RandomAddCard(); }

        currentCard.transform.position = listFreeze[indexFeez].center.position;
        currentCard.freeze = true;
        currentCard.indexFeez = indexFeez;

        return true;
    }

    #endregion

    #region Mana

    private void ManaControl()
    {
        for (int i = 0; i < listManaProperty.Count; i++)
        {
            if(GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana > i)
            {
                listManaProperty[i].color = manaEnable;
            }
            else
            {
                listManaProperty[i].color = manaDisable;
            }
        }

        for (int i = 0; i < listCard.Count; i++)
        {
            listCard[i].myButton.interactable = Mathf.FloorToInt(GameplayBattle.instance.GetStateMana(DataMana.TType.Player).curMana) >= listCard[i].card.manaCost;
        }
    }

    #endregion

    #region Deleted

    public void EnterDeleted()
    {
        enterDeleted = true;
    }

    public void ExitDeleted()
    {
        enterDeleted = false;
    }

    private bool DeletedControll()
    {
        if(enterDeleted)
        {
            bool getState = currentCard.freeze;
            RemoveCard(currentCard);
            return !getState;
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region Result Battle

    public void SetResultBattle(GameplayBattle.ResultBattle result, int value)
    {
        parentResult.SetActive(true);

        string delimetr = string.Empty;

        switch(result)
        {
            case GameplayBattle.ResultBattle.processing: break;
            case GameplayBattle.ResultBattle.Won:
                parentWin.SetActive(true);
                parentLoss.SetActive(false);

                delimetr = "+";
                txtResultValue.color = colorWin;
                break;
            case GameplayBattle.ResultBattle.Lost:
                parentWin.SetActive(false);
                parentLoss.SetActive(true);

                delimetr = "-";
                txtResultValue.color = colorLoss;
                break;
        }

        txtResultValue.text = delimetr + Mathf.Abs(value).ToString();
    }

    #endregion

    #endregion
}
