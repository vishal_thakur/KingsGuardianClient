﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataMana
{
    public enum TType
    {
        Player,
        Bot
    }

    [Header("Data")]
    public TType type;

    public int maxMana = 10;
    public float curMana = 2f;
    public float regenMana = 1f;
    private float timerRegen = 0f;

    public void RegenerationMana()
    {
        if (curMana >= maxMana) { return; }

        timerRegen += Time.deltaTime;

        if (timerRegen >= 1f)
        {
            curMana = Mathf.Clamp(curMana + regenMana, 0, maxMana);
            timerRegen = 0;
        }
    }
}

public class GameplayBattle : MonoBehaviour
{
    public enum ResultBattle
    {
        processing,
        Won,
        Lost
    }

    public static GameplayBattle instance;

    public ResultBattle resultBattle = ResultBattle.processing;
    public List<DataMana> listMana = new List<DataMana>();

    #region Unity

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Initialization();
    }

    private void Update()
    {
        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {
            CoreUpdate();
        }
    }

    #endregion

    #region Core

    public void Initialization()
    {

    }

    public void CoreUpdate()
    {
        ManaControl();
    }

    private void ManaControl()
    {
        for (int i = 0; i < listMana.Count; i++)
        {
            listMana[i].RegenerationMana();
        }
    }

    public DataMana GetStateMana(DataMana.TType type)
    {
        for (int i = 0; i < listMana.Count; i++)
        {
            if(listMana[i].type == type)
            {
                return listMana[i];
            }
        }

        DataMana newData = new DataMana();
        newData.type = type;

        listMana.Add(newData);
        return listMana[listMana.Count - 1];
    }

    #region Mana

    #endregion

    public void CellResultBattle(ResultBattle result)
    {
        //Donot Call Results if Match results are out already
        if (resultBattle == ResultBattle.Won) { return; }
        if (resultBattle == ResultBattle.Lost) { return; }

        //The value by which Ally or Enemy wins or loses the battle (Rewards points that are credited or Debited)
        int value = 0;


        //if Results have not been Processed and are yet to be processed
        if (result != ResultBattle.processing)
        {

            resultBattle = result;

            //Win - Give x ammount of reward
            if(resultBattle == ResultBattle.Won)
            {
                value += 30;
                GameManager.Instance.LocalPlayer.AddMaterial(Currency.BloodCrystal, value);
                Globals.battleActive = false;
            }
            //Lose - Substract x ammount of Rewards
            else
            {
                value -= 30;
                GameManager.Instance.LocalPlayer.RemoveMaterial(Currency.BloodCrystal, value);
            }
        }
        //if Results have been Processed
        else
        {
            //Ally Tower Count
            int towerAlly = 0;
            //Enemy Tower Count
            int towerEnemy = 0;

            //Iterate through Ally Towers
            for (int i = 0; i < AIManager.Instance.listAllyTower.Count; i++)
            {
                //If Ally Tower is not null and is alive
                if (AIManager.Instance.listAllyTower[i] && !AIManager.Instance.listAllyTower[i].death)
                {
                    //Increment Alive Ally Tower Count
                    towerAlly++;
                }
            }

            //Iterate through Enemy Towers
            for (int i = 0; i < AIManager.Instance.listEnemyTower.Count; i++)
            {
                //If Enemy Tower is not null and is alive
                if (AIManager.Instance.listEnemyTower[i] && !AIManager.Instance.listEnemyTower[i].death)
                {
                    //Increment Alive Enemy Tower Count
                    towerEnemy++;
                }
            }

            //If All Enemy towers are dead then Ally has won the battle
            if (towerEnemy == 0)
            {
                //Set battle Result as WIN
                resultBattle = ResultBattle.Won;
                //reward Ally with x Ammount of a reward
                value += 30;
                GameManager.Instance.LocalPlayer.AddMaterial(Currency.BloodCrystal, value);
            }
            //If All Ally towers are dead then Enemy has won the battle
            if (towerAlly == 0)
            {
                //Set battle Result as LOSS
                resultBattle = ResultBattle.Lost;
                //Substact reward from Ally by x Ammount
                value -= 30;
                GameManager.Instance.LocalPlayer.RemoveMaterial(Currency.BloodCrystal, value);
            }

            //If Battle Reward Credit/Debit is zero
            if (value != 0)
            {
                print("Result: " + resultBattle.ToString());
                ManagerBattleDeck.instance.SetResultBattle(resultBattle, value);
                print("Result");
            }
        }
    }

    #endregion
}
