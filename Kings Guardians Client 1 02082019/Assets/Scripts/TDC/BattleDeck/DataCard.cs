﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataCard : MonoBehaviour
{
    public enum TypeCard
    {
        Unit,
        SkillArea
    }

    public TypeCard typeCard = TypeCard.Unit;
    public int uid;
    public Sprite visionSprite;
    public Sprite hideSprite;
    public GameObject unit;
    public int manaCost;
    public string unitName;
    public string description;
   // public bool buy = false;
    //public int buyCost;
}
