﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIView : MonoBehaviour
{
    public AIUnit aiUnit;

    private void Update()
    {
        for (int i = 0; i < aiUnit.listEnemyUnits.Count; i++)
        {
            if (aiUnit.listEnemyUnits[i] == null)
            {
                aiUnit.listEnemyUnits.RemoveAt(i);
                return;
            }

            if (aiUnit.listEnemyUnits[i].death)
            {
                aiUnit.listEnemyUnits.RemoveAt(i);
                return;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent && other.transform.parent.tag == "Unit")
        {
            //Grab The enemy unit's reference
            AIUnit enemyUnit = other.transform.parent.GetComponent<AIUnit>();

          
            Vector3 direction = (aiUnit.transform.position - enemyUnit.transform.position).normalized;

            //If I am an ally
            if (aiUnit.typeUnit == AIUnit.TUnit.Ally)
            {
                //This means
                //Enemy in Front
                if (direction.z < 0)
                {
                    //Add enemy to enemy list
                    if (enemyUnit && enemyUnit.typeUnit != aiUnit.typeUnit && !enemyUnit.death && !aiUnit.listEnemyUnits.Contains(enemyUnit))
                    {
                        aiUnit.listEnemyUnits.Add(enemyUnit);
                    }
                }
                //Else Enemy is behind

            }
            else if(aiUnit.typeUnit == AIUnit.TUnit.Enemy)
            {
                //This means
                //ally in Front
                if (direction.z > 0)
                {
                    //Add enemy to enemy list
                    if (enemyUnit && enemyUnit.typeUnit != aiUnit.typeUnit && !enemyUnit.death && !aiUnit.listEnemyUnits.Contains(enemyUnit))
                    {
                        aiUnit.listEnemyUnits.Add(enemyUnit);
                    }
                }
                //Else Ally is behind
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent && other.transform.parent.tag == "Unit")
        {
            //Grab The enemy unit's reference
            AIUnit enemyUnit = other.transform.parent.GetComponent<AIUnit>();

            if (enemyUnit && aiUnit.listEnemyUnits.Contains(enemyUnit))
            {
                aiUnit.listEnemyUnits.Remove(enemyUnit);
            }
        }
    }
}
