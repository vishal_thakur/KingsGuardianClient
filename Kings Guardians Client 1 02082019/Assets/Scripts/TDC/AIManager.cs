﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIDataParent
{
    public int index;
    public Transform targetParent;
    public List<AIDataPoint> listPoints = new List<AIDataPoint>();

    public int indexEnemySpawn;
    public int indexAllySpawn;

    public Transform triggerSpawn;

    public void Initialization()
    {
        if (!targetParent) { return; }

        int indexRate = 0;

        Transform[] parentChildren = targetParent.GetComponentsInChildren<Transform>();
        SphereCollider referenceCollider = triggerSpawn.GetComponentInChildren<SphereCollider>();
        
        foreach (Transform find in parentChildren)
        {
            if(find != targetParent)
            {
                if (find.gameObject.GetComponent<LinkPath>() == null)
                {
                    AIDataPoint newData = new AIDataPoint();

                    newData.index = indexRate;
                    newData.point = find;

                    find.gameObject.AddComponent<SphereCollider>();
                    SphereCollider colliderFind = find.GetComponent<SphereCollider>();
                    //colliderFind.radius = 0.68f;
                    colliderFind.radius = referenceCollider.radius;
                    //colliderFind.isTrigger = true;

                    //find.transform.localScale = new Vector3(25, 25, 25);
                    find.transform.localScale = referenceCollider.transform.localScale;
                    colliderFind.center = referenceCollider.center;

          
                    find.gameObject.AddComponent<LinkPath>();
                    find.GetComponent<LinkPath>().indexParent = triggerSpawn.GetComponent<LinkPath>().indexParent;

                    find.GetComponent<LinkPath>().MyIndex = indexRate;


                    find.gameObject.tag = "TriggerSpawn";

                    listPoints.Add(newData);

                    indexRate++;
                }
            }
        }
    }
}

[System.Serializable]
public class AIDataPoint
{
    [Header("Data")]
    public int index = 0;
    public Transform point;
}

public class AIManager : MonoBehaviour
{
    // (Optional) Prevent non-singleton constructor use.
    protected AIManager() { }

    //public static AIManager instance;
    public List<AIDataParent> listData = new List<AIDataParent>();

    public float timeBattle = 300;

    public List<AIUnit> listEnemyUnits = new List<AIUnit>();
    public List<AIUnit> listAllyUnits = new List<AIUnit>();

    public List<AIUnit> listEnemyTower = new List<AIUnit>();
    public List<AIUnit> listAllyTower = new List<AIUnit>();

    public List<GameObject> bufferlistEnemyTower = new List<GameObject>();
    public List<GameObject> bufferlistAllyTower = new List<GameObject>();

    public GameObject parentTowers;
    public GameObject parentTriggerZone;

    public static AIManager Instance;
    bool readyToUpdate = false;
    //public Transform rootCamera;

    #region Unity

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(this.gameObject); // or gameObject
    }

    //private void Start()
    //{
    //    Initialization();
    //}

    private void OnLevelWasLoaded(int level)
    {
        Initialization();
    }
  

    void Update()
    {

        if (readyToUpdate)
            CoreUpdate();
    }

    #endregion

    #region Core

    private void Initialization()
    {


        if (TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle)
        {

            for (int i = 0; i < listData.Count; i++)
            {
                listData[i].Initialization();
            }

            //for (int i = 0; i < listAllyTower.Count; i++)
            //{
            //    listAllyTower[i].Initialization(0);
            //}

            //for (int i = 0; i < listEnemyTower.Count; i++)
            //{
            //    listEnemyTower[i].Initialization(0);
            //}

            timeBattle = 300f;

            readyToUpdate = true;
        }
    }


    //private void OnBuildingUnitCreated(GameObject go, AIUnit.TUnit unitType)
    //{
    //  //  Debug.LogError("OnBuildingUnitCreated , name = " + go.name);
    //    Globals.battleActive = true;
        

    //    AIUnit unit = go.GetComponentInChildren<AIUnit>();
    //    //Set Tag

    //    if (unit == null)
    //        return;


    //    unit.gameObject.tag = "Unit";

    //    if (unitType == AIUnit.TUnit.Ally)
    //    {
    //        unit.typeUnit = AIUnit.TUnit.Ally;
    //        bufferlistAllyTower.Add(unit.gameObject);
    //        //Debug.LogError("added a ally" + go.name);
    //    }
    //    if (unitType == AIUnit.TUnit.Enemy)
    //    {
    //        unit.typeUnit = AIUnit.TUnit.Enemy;
    //       // Debug.LogError("added a enemy" + go.name);
    //        bufferlistEnemyTower.Add(unit.gameObject);
    //    }
    //}
    //private void OnGUI()
    //{
    //    foreach (AIUnit unit in listAllyTower)
    //    {
    //        GUILayout.TextField("Ally Tower Unit = " + unit.gameObject.name);
    //        //Debug.LogError("New card" + card.card.unitName);
    //    }
    //    GUILayout.TextField("-------------------------------------------");

    //    foreach (AIUnit unit in listEnemyTower)
    //    {
    //        GUILayout.TextField("Enemy Tower Unit= " + unit.gameObject.name);
    //        //Debug.LogError("New card" + card.card.unitName);
    //    }
    //}


    private void CoreUpdate()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            //rootCamera.transform.Rotate(Vector3.up * Input.GetAxis("Mouse X"));
        }
        else
        {
            if (!Cursor.visible)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (ManagerBattleDeck.instance && TownLogic.typeScoutMode == TownLogic.TScoutMode.Battle && GameplayBattle.instance.resultBattle == GameplayBattle.ResultBattle.processing)
        {
            //Battle Timer Logic
            timeBattle -= Time.deltaTime;

            int minute = Mathf.FloorToInt(timeBattle / 60);
            int second = Mathf.Abs(Mathf.FloorToInt(minute * 60 - timeBattle));

            string txtSecond;

            if(second > 9)
            {
                txtSecond = second.ToString();
            }
            else
            {
                txtSecond = "0" + second;
            }

            if (timeBattle > 0)
            {
                ManagerBattleDeck.instance.txtBattleTimer.text = "0" + minute.ToString() + ":" + txtSecond;
            }
            else
            {
                ManagerBattleDeck.instance.txtBattleTimer.text = "00:00";
            }
            //Debug.LogError(timeBattle.ToString());

            if(timeBattle <= 0)
            {
                int healthAlly = 0;
                int healthEnemy = 0;

                for (int i = 0; i < listAllyTower.Count; i++)
                {
                    healthAlly += listAllyTower[i].currentHealth;
                }

                for (int i = 0; i < listEnemyTower.Count; i++)
                {
                    healthEnemy += listEnemyTower[i].currentHealth;
                }

                //Debug.LogError("Ally Health" + healthAlly);
                //Debug.LogError("Enemy Health" + healthEnemy);

                if (healthAlly > healthEnemy)
                {
                    Debug.LogError("Win" + healthAlly);
                    GameplayBattle.instance.CellResultBattle(GameplayBattle.ResultBattle.Won);
                }
                else
                {
                    Debug.LogError("Loss" + healthEnemy);
                    GameplayBattle.instance.CellResultBattle(GameplayBattle.ResultBattle.Lost);
                }
            }
            else
            {
                SpawnZoneControl();
            }
        }
    }

    private void SpawnZoneControl()
    {

        //try
        //{
            int maxAllyIndex = 0;

            for (int i = 0; i < listAllyUnits.Count; i++)
            {
                if (listAllyUnits[i].aiSupport.currentStep > maxAllyIndex)
                {
                    maxAllyIndex = listAllyUnits[i].aiSupport.currentStep;
                }
            }

            //This value Changes With The Count of Link Paths Array Count
            int maxEnemyIndex = 48;

            for (int i = 0; i < listEnemyUnits.Count; i++)
            {
                if (listEnemyUnits[i].aiSupport.currentStep < maxEnemyIndex)
                {
                    maxEnemyIndex = listEnemyUnits[i].aiSupport.currentStep;
                }
            }

            for (int i = 0; i < listData.Count; i++)
            {
                for (int w = 1; w < listData[i].listPoints.Count; w++)
                {
                    if (w < maxAllyIndex)
                    {
                        listData[i].listPoints[w].point.gameObject.SetActive(true);
                    }
                    else
                    {
                        listData[i].listPoints[w].point.gameObject.SetActive(false);
                    }
                }


            //This value Changes With The Count of Link Paths Array Count
            // If total Path points are 50 then The value should be 48
            listData[i].indexAllySpawn = Mathf.Clamp(maxAllyIndex - 1, 0, 49);
            if(listData[i].triggerSpawn) listData[i].triggerSpawn.position = listData[i].listPoints[listData[i].indexAllySpawn].point.position;
                    listData[i].indexEnemySpawn = Mathf.Clamp(maxEnemyIndex + 1, 0, 49);
                //}
                //catch (System.Exception e)
                //{
                //    Debug.LogError(e.ToString());
                //}
            }
        //}
        //catch(UnityException e) {
        //    Debug.LogError(e.ToString());
        //}
    }

    #endregion
}
