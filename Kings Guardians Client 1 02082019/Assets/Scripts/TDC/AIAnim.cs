﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAnim : MonoBehaviour
{
    AIUnit aiUnit;

    private void Awake()
    {
        aiUnit = transform.parent.GetComponent<AIUnit>();
    }

    public void Damage()
    {
        aiUnit.DamageTarget(aiUnit.targetUnit);
    }
}
