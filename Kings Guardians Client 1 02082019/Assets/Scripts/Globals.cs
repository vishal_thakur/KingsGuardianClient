﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals{

	public static List<string> message = new List<string>();

	public static bool PushNotificationsEnabled;

	public static bool IsAnyUIPanelActive = false;

	public static bool CanCameraMoveTowardsBottom = true;
	public static bool CanCameraMoveTowardsTop = true;
	public static bool CanCameraMoveTowardsLeft = true;
	public static bool CanCameraMoveTowardsRight = true;

	public enum MovementDirection{
		Bottom,
		Top,
		Left,
		Right
	}

    public static bool battleActive = false;
}
