﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementLimitWall : MonoBehaviour {

	[SerializeField] Globals.MovementDirection WallType;

	void OnTriggerEnter(Collider obj){
//		Debug.LogError (gameObject.name + "Trigger Enter");
		ToggleWallPermitFlag(true);
	}

	void OnTriggerExit(Collider obj){
		//		Debug.LogError (gameObject.name + "Trigger Exit");
		ToggleWallPermitFlag(false);
	}


	void ToggleWallPermitFlag(bool flag){
		switch (WallType) {
		case Globals.MovementDirection.Top:
			Globals.CanCameraMoveTowardsTop = !flag;
			break;
		case Globals.MovementDirection.Bottom:
			Globals.CanCameraMoveTowardsBottom = !flag;
			break;
		case Globals.MovementDirection.Left:
			Globals.CanCameraMoveTowardsLeft = !flag;
			break;
		case Globals.MovementDirection.Right:
			Globals.CanCameraMoveTowardsRight = !flag;
			break;
		}
	}
}
