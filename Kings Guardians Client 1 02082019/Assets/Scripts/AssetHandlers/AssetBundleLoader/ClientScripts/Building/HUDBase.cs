﻿using UnityEngine;
using System.Collections;

public class HUDBase : MonoBehaviour 
{
	bool _initialized = false;
	Vector3 _lastCamPos;


	public void Init()
	{
		if (_initialized) return;
		OnInit();
		_initialized = true;
	}

	public void Toggle(bool show)
	{
		OnToggle(show);
	}

	protected virtual void OnInit(){}
	protected virtual void OnToggle(bool show){}


	void Update()
	{
		if (!_initialized) return;
		LookAtCamera();                  
	}

	void LookAtCamera()
	{
		// Look at the camera if its changes its position
		if (_lastCamPos != Camera.main.transform.position)
		{
			Vector3 v = Camera.main.transform.position - transform.position;
			v.x = v.z = 0.0f;
			transform.LookAt(  Camera.main.transform.position - v ); 
			transform.Rotate(0,180,0);
			_lastCamPos = Camera.main.transform.position;
		}
	}







}
