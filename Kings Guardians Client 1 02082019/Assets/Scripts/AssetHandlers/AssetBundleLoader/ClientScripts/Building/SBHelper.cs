﻿using UnityEngine;
using System.Collections;

public class SBHelper : MonoBehaviour
{
	public AnimatorCallback animatorCallback;
	public delegate void AnimatorCallback();

	void OnAnimationCallback()
	{
		if (animatorCallback != null)
			animatorCallback();
	}
}
