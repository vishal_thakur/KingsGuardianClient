﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingHUD : HUDBase
{
	public string Name
	{
		get { return _labelName.Value; } 
		set { _labelName.Value = value; }
	}

	public int Upgrade
	{
		get { return int.Parse(_labelUpgrade.Value.Replace("Upgrade ", ""));} 
		set { _labelUpgrade.Value = "Upgrade " + value.ToString(); }
	}

	public StatusBar ProgressBar;
	public GameObject Label;
	public Image ProgressIcon;
	public Image CollectIcon;

	public bool ProgressBarEnabled {
		get { return ProgressBar.gameObject.activeSelf; }
		set { ProgressBar.gameObject.SetActive(value); }
	}

	TextShadowed _labelName;
	TextShadowed _labelUpgrade;  

	protected override void OnInit()
	{
		foreach (var t in Label.GetComponentsInChildren<TextShadowed>())
		{
			if (t.name == "Name") _labelName = t;
			else if (t.name == "Upgrades") _labelUpgrade = t;
		}
		ProgressBar.Init();
		ProgressBar.Inverse = true;
	}

	protected override void OnToggle(bool show)
	{
//		Debug.LogError ("show\t" + show );
		ProgressBarEnabled = show;
		if(!show) CollectIcon.enabled = false;
		ToggleLabel(show);
	}


	public void ToggleLabel(bool t)
	{

//		Debug.LogError ("show\t" + t);
		Label.SetActive(t);
	}

	public void UpdateProgressBar(float value, float max = 0)
	{
		if (max > 0)
			ProgressBar.MaxValue = max;
		ProgressBar.Value = value;


		if (ProgressBar.Value == ProgressBar.MaxValue)
			ProgressBar.Text = "completed";
		else
			ProgressBar.Text = SecondsToString(ProgressBar.MaxValue - ProgressBar.Value);          
	}

	string SecondsToString(float seconds)
	{
		System.TimeSpan t = System.TimeSpan.FromSeconds((double)seconds);

		// Formated
		if (t.Days > 0)
			return "" + Mathf.CeilToInt((float)t.TotalDays) + ((t.TotalDays > 1) ? " days" : " day");
		if (t.Hours > 0)
			return "" + Mathf.CeilToInt((float)t.TotalHours) + ((t.TotalHours > 1) ? " hours" : " hour");
		if (t.Minutes > 0)
			return "" + Mathf.CeilToInt((float)t.TotalMinutes) + ((t.TotalMinutes > 1) ? " minutes" : " minute");
		if (t.Seconds > 0)
			return "" + Mathf.CeilToInt((float)t.TotalSeconds) + ((t.TotalSeconds > 1) ? " seconds" : " second");

		return "";
	}

}
