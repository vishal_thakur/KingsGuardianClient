﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BuildingController : MonoBehaviour
{
	public bool RuinsVisible { get; private set; }
	public bool FootprintVisible { get; private set; }

	public List<Dictionary_Int_GameObject> Models = new List<Dictionary_Int_GameObject>();
	public List<Dictionary_Int_GameObject> Stands = new List<Dictionary_Int_GameObject>();
	public GameObject Ruin;
	public GameObject Base;
	public GameObject Footprint;
	public Vector2 ModelBaseSize = Vector2.zero;
	public BuildingHUD Hud;
	public RaidStatusHUD RaidHud;

    public Material m;

	public GameObject ActiveModel { get; private set; }
	GameObject _activeStand;

	Dictionary<FootprintColor, Material> _colors = new Dictionary<FootprintColor, Material>();
	MeshRenderer _footprintMesh;
	int _upgrade = 0;

	bool _hidden;


	void Awake()
	{
		var colors = (FootprintColor[])Enum.GetValues(typeof(FootprintColor));
		foreach (var c in colors)
		{
			switch (c)
			{
				case FootprintColor.Green: m = new Material(Resources.Load<Material>("Materials/FootprintGreen")); break;
				case FootprintColor.LightGreen: m = new Material(Resources.Load<Material>("Materials/FootprintGreen2")); break;
				case FootprintColor.Orange: m = new Material(Resources.Load<Material>("Materials/FootprintOrange")); break;
				case FootprintColor.Red: m = new Material(Resources.Load<Material>("Materials/FootprintRed")); break;
			}
			_colors.Add(c, m);
		}


		_footprintMesh = Footprint.GetComponent<MeshRenderer>();
		ChangeColor(FootprintColor.Green);

		foreach (var m in Models)
			m.Value.SetActive(false);
		foreach (var s in Stands)
			s.Value.SetActive(false);
		
		ToggleRuin(false);
		ToggleFootprint(false);

	}

	public void HiddenBuilding(bool t)
	{
		if(ActiveModel != null)
			ActiveModel.SetActive(!t);
		Base.SetActive(!t);
		FootprintVisible = !t;
		_hidden = t;
	}

	public void ChangeModel(int upgrade)
	{
		if (_hidden)
		{
			if (ActiveModel != null)
				ActiveModel.SetActive(false);
			return;
		}

		if (ActiveModel != null)
			ActiveModel.SetActive(false);

		ActiveModel = GetElement(Models, upgrade);

		if (!RuinsVisible)
			ActiveModel.SetActive(true);
	}

	public void ShowStand( int upgrade)
	{
		_activeStand = GetElement(Stands, upgrade);
		if(_activeStand != null)
			_activeStand.SetActive(true);
	}

	public void HideStand()
	{
		if(_activeStand != null)
			_activeStand.SetActive(false);
		_activeStand = null;
	}

	public void ToggleRuin(bool show)
	{
		if(ActiveModel != null)
			ActiveModel.SetActive(!show);
		Ruin.SetActive(show);
		RuinsVisible = show;
	}


	public void ToggleFootprint(bool show)
	{
		if (Footprint != null)
			Footprint.SetActive(show);
		Base.SetActive(!show);
		FootprintVisible = show;
	}

	public void ChangeColor( FootprintColor color )
	{
		if (_colors.ContainsKey(color))
			_footprintMesh.material = m;
	}

	public void SetRotation(Vector3 direction, float deltaTime)
	{
		Debug.LogError ("rotation set" + direction);
//		ActiveModel.transform.rotation = Quaternion.Lerp(ActiveModel.transform.rotation, Quaternion.LookRotation(direction), deltaTime);
		ActiveModel.transform.rotation = Quaternion.LookRotation(direction);
	}

	GameObject GetElement(List<Dictionary_Int_GameObject> list, int upgrade)
	{
		GameObject element = null;
		int u = -1;
		foreach (var m in list)
		{
			if (m.Key <= upgrade && m.Key > u)
			{
				element = m.Value;
				u = m.Key;
			}
		}
		return element;
	}


}

[System.Serializable]
public class Dictionary_Int_GameObject
{
	public int Key;
	public GameObject Value;
}
		
public enum FootprintColor : int
{
	Red = 0, 
	Green = 1,
	LightGreen = 2,
	Orange = 3,
}