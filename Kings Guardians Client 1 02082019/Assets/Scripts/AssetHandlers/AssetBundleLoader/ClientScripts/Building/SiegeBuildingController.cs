﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SiegeBuildingController : MonoBehaviour 
{
	public Transform LaunchPoint;

	// Animation
	public bool AttackAnimation; 	// Attack animator need "OnAnimationCallback" event!

	// Attack FX (particle + sound)
	public bool EnableAttackFX;
	public GameObject AttackFX;			// Audio goes to the particle!
	public float DestroyTime = 0f;
	GameObject _attackFX;

	// Projectile
	public bool EnableProjectile;
	public GameObject Projectile;
	public float ProjectileSpeed = 1f;
	public bool BallisticPath;
	GameObject _projectile;

	// Hit FX (particle + sound)
	public bool EnableHitFX;
	public GameObject HitFX;

	Animator _animator;
	AudioSource _audioSource;

	Vector3 _attackTarget;

	// TODO : siege weapon - show range 


	void Awake()
	{
		_animator = GetComponentInChildren<Animator>(); 
		_audioSource = GetComponent<AudioSource>();
		if (_audioSource == null)
			gameObject.AddComponent<AudioSource>();

		if (_animator != null)
		{
			var helper = _animator.gameObject.AddComponent<SBHelper>();
			helper.animatorCallback = OnAnimationCallback;
		}
	}

	public void Attack(Vector3 target)
	{
		Debug.LogError ("asdasd");
		_attackTarget = target;

		if (AttackAnimation)
			_animator.SetTrigger("Attack");
		else
			OnAnimationCallback();
	}

	public void StopAttacking()
	{
		if (EnableAttackFX && GameObject.Find(AttackFX.name))
			AttackFX.SetActive(false);
	}

	void OnAnimationCallback()
	{
		Debug.LogError ("asdasd");
		if (EnableAttackFX)
		{
			if (GameObject.Find(AttackFX.name))
			{
				var p = AttackFX.GetComponent<ParticleSystem>();
				AttackFX.SetActive(true);
			}
			else
			{
				_attackFX = Instantiate(AttackFX, LaunchPoint.position);
				Debug.Log("Created attackFX: " + _attackFX + " (" + AttackFX + ")");
				if (_attackFX == null) return;              
				_attackFX.transform.LookAt(_attackFX.transform.position + transform.forward * 100);

				if(DestroyTime > 0)
					Destroy(_attackFX, DestroyTime);
			}
		}

		if (EnableProjectile)
		{
			_projectile = Instantiate(Projectile, LaunchPoint.position);
			if (_projectile != null)
				_projectile.transform.LookAt(_attackTarget);
		}
		else if(onHitCallback != null)
			onHitCallback(); 
	}

	void Update()
	{
		if (_projectile != null)
		{
			if (BallisticPath)
				BallisticProjectileMovement();
			else
				SimpleProjectileMovement();         
		}
	}


	GameObject Instantiate(GameObject prefab, Vector3 position)
	{
		if (prefab == null) return null;
		return (GameObject)Instantiate(prefab, position, Quaternion.identity) as GameObject;
	}


	void SimpleProjectileMovement()
	{
		_projectile.transform.position = Vector3.MoveTowards(_projectile.transform.position, _attackTarget, ProjectileSpeed * Time.deltaTime);

		if (Vector3.Distance(_attackTarget, _projectile.transform.position) <= 0.5f)
			OnDestinationReached();         

	}

	void BallisticProjectileMovement()
	{
		Vector3 targetPosition = _projectile.transform.position + BallisticVel(_attackTarget + Vector3.up, 30f);
		targetPosition += Physics.gravity / 2;

		_projectile.transform.position = Vector3.MoveTowards(_projectile.transform.position, targetPosition, ProjectileSpeed * Time.deltaTime);

		if (Vector3.Distance(_attackTarget, _projectile.transform.position) <= 1f)
			OnDestinationReached();         
	}

	Vector3 BallisticVel(Vector3 target, float angle) 
	{
		var dir = target - _projectile.transform.position; // get target direction 
		var h = dir.y; // get height difference 
		dir.y = 0; // retain only the horizontal direction 

		var dist = dir.magnitude ; // get horizontal distance 
		var a = angle * Mathf.Deg2Rad; // convert angle to radians 
		dir.y = dist * Mathf.Tan(a); // set dir to the elevation angle 
		dist += h / Mathf.Tan(a); // correct for small height differences 

		// calculate the velocity magnitude 
		var vel = Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a)); 
		return vel * dir.normalized; 
	}

	void OnDestinationReached()
	{
		if (EnableHitFX)
		{
			var go = Instantiate(HitFX, _projectile.transform.position);
			if (go != null) Destroy(go, 2f);
		}

		Destroy(_projectile, 0.2f);
		_projectile = null;

		if(onHitCallback != null)
			onHitCallback();  
	}

	public delegate void Delegate();
	public Delegate onHitCallback;


	#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		GUIStyle style = GUI.skin.label;
		style.active.textColor = Color.cyan;
		Gizmos.color = Color.cyan;

		if (LaunchPoint != null)
		{
			UnityEditor.Handles.Label(LaunchPoint.position, "Spawn Point", style);
			Gizmos.DrawSphere(LaunchPoint.position, 0.2f);
		}     
	}
	#endif


	/*

	List<Vector3> _ballisticPath = new List<Vector3>();
    int _ballisticPathStep = -1;

    List<Vector3> CalculateBalisticPath( Vector3 start, Vector3 end, float angle, float speed, int resulution)
    {
        float height = speed * Mathf.Pow(Mathf.Sin(angle), 2f) / 2 * 9.84f;
        List<Vector3> path = new List<Vector3>();
        for ( float i = 0; i < resulution + 1; i++ )
        {
            path.Add(SampleParabola( start, end, height, i / resulution ));          
        }
        return path;
    }

    Vector3 SampleParabola ( Vector3 start, Vector3 end, float height, float t ) 
    {
        float parabolicT = t * 2 - 1;
        if ( Mathf.Abs( start.y - end.y ) < 0.1f )
        {
            //start and end are roughly level, pretend they are - simpler solution with less steps
            Vector3 travelDirection = end - start;
            Vector3 result = start + t * travelDirection;
            result.y += ( -parabolicT * parabolicT + 1 ) * height;
            return result;
        } 
        else
        {
            //start and end are not level, gets more complicated
            Vector3 travelDirection = end - start;
            Vector3 levelDirecteion = end - new Vector3( start.x, end.y, start.z );

            Vector3 right = Vector3.Cross( travelDirection, levelDirecteion );
            Vector3 up = Vector3.Cross( right, levelDirecteion );
            if ( end.y > start.y ) up = -up;

            Vector3 result = start + t * travelDirection;
            result += ( ( -parabolicT * parabolicT + 1 ) * height ) * up.normalized;
            return result;
        }
    }

*/

}
