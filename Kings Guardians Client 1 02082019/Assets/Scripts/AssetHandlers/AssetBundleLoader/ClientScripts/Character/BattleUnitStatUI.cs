﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUnitStatUI : MonoBehaviour 
{
	public Text Level;
	public Slider HP;
	public Slider Shield;
	public Slider Readiness;
	public List<BuffIconUI> Buffs = new List<BuffIconUI>();
	public Image Arrow;

	Transform _camera;
	FloatingText _floatingText;

	public void Init()
	{
		var canvas = GetComponent<Canvas>();
		canvas.worldCamera = Camera.main;
		_camera = Camera.main.transform;

		_floatingText = GetComponent<FloatingText>();

		Arrow.enabled = false;

		HideBuffs();
	}

	public void Set( int level, int maxHP, int currHP, int currShield)
	{
		Level.text = level.ToString();
		HP.maxValue = maxHP;
		HP.value = currHP;

		Shield.maxValue = maxHP;
		Shield.value = currShield;
	}

	public void UpdateBars(int hp, int shield, float readiness)
	{
		if (HP.value != hp)
		{
			var hpDiff = hp - HP.value;
			_floatingText.AddText(hpDiff.ToString(), (hpDiff > 0) ? CommandMessageColor.Green : CommandMessageColor.Red );
			HP.value = hp;
		}

		if (Shield.value != shield)
		{
			var shieldDiff = Shield.value - shield;
			if (shieldDiff < 0)
				_floatingText.AddText(shieldDiff.ToString(), CommandMessageColor.Yellow);
			Shield.value = shield;
		}

		Readiness.value = readiness;
	}

	public void FloatCustomText(string text)
	{
		_floatingText.AddText(text, CommandMessageColor.White);
	}

	public void UpdateBuff(int cnt, int duration, Sprite icon)
	{
		if (cnt > Buffs.Count || cnt < 0)
			return;

		Buffs[cnt].Duration.text = duration.ToString();
		Buffs[cnt].Icon.sprite = icon;
	}


	void Update()
	{
		if(_camera != null)
			transform.LookAt(_camera.position);
	}

	public void HideBuffs()
	{
		foreach (var b in Buffs)
		{
			b.Icon.enabled = false;
			b.Duration.enabled = false;
		}
	}

}


[System.Serializable]
public class BuffIconUI
{
	public Image Icon;
	public Text Duration;
}
