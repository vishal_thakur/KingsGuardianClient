﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimationEnums 
{
	Idle,
	MoveBackward,
	MoveForward,
	BasicAttack,
	CastBuff,
	CastOffensive, 
	Death,
	OnHit,
	Jump
}
	
public class CharacterController : MonoBehaviour
{
	public float ForwardSpeed = 1f;
	public float BackwardSpeed = 1f;

	int FORWARD_STATE = Animator.StringToHash("MoveForward");
	int BACKWARD_STATE = Animator.StringToHash("MoveBackward");

	[System.Serializable]
	public class OverrideAnimation
	{
		public string Key;
		public AnimationClip Value;
	}

	public List<OverrideAnimation> AnimationClips = new List<OverrideAnimation>();

	[System.Serializable]
	public class AttackSequence
	{
		public bool MoveCloserFirst = true;
	}
	public AttackSequence BasicAttackSequence = new AttackSequence();

	public ActionID CurrentAction { get; private set; }

	readonly Vector3 INVALID_DESTINATION = new Vector3 (20000, 20000, 20000);
	const float STOP_DISTANCE_FORWARD = 3f; 	// Run
	const float STOP_DISTANCE_BACK = 0.5f;	  	// Walk

	Animator _animator;

	Vector3 _destination;
	Vector3 _lookDirection;

	Vector3 _startPostion;
	float _startDestinationDistance;


	public void Init()
	{

		_destination = INVALID_DESTINATION;

		_animator = GetComponent<Animator>();

		RuntimeAnimatorController myController = _animator.runtimeAnimatorController;

		AnimatorOverrideController myOverrideController = myController as AnimatorOverrideController;
		if(myOverrideController != null)
			myController = myOverrideController.runtimeAnimatorController;

		AnimatorOverrideController animatorOverride = new AnimatorOverrideController();
		animatorOverride.runtimeAnimatorController = myController;

		foreach (var clip in AnimationClips)
			animatorOverride[clip.Key] = clip.Value;

		_animator.runtimeAnimatorController = animatorOverride;

		_lookDirection = transform.forward;
	}

	#region Public Methods
	public void Move(Vector3 destination, bool backward = false)
	{
		Debug.Log("Move to " + destination + " backward? " + backward);
		_destination = destination;
		_startPostion = transform.position;
		_startDestinationDistance = Vector3.Distance(_destination, transform.position) - ((backward) ? STOP_DISTANCE_BACK : STOP_DISTANCE_FORWARD);

		// Calculate look direction
		_lookDirection = _destination - transform.position;
		if (backward) _lookDirection *= -1;
		_lookDirection = new Vector3(_lookDirection.x, 0, _lookDirection.z);

		SetAction((backward) ? ActionID.MoveBackward : ActionID.MoveForward);
	}

	public void CastBuff() { SetAction(ActionID.Buff); }
	public void CastOffensive() { SetAction(ActionID.Offensive); } 
	public void BasicAttack() { SetAction(ActionID.BasicAttack); }

	public void SetDead (bool b) { _animator.SetBool("Dead", b); }  
	public void OnHit() { _animator.SetTrigger("OnHit"); }

	public void Jump() { SetAction(ActionID.Jump); }

	public void Stop(){ SetAction(ActionID.None); }
	#endregion


	void FixedUpdate()
	{
		if (CurrentAction == ActionID.MoveForward || CurrentAction == ActionID.MoveBackward)
		{
			if (_destination == INVALID_DESTINATION)
				SetAction(ActionID.None);
			else
			{
				float distance = Vector3.Distance(transform.position, _startPostion);
				if (distance >= _startDestinationDistance)
				{					
					if (OnDestinationReached != null)
						OnDestinationReached(CurrentAction);
				}
			}
		}
	}

	void SetAction(ActionID action)
	{
//		Debug.LogError("Set Action "+CurrentAction+" => " + action);

		CurrentAction = action;
		_animator.SetInteger("ActionID", (int)action); 

		if(_destination != INVALID_DESTINATION)
			transform.LookAt (_lookDirection * 10 );

	
		
	}

	void OnAnimatorMove()
	{		
		if (_animator != null)
		{	
			var currentState = _animator.GetCurrentAnimatorStateInfo(0);    
			float move = 0f;

			if (currentState.shortNameHash == FORWARD_STATE)
				move = Time.deltaTime * currentState.speed * ForwardSpeed; 
			if (currentState.shortNameHash == BACKWARD_STATE)
				move = Time.deltaTime * currentState.speed * BackwardSpeed; 	

			if ( move != 0f)
			{
				transform.position += transform.forward * move;
				transform.LookAt (_lookDirection * 10 );
			}
		}
	}

	void OnAnimationStart() { if (OnActionStarted != null) OnActionStarted(CurrentAction); }
	void OnAnimationCallback() { if (OnAction != null) OnAction(CurrentAction); }
	void OnAnimationEnd() 
	{ 
		if (OnActionFinished != null) OnActionFinished(CurrentAction); 
	}


	#region Delegates
	public delegate void DelegatedMethod(ActionID currentAction);
	public DelegatedMethod OnDestinationReached;

	public DelegatedMethod OnActionStarted;
	public DelegatedMethod OnAction;			
	public DelegatedMethod OnActionFinished;
	#endregion
}
