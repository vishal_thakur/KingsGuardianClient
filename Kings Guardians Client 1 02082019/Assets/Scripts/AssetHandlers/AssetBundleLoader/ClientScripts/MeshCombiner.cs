﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;

public class MeshCombiner : MonoBehaviour 
{
    public ShadowCastingMode CastShadows = ShadowCastingMode.Off;
    public bool ReceiveShadows = false;
    public bool UseLightProbes = false;
    public ReflectionProbeUsage ReflectionProbes = ReflectionProbeUsage.Off;

    SkinnedMeshRenderer newSkin;
    Material _baseMaterial;
    List<SkinnedMeshRenderer> _renderers;

    void Start()
    {
        SkinnedMeshRenderer[] skinnedRenderers = GetComponentsInChildren<SkinnedMeshRenderer>(true);   
        if (skinnedRenderers.Length > 0)
            CombineSkinnedMesh(skinnedRenderers);
    }

    void CombineSkinnedMesh( SkinnedMeshRenderer[] renderers)
    {
        _renderers = new List<SkinnedMeshRenderer>();

        List<Transform> bones = new List<Transform>();        
        List<BoneWeight> boneWeights = new List<BoneWeight>();        
        List<CombineInstance> combineInstances = new List<CombineInstance>();

        string debug = "Skinned Mesh combine started, the gameobject has " + renderers.Length + " number of skinned mesh.";

        // Iterate through the skinned mesh renderes, and if it shares the same material, add it to the 
        // list that contains the renderers to combine.
        foreach(var renderer in renderers)
        {
            if (renderer.enabled == false) continue;
            if (_baseMaterial == null)
            {
                _baseMaterial = renderer.sharedMaterial;
                debug += "\n Base Material set to: " + _baseMaterial.name;
            }

            if (renderer.material = _baseMaterial)
            {
                _renderers.Add(renderer);
                debug += "\n " + renderer.name + " renderer added to the list";
            }
        }

        int numSubs = 0;
        foreach(var renderer in _renderers) numSubs += renderer.sharedMesh.subMeshCount;

        int[] meshIndex = new int[numSubs];
        for( int s = 0; s < _renderers.Count; s++ ) 
        {
            SkinnedMeshRenderer renderer = _renderers[s];

            // Make a new list of bones adding new ones as we find them
            foreach( Transform bone in renderer.bones )
            {
                if(!bones.Contains(bone))
                    bones.Add( bone );
            }

            //bone indices are are not the same for all skinned meshes
            //the skinning data requires an index for each bone
            //since our new mesh needs to share a combined bone list
            //we need to find the IndexOf the new indices
            BoneWeight[] meshBoneweights = renderer.sharedMesh.boneWeights;
            foreach( BoneWeight bw in meshBoneweights ) 
            {
                BoneWeight bWeight = bw;
                bWeight.boneIndex0 = bones.IndexOf(renderer.bones[bw.boneIndex0]); 
                bWeight.boneIndex1 = bones.IndexOf(renderer.bones[bw.boneIndex1]);
                bWeight.boneIndex2 = bones.IndexOf(renderer.bones[bw.boneIndex2]);
                bWeight.boneIndex3 = bones.IndexOf(renderer.bones[bw.boneIndex3]);
                boneWeights.Add( bWeight );
            }

            CombineInstance ci = new CombineInstance();
            ci.transform = renderer.transform.localToWorldMatrix;
            ci.mesh = renderer.sharedMesh;
            meshIndex[s] = ci.mesh.vertexCount;
            combineInstances.Add( ci );

            MonoBehaviour.DestroyImmediate(renderer.gameObject);
        }

        List<Matrix4x4> bindposes = new List<Matrix4x4>();
        for( int b = 0; b < bones.Count; b++ ) 
            bindposes.Add( bones[b].worldToLocalMatrix);

        newSkin = gameObject.AddComponent<SkinnedMeshRenderer>();
        newSkin.sharedMesh = new Mesh();
        newSkin.sharedMesh.CombineMeshes( combineInstances.ToArray(), true, true );
        newSkin.bones = bones.ToArray();
        newSkin.material = _baseMaterial;
        newSkin.sharedMesh.boneWeights = boneWeights.ToArray();
        newSkin.sharedMesh.bindposes = bindposes.ToArray();
        newSkin.sharedMesh.RecalculateBounds();

        newSkin.shadowCastingMode = CastShadows;
        newSkin.receiveShadows = ReceiveShadows;
        newSkin.useLightProbes = UseLightProbes;
        newSkin.reflectionProbeUsage = ReflectionProbes;            
    }



}
