﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Runemark.AssetEditor
{
	public class AssetNames : ScriptableObject
	{
		public List<string> Names = new List<string>();
	}
}