﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Runemark.AssetEditor
{
	public class AssetData : MonoBehaviour
	{
		public string ID = "";
		public string DisplayName = "";
		public Texture2D Icon = null;
		public List<GameObject> Assets = new List<GameObject>();
		public string LastSave;
	}
}