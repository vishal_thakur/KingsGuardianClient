﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

namespace Runemark.AssetEditor
{
	public class AssetBundleLoader
	{
		public static float Progress { get; set; }

        //        static string ASSET_BUNDLE_URL = "http://demos.runemarkstudio.com/test/";


#if UNITY_EDITOR
        static string ASSET_BUNDLE_URL = "http://kings-guardians.com/KingsGaurdiansAndroidAssets/windows/";
#elif UNITY_ANDROID
        static string ASSET_BUNDLE_URL = "http://kings-guardians.com/KingsGaurdiansAndroidAssets/android/";
#elif UNITY_IOS
        static string ASSET_BUNDLE_URL = "http://kings-guardians.com/KingsGaurdiansAndroidAssets/iOS/";
#endif


        public static AssetNames LoadAssetNames(string category)
		{
			AssetNames names = ScriptableObject.CreateInstance<AssetNames>();
			var bundle = LoadBundle(category);
			if (bundle != null)
			{
				var a = bundle.LoadAsset<AssetNames>("assetNames");
				names.Names = a.Names;
				bundle.Unload(true);
			}
			return names;
		}


		public static GameObject GetAsset(AssetBundle bundle, string name)
		{
			GameObject asset = null;
			if (bundle != null)
				asset = bundle.LoadAsset<GameObject>(name);

			if (asset == null)
				Debug.LogError("Can't get asset (" + name + ") from this bundle " + bundle.name);

			return asset;
		}

		public static GameObject GetAsset(string category, string name)
		{
			GameObject asset = null;
			var bundle = LoadBundle(category);
			asset = GetAsset(bundle, name);
			bundle.Unload(false);
			return asset;
		}

		public static AssetBundle LoadBundle(string bundleName)
		{
			var filePath = GetFilePath(bundleName);
			var bundle = (File.Exists(filePath)) ? AssetBundle.LoadFromFile(filePath) : null;
			if (bundle == null)
				Debug.LogError("Loading the assetbundle (" + bundleName + ") on path " + filePath + " failed");
			return bundle;
		}



		public static bool CheckForUpdates(string bundleName, DateTime updated)
		{		
			if (PlayerPrefs.HasKey(bundleName + "_LastDownload"))
			{
				DateTime last = Convert.ToDateTime(PlayerPrefs.GetString(bundleName + "_LastDownload"));
				return last < updated;
			}

			return true;
		}
		


		public static IEnumerator DownloadUpdates(string bundleName)
		{		
			Debug.Log(Application.platform);

			string platformFolder = "";
			switch (Application.platform)
			{
				case RuntimePlatform.Android: platformFolder = "android"; break;
				case RuntimePlatform.IPhonePlayer: platformFolder = "ios"; break;
				case RuntimePlatform.WindowsEditor:	platformFolder = "windows";	break;
			}

            //string url = ASSET_BUNDLE_URL + platformFolder + "/" + bundleName;
            string url = ASSET_BUNDLE_URL + bundleName;
            string filePath = GetFilePath(bundleName);

            //Globals.message.Add("Download link\t" + url);

			WWW download = new WWW(url);
			if (download == null)
				Debug.LogError("no server object!");
			
			Debug.Log("Server: " + download +"("+download.progress+"|"+download.isDone+" )\n" + url);

			while (!download.isDone)
			{
				Progress = download.progress;
				yield return null;
			}


			if (!string.IsNullOrEmpty(download.error))
			{
				Debug.LogError(download.error); 
			}
			else
			{
				Debug.Log("Downloaded, saving to " + filePath);

				// Create the directory if it doesn't already exist
				if (!Directory.Exists(Application.persistentDataPath + "/../assetbundles/"))
					Directory.CreateDirectory(Application.persistentDataPath + "/../assetbundles/");

				// Initialize the byte string
				byte[] bytes = download.bytes;

				// Creates a new file, writes the specified byte array to the file, and then closes the file. 
				// If the target file already exists, it is overwritten.
				File.WriteAllBytes(filePath, bytes);

				// store last download time.
				PlayerPrefs.SetString(bundleName + "_LastDownload", DateTime.Now.ToString());
				Progress = 1f;
			}
		}


		static string GetFilePath(string fileName)
		{
#if UNITY_EDITOR
			return Application.dataPath + "/_AssetBundlesEditor/"+fileName;
#endif

			var filePath = Application.persistentDataPath+"/../assetbundles/"+fileName;
			if (!Directory.Exists(Application.persistentDataPath + "/../assetbundles/"))
				Directory.CreateDirectory(Application.persistentDataPath + "/../assetbundles");
			return filePath;
		}
	}
}