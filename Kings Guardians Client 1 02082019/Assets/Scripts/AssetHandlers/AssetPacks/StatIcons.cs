﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class StatIcons : ScriptableObject
{
    public List<StatIcon> Icons = new List<StatIcon>();

    public Dictionary<CharacterStats, Sprite> GetDictionary()
    {   
        Dictionary<CharacterStats, Sprite> dict = new Dictionary<CharacterStats, Sprite> ();
        foreach (StatIcon a in Icons) 
        {
            dict.Add (a.stat, a.sprite);
        }
        return dict;
    } 

}

[System.Serializable]
public class StatIcon
{
    public CharacterStats stat;
    public Sprite sprite;
}