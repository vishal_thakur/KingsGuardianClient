﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class CardIcons : ScriptableObject
{
	public List<Sprite> Icons = new List<Sprite>();
	public List<Sprite> InactiveIcons = new List<Sprite>();

	public Dictionary<string, Sprite> GetDictionary()
	{   
		Dictionary<string, Sprite> dict = new Dictionary<string, Sprite> ();

		foreach (Sprite s in Icons) 
		{
			dict.Add (s.name, s);
		}

		foreach (Sprite s in InactiveIcons) 
		{
			dict.Add (s.name+"_off", s);
		}

		return dict;
	} 

}