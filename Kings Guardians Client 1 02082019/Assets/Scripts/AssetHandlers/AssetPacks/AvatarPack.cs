﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class AvatarPack : ScriptableObject
{
	public List<Sprite> Avatars = new List<Sprite>();

	public Dictionary<string, Sprite> GetDictionary()
	{   
		Dictionary<string, Sprite> dict = new Dictionary<string, Sprite> ();

        foreach (Sprite s in Avatars) 
		{
			dict.Add (s.name, s);
		}
		return dict;
	} 

}