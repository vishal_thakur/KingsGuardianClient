﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class CharacterPacks : ScriptableObject
{
	public List<CharacterAssets> Characters = new List<CharacterAssets>();

	public Dictionary<string, CharacterAssets> GetDictionary()
	{	
		Dictionary<string, CharacterAssets> dict = new Dictionary<string, CharacterAssets> ();
		foreach (CharacterAssets c in Characters) 
		{
			dict.Add (c.ItemID, c);
		}
		return dict;
	}
}

[System.Serializable]
public class CharacterAssets
{
	public string ItemID;
	public Sprite Icon;
    public Sprite AbilityIcon;
	public GameObject Model;
}