﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu()]
public class CurrencyIcons : ScriptableObject
{
	
	public List<CurrencyIcon> Icons = new List<CurrencyIcon>();

	public Dictionary<Currency, Sprite> GetDictionary()
    {   
		Dictionary<Currency, Sprite> dict = new Dictionary<Currency, Sprite> ();
		foreach (CurrencyIcon a in Icons) 
        {
            dict.Add (a.currency, a.sprite);
        }
        return dict;
    } 

}

[System.Serializable]
public class CurrencyIcon
{
    public Currency currency;
	public Sprite sprite;
}