﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CardFXPack))]
public class CardFXPackEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		CardFXPack myTarget = (CardFXPack)target;

		if (GUILayout.Button ("Sort")) 
		{
			myTarget.ListToDictionary ();
			EditorUtility.SetDirty (myTarget);
			AssetDatabase.SaveAssets();
		}

		if (myTarget.GetDictionary () == null || myTarget.GetDictionary ().Count == 0)
			EditorGUILayout.HelpBox ((myTarget.Prefabs.Count == 0) ? "Please add the effects to the list" : "Please press on the sort button", MessageType.Info);
		else
		{
			foreach (KeyValuePair<string,CardFX> pair in myTarget.GetDictionary()) 
			{
				GUILayout.Label (pair.Key);
				EditorGUI.indentLevel++;
				pair.Value.PrewarmFX = (GameObject)EditorGUILayout.ObjectField ("Prewarm", pair.Value.PrewarmFX, typeof(GameObject), false);
				pair.Value.FX = (GameObject)EditorGUILayout.ObjectField ("FX", pair.Value.FX, typeof(GameObject), false);
				pair.Value.ImpactFX = (GameObject)EditorGUILayout.ObjectField ("Impact", pair.Value.ImpactFX, typeof(GameObject), false);
				EditorGUI.indentLevel--;
			}
		}
	}

}
