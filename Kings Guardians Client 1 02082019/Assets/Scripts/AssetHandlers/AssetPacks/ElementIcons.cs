﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class ElementIcons : ScriptableObject
{
    public List<ElementIcon> Icons = new List<ElementIcon>();

    public Dictionary<Element, Sprite> GetDictionary()
    {   
        Dictionary<Element, Sprite> dict = new Dictionary<Element, Sprite> ();
        foreach (ElementIcon a in Icons) 
        {
            dict.Add (a.element, a.sprite);
        }
        return dict;
    } 

}

[System.Serializable]
public class ElementIcon
{
    public Element element;
    public Sprite sprite;
}