﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class BuffFXPack : ScriptableObject
{
    public List<BuffFX> Buffs = new List<BuffFX>();

    public Dictionary<string, BuffFX> GetDictionary()
    {
        Dictionary<string, BuffFX> dict = new Dictionary<string, BuffFX> ();
        foreach (BuffFX b in Buffs) 
            dict.Add (b.ID, b);
        return dict;
    }  
}

[System.Serializable]
public class BuffFX
{
    public string ID;
    public Sprite Icon;
    public GameObject Prefab;
}