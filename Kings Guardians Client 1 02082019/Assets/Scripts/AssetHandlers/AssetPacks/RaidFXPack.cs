﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class RaidFXPack : ScriptableObject
{
	public List<RaidFX> FXs = new List<RaidFX>();

	public Dictionary<string, RaidFX> GetDictionary()
    {
		Dictionary<string, RaidFX> dict = new Dictionary<string, RaidFX> ();
		foreach (RaidFX b in FXs) 
            dict.Add (b.ID, b);
        return dict;
    }  
}

[System.Serializable]
public class RaidFX
{
    public string ID;
	public Sprite Icon;
    public GameObject AuraFX;
	public GameObject FXonTarget;
}