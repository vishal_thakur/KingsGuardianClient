﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class BuildingPacks : ScriptableObject
{	
	public float Scale = 1f;
	public List<BuildingAssets> Buildings = new List<BuildingAssets>();

	public Dictionary<string, BuildingAssets> GetDictionary()
	{	
		Dictionary<string, BuildingAssets> dict = new Dictionary<string, BuildingAssets> ();
		foreach (BuildingAssets a in Buildings) 
		{
			dict.Add (a.ItemID, a);
		}
		return dict;
	}
}

[System.Serializable]
public class BuildingAssets
{
	public string ItemID;
	public Sprite Icon;
    public GameObject Model;	
}
