﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Runemark.AssetEditor;

public class AssetPackManager : Singleton<AssetPackManager>
{
	public float Progress = 0f;

	// ONLY IN TOWN AND RAID
    public float Scale;
    GameObject _missingBuilding;
    Dictionary<string, BuildingAssets> _buildingPack = new Dictionary<string, BuildingAssets>();
    Dictionary<string, Sprite> _avatars = new Dictionary<string, Sprite>();

    // ONLY IN BATTLE
    Dictionary<string, CardFX> _cardFXs = new Dictionary<string, CardFX>();
    Dictionary<string, BuffFX> _buffFXs = new Dictionary<string, BuffFX>();
	Dictionary<string, RaidFX> _raidFXs = new Dictionary<string, RaidFX>();

    // ALWAYS
    Dictionary<string, CharacterAssets> _characterPack = new Dictionary<string, CharacterAssets>();
    Dictionary<string, CharacterAssets> _monsterPack = new Dictionary<string, CharacterAssets>();
    Dictionary<CharacterStats, Sprite> _statIcons = new Dictionary<CharacterStats, Sprite>();
    Dictionary<Element, Sprite> _elementIcons = new Dictionary<Element, Sprite>();
    Dictionary<Currency, Sprite> _currencyIcons = new Dictionary<Currency, Sprite>();
    Dictionary<string, Sprite> _cardIcons = new Dictionary<string, Sprite>();
    Sprite _missingIcon;

	public IEnumerator LoadAssets()
	{
		Debug.LogWarning("Asset Pack Manager start " + Time.time);
		Progress = 0f;

		// CHARACTERS
		var characterNames = AssetBundleLoader.LoadAssetNames("Character");
		var characterBundle = AssetBundleLoader.LoadBundle("Character");

		int cnt = 0;
		foreach (var b in characterNames.Names)
		{
			var cGo = AssetBundleLoader.GetAsset(characterBundle, b);
			var c = cGo.GetComponent<AssetData>();

			Sprite icon = null;
			if(c.Icon != null)
				icon = Sprite.Create(c.Icon, new Rect(0f, 0f, c.Icon.width, c.Icon.height), new Vector2(.5f,.5f));

			_characterPack.Add(c.ID, new CharacterAssets(){ ItemID = c.ID, Model = cGo, Icon = icon}); 

			Progress = 0.8f + ((float)cnt / (float)characterNames.Names.Count)/10;
			cnt++;

			if (c.DisplayName == "Wizard")
			{
				Debug.Log(c.ID +": " + icon +","+ cGo);
			}

			yield return null;
		}
		characterBundle.Unload(false);

		Debug.LogWarning("Asset Pack Manager - characters done "+"("+(Progress*100)+"%)" + Time.time);


		// MONSTERS
		var monsterRequest = Resources.LoadAsync<CharacterPacks> ("AssetPacks/MonsterAssets");
		while (!monsterRequest.isDone)
		{
			Progress = 0.1f + monsterRequest.progress / 20;
			yield return null;
		}			
		_monsterPack = ((CharacterPacks)monsterRequest.asset).GetDictionary();

		Debug.LogWarning("Asset Pack Manager - monsters done "+"("+(Progress*100)+"%)" + Time.time);

		// ICONS
		var staticonRequest = Resources.LoadAsync<StatIcons> ("AssetPacks/StatIcons");
		while (!staticonRequest.isDone)
		{
			Progress = 0.15f + staticonRequest.progress / 20;
			yield return null;
		}			
		_statIcons = ((StatIcons)staticonRequest.asset).GetDictionary();

		var elementiconRequest = Resources.LoadAsync<ElementIcons> ("AssetPacks/ElementIcons");
		while (!elementiconRequest.isDone)
		{
			Progress = 0.2f + elementiconRequest.progress / 10;
			yield return null;
		}			
		_elementIcons = ((ElementIcons)elementiconRequest.asset).GetDictionary();

		var currencyiconRequest = Resources.LoadAsync<CurrencyIcons> ("AssetPacks/CurrencyIcons");
		while (!currencyiconRequest.isDone)
		{
			Progress = 0.3f + currencyiconRequest.progress / 10;
			yield return null;
		}			
		_currencyIcons = ((CurrencyIcons)currencyiconRequest.asset).GetDictionary();

		var cardiconRequest = Resources.LoadAsync<CardIcons> ("AssetPacks/CardIcons");
		while (!cardiconRequest.isDone)
		{
			Progress = 0.4f + cardiconRequest.progress / 10;
			yield return null;
		}			
		_cardIcons = ((CardIcons)cardiconRequest.asset).GetDictionary();


		Debug.LogWarning("Asset Pack Manager - icons done "+"("+(Progress*100)+"%)" + Time.time);

		_missingIcon = Resources.Load<Sprite> ("AssetPacks/Missing");


		// FX
		var cardFXRequest = Resources.LoadAsync<CardFXPack> ("AssetPacks/CardFXPack");
		while (!cardFXRequest.isDone)
		{
			Progress = 0.5f + cardFXRequest.progress / 10;
			yield return null;
		}			
		_cardFXs = ((CardFXPack)cardFXRequest.asset).GetDictionary();

		Debug.LogWarning("Asset Pack Manager - cardfx done "+"("+(Progress*100)+"%)" + Time.time);

		var buffFXRequest = Resources.LoadAsync<BuffFXPack> ("AssetPacks/BuffFXPack");
		while (!buffFXRequest.isDone)
		{
			Progress = 0.6f + buffFXRequest.progress / 10;
			yield return null;
		}			
		_buffFXs = ((BuffFXPack)buffFXRequest.asset).GetDictionary();

		Debug.LogWarning("Asset Pack Manager - bufffx done "+"("+(Progress*100)+"%)" + Time.time);

		var raidFXRequest = Resources.LoadAsync<RaidFXPack> ("AssetPacks/RaidFXPack");
		while (!raidFXRequest.isDone)
		{
			Progress = 0.7f + raidFXRequest.progress / 10;
			yield return null;
		}			
		_raidFXs = ((RaidFXPack)raidFXRequest.asset).GetDictionary();

		Debug.LogWarning("Asset Pack Manager - raidfx done "+"("+(Progress*100)+"%)" + Time.time);

	

		// BUILDINGS
		var buildingNames = AssetBundleLoader.LoadAssetNames("Building");
		var buildingBundle = AssetBundleLoader.LoadBundle("Building");

		cnt = 0;
		foreach (var b in buildingNames.Names)
		{
			var bGo = AssetBundleLoader.GetAsset(buildingBundle, b);
			var d = bGo.GetComponent<AssetData>();
			_buildingPack.Add(d.ID, new BuildingAssets(){ ItemID = d.ID, Model = bGo, 
				Icon = Sprite.Create(d.Icon, new Rect(0f, 0f, d.Icon.width, d.Icon.height), new Vector2(.5f,.5f)) 
			});

			Progress = 0.8f + ((float)cnt / (float)buildingNames.Names.Count)/10;
			cnt++;
			yield return null;
		}
		buildingBundle.Unload(false);

		_missingBuilding = Resources.Load<GameObject> ("AssetPacks/MissingBuilding");


		Debug.LogWarning("Asset Pack Manager - buildings done "+"("+(Progress*100)+"%)" + Time.time);

		var avatarsRequest = Resources.LoadAsync<AvatarPack> ("AssetPacks/AvatarPack");
		while (!avatarsRequest.isDone)
		{
			Progress = 0.9f + avatarsRequest.progress / 10;
			yield return null;
		}			
		_avatars = ((AvatarPack)avatarsRequest.asset).GetDictionary();
	
		Debug.LogWarning("Asset Pack Manager finished " + Time.time);		

		Progress = 1f;
	}



    /// <summary>
    /// Gets the currency icon.
    /// </summary>
    /// <returns>The currency icon.</returns>
    /// <param name="currency">Currency.</param>
    public static Sprite GetCurrencyIcon(Currency currency)
    {
        if (Instance._currencyIcons.ContainsKey (currency))
            return Instance._currencyIcons [currency];
        return Instance._missingIcon;
    }

    /// <summary>
    /// Gets the avatar.
    /// </summary>
    /// <returns>The avatar.</returns>
    /// <param name="avatarID">Avatar ID.</param>
    public static Sprite GetAvatar(string avatarID)
    {
        if (avatarID != "" && Instance._avatars.ContainsKey (avatarID))
            return Instance._avatars [avatarID];
        return Instance._missingIcon;
    }

    /// <summary>
    /// Gets the stat icon.
    /// </summary>
    /// <returns>The stat icon.</returns>
    /// <param name="stat">Stat.</param>
    public static Sprite GetStatIcon(CharacterStats stat)
    {
        if (Instance._statIcons.ContainsKey (stat))
            return Instance._statIcons [stat];
        return Instance._missingIcon;
    }

    public static Sprite GetElementIcon(Element element)
    {
        if (Instance._elementIcons.ContainsKey (element))
            return Instance._elementIcons [element];
        return Instance._missingIcon;
    }

    /// <summary>
    /// Gets the card icon.
    /// </summary>
    /// <returns>The card icon.</returns>
    /// <param name="itemId">Item identifier.</param>
    /// <param name="active">If set to <c>true</c> active.</param>
    public static Sprite GetCardIcon(string contentId, bool active)
    {
        string key = contentId + ((active) ? "" : "_off");

        if (Instance._cardIcons.ContainsKey (key))
            return Instance._cardIcons [key];
        return Instance._missingIcon;
    }

    /// <summary>
    /// Gets the character assets.
    /// </summary>
    /// <returns>The character assets.</returns>
    /// <param name="itemID">Item I.</param>
    public static CharacterAssets GetCharacterAssets(string contentID)
    {
        if (Instance._characterPack.ContainsKey (contentID))
            return Instance._characterPack [contentID];

        CharacterAssets c = new CharacterAssets ();
        c.ItemID = contentID;
        c.Icon = Instance._missingIcon;
        c.Model = null;
        return c;
    }

    /// <summary>
    /// Gets the monster assets.
    /// </summary>
    /// <returns>The monster assets.</returns>
    /// <param name="contentID">Content I.</param>
    public static CharacterAssets GetMonsterAssets(string contentID)
    {
        if (Instance._monsterPack.ContainsKey (contentID))
            return Instance._monsterPack [contentID];

        CharacterAssets c = new CharacterAssets ();
        c.ItemID = contentID;
        c.Icon = Instance._missingIcon;
        c.Model = null;
        return c;
    }

    /// <summary>
    /// Gets the building assets.
    /// </summary>
    /// <returns>The building assets.</returns>
    /// <param name="itemID">Item I.</param>
    public static BuildingAssets GetBuildingAssets(string contentID)
    {
        if (Instance._buildingPack.ContainsKey(contentID))
        {
            if (Instance._buildingPack[contentID].Model == null)
                Instance._buildingPack[contentID].Model = instance._missingBuilding;
            if (Instance._buildingPack[contentID].Icon == null)
                Instance._buildingPack[contentID].Icon = Instance._missingIcon;
            return Instance._buildingPack[contentID];
        }
        
        BuildingAssets b = new BuildingAssets ();
        b.Icon = Instance._missingIcon;
        b.ItemID = contentID;
        b.Model = instance._missingBuilding;       
        return b;
    }

    /// <summary>
    /// Gets the card FX.
    /// </summary>
    /// <returns>The card FX.</returns>
    /// <param name="itemId">Item identifier.</param>
    public static CardFX GetCardFX(string contentId)
    {
        if (Instance._cardFXs.ContainsKey (contentId))
            return Instance._cardFXs [contentId];
        return new CardFX ();
    }
     

    /// <summary>
    /// Gets the card FX.
    /// </summary>
    /// <returns>The card FX.</returns>
    /// <param name="itemId">Item identifier.</param>
    public static BuffFX GetBuffFX(string buffID)
    {
        if (Instance._buffFXs.ContainsKey (buffID))
            return Instance._buffFXs [buffID];
        return new BuffFX();
    }


	public static RaidFX GetRaidFX(string fxID)
	{
		if (Instance._raidFXs.ContainsKey (fxID))
			return Instance._raidFXs [fxID];
		return new RaidFX();
	}
   
    #region TESTING	
    public List<string> GetItemIDs()
    {
        List<string> ids = new List<string>();
        foreach (var p in _cardFXs)
            ids.Add(p.Key);
        return ids;
    }

    public List<string> GetCharacterIDs()
    {
        List<string> ids = new List<string>();
        foreach (var p in _characterPack)
            ids.Add(p.Key);
        return ids;
    }
	

    public List<string> GetBuildingIDs()
    {
        List<string> ids = new List<string>();
        foreach (var p in _buildingPack)
            ids.Add(p.Key);
        return ids;
    }
    #endregion
}
