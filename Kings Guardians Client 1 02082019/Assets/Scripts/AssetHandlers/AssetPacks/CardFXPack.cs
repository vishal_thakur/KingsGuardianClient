﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu()]
public class CardFXPack : ScriptableObject
{
	public List<GameObject> Prefabs = new List<GameObject>();
	[HideInInspector] public List<CardFX> _list = new List<CardFX>();

	public Dictionary<string, CardFX> GetDictionary()
	{
		Dictionary<string, CardFX> dict = new Dictionary<string, CardFX> ();
		foreach (CardFX s in _list) 
			dict.Add (s.Name, s);
		return dict;
	} 

	public void ListToDictionary()
	{
		foreach (GameObject prefab in Prefabs) 
		{
			string[] name = prefab.name.Split('_');

			var fx = _list.Find (x => x.Name == name [0]);
			if (fx == null) 
			{
				fx = new CardFX () { Name = name [0] };
				fx.SetPrefab (prefab, name [1]);
				_list.Add (fx);
			}
			else
				fx.SetPrefab (prefab, name [1]);
		}
	}
}

[System.Serializable]
public class CardFX
{
	public string Name;
	public GameObject FX;
	public GameObject PrewarmFX;
	public GameObject ImpactFX;	

	public void SetPrefab(GameObject go, string suffix)
	{
		switch (suffix) 
		{
		case "particle": FX = go; break;
		case "prewarm": PrewarmFX = go; break;
		case "impact": ImpactFX = go; break;
		}
	}
}