﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class Utils
{
    


    public static bool IsInherited(System.Type type, System.Type from)
    {
        return type.IsSubclassOf(from) || type == from;
    }

    public static Quaternion StringToRotation(string s)
    {
        string[] rot = s.Split(',');
        return new Quaternion( float.Parse(rot[0]), float.Parse(rot[1]),float.Parse(rot[2]),float.Parse(rot[3]));
    }

	public static string AddSpacesToSentence(string text)
	{
		return AddSpacesToSentence (text, true);
	}

	public static string AddSpacesToSentence(string text, bool preserveAcronyms)
	{
		if (string.IsNullOrEmpty(text))
			return string.Empty;
		
		StringBuilder newText = new StringBuilder(text.Length * 2);
		newText.Append(text[0]);
		for (int i = 1; i < text.Length; i++)
		{
			if (char.IsUpper(text[i]))
			if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
				(preserveAcronyms && char.IsUpper(text[i - 1]) && 
					i < text.Length - 1 && !char.IsUpper(text[i + 1])))
				newText.Append(' ');
			newText.Append(text[i]);
		}
		return newText.ToString();
	}

    public static string ShowAdd(params int[] numbers)
    {
        string s = "";
        int sum = 0;
        foreach (int i in numbers)
        {
            if (s != "") s += " + ";
            s += i.ToString();
            sum += i;
        }
        s += " = " + sum;
        return s;
    }


    /// <summary>
    /// Converts the given seconds to readable string.
    /// If format optional parameter is not set, it will return the biggest unit (for example 2 min 10 sec -> 3 min)
    /// The format should look like this: yy.mm.dd hh:mm:ss
    /// </summary>
    /// <returns>The to string.</returns>
    /// <param name="seconds">Seconds.</param>
    /// <param name="format">Format.</param>
    public static string SecondsToString(float seconds, string format = "", string zero = "completed")
    {
        if (seconds == 0) return zero;

        System.TimeSpan t = System.TimeSpan.FromSeconds((double)seconds);

        // Formated
        if (format != "") return string.Format(format, t);
                     

        if( t.Days > 0) return "" + Mathf.CeilToInt((float)t.TotalDays) + ((t.TotalDays > 1) ? " days": " day");
        if( t.Hours > 0) return "" + Mathf.CeilToInt((float)t.TotalHours) + ((t.TotalHours > 1) ? " hours": " hour");
        if( t.Minutes > 0) return "" + Mathf.CeilToInt((float)t.TotalMinutes) + ((t.TotalMinutes > 1) ? " minutes": " minute");
        if( t.Seconds > 0) return "" + Mathf.CeilToInt((float)t.TotalSeconds) + ((t.TotalSeconds > 1) ? " seconds": " second");

        return "";
    }

    public static float TimeLeft(string startTime, float duration)
    {
        DateTime start = DateTime.Parse(startTime);
        long elapsedTicks = DateTime.Now.Ticks - start.Ticks;
        TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
        return Mathf.Max( 0, duration - (float)elapsedSpan.TotalSeconds );
    }


    public static float CraftTime(GameItemContent gItem)
    {
        if (gItem.GetType() == typeof(BuildingContent))
            return ((BuildingContent)gItem).GetUpgrade(0).UpgradeTime;
        return gItem.CraftTime;
    }

    public static List<SMaterial> CraftCost(GameItemContent gItem)
    {
        if (gItem.GetType() == typeof(BuildingContent))
            return ((BuildingContent)gItem).GetUpgrade(0).Cost;
        return gItem.Price;
    }



    public static int Add(out string log, params int[] list)
    {
        log = "";
        int sum = 0;
        foreach (int i in list)
        {
            if (log != "")
                log += " + ";
            log += i.ToString();
            sum += i;
        }

        log += " = " + sum;
        return sum;
    }
       
}
