﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RaycastResult
{
    public bool Dragging = false;
	public bool Touch = false;
	public bool ValidHit = false;
	public RaycastHit Hit = new RaycastHit();
}

public class MultiplatformInput 
{
    public static RaycastResult Last;

    static bool initialized = false;

	public static RaycastResult Raycast()
	{
        if (!initialized)
        {
            Input.simulateMouseWithTouches = true;
            initialized = true;
        }

		RaycastResult result = new RaycastResult ();
        result.Touch = false;
        result.Dragging = false;


        // TOUCH VERSION
        if(Input.touchCount > 0)
        {            
            var t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began || t.phase == TouchPhase.Moved)
            {
                result.Touch = !EventSystem.current.IsPointerOverGameObject(t.fingerId);
                if (result.Touch) 
                {
                    var h = Raycast (t.position);  
                    if (h != null) 
                    {
                        result.ValidHit = true;
                        result.Hit = (RaycastHit)h;
                    }
                }             
            }           
        }
		// MOUSE VERSION
        else if (Input.GetMouseButton(0))
		{
            result.Touch = !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject ();
			if (result.Touch) 
			{
				var h = Raycast (Input.mousePosition);		
				if (h != null) 
				{
					result.ValidHit = true;
					result.Hit = (RaycastHit)h;
				}
			}           
		}

        result.Dragging = (Last != null && Last.Touch && result.Touch);
        Last = result;

		return result;
	}



	/// <summary>
	/// Raycasts to the specified position.
	/// </summary>
	/// <param name="position">Position.</param>
	public static RaycastHit? Raycast(Vector3 position)
	{
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (position); 
        if (Physics.Raycast(ray, out hit))
            return hit;
		return null;
	}      
}
