﻿using UnityEngine;
using System.Collections;
//using Facebook.Unity;
using System.Collections.Generic;

public class AuthTools
{

    /// <summary>
    /// Gets the get device identifier.
    /// Only works on Android or IPhone. On other devices it returns an empty string.
    /// </summary>
    /// <value>The get device identifier.</value>
    public static string GetDeviceId
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                string deviceID = string.Empty;

                #if UNITY_ANDROID
                //Source: http://answers.unity3d.com/questions/430630/how-can-i-get-android-id-.html
                AndroidJavaClass clsUnity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject objActivity = clsUnity.GetStatic<AndroidJavaObject>("currentActivity");
                AndroidJavaObject objResolver = objActivity.Call<AndroidJavaObject>("getContentResolver");
                AndroidJavaClass clsSecure = new AndroidJavaClass("android.provider.Settings$Secure");
                deviceID = clsSecure.CallStatic<string>("getString", objResolver, "android_id");
                #endif

                #if UNITY_IPHONE
                deviceID = UnityEngine.iOS.Device.vendorIdentifier;
                #endif
                return deviceID;
            }
            else
                LevelLoader.Instance.LogError("Must be using android or ios platforms to use deveice id.");
            return string.Empty;
        }
    }


    public static string FacebookToken
    {
        get
        { 
            string token = string.Empty;
//            FB.Init (delegate (){               
//                FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, 
//                    delegate(ILoginResult result) 
//                    {
//                        FB.Mobile.RefreshCurrentAccessToken( delegate(IAccessTokenRefreshResult r) {
//                            if (r.AccessToken == null) 
//                                token = r.AccessToken.TokenString;
//                        });
//                     });
//            });
            return token;
        }
    }     


    public static string GoogleToken
    {
        get
        { 
            string token = string.Empty;


            return token;
        }
    }

	


   /* From MainMenu, but this didn't work...
    * AndroidJavaClass activityClass;
    const int GET_TOKEN_REASON = 2;
    const int SIGN_IN_REASON = 1;

    public void GooglePlusLogin()
    {
        // Using Outh2 https://developers.google.com/identity/protocols/OAuth2ForDevices

        // ClientID: 494387956116-7u938n62ct4e8jls35puounojjvu20lq.apps.googleusercontent.com
        // https://accounts.google.com/o/oauth2/device/code
        // POST client_id=[CLIENT-ID]&scope=email%20profile

        // Response JSON: device_code, user_code, verification_url

        // >> https://www.googleapis.com/oauth2/v4/token
        // client_id=[CLIENT-ID]&
        // client_secret={clientSecret}&
        //  code=4/4-GMMhmHCXhWEzkobqIHGG_EnNYYsAkukHspeYUk9E8&
        //  grant_type=http://oauth.net/grant_type/device/1.0

        // Response JSON: access token


        LevelLoader.Instance.Log("Google+ Login");

        AndroidJNI.AttachCurrentThread();        
        string className = "com.ThugLeaf.GooglePlusForUnity.GooglePlusActivity";
        activityClass = new AndroidJavaClass(className);
        activityClass.SetStatic<string>("UnityObjectName", "_MainMenuLogic");

        activityClass.SetStatic<string>("OnTokenReceivedCallback", "OnSignInSuccess");
        activityClass.CallStatic("Start", new object[] { SIGN_IN_REASON });
    }

    public void OnSignInSuccess(string googleId) 
    {
        activityClass.SetStatic<string>("OnTokenReceivedCallback", "OnGoogleTokenReceived"); 
        activityClass.CallStatic("Start", new object[] { GET_TOKEN_REASON });
    }

    public void OnTokenReceivedCallback(string accessToken) 
    {
        LevelLoader.Instance.Log ("Google Token recieved: " + accessToken);
    }*/
}
