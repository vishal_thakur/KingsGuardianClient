﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using System.Text.RegularExpressions;

public class MassRenamer : EditorWindow
{
	[MenuItem ("Window/Mass Renamer")]
	static void Init () {
		// Get existing open window or if none, make a new one:
		MassRenamer window = (MassRenamer)EditorWindow.GetWindow <MassRenamer>("Mass Renamer", true);
		window.Show ();
	}

	Texture2D _texture;
	TextAsset _textAsset;

	string _path; 
	TextureImporter _textureImporter; 
	SpriteMetaData[] _sprites;
	string[] _newNames;

	Vector2 _spriteScrollPos = Vector2.zero;

	void OnGUI () 
	{
		GUILayout.BeginHorizontal ();

		// LOAD SPRITE
		GUILayout.BeginVertical ();
		_texture = (Texture2D)EditorGUILayout.ObjectField (_texture, typeof(Texture2D), false);
		LoadSpriteSheet ();
		GUILayout.EndVertical ();

		GUILayout.Space (10);

		// LOAD TEXT FILE
		GUILayout.BeginVertical ();
		_textAsset = (TextAsset)EditorGUILayout.ObjectField (_textAsset, typeof(TextAsset), false);
		if (_textAsset == null) 
			EditorGUILayout.HelpBox ("Please select a text asset!", MessageType.Info);
		else if (GUILayout.Button ("Load Text File"))
			_newNames = _textAsset.text.Split ('\n');
		GUILayout.EndVertical ();

		// LIST
		GUILayout.EndHorizontal ();
		if (_sprites != null)
		{
			_spriteScrollPos = GUILayout.BeginScrollView (_spriteScrollPos);
			GUILayout.Label ("Sprites:");
			for (int i = 0; i < _sprites.Length; i++) 
			{
				string text = " - " + _sprites [i].name;
				if (_newNames != null && i < _newNames.Length)
					text += " -> " + _newNames [i];
				GUILayout.Label (text);
			}
			GUILayout.EndScrollView ();
		}

		// RENAME BUTTON
		if (_sprites != null && _newNames != null) 
		{
			if (_sprites.Length != _newNames.Length)
				EditorGUILayout.HelpBox ("The number of sprites(" + _sprites.Length + ") and the names(" + _newNames.Length + ") in the text file are different.", MessageType.Error);
			else if (GUILayout.Button ("Rename")) 
				Rename ();
		}	
	}


	void OnInspectorUpdate() {
		Repaint();
	}


	void LoadSpriteSheet()
	{
		if (_texture == null) {
			EditorGUILayout.HelpBox ("Please select a spritesheet!", MessageType.Info);
			return;
		}

		if (GUILayout.Button ("Load Sprites"))
		{
			_path = AssetDatabase.GetAssetPath(_texture); 
			_textureImporter = AssetImporter.GetAtPath(_path) as TextureImporter; 
			_sprites = _textureImporter.spritesheet; 
		}
	}


	void Rename()
	{
		for (int i = 0; i < _sprites.Length; i++) 
		{
			_newNames [i] = Regex.Replace (_newNames[i], "[^\\w\\._]", "");
			_newNames [i] = Regex.Replace (_newNames[i], " ( )+", "");
			_sprites [i].name = _newNames [i];
			EditorUtility.DisplayProgressBar ("Renaming Sprites", "Renaming " + _sprites [i].name + " to " + _newNames [i] + ".", i / (_sprites.Length+2));
		}

		EditorUtility.DisplayProgressBar ("Renaming Sprites", "Saving Assets.", _sprites.Length / _sprites.Length+2);
		_textureImporter.spritesheet = _sprites; 
		EditorUtility.SetDirty(_textureImporter); 
		_textureImporter.SaveAndReimport(); 

		// Reimport asset. 
		EditorUtility.DisplayProgressBar ("Renaming Sprites", "Reimporting Assets.", _sprites.Length+1 / _sprites.Length+2);
		AssetDatabase.ImportAsset(_path, ImportAssetOptions.ForceUpdate); 

		EditorUtility.ClearProgressBar();
	}
}
