﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;



//[CustomEditor(typeof(Light))]
public class keepLightEditor : keepComponentEditor<Light>
{
    protected override  string ID { get { return "keepLight"; }}

    protected override void OnSaveData(ref List<string> saveData)
    {        
        saveData.Add(((int)myTarget.type).ToString());

        saveData.Add(myTarget.color.a.ToString());
        saveData.Add(myTarget.color.r.ToString());
        saveData.Add(myTarget.color.g.ToString());
        saveData.Add(myTarget.color.b.ToString());

        saveData.Add(myTarget.intensity.ToString());
        saveData.Add(myTarget.bounceIntensity.ToString());

        saveData.Add(myTarget.cullingMask.ToString());
    }

    protected override void OnLoadData(string[] lines)
    {
        myTarget.type = (LightType)int.Parse(lines[1]);
        myTarget.color = new Color(float.Parse(lines[3]), float.Parse(lines[4]), float.Parse(lines[5]), float.Parse(lines[2]));

        myTarget.intensity = float.Parse(lines[6]);
        myTarget.bounceIntensity = float.Parse(lines[7]);
        myTarget.cullingMask = int.Parse(lines[8]);
    }   

    protected override void DrawInspector()
    {
        // Replicate the standard transform inspector gui        
        EditorGUIUtility.labelWidth = 150;
        EditorGUIUtility.fieldWidth = 50;

        EditorGUI.indentLevel = 0;

        myTarget.type = (LightType)EditorGUILayout.EnumPopup("Type", myTarget.type);
        myTarget.color = EditorGUILayout.ColorField("Color", myTarget.color);
        myTarget.intensity = EditorGUILayout.Slider("Intensity", myTarget.intensity, 0, 8);
        myTarget.bounceIntensity = EditorGUILayout.Slider("Bounce Intentsity",myTarget.bounceIntensity, 0, 8);
        myTarget.cullingMask = EditorGUILayout.MaskField("Culling Mask", myTarget.cullingMask, GetLayerNames());

        EditorGUIUtility.labelWidth = 0;
        EditorGUIUtility.fieldWidth = 0;

        if (GUI.changed)
        {
            Undo.RecordObject(myTarget, "Transform Change");
        }
    }

}


//[CustomEditor(typeof(Transform))]
public class keepTransformEditor : keepComponentEditor<Transform>
{
    protected override  string ID { get { return "keepTransform"; }}

    protected override void OnSaveData(ref List<string> saveData)
    {
        saveData.Add(myTarget.localPosition.x.ToString());
        saveData.Add(myTarget.localPosition.y.ToString());
        saveData.Add(myTarget.localPosition.z.ToString());

        saveData.Add(myTarget.localRotation.eulerAngles.x.ToString());
        saveData.Add(myTarget.localRotation.eulerAngles.y.ToString());
        saveData.Add(myTarget.localRotation.eulerAngles.z.ToString());

        saveData.Add(myTarget.localScale.x.ToString());
        saveData.Add(myTarget.localScale.y.ToString());
        saveData.Add(myTarget.localScale.z.ToString());
    }

    protected override void OnLoadData(string[] lines)
    {
        myTarget.localPosition = new Vector3(System.Convert.ToSingle(lines[1]), System.Convert.ToSingle(lines[2]), System.Convert.ToSingle(lines[3]));
        myTarget.localRotation = Quaternion.Euler(System.Convert.ToSingle(lines[4]), System.Convert.ToSingle(lines[5]), System.Convert.ToSingle(lines[6]));
        myTarget.localScale = new Vector3(System.Convert.ToSingle(lines[7]), System.Convert.ToSingle(lines[8]), System.Convert.ToSingle(lines[9]));
    }   

    protected override void DrawInspector()
    {
        // Replicate the standard transform inspector gui        
        EditorGUIUtility.labelWidth = 25;
        EditorGUIUtility.fieldWidth = 50;

        EditorGUI.indentLevel = 0;
        Vector3 position = EditorGUILayout.Vector3Field("Position", myTarget.localPosition);
        Vector3 eulerAngles = EditorGUILayout.Vector3Field("Rotation", myTarget.localEulerAngles);
        Vector3 scale = EditorGUILayout.Vector3Field("Scale", myTarget.localScale);

        EditorGUIUtility.labelWidth = 0;
        EditorGUIUtility.fieldWidth = 0;

        if (GUI.changed)
        {
            Undo.RecordObject(myTarget, "Transform Change");

            myTarget.localPosition = FixIfNaN(position);
            myTarget.localEulerAngles = FixIfNaN(eulerAngles);
            myTarget.localScale = FixIfNaN(scale);
        }
    }

}















public class keepComponentEditor<T> : Editor where T : Component
{
    protected T myTarget { get { return (T)target; }} 
    protected virtual string ID { get { return "keepTransform"; }}

    protected virtual void OnSaveData(ref List<string> saveData) {}
    protected virtual void OnLoadData(string[] lines){}
    protected virtual void DrawInspector(){ DrawDefaultInspector(); }

    public override void OnInspectorGUI()
    {
        DrawInspector();

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
            SaveData(myTarget.gameObject);

        if (GUILayout.Button("Load"))
            LoadData(myTarget.gameObject);

        EditorGUILayout.EndHorizontal();
    }

    string GetInstanceFileName(GameObject baseObject)
    {
        return System.IO.Path.GetTempPath() + baseObject.name + "_" + baseObject.GetInstanceID() + "."+ID+".txt";
    }

    void SaveData(GameObject baseObject)
    {
        List<string> saveData = new List<string>();

        saveData.Add(this.GetInstanceID().ToString());
        OnSaveData(ref saveData);
        System.IO.File.WriteAllLines(GetInstanceFileName(baseObject), saveData.ToArray());
    }

    void LoadData(GameObject baseObject)
    {
        string[] lines = System.IO.File.ReadAllLines(GetInstanceFileName(baseObject));
        if (lines.Length > 0)
        {
            OnLoadData(lines);
            System.IO.File.Delete(GetInstanceFileName(baseObject));
        }
    }


    protected Vector3 FixIfNaN(Vector3 v)
    {
        if (float.IsNaN(v.x))
            v.x = 0.0f;
        if (float.IsNaN(v.y))
            v.y = 0.0f;
        if (float.IsNaN(v.z))
            v.z = 0.0f;
        return v;
    }  

    protected string[] GetLayerNames()
    {
        List<string> layerNames = new List<string>();
        for (int i = 8; i <= 31; i++) //user defined layers start with layer 8 and unity supports 31 layers
        {
            var layerN = LayerMask.LayerToName(i); //get the name of the layer
            if (layerN.Length > 0) //only add the layer if it has been named (comment this line out if you want every layer)
                layerNames.Add(layerN);
        }
        return layerNames.ToArray();
    }
}
