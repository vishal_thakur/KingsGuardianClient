﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class SceneLoader : EditorWindow {

	[MenuItem ("Window/SceneList")]
	static void Init () 
	{
		// Get existing open window or if none, make a new one:
		SceneLoader window = (SceneLoader)EditorWindow.GetWindow <SceneLoader>("Scene List", true);
		window.Show ();
	}

	EditorBuildSettingsScene[] _scenes;
	string[] _sceneNames;
	GUIStyle _style;


	void OnGUI ()
	{
		if (_style == null) 
		{
			_style = EditorStyles.toolbarButton;
			_style.alignment = TextAnchor.MiddleLeft;

		}

		if ( _scenes == null || _scenes.Length != EditorBuildSettings.scenes.Length) 
		{
			_scenes = EditorBuildSettings.scenes;
			_sceneNames = new string[_scenes.Length];

			for (int i = 0; i < _sceneNames.Length; i++) 
			{
				string[] path = _scenes [i].path.Split('/');
				string[] filename = path [path.Length - 1].Split ('.');
				_sceneNames [i] = filename [0];
			}
		}

		for (int cnt = 0; cnt < _sceneNames.Length; cnt++) 
		{
			string label = ((_scenes[cnt].enabled)?"[X] ":"[  ] ")+ _sceneNames [cnt];

			if (EditorSceneManager.GetActiveScene ().name == _sceneNames [cnt]) 
			{
				Color c = GUI.color;
				GUI.color = Color.green;
				GUILayout.Label (label, _style);
				GUI.color = c;
			}
			else if (GUILayout.Button (label, _style ))
				LoadScene (cnt);
		}
	}

	void LoadScene(int cnt)
	{
		string path = _scenes [cnt].path;

		Debug.Log ("Try to load scene: " + path);

		EditorSceneManager.SaveOpenScenes ();
		EditorSceneManager.OpenScene (path);

		//
		//EditorSceneManager.CloseScene ();


	}
}
