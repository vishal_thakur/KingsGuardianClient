﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class Polycount
{
	[MenuItem ("Window/Show Polycount")]
	public static void ShowPolycount()
	{
		if (Selection.activeObject.GetType () != typeof(GameObject)) { Debug.Log (Selection.activeObject.name + " is not a game object!"); return; }

        int totalTriCount = 0;

        MeshFilter[] meshfilters = Selection.activeGameObject.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter filter in meshfilters)
            totalTriCount +=  filter.sharedMesh.triangles.Length / 3;

        SkinnedMeshRenderer[] skinnedRenderer = Selection.activeGameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true);
        foreach (SkinnedMeshRenderer renderer in skinnedRenderer)
            totalTriCount +=  renderer.sharedMesh.triangles.Length / 3;
         
        Debug.Log (Selection.activeGameObject.name + " has " + totalTriCount + " triangles.");

		
		// Get count of parent object if applicable
		/*var meshfilter = Selection.activeGameObject.GetComponent<MeshFilter>();
 
		if (meshfilter != null )
			totalTriCount +=  meshfilter.sharedMesh.triangles.Length / 3;

		// Get count of all children of object
		var allChildren = Selection.activeGameObject.GetComponentsInChildren<Transform>();
		foreach (Transform child in allChildren) 
		{
			var mf = child.GetComponent<MeshFilter>();
			if (mf != null)
				totalTriCount +=  mf.sharedMesh.triangles.Length / 3;
		}

		Debug.Log (Selection.activeGameObject.name + " has " + totalTriCount + " triangles.");*/
	}

  



}