﻿using UnityEngine;
using System;


public class GUID 
{
    public static string Create()
    {
        var random = new System.Random();
        DateTime epochStart = new DateTime(1970, 1, 1, 8, 0, 0, DateTimeKind.Utc);
        double timestamp = (System.DateTime.UtcNow - epochStart).TotalSeconds;
        string uniqueID =   Application.systemLanguage + "-" + 
                            Application.platform+"-"+
                            String.Format("{0:X}", Convert.ToInt32(timestamp))+"-"+
                            String.Format("{0:X}", Convert.ToInt32(Time.time*1000000))+"-"+
                            String.Format("{0:X}", random.Next(100000000));
        return uniqueID;                
    }
}
