﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyScoutManager : MonoBehaviour {

	[SerializeField] GameObject PlayerBaseTerrain;
	[SerializeField] GameObject BattleBaseTerrain;



	void Start(){
//		Debug.LogError ("Star of something beautiful");
//		Messenger.AddListener (Events.OnScoutModeSceneLoaded , RefreshScene);
	}

//	void OnDestroy(){
//		Messenger.RemoveListener(Events.OnScoutModeSceneLoaded , RefreshScene);
//	}

	void OnEnable(){
		RefreshScene ();
	}

	/// <summary>
	/// Refreshs the scene.
	/// </summary>
	public void RefreshScene(){
		//Grab Scout Mode Flag
//		Debug.LogError ("Working yar!!" +  Time.time);
		bool flag = TownLogic.Instance.ScoutMode;


      //  Debug.LogError("flag = " + TownLogic.Instance.ScoutMode);
		//Manipulate Player Base and Battle Base According to the Battle/Scout Mode
		PlayerBaseTerrain.GetComponent<MeshRenderer>().enabled = !flag;
//		Debug.LogError ("Player base is set: " + (!flag).ToString());
		BattleBaseTerrain.SetActive (flag);
//		Debug.LogError ("Battle base is set: " + flag.ToString());

	}

}
