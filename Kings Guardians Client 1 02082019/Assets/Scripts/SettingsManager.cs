﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {

	[SerializeField] Toggle PushNotificationsToggleRef;



	void Start(){
		LoadPushNotificationsToggleStatus ();
	}


	void LoadPushNotificationsToggleStatus(){
		if (PlayerPrefs.HasKey ("pushNotificationsEnabled")) {
			//Get Push Notif Enabled Flag and Set Push Notifcation 
			SetPushNotificationsEnabled(PlayerPrefs.GetInt ("pushNotificationsEnabled") == 0 ? false : true); 

		} 
		else {
			//Set Push Notif Enabled By default
			PlayerPrefs.SetInt ("pushNotificationsEnabled", 1);
			SetPushNotificationsEnabled (true);
		}
	}
 


	void SetPushNotificationsEnabled(bool flag){
		Globals.PushNotificationsEnabled = PushNotificationsToggleRef.isOn = flag;
		PlayerPrefs.SetInt ("pushNotificationsEnabled", flag == false ? 0 : 1);

		//Send Message to Server to Ban/Unban the specified Player
		var msg = new SimpleMessage ();
		msg.Add (ParameterCode.StringValue, flag.ToString());
		NetworkManager.SendRequest (RequestCode.TogglePushNotifEnabled, msg);
	}





	//---------------------Callbacks---------------------

	public void OnPushNotificationsToggled(Toggle toggle){
		SetPushNotificationsEnabled (toggle.isOn);
	}
}
