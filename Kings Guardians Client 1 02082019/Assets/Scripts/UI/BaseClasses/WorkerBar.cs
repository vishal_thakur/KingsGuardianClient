﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorkerBar : MonoBehaviour
{
    public bool Warlock = false;
    public Image Icon;
    Text _text;

    // Use this for initialization
    void Awake ()
    {
        _text = GetComponentInChildren<Text> ();
    }

    // Update is called once per frame
    void Update ()
    {
		if (Warlock)
			_text.text = GameManager.Instance.LocalPlayer.FreeWarlocks +"/"+GameManager.Instance.LocalPlayer.Warlocks.Count;
        else
            _text.text = GameManager.Instance.LocalPlayer.FreeWorkers +"/"+GameManager.Instance.LocalPlayer.Workers.Count;
    }
}
