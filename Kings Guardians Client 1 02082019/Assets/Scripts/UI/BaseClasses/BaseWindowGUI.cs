﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.UI;

public class BaseWindowGUI : MonoBehaviour 
{
    public Text Title;
	public GameObject Window;
	public bool IsOpen { get { return Window.activeSelf; } }

    [HideInInspector]public bool foldout = true;
    [HideInInspector]public List<GameObjectEvent> ToggleOnEvents = new List<GameObjectEvent>();
    [HideInInspector]public List<GameObjectEvent> ToggleOffEvents = new List<GameObjectEvent>();

	public void Toggle (bool t)
	{

		Globals.IsAnyUIPanelActive = t;
		if(Window != null)
		{
			Window.SetActive (t);
            HandleToggleEvents(t);
            if (t) OnOpened ();
			else OnClosed();
		}
	}

	public void Toggle()
	{
		Toggle(!Window.activeSelf);
	}


	/// <summary>
	/// Raises the opened event.
	/// </summary>
	protected virtual void OnOpened(){}

	/// <summary>
	/// Raises the closed event.
	/// </summary>
	protected virtual void OnClosed(){}


    void HandleToggleEvents(bool t)
    {
        List<GameObjectEvent> list = (t) ? ToggleOnEvents : ToggleOffEvents;
        foreach (var e in list)
        {
            switch (e.Event)
            {
                case GameObjectEvents.Show: e.GameObject.SetActive(true); break;
                case GameObjectEvents.Hide: e.GameObject.SetActive(false); break;
            }
        }
    }


}




[System.Serializable]
public class GameObjectEvent
{
    public GameObject GameObject;
    public GameObjectEvents Event;
}
public enum GameObjectEvents
{
    Hide,
    Show,
}

