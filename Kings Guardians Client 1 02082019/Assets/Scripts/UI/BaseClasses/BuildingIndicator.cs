﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuildingIndicator
{
	BuildingHUD _hud;
	BuildingBehaviour _building;
	QueueInfo _process;

	float _lastCheck = -1f;
	bool _progressBarCompleted = false;

	public void Init(BuildingHUD hud, BuildingBehaviour building)
	{
		_hud = hud;
		if (_hud == null)
		{
			Debug.Log(building.Reference.ContentID + " doesn't have building HUD set");
			return;
		}
		_hud.Init();

		_building = building;
		_hud.Name = _building.Reference.ContentData.Name;
		_hud.Upgrade = _building.Reference.Upgrade;

		_hud.CollectIcon.sprite = AssetPackManager.GetCurrencyIcon(building.Reference.ContentData.Material);
		_hud.CollectIcon.enabled = false;

	}

    public void Update()
    {
        if (_building == null || _hud == null) return;
		if (_building.Reference.ContentID == "Wall")
			return;	


		_hud.ToggleLabel(_building.Selected);   

		if (_lastCheck < _building.Reference.LastUpdated)
		{
			_hud.Upgrade = _building.Reference.Upgrade;

			// If upgrading or constructin
			if (_building.CurrentState == BuildingState.Inactive)
			{
				if (_building.Reference.AssignedWorker != null && !_hud.ProgressBarEnabled)
				{
					_hud.ProgressBarEnabled = true;
					_process = _building.Reference.AssignedWorker.WorkingOn;
					_hud.UpdateProgressBar(0, _process.Duration);
				}
			}

			// If producing something
			else if (_building.Reference.Queue != null && _building.Reference.Queue.Count > 0)
			{
				if (!_hud.ProgressBarEnabled || _progressBarCompleted)
				{
					_hud.ProgressBarEnabled = true;
					_process = _building.Reference.Queue[0];
					_hud.UpdateProgressBar(0, _process.Duration);
				}
			}

			// Otherwise hide
			else if (_hud.ProgressBarEnabled)
			{
				_hud.ProgressBarEnabled = true;
				_process = null;
			}
			_lastCheck = Time.time;
		}

        // Update progressbar
        if (_process != null)
        {
            float timeLeft = Utils.TimeLeft(_process.StartTime, _process.Duration);
           
            bool noStorage = false;
            if (timeLeft == 0)
            {
                int storage = GameManager.Instance.LocalPlayer.GetStorage(_building.Reference.ContentData.Material);
                int material = GameManager.Instance.LocalPlayer.GetMaterial(_building.Reference.ContentData.Material);
                noStorage = storage <= material;
                _progressBarCompleted = true;
            }

			_hud.UpdateProgressBar(timeLeft);  

			if (noStorage)
				_hud.ProgressBar.Text = "full";
       }                        
    }
	 
    public void Toggle(bool t)
    {
		if(_hud != null)
			_hud.Toggle(t);
    }

	public void ToggleCollectIcon(bool t)
	{
		if (t && TownLogic.Instance.ScoutMode)
			return;
		
        if(_hud.CollectIcon != null)
    		_hud.CollectIcon.enabled = t;	
	}
}
