﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CostField : MonoBehaviour 
{
	public Currency Material;
	public Image Icon;
	public Text Text;
	public bool CompareToPlayerVault = true;

	int _amount;

	// Use this for initialization
	void Awake ()
	{
		Icon.sprite = AssetPackManager.GetCurrencyIcon (Material);
	}

    void Update()
    {
		Text.color = (CompareToPlayerVault && GameManager.Instance.LocalPlayer.GetMaterial(Material) < _amount) ? Color.red : Color.black;
    }


	public void Set( Currency mat, int amount)
	{
		Material = mat;
		Icon.sprite = AssetPackManager.GetCurrencyIcon (Material);
		Set (amount);
	}

	// Update is called once per frame
	public void Set (int amount)
	{
		Toggle (true);
		_amount = amount;
		Text.text = (amount > 0) ? amount.ToString () : "Free";
	}

    public void Hide()
	{
		Toggle (false);
	}

	public void Toggle(bool t)
	{
		Icon.enabled = t;
		Text.enabled = t;
	}
}
