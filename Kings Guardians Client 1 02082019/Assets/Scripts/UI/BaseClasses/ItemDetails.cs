﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemDetails : MonoBehaviour
{
    public Text Name;
    public Text Description;
    public Image Icon;
    public CostField[] Cost;
    public Text CraftTime;

    public void Set(GameItemContent item)
    {
        Name.text = item.Name;
        Description.text = item.GetDescription();

        if(CraftTime != null)
            CraftTime.text = Utils.SecondsToString(Utils.CraftTime(item));

        if (Icon != null)
        {
            Icon.enabled = true;
            if ( Utils.IsInherited(item.GetType(), typeof(BuildingContent)))
                Icon.sprite = AssetPackManager.GetBuildingAssets(item.ContentID).Icon;
            else if ( Utils.IsInherited(item.GetType(), typeof(CharacterContent)))
                Icon.sprite = AssetPackManager.GetCharacterAssets(item.ContentID).Icon;
            else if ( Utils.IsInherited(item.GetType(), typeof(ItemContent)) )
                Icon.sprite = AssetPackManager.GetCardIcon(item.ContentID, true);
            else
            {
                Icon.enabled = false;
                Debug.Log(item.GetType());
            }

        }

        int cnt = 0;
        foreach (var p in Utils.CraftCost(item))
        {
            if (Cost.Length > cnt)
                Cost[cnt].Set(p.Material, p.Amount);
            cnt++;
        }       
    }
}
