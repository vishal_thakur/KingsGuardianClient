﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MaterialBar : MonoBehaviour 
{
	public Currency Material;
	public Image Icon;
	public bool ShowStorage = false;
	Text _text;

    float _lastUpdate = -1f;

	Player _player;
	bool _initialized;

	// Use this for initialization
	void Awake ()
	{
		_text = GetComponentInChildren<Text> ();
        Icon.sprite = AssetPackManager.GetCurrencyIcon (Material);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!TownLogic.Instance.Ready)
			return;

		if (!_initialized)
		{
			if (!TownLogic.Instance.ScoutMode)
				_player = GameManager.Instance.LocalPlayer;
			else
				_player = GameManager.Instance.TargetPlayer;

			_initialized = true;
		}

		if (_player == null)
			return;


//		if (_lastUpdate < _player.LastUpdate)
//        {
			_text.text = _player.GetMaterial(Material).ToString();
			if (ShowStorage) {
//				_text.text += "\n<size=25>Max: "+_player.GetStorage(Material) +"</size>";
				_text.text += "\nMax: " + _player.GetStorage (Material);
			}
//			else
//				_lastUpdate = Time.time;
//				_text.text = _player.GetMaterial(Material).ToString();

//			_lastUpdate = Time.time;
//        }      
    }


	public void OnMaterialClicked(){
		StopCoroutine("ShowStorageForAWhile");
		StartCoroutine (ShowStorageForAWhile(3));
	}

	IEnumerator ShowStorageForAWhile(int seconds){
		ShowStorage = true;
		yield return new WaitForSeconds (seconds);
		ShowStorage = false;
		StopCoroutine ("ShowStorageForAWhile");
	}

}
