﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(ImageSlider), true)]
public class ImageSliderEditor : Editor
{  

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ImageSlider myTarget = (ImageSlider)target;

        EditorGUI.BeginChangeCheck();

        myTarget.Value = EditorGUILayout.Slider("Value", myTarget.Value, myTarget.minValue, myTarget.maxValue);

        if (EditorGUI.EndChangeCheck())
        {
            EditorUtility.SetDirty(myTarget);
        }
    }

}
