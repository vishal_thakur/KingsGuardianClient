﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(BaseWindowGUI), true)]
public class BaseWindowEditor : Editor
{
    ReorderableList _toggleOnEvents;
    ReorderableList _toggleOffEvents;

    protected void OnEnable()
    {
        _toggleOnEvents = new ReorderableList(serializedObject, serializedObject.FindProperty("ToggleOnEvents"), true, true, true, true);
        _toggleOnEvents.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Toggle ON events");
        };

        _toggleOnEvents.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = _toggleOnEvents.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("GameObject"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Event"), GUIContent.none);
        };


        _toggleOffEvents = new ReorderableList(serializedObject, serializedObject.FindProperty("ToggleOffEvents"), true, true, true, true);
        _toggleOffEvents.drawHeaderCallback = (Rect rect) => {  
            EditorGUI.LabelField(rect, "Toggle OFF events");
        };

        _toggleOffEvents.drawElementCallback =  
            (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = _toggleOffEvents.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("GameObject"), GUIContent.none);
            EditorGUI.PropertyField(
                new Rect(rect.x + rect.width/2, rect.y, rect.width/2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("Event"), GUIContent.none);
        };
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BaseWindowGUI myTarget = (BaseWindowGUI)target;

        myTarget.foldout = EditorGUILayout.Foldout(myTarget.foldout, "Toggle Events");

        if (myTarget.foldout)
        {
            serializedObject.Update();
            _toggleOnEvents.DoLayoutList();
            serializedObject.ApplyModifiedProperties();

            serializedObject.Update();
            _toggleOffEvents.DoLayoutList();
            serializedObject.ApplyModifiedProperties();
        }       
    }

}
