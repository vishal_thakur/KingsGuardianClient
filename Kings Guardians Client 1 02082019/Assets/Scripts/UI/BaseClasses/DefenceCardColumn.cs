﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DefenceCardColumn : MonoBehaviour
{
	public Element Element;
	Text _value;
    Text _label;
    Image _icon;

    public bool Visible {
        get { return _value.enabled; } 
        set 
        {
            _value.enabled = value; 
            _label.enabled = value;
            _icon.enabled = value;
        }
    }

	void Awake()
	{
        foreach (var t in GetComponentsInChildren<Text>())
        {
            if (t.name == "Value")
                _value = t;
            else if (t.name == "Label Text")
                _label = t;
        }	
        _icon = GetComponentInChildren<Image>();
	}

	public void Set(string txt)
	{
        if(_icon != null)
            _icon.sprite = AssetPackManager.GetElementIcon(Element);
        
        if(_label != null)
            _label.text = Utils.AddSpacesToSentence(Element.ToString());
        
        if(_value != null)
            _value.text = txt;
	}
}
