﻿using UnityEngine;
using System.Collections;

public class BasePanelGUI 
{
    public bool Visible 
    {
        get{ return Panel.activeSelf; }
        set { Panel.SetActive(value);  if (value == true) OnVisible();}
    }

    public GameObject Panel;

    protected virtual void OnVisible(){ }

}
