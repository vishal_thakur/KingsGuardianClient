﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageSlider : MonoBehaviour 
{
    public float minValue;
    public float maxValue;
    public float Value 
    { 
        get { return _value; } 
        set
        { 
            _value = Mathf.Clamp(value, minValue, maxValue);
            float percentage = Mathf.Clamp((_value - minValue) / (maxValue - minValue), 0, 1);
            Amount.text = Mathf.RoundToInt(percentage * 100) + "%";
            Slider.fillAmount = percentage;
        }
    }

    public Image Slider;
    public Text Amount;
   
    float _value = 0;


}
