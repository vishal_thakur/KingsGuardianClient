﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SlotBase : MonoBehaviour 
{
    public GameObject LockedPanel;
    public Text LockedText;

    public ISlotHandler SlotHandler { get; set; }
    public int Index { get; protected set; }
    public bool Locked = false;   

	public virtual void SetIndex(int index)
    {
        Index = index;
    }

    protected void SetText(Text text, string value)
    {
        if (text == null) return;
        text.enabled = value != "";
        text.text = value;
    }

    protected void SetImage(Image image, Sprite sprite)
    {
        if (image == null) return;
        image.enabled = (sprite != null);
        image.sprite = sprite;
    }

    protected void Lock(string message)
    {
        Locked = true;
        LockedPanel.SetActive(true);
        SetText(LockedText, message);
    }

    protected void Unlock()
    {
        Locked = false;
        LockedPanel.SetActive(false);
    }



    public virtual void Select()
    {
        if( !Locked)
            SlotHandler.Select(this);    
    }

	public virtual void Deselect() 
    {
    }
}
