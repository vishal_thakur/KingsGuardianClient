﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ListGUI<L, S> : MonoBehaviour, ISlotHandler where L : IList where S : SlotBase
{
    public IList List { get { return list; }}    
    public L list;

    public Text PageLabel;

	public Transform ElementListParent;

	public GameItemSlot GameItemSlotPrefab;


    protected int page { get; private set; }

    protected int pages { get
		{
			int val = 0;
			try{
			val = Mathf.CeilToInt (List.Count<L> () / slots.Count) + 1;
			}
			catch(System.Exception e){
			}			

			return val;  
		} 
	}
    protected List<S> slots = new List<S>();



    protected virtual bool multiSelection { get { return false; }}

    void Awake()
    {
        Init();       
    }

//	/// <summary>
//	/// Raises the GU event.
//	/// </summary>
//	void OnGUI(){
//		for (int i = 0; i < slots.Count; i++) {
//			GUILayout.TextField ("Index : " + slots[i].Index);
//			GUILayout.TextField ("Locked panel GO : " + slots[i].LockedPanel.name);
//			GUILayout.TextField ("Locked Text : " + slots[i].LockedText.text);
//		}
//	}


    public void Init()
    {
        slots = GetComponentsInChildren<S>().ToList();
        foreach (var slot  in slots)
            slot.SlotHandler = this;
    }

    public void NextPage() { SetPage(page + 1); }
    public void PrevPage() { SetPage(page - 1); }
    

	//Shows All elements in a category
	public IEnumerator ShowCategoryElements(List<GameItemContent> elements){
		//Sorting Algo


		yield return null;


		// Step 1 - Filter Out Locked Units
		// Step 2 - Filter Out UnLocked Units
		// Step 3 - We have the elements sorted now add them to the GridContent Child list
		List<GameItemSlot> unlockedUnits = new List<GameItemSlot>();
		List<GameItemSlot> lockedUnits = new List<GameItemSlot>();

		for (int i = 0; i < elements.Count; i++) {
			//Grab the Game item content
			GameItemSlot tempGameItemSlot = Instantiate(GameItemSlotPrefab)as GameItemSlot;

			//Set Slot handler
			tempGameItemSlot.SlotHandler = this;

			//Set Index
			tempGameItemSlot.Initialize (i , elements[i]);



			// Sorting Step 1 and Step 2 
			if (tempGameItemSlot.isLocked)
				lockedUnits.Add (tempGameItemSlot);
			else
				unlockedUnits.Add (tempGameItemSlot);
		}


		foreach(GameItemSlot unit in unlockedUnits)
			unit.transform.SetParent (ElementListParent.transform, false);
		
		foreach(GameItemSlot unit in lockedUnits)
			unit.transform.SetParent (ElementListParent.transform, false);
	}



	public void SetPage(int p)
    {
        p = Mathf.Clamp(p, 1, pages);
        page = p;
        int sCount = slots.Count;

        for (int i = 0; i < sCount; i++)
            slots[i].SetIndex((p-1) * sCount + i);

//		Set page label
        if (PageLabel != null)
            PageLabel.text = page + "/" + pages;
    }


    public virtual void Select(SlotBase slot) 
    { 
        if (multiSelection) return;

        foreach (var s in slots)
        {
            if (s != slot)
                s.Deselect();
        }
    }

     

}
