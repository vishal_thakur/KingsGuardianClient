﻿using System.Collections.Generic;


public interface ISlotHandler
{
    IList List { get; }
    void Select(SlotBase slot);
}
