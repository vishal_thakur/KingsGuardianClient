﻿using System.Collections.Generic;

public interface IList
{
    int Count<T>();
    void Select(SlotBase slot);
}

public interface IGameItemList : IList
{
    GameItemContent GetContent(int index);     
}

public interface IItemList : IList
{
	ItemReference GetItem(int index);
}

public interface IPlayerList : IList
{
    PlayerInfo GetPlayer(int index);
}

public interface ICharacterList : IList
{
	CharacterReference GetCharacter(int index);
}
