﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class StatusBar : MonoBehaviour
{
    public bool Inverse = false;

    public Color Color 
    {
        get { return _fill.color; }
        set { _fill.color = value; }
    }


    public float MaxValue 
    {
        get { return _bar.maxValue; }
        set { _bar.maxValue = value; } 
    }

    public float Value { 
        get { return _bar.value; }
        set 
        { 
            if (_bar != null)
            {
                _bar.value = (Inverse ) ? _bar.maxValue - value : value;
                Text = String.Format(TextFormat, _bar.value, _bar.maxValue);
            }
        }
    }

    public string Text 
    {
        get { return _details.text; }
        set 
        { 
            if (_details != null)
               _details.text = value;
        }
    }


    public string TextFormat = "{0}%";

    Slider _bar;
    Text _details;
    Image _fill;

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        _bar = GetComponentInChildren<Slider>();
        _details = GetComponentInChildren<Text>();
        Value = 0;             
        _fill = _bar.fillRect.GetComponent<Image>();
    }

  
}
