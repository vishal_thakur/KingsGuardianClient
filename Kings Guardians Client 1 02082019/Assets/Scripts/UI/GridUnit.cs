﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridUnit : MonoBehaviour {

    [SerializeField]
    Text TextRef;

    [SerializeField]
    Color ToggleOffColor;
    [SerializeField]
    Color ToggleOnColor;

    Toggle myToggleRef;

    private void Start()
    {
        myToggleRef = GetComponent<Toggle>();

        if (myToggleRef.isOn)
            SetColorsForText(true);
        else
            SetColorsForText(false);

        myToggleRef.onValueChanged.AddListener((bool flag) => OnUnitToggled(myToggleRef.isOn));
    }


    public void OnUnitToggled(bool flag) {
        SetColorsForText(flag);
    }


    void SetColorsForText(bool flag)
    {
        if (flag)
            TextRef.color = ToggleOnColor;
        else
            TextRef.color = ToggleOffColor;
    }
    
}
