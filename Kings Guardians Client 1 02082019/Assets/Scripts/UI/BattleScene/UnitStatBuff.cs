﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitStatBuff : MonoBehaviour
{
	public Image Icon;
	public Text Duration;

    public string ID;

    public void Set(string id, int turns)
	{
		ID = id;
        Icon.enabled = true;
        Duration.enabled = true;

        if (turns > -1)
        {
            Icon.sprite = AssetPackManager.GetBuffFX(id).Icon;
            Duration.text = (turns > 0) ? turns.ToString() : "";
        }
        else
            Hide();
	}

    public void Hide()
    {
        Icon.enabled = false;
        Duration.enabled = false;
    }
}