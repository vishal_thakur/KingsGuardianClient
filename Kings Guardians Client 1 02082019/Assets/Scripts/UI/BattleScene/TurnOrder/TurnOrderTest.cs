﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TurnOrderTest : MonoBehaviour 
{
    [System.Serializable]
    public struct Slot
    {
        public Image Icon;
        public Image Outline;
        public Text Readiness;
    }    
    public List<Slot> Icons = new List<Slot>();

    bool _inited = false;
    float _first;

    void Awake() { BattleLogic.Instance.OnTurnStarted += TurnStarted; }

    public void TurnStarted()
    {
        if (!_inited)
        {
            int cnt = 0;
            foreach (var p in BattleLogic.Instance.Characters)
            {
                Icons[cnt].Icon.sprite = p.Value.Icon;
                Icons[cnt].Outline.color = (!p.Value.AI) ? Color.white : Color.red;
                _first = Icons[cnt].Icon.rectTransform.position.x;
                cnt++;
            }   
            _inited = true;
        }

        int i = 0;
        foreach (var p in BattleLogic.Instance.Characters)
        {
            var rect = Icons[i].Icon.rectTransform;
            var pos = rect.position;
            pos.x = _first + 475 * (1f - p.Value.Statistics.Readiness/100);
            Icons[i].Readiness.text = Mathf.RoundToInt(p.Value.Statistics.Readiness) + "%";
       //     rect.position = pos;
            i++;
        }       
    }
}
