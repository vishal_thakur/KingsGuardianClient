﻿using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ActionBarCharacterPanel : BasePanelGUI 
{
	public Image Icon;
	public Text Name;
	public StatusBar HPBar;
    public ImageSlider Rage;

    public List<StatColumn> Stats = new List<StatColumn>();
    public List<DefenceCardColumn> ElementDefenses = new List<DefenceCardColumn>();

    public void OnTurnStarted()
	{
		var character = BattleLogic.Instance.ActiveCharacter;
        Visible = character != null;
        if (character == null) return;


		Icon.sprite = character.Icon;
        Name.text = character.Statistics.ContentData.Name;

        foreach(StatColumn slot in Stats)
		{            
            if( character.Statistics.Stats != null && character.Statistics.Stats.ContainsKey(slot.Stat))
                slot.Set(character.Statistics.Stats[slot.Stat].ToString());
		}


        foreach (var def in ElementDefenses)
        {            
            var d = character.Statistics.Defences.Find(x => ((DefenseContent)x.ContentData).Element == def.Element || ((DefenseContent)x.ContentData).Element == Element.All);
            if (d != null)
                def.Set(((DefenseContent)d.ContentData).Amount +"%");
            else
                def.Set("0%");
        }

        int max = character.Statistics.MaxHP;
		int curr = character.Statistics.CurrHP;
		
        HPBar.MaxValue = max;
        HPBar.Value = curr;

        Rage.Value = (character.Statistics.Rage != null) ? character.Statistics.Rage : 0;
        Rage.GetComponent<Button>().interactable = (Rage.Value == Rage.maxValue);
	}
}
