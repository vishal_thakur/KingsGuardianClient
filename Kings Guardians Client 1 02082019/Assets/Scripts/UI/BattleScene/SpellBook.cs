﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpellBook : BaseWindowGUI, IItemList
{
    public GameObject CombatCardList;
    public ItemList ItemList;
    public GameObject ItemDetails;
    public Image Icon;
    public Text Description;

	CharacterReference _character;
	ItemReference _selectedCard;

    void Awake()
    {
        ItemList.list = this;     
        ItemList.Init();
        Toggle(false);
    }

    protected override void OnOpened()
    {
        CombatCardList.SetActive(true);
        ItemDetails.SetActive(false);
    }

	public void Set(CharacterReference character)
    {        
        _character = character;
        ItemList.SetPage(1);
    }

   
    #region IItemList implementation
	public ItemReference GetItem(int index)
    {
        if(index >= 0 && _character != null && _character.Inventory != null && index < _character.Inventory.Count)
            return _character.Inventory[index];
        return null;
    }
    public int Count<T>()
    {
        if(_character != null && _character.Inventory != null)
            return _character.Inventory.Count;
        return 0;
    }
    public void Select(SlotBase slot)
    {
        if (slot.Index < 0 || slot.Index > _character.Inventory.Count || _character.Inventory[slot.Index] == null)
            return;

        CombatCardList.SetActive(false);
        ItemDetails.SetActive(true);

        _selectedCard = _character.Inventory[slot.Index];

        Icon.sprite = AssetPackManager.GetCardIcon(_selectedCard.ContentID, true);
        Title.text = _selectedCard.ContentData.Name;
        Description.text = _selectedCard.Description;           
    }
    #endregion

    public void Use()
    {
        BattleLogic.Instance.SelectCard(_selectedCard.ContentID, _selectedCard.Stacks);
        Toggle(false);
    }

    public void Back()
    {
        _selectedCard = null;
        CombatCardList.SetActive(true);
        ItemDetails.SetActive(false);
    }
}
