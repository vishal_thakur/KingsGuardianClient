﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UnitStat : MonoBehaviour
{
    public Text Level;
    public Slider HPBar;
    public Slider ShieldBar;
    public Slider ReadinessBar;
    public Image Arrow;

    public List<UnitStatBuff> BuffSlots = new List<UnitStatBuff> ();

    Transform _camera;
    BattleCharacterBehaviour _character;
    bool _initalized = false;

    void Start()
    {
        var canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
        _camera = Camera.main.transform;

        Arrow.enabled = false;

        if(BattleLogic.Instance != null)
            BattleLogic.Instance.OnBuffRefreshed += OnBuffChanged;

        foreach (var b in BuffSlots)
            b.Hide();
    }

    void OnBuffChanged()
    {
        foreach (var b in BuffSlots) b.Hide();

        int cnt = 0;
        foreach (var p in _character.Buffs)
        {
            if (cnt < BuffSlots.Count)
                BuffSlots[cnt].Set(p.Key, p.Value);            
            cnt++;
        }
    }

    void Update()
    {
        if(!_initalized)
        {
            if (_character == null) _character = GetComponentInParent<BattleCharacterBehaviour> ();

            if(_character != null && _character.Statistics != null)
            {
                Level.text = _character.Statistics.Level.ToString();
                HPBar.maxValue = _character.Statistics.MaxHP;
                HPBar.value = _character.Statistics.CurrHP;

                ShieldBar.maxValue = HPBar.maxValue;
                ShieldBar.value = _character.Statistics.Shield;

                _initalized = true;
            }
            return;
        }

        transform.LookAt(_camera.position);
        HPBar.value = _character.Statistics.CurrHP;
        ReadinessBar.value = _character.Statistics.Readiness / 100;
        ShieldBar.value = _character.Statistics.Shield;

        Arrow.enabled = _character.Selectable;
    }
}