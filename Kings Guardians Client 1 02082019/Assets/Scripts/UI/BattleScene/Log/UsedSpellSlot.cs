﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UsedSpellSlot : MonoBehaviour 
{
	public Image Character;
	public Image CharacterOutline;
	public Image Spell;

	public void Set(BattleCharacterBehaviour character, string cardContentID)
	{
		if (this.Character == null)
			Debug.Log (name);

		if (character != null) 
		{
			Character.sprite = character.Icon;
			Character.color = Color.white;
			CharacterOutline.color = (!character.AI) ? Color.white : Color.red;
		}
		else
		{
			Character.sprite = null;
			Character.color = Color.black;
			CharacterOutline.color = Color.white;
		}

        if(cardContentID != "")
		{
            Spell.sprite = AssetPackManager.GetCardIcon(cardContentID, true);
			Spell.color = Color.white;
		}
		else 
		{
			Spell.sprite = null;
			Spell.color = Color.black;
		}
	}
}
