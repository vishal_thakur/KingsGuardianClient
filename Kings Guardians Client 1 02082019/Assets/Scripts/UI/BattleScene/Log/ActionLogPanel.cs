﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ActionLogPanel : MonoBehaviour 
{
	public GridLayoutGroup Container;
	List<UsedSpellSlot> _slots = new List<UsedSpellSlot>();

	void Awake()
	{
		_slots = Container.GetComponentsInChildren<UsedSpellSlot> ().ToList();	
        BattleLogic.Instance.OnTurnStarted += TurnStarted;
	}
	
    public void TurnStarted()
	{
        var list = BattleLogic.Instance.Log;
		int cnt = list.Count - 1;
		foreach(UsedSpellSlot slot in _slots)
		{
            if (cnt < 0)
                slot.Set(null, null);
            else
            {
                var c = BattleLogic.Instance.Characters[list[cnt].CharacterID];
                slot.Set(c, list[cnt].CardID);
            }
			cnt--;
		}
	}
}
