﻿using UnityEngine;
using System.Collections;

public class ActionBar : BaseWindowGUI
{
    public static ActionBar Instance;

    public ActionBarCharacterPanel Character = new ActionBarCharacterPanel();
    public SpellBook Spellbook;

    void Awake()
    {
        Instance = this;
        BattleLogic.Instance.OnTurnStarted += OnTurnStarted;


     /*   _window = GetComponent<BaseWindowGUI> ();
        _characterPanel = _window.GetComponentInChildren<ActiveCharacterPanel> ();

        SpellDetails.onNumberSelected = OnStackSelected;
        SpellDetails.onCancel = OnCancel;

        _window.Toggle (false);
        SpellBook.Toggle (false);*/
    }

    void OnTurnStarted()
    {
        Character.OnTurnStarted();
        Spellbook.Set(BattleLogic.Instance.ActiveCharacter.Statistics);
    }
        
}
