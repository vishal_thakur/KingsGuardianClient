﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleFinishCharacterSlot : MonoBehaviour
{
	public Text Name;
	public Image Avatar;
	public Text Experience;
	public Slider XPBar;
	public Text Level;

	public CharacterReference Character { get; private set; }

	float _xp = 0;
	int _lastVirtualLevel = 0;
	float _speed = 1f;

	public void Set(CharacterReference c)
	{
		Character = c;

		if (c == null) return;

		Name.text = c.ContentData.Name;
        Avatar.sprite = AssetPackManager.GetCharacterAssets(c.ContentID).Icon;
        Avatar.enabled = Avatar.sprite != null;

		_xp = c.Exp;

		Experience.text = "Experience Points: " + _xp;
		Level.text = "LvL "+c.Level;

		XPBar.maxValue = 1;
		XPBar.value = 0;

		_lastVirtualLevel = c.Level;
	}

	public void ApplyRewards(int xp)
	{ 
		_speed = (float)xp / 5f;
		Character.Exp += xp;
	}


	void Update()
	{
		if (Character == null || _xp == Character.Exp) return;

		_xp += Time.deltaTime * _speed;
		if (_xp > Character.Exp) _xp = Character.Exp;

		int xp = Mathf.RoundToInt (_xp);


		Experience.text = "Experience Points: " + xp;
        XPBar.value = Calculators.LevelProgress (_lastVirtualLevel, xp);
		if (XPBar.value >= XPBar.maxValue) 
		{
            int virtualLevel = Calculators.CalculateLevel (xp);

			if (virtualLevel != _lastVirtualLevel) 
			{
				Level.text = "LvL " + virtualLevel;
				_lastVirtualLevel = virtualLevel;
			}
		}
	}
}
