﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BattleFinishWindow : BaseWindowGUI 
{
    public Text Gold;
    public List<BattleFinishCharacterSlot> CharacterSlots = new List<BattleFinishCharacterSlot>();

    void Awake() { Toggle (false); }

    public void Open ( bool win )
    {
        Toggle (true);

        Title.text = (win) ? "CONGRATULATION! YOU WON!" : "YOU LOST!";

		CharacterReference[] squad = GameManager.Instance.LocalPlayer.Squad;

        for (int i = 0; i < CharacterSlots.Count; i++) 
            CharacterSlots [i].Set ( (i < squad.Length) ? squad [i] : null );           
    }


}




	

	/*bool _win = false;

	int _targetBalance = 0;
	int _reward = 0;
	float _virtualBalance = -1;
	float _speed = 1f;

	
	

	void Update()
	{
		if (_virtualBalance == -1 || _virtualBalance == _targetBalance) return;

		_virtualBalance += Time.deltaTime * _speed;
		if (_virtualBalance > _targetBalance)
			_virtualBalance = _targetBalance;

		int virtualBalance = Mathf.RoundToInt (_virtualBalance);
		Gold.text = "Gold: " + virtualBalance + "(+"+ (_reward - (_targetBalance - virtualBalance)) +")";
	}

	public void SetRewards(int goldBalance, int goldReward, int xpReward)
	{
		foreach (var slot in CharacterSlots) 
			slot.ApplyRewards (xpReward);

		_reward = goldReward;
		_targetBalance = goldBalance;
		_virtualBalance = goldBalance - goldReward;
		_speed = goldReward / 5f;
	}

	public void Next()
	{
		if(_win && NetworkInfo.Instance.Mode != BattleMode.Tutorial )
			LevelLoader.Instance.LoadScene("Loot");
		else
			LevelLoader.Instance.LoadScene("Town");
	}

*/