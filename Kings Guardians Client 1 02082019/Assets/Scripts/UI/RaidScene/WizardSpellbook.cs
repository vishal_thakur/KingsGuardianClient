﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class WizardSpellbook : BaseWindowGUI
{
	public CharacterReference Character;
	public GridLayoutGroup List; 

	List<Button> _slots;
	bool _initialized;



	protected override void OnOpened()
	{
		if (!_initialized)
			_slots = List.gameObject.GetComponentsInChildren<Button>().ToList();
		
		int cnt = 0;
		foreach (var a in Character.ContentData.ActiveAbilities)
		{
			var icon = AssetPackManager.GetRaidFX(a.Name).Icon;
			_slots[cnt].image.sprite = icon; 
			cnt++;
		}
	}

	public void UseAbility(int cnt)
	{
		RaidHUD.Instance.UseAbility(Character.InstanceID, cnt);
	}




}
