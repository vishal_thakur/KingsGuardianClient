﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RaidUnitStat
{
	RaidStatusHUD _hud;
	IDamageable _unit;

	public void Init(IDamageable unit, RaidStatusHUD hud)
	{
		_hud = hud;
		if (_hud == null)
			return;
		_hud.Init();

		_unit = unit;
        if(_unit != null && _unit.Exists)
        {     
			_hud.UpdateHPBar(_unit.CurrHP, _unit.MaxHP);
			_hud.UpdateShieldBar(_unit.Shield, _unit.MaxHP);
        }
	}

    public void Update()
	{
		if (_unit == null || _hud == null) return;

		_hud.UpdateHPBar(_unit.CurrHP);
		_hud.UpdateShieldBar(_unit.Shield);
	}

	public void Toggle(bool t)
	{
		if(_hud != null)
			_hud.Toggle(t);
	}
}
