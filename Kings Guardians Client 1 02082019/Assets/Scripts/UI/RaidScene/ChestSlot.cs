﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChestSlot : MonoBehaviour 
{
    public RewardWindow window;
    public Text Amount;
    public Image Chest;
    public Image Reward;


	public bool RewardShown = false;
	public bool isSpecialRewardSlot = false;

    void Awake()
    {
        HideReward();
    }

    public void Select()
    {
        window.GetNextReward(this);
    }




    public void ShowReward(Sprite icon, int amount){
		if (icon == null)
            icon = Resources.Load<Sprite>("No Reward");

		RewardShown = true;
        Chest.enabled = false;
        Reward.enabled = true;
        Reward.sprite = icon;
        Amount.text = (amount > 1) ? amount.ToString() : "";
    }

    public void HideReward(){
		RewardShown = false;
        Chest.enabled = true;
        Reward.enabled = false; 
        Amount.text = "";
    }
}
