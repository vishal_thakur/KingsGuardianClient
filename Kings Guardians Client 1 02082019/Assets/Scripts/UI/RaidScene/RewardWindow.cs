﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RewardWindow : BaseWindowGUI
{
    public Button NextButton;

    ChestSlot _slot;
    int _buildingCount;
    bool _backToVillage;

	[SerializeField] ChestSlot[] AllSlots;

	[SerializeField] ChestSlot CastleDestroyedSpecialSlot;

    void Awake()
    {
        RaidLogic.Instance.onRaidFinished = Open;
        RaidLogic.Instance.onRewardGet = ShowReward;
		CastleDestroyedSpecialSlot.gameObject.SetActive (false);
        Toggle(false);
//		NextButton.gameObject.SetActive (false);
    }

    void Open(int cnt)
    {
//        _buildingCount = cnt;

//        ChangeButton();
        Toggle(true);


    }

    public void GetNextReward(ChestSlot slot)
    {
        if (slot == null) return;
        _slot = slot;
        RaidLogic.Instance.GetNextReward();
    }

	public void ShowReward(Sprite icon, int amount , bool isSpecialReward){
		
		foreach (ChestSlot chestSlot in AllSlots) {
			if (!chestSlot.RewardShown && !isSpecialReward) {
				chestSlot.gameObject.SetActive (true);
				chestSlot.ShowReward (icon, amount);
				return;
			}
			else if(!chestSlot.RewardShown && isSpecialReward){
				CastleDestroyedSpecialSlot.gameObject.SetActive (true);
				CastleDestroyedSpecialSlot.ShowReward (icon , amount);
				return;
			}
				
		}

//        _slot.ShowReward(icon, amount);
    }

    public void Next()
    {

		RaidLogic.Instance.BackToVillage();
//        if (_backToVillage)
//        {
//            RaidLogic.Instance.BackToVillage();
//            return;
//        }
//
//		try{
//        _slot.HideReward();
//		}
//		catch(System.Exception e){
//			Debug.LogError ("Fucked!!" + e);
//		}
//        _slot = null;
//        _buildingCount--;
//
//        ChangeButton();
    }

//    void ChangeButton()
//    {
//        _backToVillage = _buildingCount == 0;
//        var t = NextButton.GetComponentInChildren<Text>();
//        t.text = (_backToVillage) ? "Back to Town" : "Next";       
//    }
}
