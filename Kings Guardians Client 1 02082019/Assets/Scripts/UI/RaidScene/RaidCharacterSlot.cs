﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RaidCharacterSlot : SlotBase
{
	public static Sprite SpellBookIcon { get 
		{
			if (_spellbookIcon == null)
				_spellbookIcon = Resources.Load<Sprite>("Icons/Icon_Book");
			return _spellbookIcon;
		}}
	static Sprite _spellbookIcon;

    public Image Icon;
    public Text Count;
    public Image Selected;
    public GameObject ActiveAbility;

	public void Set(CharacterReference c)
    {
        SetImage(Icon, (c != null) ? AssetPackManager.GetCharacterAssets(c.ContentID).Icon : null);

		if(c != null)
		{			
			var i = ActiveAbility.GetComponent<Image>();

			if (c.ContentData.ActiveAbilities.Count == 1)
				i.sprite = AssetPackManager.GetRaidFX(c.ContentData.ActiveAbilities[0].Name).Icon;
			else
				i.sprite = RaidCharacterSlot.SpellBookIcon;
		}
		ActiveAbility.SetActive(false);      

        SetText(Count, (c != null) ? "1" : "");
        Selected.enabled = false;
       
        Locked = c == null;
    }

    public override void SetIndex(int index)
    {
        base.SetIndex(index);

        if (SlotHandler != null)
        {
            var list = (ICharacterList)SlotHandler.List;
			CharacterReference c = list.GetCharacter(Index);
            Set(c);
        }
    }

    public override void Select()
    {
        if (!Locked)
        {
            SlotHandler.Select(this);
            Selected.enabled = true;
            ActiveAbility.SetActive(true);
        }
    }

    public override void Deselect()
    {
        base.Deselect();
        Selected.enabled = false;
        ActiveAbility.SetActive(false);
    }

    public void ActivateAbility()
    {
        RaidHUD.Instance.UseActiveAbility(this);
    }
}
