﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class RaidHUD : SingletonMono<RaidHUD>, ICharacterList
{
    public Text EnemyName;
    public Text GoldMedalsWin;
    public Text GoldMedalsLose;
    public Text Timer;
	public Text AbilityPoints;

    public List<CostField> AvailableMaterials = new List<CostField>();
    public StatusBar Percentage;

    public RaidCharacterList Characters;

	public WizardSpellbook SpellBook;

    float _startTime;
    RaidBuildingBehaviour _castle;

    public void Init()
    {
        var enemy = GameManager.Instance.TargetPlayer;
        var player = GameManager.Instance.LocalPlayer;

        EnemyName.text = enemy.Username + " (Level "+enemy.Level+")";

        int win = Mathf.Clamp(20 + enemy.Level - player.Level, 5, 35);
        int lose = Mathf.Clamp(20 - enemy.Level + player.Level, 5, 35);

        GoldMedalsWin.text = "Win: +"+win;
        GoldMedalsLose.text = "Lose: -"+lose;

        foreach (var m in AvailableMaterials)
            m.Set(enemy.GetMaterial(m.Material));

        Percentage.Init();
        Percentage.Value = 0f;
        Percentage.MaxValue = 100f;
        Percentage.Color = Color.red;

        Characters.list = this;
        Characters.SetPage(1); 
              
		SpellBook.Toggle(false);

        _startTime = Time.time;
        Timer.text = "Time: "+ Utils.SecondsToString(((Time.time - _startTime) - (12*60)), "mm:ss");
    }

    void Update()
    {
        var townLoader = RaidLogic.Instance.TownLoader;

        if(_castle == null) _castle = townLoader.Castle;    
 
        int totalBuildings = townLoader.BuildingCount;
        int destroyedBuildings = townLoader.DestroyedBuildingCount;
        Percentage.Value = 100f * ((float)destroyedBuildings / (float)totalBuildings);

        if ( Percentage.Color != Color.green && (Percentage.Value >= 75f || (_castle != null && _castle.Reference.Dead )))
            Percentage.Color = Color.green;

		AbilityPoints.text = RaidLogic.Instance.AbilityPoints.ToString();
        Timer.text = "Time: "+ Utils.SecondsToString(((Time.time - _startTime) - (12*60)), "mm:ss");
    }


    #region ICharacterList implementation
	public CharacterReference GetCharacter(int index)
    {
        if (index < 0 || index >= GameManager.Instance.LocalPlayer.Characters.Count)
            return null;
        return GameManager.Instance.LocalPlayer.Characters[index];
    }
    #endregion

    #region IList implementation
    public int Count<T>()
    {
        return GameManager.Instance.LocalPlayer.Characters.Count;
    }

    public void Select(SlotBase slot)
    {
        RaidLogic.Instance.SelectCharacter(GetCharacter(slot.Index).InstanceID);
    }
    #endregion

    public void UseActiveAbility(RaidCharacterSlot slot)
    {
        var character = GetCharacter(slot.Index);
		if (character.ContentID == "Wizard")
		{      
			SpellBook.Character = character;
			SpellBook.Toggle();
		}
		else
			UseAbility(character.InstanceID, 0);
    }   

	public void UseAbility(string character, int ability)
	{
		RaidLogic.Instance.UseActiveAbility(character, ability);
	}
}
