﻿using UnityEngine;
using System.Collections;

public class RaidCharacterList : ListGUI<ICharacterList, RaidCharacterSlot>
{ 
    protected override bool multiSelection { get { return false; } }

    public override void Select(SlotBase slot)
    {
        List.Select(slot);
        base.Select(slot);
    }
}
