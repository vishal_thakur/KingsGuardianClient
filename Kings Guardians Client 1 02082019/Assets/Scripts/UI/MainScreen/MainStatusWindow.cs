﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Runemark.AssetEditor;

public class MainStatusWindow : BaseWindowGUI
{
	public InputField Host;
	public InputField Port;
	public GameObject ConnectPanel;

	public Text GameVersion;
    public Text Status;

    public InputField Username;
    public InputField Password;

    public GameObject LoginPanel;
    public GameObject StatusPanel;

    enum Panels { Login, Status, ManualConnect }
    Panels _currentPanel;

	void Awake()
    {
        // REMEMBER ME
        if (PlayerPrefs.HasKey("Username")) Username.text = PlayerPrefs.GetString("Username");
        if (PlayerPrefs.HasKey("Password")) Password.text = PlayerPrefs.GetString("Password");

        // Set game version
        GameVersion.text = Application.version;         

		if (NetworkManager.Instance.ManualConnect)
		{			
			Host.text = (PlayerPrefs.HasKey("Host")) ? PlayerPrefs.GetString("Host") : NetworkManager.Instance.Host;
			Port.text = (PlayerPrefs.HasKey("ServerPort")) ? PlayerPrefs.GetInt("ServerPort").ToString() : NetworkManager.Instance.ServerPort.ToString();
			TogglePanel(Panels.ManualConnect);
		}
		else
		{
			TogglePanel(Panels.Status);
			UpdateStatus();
		}
	}

	public void Connect()
	{
		NetworkManager.Instance.Host = Host.text;
		NetworkManager.Instance.ServerPort = int.Parse(Port.text);

		PlayerPrefs.SetString("Host", NetworkManager.Instance.Host);
		PlayerPrefs.SetInt("ServerPort", NetworkManager.Instance.ServerPort);

		NetworkManager.Instance.Connect();

		TogglePanel(Panels.Status);
		UpdateStatus();
	}

	public void Register()
    {
        if (Username.text == "" || Password.text == "")
            return;

        PlayerPrefs.SetString("Username", Username.text);
        PlayerPrefs.SetString("Password", Password.text);

        NetworkManager.Instance.Register(Username.text, Password.text);
    }

    public void Login()
    {
//		if (Username.text == "" || Password.text == "") {
//			Status.text = "Invalid Credentials";
//			return;
//		}

        PlayerPrefs.SetString("Username", Username.text);
        PlayerPrefs.SetString("Password", Password.text);

        NetworkManager.Instance.Login(Username.text, Password.text);
    }
     
    public void LoginGooglePlus() { }
    public void LoginFacebook() { }



    void Update()
    {    
		if (NetworkManager.Instance.Status == ClientStatus.Connected)
		{
			if (!LoginPanel.activeSelf)
				TogglePanel(Panels.Login);
		}
		else
		{
			if (LoginPanel.activeSelf)
				TogglePanel(Panels.Status);
			UpdateStatus();
		}
    }

    void TogglePanel(Panels panel)
    {
        LoginPanel.SetActive(panel == Panels.Login);
        StatusPanel.SetActive( panel == Panels.Status );
		ConnectPanel.SetActive(panel == Panels.ManualConnect);
    }

    void UpdateStatus()
    {
        string s = NetworkManager.Instance.Status.ToString();
        if (NetworkManager.Instance.Status == ClientStatus.Disconnected)
            s += " <i>Next attempt to login: " + Mathf.RoundToInt(NetworkManager.Instance.NextConnectionAttempt - Time.time) + " seconds</i>";    

		else if (NetworkManager.Instance.Status == ClientStatus.Updating)
			s += " <i>" + Mathf.RoundToInt(AssetBundleLoader.Progress * 100f)  + "%</i>";    

		else if (NetworkManager.Instance.Status == ClientStatus.LoadingAssets)
			s += " <i>" + Mathf.RoundToInt(AssetPackManager.Instance.Progress * 100f)  + "%</i>";    

        Status.text = s;
    }
   
}
