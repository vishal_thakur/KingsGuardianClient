﻿using UnityEngine;
using System.Collections;

public class GameItemList : ListGUI<IGameItemList, GameItemSlot>
{ 
    public override void Select(SlotBase slot)
    {
        List.Select(slot);
        base.Select(slot);
    }
}
