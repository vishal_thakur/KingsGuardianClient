﻿using UnityEngine;
using System.Collections;

public class ItemList : ListGUI<IItemList, ItemSlot>
{ 
    public override void Select(SlotBase slot)
    {
        List.Select(slot);
        base.Select(slot);
    }
}
