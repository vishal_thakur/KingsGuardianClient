﻿using UnityEngine;
using System.Collections;

public class CharacterList : ListGUI<ICharacterList, CharacterSlot>
{ 
    public override void Select(SlotBase slot)
    {
        List.Select(slot);
        base.Select(slot);
    }
}
