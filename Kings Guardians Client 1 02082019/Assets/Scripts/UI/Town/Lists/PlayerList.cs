﻿using UnityEngine;
using System.Collections;

public class PlayerList : ListGUI<IPlayerList, PlayerSlot>
{ 
    public override void Select(SlotBase slot)
    {
        List.Select(slot);
        base.Select(slot);
    }
}
