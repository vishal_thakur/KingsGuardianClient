﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerInfoUI : MonoBehaviour 
{
    public Text Username;
    public Image Avatar;

    public StatusBar XPBar;
    public Text Level;

	Player _player;
	bool _initialized;


	void Update()
    {
		if (!TownLogic.Instance.Ready)
			return;

		if (!_initialized)
		{
			if (!TownLogic.Instance.ScoutMode)
				_player = GameManager.Instance.LocalPlayer;
			else
				_player = GameManager.Instance.TargetPlayer;

			_initialized = true;
		}

		if (_player == null)
			return;

        Username.text = _player.Username;
		Avatar.sprite = _player.Avatar;
		XPBar.Value = _player.LevelProgress;
		Level.text = "Level "+_player.Level;
    }
}
