﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public enum NotificationTypes { Success, Info, Error }

public class Notification : SingletonMono<Notification> 
{
	public GameObject Window;
	public Text Message;
	public Image Icon;
    public Image Border;

    public float HideTime = 3f;

	[System.Serializable]
	public class NotificationParameters
	{
		public NotificationTypes Type;
		public Color Color;
		public Sprite Icon;
	}
	public List<NotificationParameters> Types = new List<NotificationParameters> ();

	float _time = 0f;


	void Awake() { Window.SetActive (false); }

	public void Show (string message, NotificationTypes type)
	{
		Window.SetActive (true);
		Message.text = message;

		var t = Types.Find (x => x.Type == type);

		Message.color = t.Color;
        Border.color = t.Color;
		Icon.sprite = t.Icon;

		#if UNITY_EDITOR
		Debug.Log ("["+type.ToString()+"]: " + message);
		#endif

		_time = Time.time;
	}

	void Update()
	{
        if (Window.activeSelf && _time + HideTime < Time.time) 
			Window.SetActive (false);
	}






	/*public static void Error(MessageCode code)
	{
		Instance.Show (Messages.Get(code), NotificationTypes.Error);
	}*/

	public static void Error(string message)
	{
        if(Instance != null)
		    Instance.Show (message, NotificationTypes.Error);
	}

	public static void Info(string message)
	{
        if(Instance != null)
		    Instance.Show (message, NotificationTypes.Info);
	}


}
