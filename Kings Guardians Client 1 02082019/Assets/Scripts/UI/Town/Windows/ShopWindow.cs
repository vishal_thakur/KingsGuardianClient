﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public enum ShopItemType { Building, Character, Item }

public class ShopWindow : BaseWindowGUI, IGameItemList
{
    public Button BackButton;

    public GameObject CategoryPanel;
    public GameItemList ItemListPanel;
    public ItemDetails DetailsPanel;

	[SerializeField] GameObject BottomMenuRef;

    string _shopOwnerID = ""; 

    ShopItemType _itemType;

    int _minPanel = 0;
    string _category = "";
    int _content = -1;

	List<GameItemContent> _list = new List<GameItemContent>();
    List<GameItemContent> _filteredList = new List<GameItemContent>();

    public static ShopWindow Instance;
    [SerializeField]
    RectTransform CategoryContentRef;

//	void OnDisable(){
//		Debug.LogError ("OFF");
//	}

    void Awake()
    {
        ItemListPanel.list = this;
		Instance = this;
		Toggle(false);
    }

    public void OpenBuildingShop()
    {
        Open(ShopItemType.Building);
    }

//	void OnGUI(){
//		foreach(GameItemContent content in _filteredList)
//			GUILayout.TextField (content.Name + "---------");
//	}

    public void Open(ShopItemType type, string category = "", string shopOwnerID = "")
    {      
        Toggle(true);
        _list.Clear();
        _itemType = type;
        _shopOwnerID = shopOwnerID;

        switch (type)
        {
		case ShopItemType.Building:
			BottomMenuRef.SetActive (true);
				Title.text = "Building Shop";
				foreach (var p in GameManager.Instance.GameContent.Buildings)
                {
					if (p.Key != "Castle")
					{
						var next = GameManager.Instance.LocalPlayer.GetCastleUpgradeLimitIncreases(p.Value);
						if(next >= 0)
							_list.Add(p.Value);
					}
                }


//			OnCategoryExit(CategoryContentRef);
			SelectCategory("Resource");
                break;
		case ShopItemType.Character:
			BottomMenuRef.SetActive (false);
                Title.text = "Character Shop";
                foreach (var p in GameManager.Instance.GameContent.Characters)
                {
                    if(GameManager.Instance.LocalPlayer.Characters.Find( x=> x.ContentID == p.Key) == null
                        && (p.Value.BuildingLevelReq == 0 || p.Value.BuildingLevelReq > BuildingWindow.Building.Upgrade))
                        _list.Add(p.Value);
                }
                break;
		case ShopItemType.Item:
			BottomMenuRef.SetActive (false);
                Title.text = "Card Shop";
                foreach (var p in GameManager.Instance.GameContent.Items)
                    _list.Add(p.Value);
                break;                
        }
        if (category != "")
            _minPanel = 1;

        SelectCategory(category);
    }

    public void SelectCategory(string category)
	{  
		Back ();
//		Debug.LogError (category);
        _category = category;

		//"" = 0 = Home page of all Categories
        if (category == "")
            TogglePanel(0);        
        else
            TogglePanel(1);
    }

    public void Back()
    {
        if (_content > -1)
        {
            TogglePanel(1);
            _content = -1;
        }
        else if (_category != "")
        {
            TogglePanel(0);
            _category = "";
        }            
    }

  

    void TogglePanel(int index)
    {
//        CategoryPanel.SetActive(index == 0);
        ItemListPanel.gameObject.SetActive(index == 1);
        DetailsPanel.gameObject.SetActive(index == 2);

//        BackButton.gameObject.SetActive(index > _minPanel);

        switch (index)
        {
            case 0: // CATEGORY PANEL     
                _content = -1;
                _category = "";
                break;

		case 1: // LIST PANEL    
			if (_category == "ALL" || _itemType == ShopItemType.Character)
				_filteredList = _list;
			else if (_itemType == ShopItemType.Building) {
				List<BuildingCategory> categories = new List<BuildingCategory> ();
				switch (_category) {
				case "Resource": 
					categories.Add (BuildingCategory.Resource_generator);
					categories.Add (BuildingCategory.Resource_mine);
					categories.Add (BuildingCategory.Resource_storageOnly);
					break;
				case "CardCreator":
					categories.Add (BuildingCategory.CardCreator);
					break;
				case "Defense":
					categories.Add (BuildingCategory.Defense);
					break;
				case "Other":
					categories.Add (BuildingCategory.CharacterCreator);
					categories.Add (BuildingCategory.WorkerCreator);
					break;
				}
				_filteredList = _list.FindAll (x => categories.Contains (((BuildingContent)x).Category)); 
			} else
				_filteredList = _list.FindAll (x => ((ItemContent)x).Category.ToString () == _category);                  
                

			//Show Unlocked Items First and Locked later

//			foreach (GameItemContent content in _filteredList) {
//				
//				_filteredList.Remove ();
//			}


				//Show First page of the Selected Category
			ItemListPanel.SetPage (1);
			_content = -1;

			ItemListPanel.StartCoroutine(ItemListPanel.ShowCategoryElements(_filteredList));

                break;

            case 2: // DETAILS PANEL
                DetailsPanel.Set(_filteredList[_content]);
                break;
        }
    }   

	void SendItemToLast(GameItemContent content){

		//Remove Specified Content 
		_filteredList.Remove (content);

		//Add At Bottom
		_filteredList.Add (content);

//		foreach (GameItemList contentToCompare in _filteredList) {
//			
//		}
	}

//	void OnGUI(){
//		for (int i = 0; i < _filteredList.Count; i++) {
//			GUILayout.TextField ("Name" + _filteredList[i].Name);
//			GUILayout.TextField ("Content ID : " + _filteredList[i].ContentID);
//			GUILayout.TextField ("Description : " + _filteredList[i].Description);
//		}
//			
//	}

    #region IGameItemList implementation 
	public GameItemContent GetContent(int index) { 
//		Debug.LogError ("New List Fetched with length : \t" + _filteredList.Count.ToString());
		return (_filteredList != null && _filteredList.Count > index) ? _filteredList[index] : null; 
	}
    public int Count<T> () { return (_filteredList != null ) ? _filteredList.Count : 0; }
    public void Select(SlotBase slot)
    {
        _content = slot.Index;
        TogglePanel(2);
    }
	public ItemReference GetItem(int index)
    {
        return null;
    }
    #endregion

 


    public void Buy()
    {
        GameItemContent item = _filteredList[_content];
        if (GameManager.Instance.LocalPlayer.HasMaterials(item.GetPrice()))
        {
            if (_itemType == ShopItemType.Building)
            {
                if(GameManager.Instance.LocalPlayer.HasWorker())
                {
                    TownLogic.Instance.ConstructBuilding( item.ContentID);
                    Toggle(false);            
                }
            }
            else
            {
                TownLogic.Instance.StartBuildingProduction(_shopOwnerID, item.ContentID);
                TogglePanel(1);
                Toggle(false); 
            }    
        }
    }
  
  

	public void OnCategoryExit(Transform obj){
		GameItemSlot[] children = obj.GetComponentsInChildren<GameItemSlot> ();

		for (int i = 0; i < children.Length; i++) {
			Destroy (children[i].gameObject);
		}
	}

}