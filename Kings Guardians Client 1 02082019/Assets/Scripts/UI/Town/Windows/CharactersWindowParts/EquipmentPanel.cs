﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EquipmentPanel : BasePanelGUI, IItemList, ISlotHandler
{
    public int SelectedSlot = -1;

    public IList List { get { return null; }}    
    public int Count<T>() { return (_character != null && _character.Inventory != null) ? _character.Inventory.Count : 0; }

    public ItemSlot Weapon;
    public List<ItemSlot> Defenses = new List<ItemSlot>();
    public ItemList ItemList;

	CharacterReference _character;
    CharactersWindow _window;

    public void Init(CharactersWindow window)
    {
        _window = window;
        Weapon.SlotHandler = this;
        foreach (var def in Defenses)
            def.SlotHandler = this;
        
        ItemList.list = this;
    }

	public void Set(CharacterReference character)
    {
        _character = character;
        Refresh();
    }

    protected override void OnVisible()
    {
        Refresh();
    }

    public void Refresh()
    {
        Weapon.Set(_character.Weapon, 0);

        if (_character.Defences != null)
        {
            for (int i = 0; i < Defenses.Count; i++)
            {
                if (_character.Defences.Count > i)
                    Defenses[i].Set(_character.Defences[i], i);
                else
                    Defenses[i].Set(null, i);
            }
        }
        ItemList.SetPage(1);
    }


	public ItemReference GetItem(int index)
    {
        if (_character.Inventory == null ||  index >= _character.Inventory.Count || index < 0)
            return null;

        return _character.Inventory[index];
    }

    public void Select(SlotBase slot)
    {
        ItemSlot s = (ItemSlot)slot;
        bool weapon = s == Weapon;
        SelectedSlot = slot.Index;

        if (weapon || Defenses.Contains(s))
        {
            var item = s.Item;
            if (item != null && item.ContentID != "")
                _window.ShowItemDetails(item);
            else if (weapon)
                _window.ShowInventory(ItemCategory.Weapon);
            else 
                _window.ShowInventory(ItemCategory.Defence);           
        }       
        else
        {
            var c = GetItem(slot.Index);
            if (c != null && c.ContentID != "null")
                _window.ShowItemDetails(c);
            else
                _window.ShowInventory();
        }
    }   
  
}
