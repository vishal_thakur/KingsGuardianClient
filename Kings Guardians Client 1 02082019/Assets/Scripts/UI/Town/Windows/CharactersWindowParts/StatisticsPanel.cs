﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class StatisticsPanel : BasePanelGUI
{ 
    public Text Level;
    public StatusBar XPBar;
    public Text XP;	
    public List<StatColumn> Stats = new List<StatColumn>();
   
	CharacterReference _character;

	public void Set(CharacterReference character)
    {
        _character = character;
        OnVisible();
    }

    protected override void OnVisible()
    {       
        if (_character == null)
            return;

        Level.text = "Level " +_character.Level;
        XPBar.Value = _character.LevelProgress;
        XP.text = _character.Exp + " / " + _character.LevelEnd;

        foreach (var stat in Stats)
        {
            if (_character.Stats.ContainsKey(stat.Stat))
            {
                if (stat.Stat == CharacterStats.Health)
                    stat.Set(_character.CurrHP + "/" + _character.MaxHP);
                else
                    stat.Set(_character.Stats[stat.Stat].ToString());
            }
            else
                stat.Set("[unknown]");
        }
    }
}
