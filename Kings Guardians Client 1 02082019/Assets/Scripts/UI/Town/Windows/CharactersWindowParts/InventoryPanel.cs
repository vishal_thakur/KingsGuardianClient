﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class InventoryPanel : BasePanelGUI, IItemList
{
    public Text Header;
    public ItemList ItemList;
    public ItemCategory Category { get; private set; }

	List<ItemReference> _list = new List<ItemReference>();
    CharactersWindow _window;

    public void Init(CharactersWindow window)
    {
        _window = window;
        ItemList.list = this;
    }

    public void SetCategory(ItemCategory category)
    {
        Category = category;
        Header.text = "Inventory - " + category;

        _list = GameManager.Instance.LocalPlayer.Inventory.Items.FindAll(x => x != null && ((ItemContent)x.ContentData).Category == category);
        ItemList.SetPage(1);
    }

    protected override void OnVisible()
    {
        ItemList.SetPage(1);
    }

    #region IGameItemList implementation
    public void Select(SlotBase slot)
    {
        var c = GetItem(slot.Index);
        if (c == null)
            return;
        
        _window.ShowItemDetails(c);
    }

    public int Count<T>() { return _list.Count; }

	public ItemReference GetItem(int index)
    {
        if (index < 0 || index >= _list.Count || _list[index] == null)
            return null;
        return _list[index];
    }
    #endregion
    
}
