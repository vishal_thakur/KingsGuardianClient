﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SquadDetails : BasePanelGUI
{
    public List<CharacterSlot> Slots = new List<CharacterSlot>();

    protected override void OnVisible()
    {
        Refresh();
    }

    public void Refresh()
    {
        var list = GameManager.Instance.LocalPlayer.Squad;
        for (int i = 0; i < Slots.Count; i++)
        {
            Slots[i].SetIndex(i);
            Slots[i].Set((i < list.Length) ? list[i] : null);
        }
    }
	
}
