﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class PlayerDetails : BasePanelGUI
{
    public PlayerInfo Player { get; private set; }

    public Text PlayerName;
    public Text PlayerLevel;
    public List<CharacterSlot> Squad = new List<CharacterSlot>();

    public void Refresh(PlayerInfo player)
    {
        Visible = true;
        Player = player;
        if (player != null)
        {
            PlayerName.text = player.Username;
            PlayerLevel.text = "Level " + player.Level; 

            for (int i = 0; i< Squad.Count; i++)
            {
                if (player.Squad != null && player.Squad[i] != null)
                    Squad[i].Set(player.Squad[i]);                  
                else
                    Squad[i].Set();
            }     
        }
    }
}
