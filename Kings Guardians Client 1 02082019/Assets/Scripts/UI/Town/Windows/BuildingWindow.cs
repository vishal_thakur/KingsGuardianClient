﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class BuildingWindow : BaseWindowGUI
{
    public static BuildingReference Building; 

    // Panels
    public UpgradePanel UpgradePanel = new UpgradePanel();
    public RepairPanel RepairPanel = new RepairPanel();
    public ProductionPanel ProductionPanel = new ProductionPanel();
    public CastlePanel CastlePanel = new CastlePanel();
    public InfoPanel InfoPanel = new InfoPanel();

    float _lastBuildingCheck = 0f;

    void Awake()
    {    
        UpgradePanel.Init(this);
        RepairPanel.Init(this);
        ProductionPanel.Init(this);
        CastlePanel.Init(this);
        InfoPanel.Init(this);
    }

    public void OpenPanel(int cnt)
    {
        if (TownLogic.Instance.SelectedBuilding == null && TownLogic.Instance.SelectedBuilding.CurrentState != BuildingState.Active) return;

        Building = TownLogic.Instance.SelectedBuilding.Reference;

        Debug.Log("open " + cnt);

        switch (cnt)
        {
            case 0: OpenPanel(UpgradePanel); break;
            case 1: OpenPanel(RepairPanel); break;
            case 2:
                if (Building.ContentData.Category == BuildingCategory.WorkerCreator)
                    OpenPanel(CastlePanel);
                else if (Building.ContentData.Category == BuildingCategory.CharacterCreator)
                    Create();
                else if (ProductionPanel.CategoryFilter.Contains(Building.ContentData.Category))
                    OpenPanel(ProductionPanel);               
                else
                    OpenPanel(InfoPanel);
                break;
        }
    }

    void OpenPanel(BuildingWindowPanelBase panel)
    {
        if (panel.Visible)
            panel.Visible = false;
        else
        {
            UpgradePanel.Visible = false;
            RepairPanel.Visible = false;
            ProductionPanel.Visible = false;
            CastlePanel.Visible = false;
            InfoPanel.Visible = false;

            panel.Visible = true;
            panel.Set(Building);
        }
    }


    void Update()
    {
        // If the selected building is null, close every panel!
        if (TownLogic.Instance.SelectedBuilding == null || TownLogic.Instance.SelectedBuilding.Reference != Building)
        {
            UpgradePanel.Visible = false;
            RepairPanel.Visible = false;
            ProductionPanel.Visible = false;
            CastlePanel.Visible = false;
            InfoPanel.Visible = false; 
            return;
        }

        // Refresh the castle panel per update
        if (CastlePanel.Visible)
            CastlePanel.Set(Building);

        if (Building.LastUpdated > _lastBuildingCheck)
            Refresh();       
    }


    void Refresh()
    {
        UpgradePanel.Set(Building);                
        RepairPanel.Set(Building);
        ProductionPanel.Set(Building);
        InfoPanel.Set(Building);

        _lastBuildingCheck = Time.time;
    }









    public float GetStoredMaterials(BuildingReference building)
    {
        var material = building.ContentData.Material;

        int totalStorage = (GameManager.Instance.LocalPlayer.Storages.ContainsKey(material)) ? GameManager.Instance.LocalPlayer.Storages[material] : 0; 
        int totalMaterial = (GameManager.Instance.LocalPlayer.Materials.ContainsKey(material)) ? GameManager.Instance.LocalPlayer.Materials[material] : 0; 

        float ratio = (float) building.GetUpgrade().Storage / (float)totalStorage;
        return Mathf.RoundToInt(ratio * (float)totalMaterial);
    }





    #region Actions
    public void Upgrade()
    {
        var u = Building.ContentData.GetUpgrade(Building.Upgrade+1);
        if (u != null && GameManager.Instance.LocalPlayer.HasMaterials(u.Cost) && GameManager.Instance.LocalPlayer.HasWorker())
            TownLogic.Instance.UpgradeBuilding(Building); 

		UpgradePanel.Visible = false;
    }

    public void Repair()
    {        
        var u = Building.ContentData.GetUpgrade(Building.Upgrade);
        List<SMaterial> repairCost = new List<SMaterial>();
        if( u != null )
            foreach( var c in u.Cost)
                repairCost.Add( new SMaterial(){ Material = c.Material, Amount = Mathf.CeilToInt((float)c.Amount * 0.01f )});

        if (GameManager.Instance.LocalPlayer.HasMaterials(repairCost) && GameManager.Instance.LocalPlayer.HasWorker())
            TownLogic.Instance.RepairBuilding(Building); 
    }

    public void AttachWarlock()
    {
        if (!GameManager.Instance.LocalPlayer.HasWarlock()) return;        
		TownLogic.Instance.AttachWarlock(Building);
    }

	public void DetachWarlock()
	{     
		TownLogic.Instance.DetachWarlock(Building);
	}


	float _lastcreate = 0f;

    public void Create()
    {
		if (_lastcreate + 0.5f >= Time.time)
			return;

        // If mine
        if (Building.ContentData.Category == BuildingCategory.Resource_mine)
        {
            if (GameManager.Instance.LocalPlayer.HasMaterials(Building.ContentData.MiningPrices))
                TownLogic.Instance.StartBuildingProduction(Building.InstanceID, "");
        }
        else
        {
            if (Building.ContentData.Category == BuildingCategory.CardCreator)
                ShopWindow.Instance.Open(ShopItemType.Item, Building.ContentData.ItemCategory.ToString(), Building.InstanceID);
            else if (Building.ContentData.Category == BuildingCategory.CharacterCreator)
                ShopWindow.Instance.Open(ShopItemType.Character, "ALL", Building.InstanceID);
        }     
		_lastcreate = Time.time;
    }

    public void CreateWorker(bool warlock)
    {
        if (Building.ContentData.Category != BuildingCategory.WorkerCreator) return;
        TownLogic.Instance.CreateWorker(warlock);      
    }

    public void Cancel()
    {
        TownLogic.Instance.CancelBuildingProduction(Building);
    }
    #endregion
}
