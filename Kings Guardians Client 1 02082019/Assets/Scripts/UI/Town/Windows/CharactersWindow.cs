﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharactersWindow : BaseWindowGUI
{
    public enum Panels { Stats, EquipedItemDetails, InventoryCategories, Inventory, ItemDetails }
    public Panels CurrentPanel { get; private set; }

    public Button MainButton;

    public GameObject NoCharacter;
    public GameObject PreviewPanel;
    public CharacterPreview Preview;
    public EquipmentPanel Equipments = new EquipmentPanel();

    public StatisticsPanel CharacterStats = new StatisticsPanel();
    public ItemDetails ItemDetails;
    public Button EquipButton;
    public Button UnequipButton;
    public InventoryPanel Inventory = new InventoryPanel();
    public GameObject InventoryCategories;

    public Button PrevCharacterButton;
    public Button NextCharacterButton;
    int _currentCharacter = 0;

    public void PrevCharacter(){ LoadCharacter(_currentCharacter-1); }
    public void NextCharacter() { LoadCharacter(_currentCharacter+1); }

	ItemReference _selectedItem;
    float _lastCheck = -1f;

    void Awake()
    {
        Equipments.Init(this);
        Inventory.Init(this);      
        Toggle(false);
    }

    void Update()
    {
        if(!IsOpen) return;

        if (GameManager.Instance.LocalPlayer.Characters != null && GameManager.Instance.LocalPlayer.Characters.Count > _currentCharacter && _currentCharacter > -1)
        {
            var character = GameManager.Instance.LocalPlayer.Characters[_currentCharacter];
            if (_lastCheck > -1 && character != null && character.LastUpdated > _lastCheck)
                Refresh();
        }
    }


    protected override void OnOpened()
    {
        bool hasCharacter = GameManager.Instance.LocalPlayer.Characters != null && GameManager.Instance.LocalPlayer.Characters.Count > 0;

        if (hasCharacter)
            LoadCharacter(0);
        else
            Title.text = "Characters";
        
        NoCharacter.SetActive(!hasCharacter);
        PrevCharacterButton.gameObject.SetActive(hasCharacter);
        NextCharacterButton.gameObject.SetActive(hasCharacter);    

        ChangePanel(Panels.Stats);
        Equipments.Visible = !PreviewPanel.activeSelf;
        ChangeMainButton();
    }

    void Refresh()
    {
        var character = GameManager.Instance.LocalPlayer.Characters[_currentCharacter];
        var asset = AssetPackManager.GetCharacterAssets(character.ContentID);
        if (asset == null)
        {
            Debug.LogError("Assets not set for " + character.ContentID);
            return;
        }

        Title.text = character.ContentData.Name;  

        Preview.ShowCharacter(asset.Model);
        Equipments.Set(character);
        CharacterStats.Set(character);     
        Equipments.Refresh();

        _lastCheck = Time.time;
    }

    void LoadCharacter(int index)
    {
        int max = GameManager.Instance.LocalPlayer.Characters.Count-1;
        int cnt = Mathf.Clamp(index, 0, max);
        _currentCharacter = cnt;
        PrevCharacterButton.gameObject.SetActive(_currentCharacter > 0);
        NextCharacterButton.gameObject.SetActive(_currentCharacter < max);    

        Refresh();
    }

    public void Equip(bool b)
    {
        if(b) 
            TownLogic.Instance.Equip(GameManager.Instance.LocalPlayer.Characters[_currentCharacter], _selectedItem, Equipments.SelectedSlot);
        else
            TownLogic.Instance.Unequip(GameManager.Instance.LocalPlayer.Characters[_currentCharacter], _selectedItem, Equipments.SelectedSlot);
        ChangePanel(Panels.Stats);
    }

    public void ShowInventory()
    {
        ChangePanel(Panels.InventoryCategories);
    }

    public void ShowInventory(Button b)
    {
        ItemCategory c = (ItemCategory)System.Enum.Parse(typeof(ItemCategory), b.name);
        ShowInventory(c);
    }

    public void ShowInventory( ItemCategory category)
    {
        ChangePanel(Panels.Inventory);
        Inventory.SetCategory(category);
    }

	public void ShowItemDetails(ItemReference item)
    {
        _selectedItem = item;

        ChangePanel( (CurrentPanel == Panels.Inventory) ? Panels.ItemDetails : Panels.EquipedItemDetails);
        ItemDetails.Set(item.ContentData);

        EquipButton.gameObject.SetActive(CurrentPanel == Panels.ItemDetails);
        UnequipButton.gameObject.SetActive(CurrentPanel == Panels.EquipedItemDetails);
    }

    public void MainButtonPressed()
    {
        // MANAGE
        if (PreviewPanel.activeSelf)
            PreviewPanel.SetActive(false);
        else
        {
            Panels panel = Panels.Stats;
            switch (CurrentPanel)
            {
                case Panels.Inventory: 
                    if (Inventory.Category == ItemCategory.Defence || Inventory.Category == ItemCategory.Weapon)
                        panel = Panels.Stats;
                    else
                        panel = Panels.InventoryCategories;
                    break;

                case Panels.InventoryCategories: 
                    panel = Panels.Stats;
                    break;

                case Panels.ItemDetails: 
                    panel = Panels.Inventory;
                    break;

                case Panels.EquipedItemDetails: 
                    panel = Panels.Stats;
                    break;
            }

            if (CurrentPanel != Panels.Stats)
                ChangePanel(panel);
            else
                PreviewPanel.SetActive(true);            
        }

        Equipments.Visible = !PreviewPanel.activeSelf;

        ChangeMainButton();
    }
  

    public void ChangeMainButton()
    {
        if (GameManager.Instance.LocalPlayer.Characters == null || GameManager.Instance.LocalPlayer.Characters.Count == 0)
            MainButton.gameObject.SetActive(false);
        else
        {
            MainButton.gameObject.SetActive(true);
            Text t = MainButton.GetComponentInChildren<Text>();
            t.text = (PreviewPanel.activeSelf) ? "Manage" : "Back";
        }
    }


    void ChangePanel(Panels panel)
    {      
        CharacterStats.Visible = panel == Panels.Stats;
        ItemDetails.gameObject.SetActive(panel == Panels.ItemDetails || panel == Panels.EquipedItemDetails);
        Inventory.Visible = panel == Panels.Inventory;
        InventoryCategories.SetActive(panel == Panels.InventoryCategories);

        CurrentPanel = panel;
    }

}
