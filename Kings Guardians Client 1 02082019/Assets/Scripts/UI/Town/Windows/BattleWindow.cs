﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class BattleWindow : BaseWindowGUI, IPlayerList, ICharacterList
{
    public SquadDetails Squad = new SquadDetails();
    public PlayerDetails Player = new PlayerDetails();

    public GameObject CharacterList;
    public CharacterList Characters;

    public GameObject PlayerList;
    public PlayerList Players;  
     
	List<CharacterReference> _filteredCharacterList = new List<CharacterReference>();

    float _opponentsRefreshed = -1;
    float _squadRefreshed = -1;
    int _squadSlot = -1;


    void Awake()
    {
        Players.list = this;
        Characters.list = this;
        Player.Visible = false;

        CharacterList.SetActive(false);
        PlayerList.SetActive(true);
        Toggle(false);
    }

    void Update()
    {
        if (!IsOpen)
            return;
        
        // Refresh Oponents
        if (_opponentsRefreshed < GameManager.Instance.OpponentsUpdated)
        {
            Players.SetPage(1);
            _opponentsRefreshed = Time.time;
        }

        // Refresh Squad
        if (_squadRefreshed < GameManager.Instance.LocalPlayer.LastUpdate)
        {
            Squad.Refresh();
            _squadRefreshed = Time.time;
        }
    }


    public void RefreshOpponents() { GameManager.Instance.RefreshOpponents();  }


    public void Select(SlotBase slot)
    {
        // Player Selected
        if (slot.GetType() == typeof(PlayerSlot))         
            Player.Refresh(GetPlayer(slot.Index));
        
        // Character Selected
        else if (slot.GetType() == typeof(CharacterSlot))
        {
            CharacterSlot cSlot = (CharacterSlot)slot;
            if (Squad.Slots.Contains(cSlot))
            {
                _squadSlot = cSlot.Index;
                FilterCharacterList();
                CharacterList.SetActive(true);
                PlayerList.SetActive(false);
                Characters.SetPage(1);
            }
            else
            {
                // Send squad request
                TownLogic.Instance.UpdateSquad(GetCharacter(cSlot.Index), _squadSlot);

                CharacterList.SetActive(false);
                PlayerList.SetActive(true);
                Players.SetPage(1);
            }
        }      
    }

    public void Attack()
    {
        if( Player.Player != null)
            TownLogic.Instance.StartBattle(Player.Player.Username);
    }

    public void AttackScout()
    {
        if (Player.Player != null)
            TownLogic.Instance.ScoutEnemy(Player.Player.Username, TownLogic.TScoutMode.Battle);
    }

	public void Scout()
	{
		if( Player.Player != null)
			TownLogic.Instance.ScoutEnemy(Player.Player.Username, TownLogic.TScoutMode.Native);
	}


    protected override void OnOpened()
    {
        CharacterList.SetActive(false);
        PlayerList.SetActive(true);

        // Get opponents
        if (Count<IPlayerList>() == 0)
            RefreshOpponents();
        
        Squad.Refresh();
    }

    protected override void OnClosed()
    {       
        Player.Visible = false;
    }

   

    #region IList implementation
    public int Count<T>()
    {
        if (typeof(T) == typeof(IPlayerList) && GameManager.Instance.Opponents != null)
            return GameManager.Instance.Opponents.Count; 
        else  if (typeof(T) == typeof(ICharacterList) &&  GameManager.Instance.LocalPlayer.Characters != null)
            return _filteredCharacterList.Count; 
        return 0;
    }

    public PlayerInfo GetPlayer(int index)
    {
        if (index < Count<IPlayerList>())
            return GameManager.Instance.Opponents[index];
        return null;
    }

	public CharacterReference GetCharacter(int index)
    {
        if (index < Count<ICharacterList>())
            return _filteredCharacterList[index];
        return null;
    } 

    void FilterCharacterList()
    {
        _filteredCharacterList.Clear();
        foreach (var c in GameManager.Instance.LocalPlayer.Characters)
        {
            if (!GameManager.Instance.LocalPlayer.SquadCharacterIDs.Contains(c.InstanceID))
                _filteredCharacterList.Add(c);
        } 
    }
    #endregion


}


