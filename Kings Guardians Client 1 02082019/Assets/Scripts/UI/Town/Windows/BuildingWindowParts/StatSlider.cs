﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatSlider : MonoBehaviour
{
    public Text Label;
    public Slider Bar;
    public Text BarText;

    public void Set(string stat, int current, int upgraded)
    {
        Label.text = stat;
        Bar.maxValue = upgraded;
        Bar.value = current;
        BarText.text = current + "<b>+" + (upgraded - current)+"</b>";

        Toggle(true);
    }


    public void Hide()
    {
        Toggle(false);
    }

    void Toggle(bool b)
    {
        Label.enabled = b;
        Bar.gameObject.SetActive(b);
        BarText.enabled = b;
    }
	
}
