﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class RepairPanel : BuildingWindowPanelBase
{  
	// Worker and Repair
    public GameObject RepairFullHP;
    public GameObject RepairNoWorker;
    public GameObject RepairActive;

    public StatusBar HPBar;
    public GridLayoutGroup RepairCosts;
    public Text RepairTime;

	// Warlock and Shield
	public GameObject AttachButton;
	public GameObject DetachButton;
	public Text DetailText;
	public Text ButtonText;
	public Text NoWarlock;
   
    List<CostField> _repairCosts = new List<CostField>();

    public void Init()
    {
        _repairCosts = RepairCosts.GetComponentsInChildren<CostField>().ToList();
        base.Init(window);
    }

    protected override void OnActive(BuildingReference building)
    {
        bool maxHP = building.CurrHP == building.MaxHP;

        RepairFullHP.SetActive(maxHP);
        RepairNoWorker.SetActive(!maxHP && GameManager.Instance.LocalPlayer.FreeWorkers == 0);
        RepairActive.SetActive(!maxHP && GameManager.Instance.LocalPlayer.FreeWorkers > 0);

        if (!maxHP && GameManager.Instance.LocalPlayer.FreeWorkers > 0)
        {
            var current = building.ContentData.GetUpgrade(building.Upgrade);
            RepairTime.text = Utils.SecondsToString(30);      
            HPBar.MaxValue = building.MaxHP;
            HPBar.Value = building.CurrHP;

            for (int i = 0; i < _repairCosts.Count; i++)
            {
                if (current.Cost.Count > i)
                    _repairCosts[i].Set(current.Cost[i].Material, Mathf.CeilToInt((float)current.Cost[i].Amount * 0.01f));
                else
                    _repairCosts[i].Hide();
            }
        }

        // Shield
		bool attached = GameManager.Instance.LocalPlayer.Warlocks.Find(x => x.Building == building.InstanceID) != null;

		AttachButton.SetActive(!attached);
		DetachButton.SetActive(attached);

		DetailText.enabled = attached;
		ButtonText.text = (attached) ? "Detach Warlock" : "Attach Warlock";

		NoWarlock.enabled = !attached && GameManager.Instance.LocalPlayer.FreeWarlocks == 0;
    }


}
