﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class UpgradePanel : BuildingWindowPanelBase
{
    public UpgradeNumber CurrentUpgrade;
    public UpgradeNumber NextUpgrade;
    public GridLayoutGroup Stats;   
    public GridLayoutGroup Costs;
    public Button UpgradeButton;

	public Text ColumnLeft;
	public Text ColumnRight;
  
    Text _time;

    List<CostField> _costs = new List<CostField>();
    List<StatSlider> _stats = new List<StatSlider>();

    public override void Init(BuildingWindow window)
    {       
        _costs = Costs.GetComponentsInChildren<CostField>().ToList();
        _stats = Stats.GetComponentsInChildren<StatSlider>().ToList();
        _time = UpgradeButton.GetComponentInChildren<Text>();
        base.Init(window);
    }

    protected override void OnActive(BuildingReference building)
    {
        CurrentUpgrade.Set(building.Upgrade);
        NextUpgrade.Set(building.Upgrade + 1);

        var current = building.ContentData.GetUpgrade(building.Upgrade);
        var next = building.ContentData.GetUpgrade(building.Upgrade + 1);

        _time.text = "Start ("+Utils.SecondsToString(next.UpgradeTime)+")";

        _stats[0].Set("Health", current.MaxHealth, next.MaxHealth);

        // MINE
        if (building.ContentData.Category == BuildingCategory.Resource_mine)
        {
            _stats[1].Set("Storage", current.Storage, next.Storage);
            _stats[2].Set("Amount", current.ProductionAmount, next.ProductionAmount);
        }

        // STORAGE
        else if (building.ContentData.Category == BuildingCategory.Resource_storageOnly)
        {
            _stats[1].Set("Storage", current.Storage, next.Storage);
            _stats[2].Hide();
        }

        // GENERATOR
        else if (building.ContentData.Category == BuildingCategory.Resource_generator)
        {
            _stats[1].Set("Amount", current.ProductionAmount, next.ProductionAmount);
            _stats[2].Hide();
        }

        // DEFENSE
        else if(building.ContentData.Category == BuildingCategory.Defense)
        {
            int cnt = 1;
            if (current.Damage != next.Damage)
            {
                _stats[cnt].Set("Damage", current.Damage, next.Damage);
                cnt++;
            } 
            if (current.Range != next.Range)
            {
                _stats[cnt].Set("Range", current.Range, next.Range);
                cnt++;
            }

            if (cnt < 2)
                _stats[1].Hide();
            if (cnt < 3)
                _stats[2].Hide();
        }

        else
        {
            _stats[1].Hide();
            _stats[2].Hide();
        }

        // Set the costs
        for (int i = 0; i < _costs.Count; i++)
        {
            if (next.Cost.Count > i)
                _costs[i].Set(next.Cost[i].Material, next.Cost[i].Amount);
            else
                _costs[i].Hide();
        }

		// Set Castle Text
		if (building.ContentID == "Castle")
		{
			var info = GameManager.Instance.LocalPlayer.GetNextCastleUpgradeInfo();

			int cnt = 1;
			string cLeft = "";
			string cRight = "";
			foreach (var p in info)
			{
				if(cnt <= info.Count/2)
					cLeft += ((cLeft != "") ? "\n" : "")+ p.Value +"x "+ p.Key;
				else
					cRight += ((cRight != "") ? "\n" : "")+ p.Value +"x "+ p.Key;

				cnt++;
			}
			ColumnLeft.text = cLeft; //4row
			ColumnRight.text = cRight; //4row
		}



    }
}
