﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class InfoPanel : BuildingWindowPanelBase
{
    public Text Title;
    public UpgradeNumber Upgrade;

    public StatusBar HP;
    public StatusBar Shield;

    public GameObject StorageRow;
    public StatusBar Storage;
    public Text Generation;

    public Image[] MaterialIcons;

    public GameObject Stats;

    public Text Range;
    public Text Radius;
    public Text Speed;
    public Text Damage;

    protected override void OnActive(BuildingReference building)
    {
        Title.text = building.ContentData.Name;
        Upgrade.Set(building.Upgrade);

        HP.MaxValue = building.MaxHP;
        HP.Value = building.CurrHP;

        Shield.MaxValue = building.MaxHP;
        Shield.Value = building.Shield;

        var u = building.ContentData.GetUpgrade(building.Upgrade);

        // STORAGE
        if (building.ContentData.Category == BuildingCategory.Resource_storageOnly)
        {      
            StorageRow.SetActive(true); 
            Storage.TextFormat = "{0}/{1}";
            Storage.MaxValue = u.Storage;
            Storage.Value = window.GetStoredMaterials(building);             
        }
        else
            StorageRow.SetActive(false);  

        // GENERATION
        if (building.ContentData.Category == BuildingCategory.Resource_generator)
        {
            Generation.gameObject.SetActive(true);
            Generation.text = "This building generates "+u.ProductionAmount+"       per seconds";
        }
        else
            Generation.gameObject.SetActive(false);

        // DEFENSE
        if (building.ContentData.Category == BuildingCategory.Defense)
        {
            Stats.gameObject.SetActive(true);
            Range.text = (u.Range == 0) ? "adjacent" : u.Range + ((u.Range == 1) ? "unit":" units");
            Radius.text = (u.DamageRadius == 0) ? "single target" : u.DamageRadius + ((u.Range == 1) ? " unit":" units") +" splash";
            Speed.text = (u.AttackSpeed == 0) ? "once": u.AttackSpeed + ((u.AttackSpeed == 1) ? " second" : " seconds");
            Damage.text = u.Damage.ToString();
        }
        else
            Stats.gameObject.SetActive(false);
        
        foreach (var s in MaterialIcons)
            s.sprite = AssetPackManager.GetCurrencyIcon(building.ContentData.Material);
    }

}
