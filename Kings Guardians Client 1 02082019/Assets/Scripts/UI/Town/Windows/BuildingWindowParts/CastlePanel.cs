﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class CastlePanel : BuildingWindowPanelBase
{
    protected override List<BuildingCategory> Categories { get { return new List<BuildingCategory>(){
                BuildingCategory.WorkerCreator
            };}}

    public StatusBar WorkerCount;
    public CostField NextWorkerCost;
    public Button CreateWorker;

    public StatusBar WarlockCount;
    public CostField NextWarlockCost;
    public Button CreateWarlock;

    protected override void OnActive(BuildingReference building)
    {
        SetWorkerField(building, WorkerType.Worker, WorkerCount, NextWorkerCost, CreateWorker);
        SetWorkerField(building, WorkerType.Warlock, WarlockCount, NextWarlockCost, CreateWarlock);
    }

	void SetWorkerField(BuildingReference building, WorkerType type, StatusBar count, CostField cost, Button button)
    {
		int owned = 0; 
		int max = 0;
		int price = 0;
		switch (type)
		{
			case WorkerType.Worker:
				owned = GameManager.Instance.LocalPlayer.Workers.Count;
				max = GameManager.Instance.LocalPlayer.MaxWorkers;
				price = GameManager.Instance.LocalPlayer.NextWorkerPrice;
				break;
			case WorkerType.Warlock:
				owned = GameManager.Instance.LocalPlayer.Warlocks.Count;
				max = GameManager.Instance.LocalPlayer.MaxWarlocks;
				price = GameManager.Instance.LocalPlayer.NextWarlockPrice;
				break;
		}

        count.TextFormat = (max > 0) ? "{0} of {1}" : "not available yet";
        count.MaxValue = max;
        count.Value = owned;

		button.interactable = owned < max;
		cost.Toggle(owned < max);  
		cost.Set(price);
    }
}
