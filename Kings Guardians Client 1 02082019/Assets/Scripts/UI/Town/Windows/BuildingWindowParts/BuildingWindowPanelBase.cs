﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BuildingWindowPanelBase
{
    public bool Visible {
        get { return Panel.activeSelf; }
        set { Panel.SetActive(value); }
    }

    protected virtual List<BuildingCategory> Categories { get { return new List<BuildingCategory>(); }}

    public BuildingWindow window { get ; private set; } 

    public GameObject Panel;

    public virtual void Init(BuildingWindow window)
    {
        this.window = window;
        Visible = false;
    }


    public void Set(BuildingReference building)
    {
        if (!Visible) return;

        if ( Categories.Count == 0 || Categories.Contains(building.ContentData.Category))
            OnActive(building);
        else
            Visible = false;
    }

    protected virtual void OnActive(BuildingReference building)
    {
        
    }
}
