﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public class ProductionPanel : BuildingWindowPanelBase
{
    public List<BuildingCategory> CategoryFilter { get { return Categories; }}

    protected override List<BuildingCategory> Categories { get { return new List<BuildingCategory>(){
                BuildingCategory.CardCreator, 
                BuildingCategory.Resource_mine 
            };}}

    public ProductionSlot ActiveSlot;
    public Text ActiveSlotText;
    public StatusBar ActiveProcess;

    public Text NotEnoughStorage;

    public GameObject StoragePanel;
    public StatusBar Storage;

    public GridLayoutGroup Queue;
    List<ProductionSlot> _queue = new List<ProductionSlot>();
     
    public override void Init(BuildingWindow window)
    {
        _queue = Queue.GetComponentsInChildren<ProductionSlot>().ToList();
        NotEnoughStorage.enabled = false;
        base.Init(window);
    }

    protected override void OnActive(BuildingReference building)
    {     
        NotEnoughStorage.enabled = false;
        bool working = building.Queue != null && building.Queue.Count > 0;

        if (!working)
        {
            ActiveSlot.Hide();
            foreach (var slot in _queue)
                slot.Hide();
        }
        else
        {
            string defaultName = (building.ContentData.Category != BuildingCategory.CardCreator) ? building.ContentData.Material.ToString() : "";
            Sprite defaultIcon = (building.ContentData.Category != BuildingCategory.CardCreator) ? AssetPackManager.GetCurrencyIcon(building.ContentData.Material) : null;

            ActiveSlot.Set(building.Queue[0], defaultIcon, defaultName);

            for (int i = 0; i < _queue.Count; i++)
            {
                if (working && building.Queue.Count > i+1)
                    _queue[i].Set(building.Queue[i+1], defaultIcon, defaultName);
                else
                    _queue[i].Hide();
            } 


            if (building.ContentData.Category == BuildingCategory.Resource_mine)
            {
                StoragePanel.SetActive(true);
                var u = building.ContentData.GetUpgrade(building.Upgrade);
                if (u != null)
                {
                    Storage.TextFormat = "{0}/{1}";
                    Storage.MaxValue = u.Storage;
                    Storage.Value = window.GetStoredMaterials(building);
                }

                if (ActiveSlot.Statusbar.Value == ActiveSlot.Statusbar.MaxValue)
                {
                    int storage = GameManager.Instance.LocalPlayer.GetStorage(building.ContentData.Material);
                    int material = GameManager.Instance.LocalPlayer.GetMaterial(building.ContentData.Material);
                    NotEnoughStorage.enabled = storage <= material;
                } 
            }
            else
                StoragePanel.SetActive(false);
        }       
    }
}
