﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UpgradeNumber : MonoBehaviour
{
    public List<Sprite> Numbers = new List<Sprite>(); 

    Image _image;
	
    void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void Set(int number)
    {
        if (number >= Numbers.Count - 1)
            number = Numbers.Count - 1;
        if (number < 1)
            number = 1;

        _image.sprite = Numbers[number-1];
    }
}
