﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{ 
    public HUDMenu Menu = new HUDMenu();   
    public HUDBuildingMenu BuildingMenu = new HUDBuildingMenu();
    public HUDMenu Interaction = new HUDMenu();
  
	bool _exit = false;


	//Bottom menu show/Hide Params
	[Space(20) , Header("Bottom menu show/Hide Params")]
	[SerializeField] RectTransform BottomMenuRef;
	[SerializeField] RectTransform BottomMenuVisiblePositionRef;

	[SerializeField] RectTransform BottomMenuHiddenPositionRef;

	[SerializeField] bool isBottomMenuVisible = true;

	[SerializeField] GameObject HideMenuToggleButton;




	void Update()
    {
		if (_exit) return;	

		BuildingMenu.SettingsButton.SetActive(!TownLogic.Instance.ScoutMode);
		BuildingMenu.LayoutEditorButton.SetActive(!TownLogic.Instance.ScoutMode);
		BuildingMenu.InfoButton.SetActive(!TownLogic.Instance.ScoutMode);
		BuildingMenu.AdminControlButton.SetActive(!TownLogic.Instance.ScoutMode);
		BuildingMenu.WarLogButton.SetActive(!TownLogic.Instance.ScoutMode);

		if (TownLogic.Instance.ScoutMode)
		{
			Menu.Visible = false;
			BuildingMenu.Visible = false;
			Interaction.Visible = false;
			//Show or hide Based on Current Game mode
			HideMenuToggleButton.SetActive (false);
			_exit = true;
			return;
		}





		Menu.Visible = TownLogic.Instance.CurrentMode == TownLogic.Mode.Basic && TownLogic.Instance.SelectedBuilding == null;
        BuildingMenu.Visible = TownLogic.Instance.CurrentMode == TownLogic.Mode.Basic && TownLogic.Instance.SelectedBuilding != null;
        Interaction.Visible = TownLogic.Instance.CurrentMode != TownLogic.Mode.Basic;

		HideMenuToggleButton.SetActive (!Interaction.Visible);


        if (BuildingMenu.Visible)
            BuildingMenu.Update();        
    }
//
//	void OnGUI(){
//		GUILayout.TextField ("Menu"+Menu.Visible);
//		GUILayout.TextField ("BuildingMenu"+BuildingMenu.Visible);
//		GUILayout.TextField ("Interaction" + Interaction.Visible);
//	}
//
//

    [System.Serializable]
    public class HUDMenu 
    {
        public bool Visible {
            get { return gameObject.activeSelf; }
            set { gameObject.SetActive(value); }
        }
        public GameObject gameObject;
    }

    [System.Serializable]
    public class HUDBuildingMenu : HUDMenu 
    {
        public GameObject ActionButton;
        public Image ActionButtonIcon;
        public GameObject UpgradeButton;
        public GameObject RepairButton;
        public GameObject MoveButton;
        public GameObject CancelButton;

		//New Buttons
		public GameObject SettingsButton;
		public GameObject LayoutEditorButton;
		public GameObject InfoButton;
		public GameObject AdminControlButton;
		public GameObject WarLogButton;

        public TextShadowed BuildingName;
        public StatusBar BuildingHP;

        public void Update()
        {
            var building = TownLogic.Instance.SelectedBuilding;

            bool b = building.CurrentState == BuildingState.Inactive;
            ActionButton.SetActive(!b);
            UpgradeButton.SetActive(!b);
            MoveButton.SetActive(!b);
            CancelButton.SetActive(b);

            if (ActionButton.activeSelf)
            {
                switch (building.Reference.ContentData.Category)
                {
                    case BuildingCategory.CardCreator:
                        ActionButtonIcon.sprite = Resources.Load<Sprite>("Icons/Icon_Book");
                        break;
                    case BuildingCategory.CharacterCreator:
						ActionButtonIcon.sprite = Resources.Load<Sprite>("AssetPacks/Missing"); // TODO : add Character Icon (ART)
                        break;
                    case BuildingCategory.Resource_mine:
                        ActionButtonIcon.sprite = AssetPackManager.GetCurrencyIcon(building.Reference.ContentData.Material);
                        break;
                    case BuildingCategory.WorkerCreator:
                        ActionButtonIcon.sprite = Resources.Load<Sprite>("Icons/WorkerIcon");
                        break;

                    case BuildingCategory.Defense:
                    case BuildingCategory.Resource_generator:
                    case BuildingCategory.Resource_storageOnly:
                        ActionButtonIcon.sprite = Resources.Load<Sprite>("Icons/Information"); // Information
                        break;
                }
            }


            // Don't show the upgrade button if this is the max upgrade!
            if (building.Reference.Upgrade == building.Reference.ContentData.Upgrades.Length)
                UpgradeButton.SetActive(false);


            BuildingName.Value = building.Reference.ContentData.Name;
            BuildingHP.TextFormat = "{0}/{1}";
            BuildingHP.MaxValue = building.Reference.MaxHP;
            BuildingHP.Value = building.Reference.CurrHP;

        }
    }



	public void ToggleBottomMenuVisibility(RectTransform button){
		//Toggle Bottom menu visibility Bool
		isBottomMenuVisible = !isBottomMenuVisible;

		//hide
		if (!isBottomMenuVisible) {
			BottomMenuRef.localPosition = BottomMenuHiddenPositionRef.localPosition;
			button.localRotation = new Quaternion (0 , 0 , 0 ,0);
		}
		//Show
		else {
			BottomMenuRef.localPosition = BottomMenuVisiblePositionRef.localPosition;
			button.localRotation = new Quaternion (0 , 0 , 180 ,0);
		}

	}
}
