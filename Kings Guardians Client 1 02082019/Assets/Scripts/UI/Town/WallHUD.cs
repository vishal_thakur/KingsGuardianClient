﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallHUD : MonoBehaviour
{
	public Button UP;
	public Button DOWN;
	public Button LEFT;
	public Button RIGHT;

	public void Horizontal (bool left)
	{
		Feedback((left) ? WallParts.LEFT : WallParts.RIGHT );
	}

	public void Vertical (bool up)
	{
		Feedback((up) ? WallParts.TOP : WallParts.BOTTOM );
	}

	void Feedback(WallParts direction)
	{
		UP.gameObject.SetActive(direction != WallParts.BOTTOM);
		LEFT.gameObject.SetActive(direction != WallParts.RIGHT);
		DOWN.gameObject.SetActive(direction != WallParts.TOP);
		RIGHT.gameObject.SetActive(direction != WallParts.LEFT);

		if (wallUIFeedback != null)
			wallUIFeedback(direction);
	}

	public delegate void WallUIFeedback(WallParts direction);
	public WallUIFeedback wallUIFeedback;
}
