﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSlot : SlotBase
{
    public Text Username;
    public Image Avatar;
    public Text Details;

    public override void SetIndex(int index)
    {
        base.SetIndex(index);

        var list = (IPlayerList)SlotHandler.List;
        PlayerInfo p = list.GetPlayer(Index);

        SetText(Username, (p != null) ?  p.Username : "");
        SetText(Details, (p != null) ? "Level " + p.Level + "\t Rank: 0" : "");
        SetImage(Avatar, (p != null) ? AssetPackManager.GetAvatar(p.AvatarID) : null);
    }

  

}
