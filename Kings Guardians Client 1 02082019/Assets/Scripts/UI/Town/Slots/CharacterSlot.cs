﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterSlot : SlotBase
{
    public Text Name;
    public Image Icon;
    public Text Details;



    public override void SetIndex(int index)
    {
        base.SetIndex(index);

        if (SlotHandler != null)
        {
            var list = (ICharacterList)SlotHandler.List;
			CharacterReference c = list.GetCharacter(Index);
            Set(c);
        }
    }

    public void Set()
    {
        SetText(Name, "");
        SetText(Details, "");
        SetImage(Icon, null);
    }

    public void Set(SquadInfo s)
    {
        CharacterContent c = GameManager.Instance.GameContent.GetCharacter(s.ContentID);

        SetText(Name, (c != null) ?  c.Name : "");
        SetText(Details, (c != null) ? "Level " + s.Level : "");
        SetImage(Icon, (c != null) ? AssetPackManager.GetCharacterAssets(s.ContentID).Icon : null);
    }

	public void Set(CharacterReference c)
    {
        SetText(Name, (c != null) ?  c.ContentData.Name : "");
        SetText(Details, (c != null) ? "Level " + c.Level : "");
        SetImage(Icon, (c != null) ? AssetPackManager.GetCharacterAssets(c.ContentID).Icon : null);    

    }
  

}
