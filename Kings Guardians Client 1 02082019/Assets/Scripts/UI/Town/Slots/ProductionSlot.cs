﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProductionSlot : MonoBehaviour
{
    public Text Name;
    public StatusBar Statusbar;
    public Text Stacks;
    public Image Icon;   

    QueueInfo _process;

    public void Set(QueueInfo process, Sprite icon = null, string name = "")
    {
        _process = process;
        Toggle(true);

        Icon.sprite = (icon != null) ? icon : AssetPackManager.GetCardIcon(process.ItemContentID, true);
        Stacks.text = (process.Amount <= 1) ? "" : process.Amount.ToString();

        if (Name != null)
            Name.text = (name != "") ? name : process.ItemContentID;
        if (Statusbar != null)
        {
            Statusbar.MaxValue = process.Duration;
            Statusbar.TextFormat = "{0} mins";
            Statusbar.Value = 0;
            Statusbar.Text = Utils.SecondsToString(process.Duration);
        }
    }

    public void Hide()
    {
        Toggle(false);
    }

    void Update()
    {
        if (_process != null && Statusbar != null)
        {
            float p = Utils.TimeLeft(_process.StartTime, _process.Duration);
            Statusbar.Value = p; 
            Statusbar.Text = Utils.SecondsToString(p);
        }
    }

    void Toggle(bool b)
    {
        if (Name != null)
            Name.enabled = b;
        if (Statusbar != null)
            Statusbar.gameObject.SetActive(b);

        Icon.enabled = b;
        Stacks.enabled = b;            
    }
}
