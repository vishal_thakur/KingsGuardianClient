﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemSlot : SlotBase
{
    public Image Icon;
    public Text Stacks;

	public ItemReference Item { get; private set; }

    public override void SetIndex(int index)
    {
        base.SetIndex(index);
        if (Index >= 0)
        {
            var list = (IItemList)SlotHandler.List;
            Item = list.GetItem(index);
            Refresh();  
        }           
    }
          
	public void Set(ItemReference item, int index = 0)
    {   
        //Index = index;
        Item = item;
        Refresh();
    }
	
    public override void Select()
    {
        base.Select();
        Debug.Log("Selected: " + ((Item != null) ? Item.ContentID : "null"));
    }

    void Refresh()
    {   
        SetImage(Icon, (Item != null && Item.ContentID != "null") ? AssetPackManager.GetCardIcon(Item.ContentID, true) : default(Sprite));
        SetText( Stacks, (Item != null && Item.ContentID != "null") ? Item.Stacks.ToString() : "");  
    }

}
