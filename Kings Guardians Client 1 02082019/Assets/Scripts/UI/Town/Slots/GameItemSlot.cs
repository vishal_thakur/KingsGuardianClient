﻿using UnityEngine;
using System.Collections;
using GridSystem;
using UnityEngine.UI;

public class GameItemSlot : SlotBase
{
    public Text Name;
    public Text CraftTime;
    public Image Icon;
    public Text Built;
    public CostField[] Prices;
    public Text[] StaticLables;
	public bool isLocked = false;

	public override void SetIndex(int index)
    {
		Debug.LogError (index.ToString());
        base.SetIndex(index);

		//Unlocks All the items that Should be Unlocked as per The Gameplay Stats

        Unlock();  
		isLocked = false;

		//Get List of Elements in the Current Selected Category
        var list = (IGameItemList)SlotHandler.List;
		GameItemContent content = list.GetContent(Index);

		//Set Default Text for the Category Element Header and Built
        SetText(Name, (content != null) ? content.Name : "");
        SetText(Built, ""); 

        if (content == null)
            SetImage(Icon, default(Sprite));           

        else if (content.GetType() == typeof(BuildingContent))
        {	
			var info = GameManager.Instance.LocalPlayer.GetBuildingLimit((BuildingContent)content);
					           
			if (info.Full)
			{
				int u = GameManager.Instance.LocalPlayer.GetCastleUpgradeLimitIncreases((BuildingContent)content, info.Limit);
				if (u > 0) {
					Lock ("Need Castle Upgrade " + u);
					isLocked = true;
				}
			}

			//Set element's Thumbnail
			SetImage(Icon, AssetPackManager.GetBuildingAssets(content.ContentID).Icon);

			//Set element's built info as in 0/3
			SetText(Built, info.Built+" of "+info.Limit); 
		}
		//If Element is of type Character
		else if (content.GetType() == typeof(CharacterContent))
			//Set element's Thumbnail
            SetImage(Icon, AssetPackManager.GetCharacterAssets(content.ContentID).Icon);

		//If Element is of type item
		else if (content.GetType() == typeof(ItemContent) || content.GetType().IsSubclassOf(typeof(ItemContent)))
			//Set element's Thumbnail
            SetImage(Icon, AssetPackManager.GetCardIcon(content.ContentID, true));
		
		//Sets the Craft Time info
        SetText(CraftTime, (content != null) ? Utils.SecondsToString(Utils.CraftTime(content)) : "");



        foreach (var t in StaticLables)
            t.enabled = content != null;

        foreach (var p in Prices)
            p.Hide();   
                            
        if (content != null)
        {
            int cnt = 0;
            foreach (var p in Utils.CraftCost(content))
            {
                if (Prices.Length > cnt)
                {
                    Prices[cnt].Set(p.Material, p.Amount);
                    cnt++;
                }
            } 

            if( Utils.IsInherited( content.GetType(), typeof(ItemContent)))
            {
                var i = (ItemContent)content;
				if (BuildingWindow.Building != null && i.BuildingLevelReq > BuildingWindow.Building.Upgrade) {
					Lock ("Available\n Upgrade " + i.BuildingLevelReq);                
					isLocked = true;
				}
            }

            if( Utils.IsInherited( content.GetType(), typeof(CharacterContent)))
            {
                var c = (CharacterContent)content;
				if (c.BuildingLevelReq == 0 && BuildingWindow.Building.Upgrade > GameManager.Instance.LocalPlayer.Characters.Count) {
					Unlock ();
					isLocked = false;
				}
                else
                {
                    int u = (c.BuildingLevelReq == 0) ? BuildingWindow.Building.Upgrade + 1 : c.BuildingLevelReq;
                    Lock("Available\n Upgrade " + u);               
					isLocked = true;
                }
            }
        }
    }



	public void Initialize(int index , GameItemContent gameItemContentobj)
	{
//		Debug.LogError (index.ToString());
		base.SetIndex(index);

		//Unlocks All the items that Should be Unlocked as per The Gameplay Stats
		Unlock();  
		isLocked = false;

		//Get List of Elements in the Current Selected Category
		//        var list = (IGameItemList)SlotHandler.List;
		//		GameItemContent content = list.GetContent(Index);
		GameItemContent content = gameItemContentobj;

		//Set Default Text for the Category Element Header and Built
		SetText(Name, (content != null) ? content.Name : "");
		SetText(Built, ""); 

		//Set Default Thumbnail for the Empty elements in the Specific Category
		if (content == null)
			SetImage(Icon, default(Sprite));     
		//If Element is of type Building
		else if (content.GetType() == typeof(BuildingContent)){	

			//Locks an Element if it should be locked on the basis of Player's Game stats
			var info = GameManager.Instance.LocalPlayer.GetBuildingLimit((BuildingContent)content);
			if (info.Full){
				int u = GameManager.Instance.LocalPlayer.GetCastleUpgradeLimitIncreases((BuildingContent)content, info.Limit);
				if (u > 0) {
					Lock ("Need Castle Upgrade " + u);
					isLocked = true;
				}
			}

			//Set element's Thumbnail
			SetImage(Icon, AssetPackManager.GetBuildingAssets(content.ContentID).Icon);

			//Set element's built info as in 0/3
			SetText(Built, info.Built+" of "+info.Limit); 
		}
		//If Element is of type Character
		else if (content.GetType() == typeof(CharacterContent))
			//Set element's Thumbnail
			SetImage(Icon, AssetPackManager.GetCharacterAssets(content.ContentID).Icon);

		//If Element is of type item
		else if (content.GetType() == typeof(ItemContent) || content.GetType().IsSubclassOf(typeof(ItemContent)))
			//Set element's Thumbnail
			SetImage(Icon, AssetPackManager.GetCardIcon(content.ContentID, true));

		//Sets the Craft Time info
		SetText(CraftTime, (content != null) ? Utils.SecondsToString(Utils.CraftTime(content)) : "");



		foreach (var t in StaticLables)
			t.enabled = content != null;

		foreach (var p in Prices)
			p.Hide();   

		if (content != null)
		{
			int cnt = 0;
			foreach (var p in Utils.CraftCost(content))
			{
				if (Prices.Length > cnt)
				{
					Prices[cnt].Set(p.Material, p.Amount);
					cnt++;
				}
			} 

			if( Utils.IsInherited( content.GetType(), typeof(ItemContent)))
			{
				var i = (ItemContent)content;
				if (BuildingWindow.Building != null && i.BuildingLevelReq > BuildingWindow.Building.Upgrade) {
					Lock ("Available\n Upgrade " + i.BuildingLevelReq);                
					isLocked = true;
				}
			}

			if( Utils.IsInherited( content.GetType(), typeof(CharacterContent)))
			{
				var c = (CharacterContent)content;
				if (c.BuildingLevelReq == 0 && BuildingWindow.Building.Upgrade > GameManager.Instance.LocalPlayer.Characters.Count) {
					Unlock ();
					isLocked = false;
				}
				else
				{
					int u = (c.BuildingLevelReq == 0) ? BuildingWindow.Building.Upgrade + 1 : c.BuildingLevelReq;
					Lock("Available\n Upgrade " + u);               
					isLocked = true;
				}
			}
		}
	}

}
