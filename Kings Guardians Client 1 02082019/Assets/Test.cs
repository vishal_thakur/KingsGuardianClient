﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
	[SerializeField] MeshRenderer[] CornerObjs;



	void OnGUI(){
		foreach (MeshRenderer mesh in CornerObjs)
			GUILayout.TextField (mesh.gameObject.name + " Visible:\t" + mesh.isVisible);
	}
}
